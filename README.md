# MIA-FE README #

## INSTALL NODE.JS ##
1.1. ---Windows and MacOsX
dowload the appropriate package from https://nodejs.org/en/download/ and double click on it.

- version thar works https://nodejs.org/download/release/v7.1.0/


1.2. ---Linux
a) dowload the appropriate package from https://nodejs.org/en/download/ 
e.g. node-vx.x.x-linux-x64.tar.xz

---
how-to alternativo:
https://www.thegeekstuff.com/2015/10/install-nodejs-npm-linux/

cd /usr/local/
tar --strip-components 1 -xf /home/lorenzo/Documents/Development/FE/MIA-FE/node-v7.1.0-linux-x64.tar.xz 

node -v

---


b) Open a terminal and perform the following commands

bash$ sudo mkdir /usr/lib/nodejs

bash$ sudo tar -xJvf node-vx.x.x-linux-x64.tar.xz -C /usr/lib/nodejs

bash$ sudo cd /usr/lib/nodejs

bash$ sudo mv node-vx.x.x-linux-x64 node-vx.x.x

c) edit the file ~/.profile and at the end add the following lines

export NODEJS_HOME=/usr/lib/nodejs/node-vx.x.x

export PATH=$NODEJS_HOME/bin:$PATH

d) reboot the system

e) test the installation by performing the following commands in a terminal:

bash$ node -v

output should be like:
v6.5.0

bash$ npm version

output should be like:
{ npm: '3.10.3',
ares: '1.10.1-DEV',
http_parser: '2.7.0',
icu: '57.1',
modules: '48',
node: '6.5.0',
openssl: '1.0.2h',
uv: '1.9.1',
v8: '5.1.281.81',
zlib: '1.2.8' }

## INSTALL GIT ##
Download and install GIT for command line


## CONFIGURE NODE.JS ##
Same on all operating systems

Open a terminal and perform the following commands:

npm i -g gulp

npm i -g bower

## CONFIGURE MIA-FE DEVELOPMENT ENVIRONMENT ##

a) go where you want to put the mia-fe dicrectory

b) create a directory called MIA-FE

c) jump into that directory 

d) git clone https://lallori@bitbucket.org/map-firenze/mia-fe.git (NOTE: git should be installed before issuing this command)

e) perform the following commands in the correct order:

cd mia-fe

git fetch && git checkout develop

remove directory node_modules 
remove directory bower_components

## Configure NODE MODULES ##

download: http://bitbucket.org/map-firenze/mia-fe/downloads/node_7_1_bower_backend_modules-_05-01-2020.rar

unpack this package in the mia-fe directory

then

npm i

bower i

cd backend

npm i

cd ..


2- create a file named .gitignore in the root dir of your workspace with the following contents:
.vscode/
.gitignore
bower_components/
node_modules/
dist/
backend/node_modules/
backend/upload/
gulpfile.coffee
gulpfile.js

and save it.



IF PROBLEMS --- YOU NORMALLY DO NOT IT THIS
# update to gulp 4.x -- PROBABLY YOU CAN SKIP THIS do a gulp -v #
(got from https://demisx.github.io/gulp4/2015/01/15/install-gulp4.html)

npm rm gulp -g

npm rm gulp-cli -g

npm rm gulp --save-dev

npm rm gulp --save

npm rm gulp --save-optional

npm cache clean

npm install gulpjs/gulp-cli -g

npm install gulpjs/gulp#4.0 --save-dev

# Check the versions installed. Make sure your versions are not lower than shown.

gulp -v

Example output:
---
[10:48:35] CLI version 1.2.2

[10:48:35] Local version 4.0.0-alpha.2

# Final Tuning

1- replace ng-file-upload.js with MIA customized version:
download the file from:
https://bitbucket.org/map-firenze/mia-fe/downloads/ng-file-upload.js

copy it into 

mia-fe\bower_components\ng-file-upload\
