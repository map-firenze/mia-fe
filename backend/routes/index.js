fs = require('fs');
var express = require('express');
var router = express.Router();

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

function sleep(milliseconds) {
  var start = new Date().getTime();
  while(true) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

/*-------------*/
/* ARCHIVES    */
/*-------------*/

/* GET ARCHIVES */
router.get('/json/archive/getArchives/:aeNuber/:thumbNumb/:pageNum', function(req, res, next) {
  //sleep(1000);
  if (req.params.pageNum == 1) {
    fs.readFile('backend/json/archives/pag1.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
  if (req.params.pageNum == 2) {
    fs.readFile('backend/json/archives/pag2.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
  if (req.params.pageNum == 3) {
    fs.readFile('backend/json/archives/pag3.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
});


/*--------------------*/
/* SINGLE ARCHIVES    */
/*--------------------*/

/* GET SINGLE UPLOAD */
router.get('/json/archive/getArchives/aentity/:uploadId/:thumbNumb/:pageNum', function(req, res, next) {
  //sleep(1000);
  if (req.params.pageNum == 1) {
    fs.readFile('backend/json/singleArchive/page1.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
  if (req.params.pageNum == 2) {
    fs.readFile('backend/json/singleArchive/page2.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
  if (req.params.pageNum == 3) {
    fs.readFile('backend/json/singleArchive/page3.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
});

/* GET SINGLE UPLOAD BY FILE ID*/
router.get('json/archive/getArchives/aentityByUploadFile/:uploadFileId/:thumbNumb/:pageNum', function(req, res, next) {
  fs.readFile('backend/json/singleArchive/aentityByUploadFile.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* CHANGE ORDER */
router.get('/json/archive/orderFileinAE/:uploadId/:newImagOrder', function(req, res, next) {
  sleep(1000);
  fs.readFile('backend/json/singleArchive/page1sorted.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* DELETE UPLOAD */
router.get('/json/archive/deleteAE/:uploadId', function(req, res, next) {
  sleep(1000);
  fs.readFile('backend/json/orderChanged.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* DELETE FILE */
router.get('/json/archive/deleteFileInAE/:uploadFileId', function(req, res, next) {
  sleep(1000);
  fs.readFile('backend/json/orderChanged.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* SET PRIVACY */
router.get('/json/archive/setPrivacyUpload/:uploadId/:privacyValue', function(req, res, next) {
  sleep(1000);
  if (req.params.privacyValue == 0) {
    fs.readFile('backend/json/privacy/public.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  } else {
    fs.readFile('backend/json/privacy/private.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  }
});

/* SET SINGLE FILE PRIVACY */
router.get('/json/archive/setPrivacyOneFile/:uploadId/:privacyValue', function(req, res, next) {
  sleep(1000);
  if (req.params.privacyValue == 0) {
    fs.readFile('backend/json/privacy/public.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  } else {
    fs.readFile('backend/json/privacy/private.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  }
});

/* FIX UPLOAD */
router.get('/json/archive/repairarchive/:uploadId', function(req, res, next) {
  sleep(1000);
  fs.readFile('backend/json/fixUpload.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});


/* UPDATE UPLOAD METADATA */
router.post('/json/archive/updateMetaDataAE/:uploadId', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/updateMetaDataAE.json', function(err, data){
    try{
      if(err) next(err)
      console.log("postTest service called");
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/*-------------*/
/* METADATA    */
/*-------------*/

/* GET POSSIBLE REPOS */
router.get('/json/upload/findRepository/:text', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getRepository.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

router.get('/json/upload/findAllRepositories', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getRepositoriesList.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});


/* CHECK REPOSITORY */
router.get('/json/upload/checkRepository/:repoName/:repoLocation/:repoAbbreviation/', function(req, res, next) {
  //sleep(1000);
  if (req.params.repoAbbreviation == "ASF"){
    fs.readFile('backend/json/checkRepository/ko.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  } else {
    fs.readFile('backend/json/checkRepository/ok.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
});

/* CHECK COLLECTION */
router.get('/json/upload/checkCollection/:collectionName/:repoID', function(req, res, next) {
  //sleep(1000);
  if (req.params.collectionName == "Mediceo"){
    fs.readFile('backend/json/checkRepository/ko.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  } else {
    fs.readFile('backend/json/checkRepository/ok.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  };
});



/* GET POSSIBLE COLLECTION */
router.get('/json/upload/findCollection/:text/:repo', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getCollection.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET POSSIBLE SERIES */
router.get('/json/upload/findSeries/:text/:coll', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getSeries.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET POSSIBLE VOLUMES */
router.get('/json/upload/findVolume/:text/:idColl', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getVolume.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET POSSIBLE VOLUMES */
router.get('/json/upload/findInsert/:text/:volume', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/getInsert.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});
/*-------------*/
/* DOCUMENTS   */
/*-------------*/

/* GET ALL DOCUMENT */
router.get('/json/document/findDocuments', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getAllDoc.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET DOCUMENT ENTITY BY ID */
router.get('/json/document/findDocument/:documentId', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getDocumentById.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET DOCUMENT ENTITY BY FILE ID */
router.get('/json/document/findDocumentsByUploadFile/:uploadFileId', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getDocumentList.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET Categories and typologies */
router.get('/json/document/findDocumentTypologies', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/findDocumentTypologies.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* Check Folio */
router.get('/json/document/checkFolio/:uploadFileId', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getFoliosByUploadFileId.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});


/* GET document fields */
router.get('/json/document/findDocumentFields/:type', function(req, res, next) {
  //sleep(1000);
  if (req.params.type == 'Correspondence') {
    fs.readFile('backend/json/documentEntity/findDocumentFields1.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });
  } else {
    fs.readFile('backend/json/documentEntity/findDocumentFields2.json', function(err, data){
      try{
        if(err) next(err)
        console.log(data)
        res.json (JSON.parse(data));
      } catch (err)  {
        console.log(err)
        next(err);
      }
    });    
  }
});

/* POST Create Document Entity */
router.post('/json/document/createDe', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/createDe.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});


/* GET PEOPLE */
router.get('/json/document/findPeople/:searchQuery', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getPeople.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* ADD PEOPLE */
router.post('/json/document/addNewPeople/', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/addNewPeople.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* FIND PLACES */
router.get('/json/document/findPlaces/:searchQuery', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getPlace.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* ADD PLACES */
router.post('/json/document/addNewPlace/', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/addNewPlace.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* FIND PLACE TYPE */
router.get('/json/document/findPlaceTypes/', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/findPlaceTypes.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* GET PLACES */
router.get('/json/document/findPlaces/:searchQuery', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/documentEntity/getPlace.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});
/*-------------*/
/* UPLOAD      */
/*-------------*/

/* POST PICTURE-SSSS */
router.post('/json/upload/uploadWithMeta/', multipartMiddleware, function(req, res) {
  for (i in req.files.files) {
    var serverPath = '/pics/' + req.files.files[i].originalFilename;
    require('fs').rename(
      req.files.files[i].path,'/Users/giova/dev/mia-fe/backend/upload/' + serverPath,
      function(error) {
        if(error) {
          res.send({
            error: 'Ops! Something bad happened'
          });
        return;
        }
        res.send({
          path: serverPath
        });
      }
    );
  }
});

/* ADD FILE-SSSS */
router.post('/json/archive/addfilestoAE/:uploadId', multipartMiddleware, function(req, res) {
  for (i in req.files.files) {
    var serverPath = '/pics/' + req.files.files[i].originalFilename;
    require('fs').rename(
      req.files.files[i].path,'/Users/giova/dev/mia-fe/backend/upload/' + serverPath,
      function(error) {
        if(error) {
          res.send({
            error: 'Ops! Something bad happened'
          });
        return;
        }
        res.send({
          path: serverPath
        });
      }
    );
  }
});

/*-------------*/
/* TESTS       */
/*-------------*/

/* GET TEST */
router.get('/json/test/JsonTest/', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/test/getTest.json', function(err, data){
    try{
      if(err) next(err)
      console.log("getTest service called");
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/* POST TEST */
router.post('/json/test/JsonTest/jsonTestInput/', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/test/postTest.json', function(err, data){
    try{
      if(err) next(err)
      console.log("postTest service called");
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});


/*-------------*/
/* LANGUAGE    */
/*-------------*/


/* POST LANGUAGE DATA */
router.post('/user/:userCode/language/save', function(req, res, next) {
  //sleep(1000);
  fs.readFile('backend/json/success.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

/*-----------------*/
/* News            */
/*-----------------*/

router.get('/json/news/getDailyUpdates/', function(req, res, next) {
  fs.readFile('backend/json/news/dayNews.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

router.get('/json/news/getWeeklyUpdates/', function(req, res, next) {
  fs.readFile('backend/json/news/weekNews.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

router.get('/json/news/getMontlyUpdates/', function(req, res, next) {
  fs.readFile('backend/json/news/monthNews.json', function(err, data){
    try{
      if(err) next(err)
      console.log(data)
      res.json (JSON.parse(data));
    } catch (err)  {
      console.log(err)
      next(err);
    }
  });
});

module.exports = router;
