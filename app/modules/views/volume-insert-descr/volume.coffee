volumeDescrApp = angular.module 'volumeDescrApp',[
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'people'
    'places'
    'volInsDescrApp'
    'volumeInsertDescrApp'
    'actionsHistory'
    'commentsModule'
  ]


volumeDescrApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.volumeDescr',
        url: '/volume/:volumeId?scrollTo'
        reloadOnSearch: false
        params: { volumeId: '',
        collectionId: '' }
        views:
          'content':
            controller: 'volumeDescrController'
            templateUrl: 'modules/views/volume-insert-descr/volume.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

volumeDescrApp.controller 'volumeDescrController', (volumeInsertDescrFactory, deFactory, $scope, $sce, $rootScope, $state, $filter, modalFactory, $stateParams, searchDeService, searchFactory) ->
  $rootScope.pageTitle = "Volume description"

  $scope.volumeDescription = {}
  $scope.allClosed = true
  $scope.createPlaceMode = false

  $scope.switchAllPanels = () ->
    $scope.allClosed = !$scope.allClosed
    for section in $scope.sections
      section.open = !$scope.allClosed
  $scope.params = $stateParams
  $scope.sections = []
  $scope.volumeId = $scope.$stateParams.volumeId or JSON.parse(localStorage.getItem('volumeId'))
  $scope.aes = 0
  $scope.des = 0

  searchDeService.getRepositories().then (resp) ->
    mainRepo = _.find resp, (o) -> o.repositoryId == '1'
    $scope.repository = mainRepo

  volumeInsertDescrFactory.findVolumeDescription($scope.volumeId).then (resp) ->
#    console.log 'findVolumeDescription', resp
    volumeInsertDescrFactory.recordVolAction($scope.volumeId, 'VIEW')
    if resp.status is "ok"
#      console.log resp.data
      $scope.volumeDescription = resp.data
      $scope.volumeDescription.data.startMonth = +$scope.volumeDescription.data.startMonth
      $scope.volumeDescription.data.endMonth = +$scope.volumeDescription.data.endMonth

  $scope.uploadModifyVolumeSpine = (form) ->
    volumeInsertDescrFactory.uploadModifyVolumeSpine(form).then (resp) ->
#      console.log 'uploadModifyVolumeSpine', resp

  $scope.findVolumeSpine = (volumeId) ->
    volumeInsertDescrFactory.findVolumeSpine(volumeId).then (resp) ->
#      console.log 'findVolumeSpine', resp
      if resp.status is "ok"
#        console.log resp
        $scope.spine = resp.data

  $scope.countVolumeAESandDES = (volumeId) ->
    volumeInsertDescrFactory.countVolumeAESandDES(+volumeId).then (resp) ->
#      console.log 'countVolumeAESandDES', resp
      $scope.aes = resp.data.totalAEs
      $scope.des = resp.data.totalDEs

  $scope.findVolumeAES = (volumeId) ->
    volumeInsertDescrFactory.findVolumeAES(volumeId).then (resp) ->
#      console.log 'findVolumeAES', resp

  $scope.findVolumeDES = (volumeId) ->
    volumeInsertDescrFactory.findVolumeDES(volumeId).then (resp) ->
#      console.log 'findVolumeDES', resp

  $scope.searchAeDeByVolume = (type) ->
    if type == 'doc' then searchDe() else searchAe()

  searchDe = () ->
    volumeSearchData = {
      searchSection:  'archivalLocationSearch',
      type:           'archivalLocationAdvancedSearch',
      isActiveFilter: if $scope.volumeDescription.data.collectionId == 0 then false else true,
      collection:     $scope.volumeDescription.data.collectionName,
      series:         null,
      volume:         $scope.volumeDescription.data.volumeName,
      insert:         null
    }

    $scope.searchArray = prepareSearchArray(volumeSearchData)

    searchDeService.getResults($scope.searchArray).then((resp) ->
      newResult = {}
      newResult.searchName = "Document Search"
      newResult.type = 'doc_advanced'
      newResult.results = resp.data
      newResult.searchArray = $scope.searchArray
      newResult.content = {
        searchType: newResult.searchName,
        where: '',
        title: 'Document Search',
        countResult: $scope.des
      }

      newResult.searchForm = createSearchForm($scope.volumeDescription.data)

      $rootScope.$broadcast('new-result', newResult))

  searchAe = () ->
    params = {}
    params.owner = $rootScope.me.account
    params.repositoryId = $scope.repository.repositoryId
    params.collectionId = $scope.volumeDescription.data.collectionId || ''
    params.seriesId = ''
    params.volumeId = $scope.volumeDescription.data.volumeId
    params.insertId = ''
    params.onlyMyArchivalEnties          = false
    params.everyoneElsesArchivalEntities = false
    params.allArchivalEntities           = true

    searchFactory.browseAe(params).then (resp) ->
      if resp.data
        newResult = {}
        newResult.searchName = 'Images search'
        newResult.type = 'ae'
        newResult.subtype =  "archival_search"
        newResult.results = resp.data.aentities
        newResult.content = {
          searchType: newResult.searchName,
          where: $scope.repository.repositoryName + ' ' + $scope.volumeDescription.data.collectionName + ' ' + $scope.volumeDescription.data.volumeName,
          title: 'Images search',
          countResult: $scope.aes
        }
        $rootScope.$broadcast('new-result', newResult)

  $scope.goToBAL = () ->
    $state.go('mia.browseArchive', { type: 'vol', id: $scope.volumeId })

  $scope.countVolumeAESandDES($scope.volumeId)
  $scope.findVolumeSpine($scope.volumeId)


  # To build correctly the result box of the right-sidebar a searchArray is needed with the right elements
  # in specific positions
  prepareSearchArray = (volumeSearchData) ->
    searchArray = [
      volumeSearchData,
      { searchSection: "categoryAndTypologySearch", type: "categoryAndTypologyAdvancedSearch", isActiveFilter: false },
      { searchSection: "transcriptionSearch", type: "transcriptionAdvancedSearch", isActiveFilter: false },
      { searchSection: "synopsisSearch", type: "synopsisAdvancedSearch", isActiveFilter: false },
      { searchSection: "placesSearch", type: "placesAdvancedSearch", isActiveFilter: false },
      { searchSection: "peopleSearch", type: "peopleAdvancedSearch", isActiveFilter: false},
      { searchSection: "topicsSearch", type: "topicsAdvancedSearch", isActiveFilter: false },
      { searchSection: "dateSearch", type: "dateAdvancedSearch", isActiveFilter: false }
      { searchSection: "documentOwnerSearch", type: "documentOwnerAdvancedSearch", isActiveFilter: false }
      { searchSection: "languagesSearch", type: "languagesAdvancedSearch", isActiveFilter: false }
    ]

    return searchArray


  createSearchForm = (volumeData) ->
    searchForm = {
      archivalLocation: {
        collection: {
          collectionId: volumeData.collectionId,
          collectionName: volumeData.collectionName,
          collectionAbbreviation: volumeData.collectionAbbr,
          # studyCollection: true # MANCA
        },
        serie: { seriesId: 0, seriesName: ""},
        volume: {
          volumeId: volumeData.volumeId,
          volumeName: volumeData.volumeName,
          aeCount: volumeData.aeCount
        },
        insert: { insertId: 0, insertName: "" }
      }
    }
	  