volumeInsertDescrServiceApp = angular.module 'volumeInsertDescrApp', []

volumeInsertDescrServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
]
volumeInsertDescrServiceApp.factory 'volumeInsertDescrFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  volumeInsertDescrBasePath = ENV.volumeInsertDescrBasePath

  factory =

    findVolumeDescription: (params) ->
      return $http.get(volumeInsertDescrBasePath + 'findVolume/' + params ).then (resp) ->
        resp.data

    findInsertDescription: (params) ->
      return $http.get(volumeInsertDescrBasePath + 'findInsert/' + params ).then (resp) ->
        resp.data

    modVolumeBasicMeta: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'modifyVolumeBasicMetadata', params).then (resp) ->
        resp.data

    modVolumeAdvancedMeta: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'modifyVolumeAdvancedMetadata', params).then (resp) ->
        resp.data

    modInsertAdvancedMeta: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'modifyInsertAdvancedMetadata', params).then (resp) ->
        resp.data

    modInsertBasicMeta: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'modifyInsertBasicMetadata', params).then (resp) ->
        resp.data

    addInsert: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'addInsert', params).then (resp) ->
        resp.data

    addVolume: (params) ->
      return $http.post(volumeInsertDescrBasePath + 'addVolume', params).then (resp) ->
        resp.data

    uploadModifyVolumeSpine: (params) ->
      return $http.post(volumeInsertDescrBasePath + "uploadVolumeSpine", params).then (resp) ->
        resp.data

    findVolumeSpine: (params) ->
      return $http.get(volumeInsertDescrBasePath + 'findVolumeSpine/' + params).then (resp) ->
        resp.data

    findInsertGuardia: (params) ->
      return $http.get(volumeInsertDescrBasePath + 'findInsertGuardia/' + params).then (resp) ->
        resp.data

    uploadModifyInsertGuardia: (params) ->
      return $http.post(volumeInsertDescrBasePath + "uploadInsertGuardia", params).then (resp) ->
        resp.data

    deleteVolumeRecord: (params) ->
      return $http.get(volumeInsertDescrBasePath + "deleteVolume/" + params).then (resp) ->
        resp.data

    deleteInsertRecord: (params) ->
      return $http.get(volumeInsertDescrBasePath + "deleteInsert/" + params).then (resp) ->
        resp.data

    countVolumeAESandDES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "countVolumeAESandDES/" + params).then (resp) ->
        resp.data

    countInsertAESandDES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "countInsertAESandDES/" + params).then (resp) ->
        resp.data

    findVolumeAES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "FindVolumeAES/" + params).then (resp) ->
        resp.data

    findVolumeDES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "FindVolumeDES/" + params).then (resp) ->
        resp.data

    findInsertAES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "FindInsertAES/" + params).then (resp) ->
        resp.data

    findInsertDES: (params) ->
      return $http.get(volumeInsertDescrBasePath + "FindInsertDES/" + params).then (resp) ->
        resp.data
    
    findSeriesBasicMeta: (seriesName, collectionId) ->
      return $http.get('json/upload/findSeries/'+ seriesName+'/'+collectionId).then (resp) ->
        resp.data

    recordVolAction: (volumeId, type) ->
      volInfo = {
        "recordType": "VOL"
        "entryId": Number(volumeId)
        "action": type
      }
#      console.log 'volInfo', volInfo
      return $rootScope.$broadcast('recordVolAction', volInfo)

    recordInsAction: (insertId, type) ->
      insInfo = {
        "recordType": "INS"
        "entryId": Number(insertId)
        "action": type
      }
#      console.log 'insInfo', insInfo
      return $rootScope.$broadcast('recordInsAction', insInfo)

  return factory
