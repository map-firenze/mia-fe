insertDescrApp = angular.module 'insertDescrApp',[
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'people'
    'places'
    'volInsDescrApp'
    'volumeInsertDescrApp'
    'actionsHistory'
    'commentsModule'
  ]


insertDescrApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.insertDescr',
        url: '/insert/:insertId?scrollTo'
        reloadOnSearch: false
        params: { insertId: '',
        collectionId: '' }
        views:
          'content':
            controller: 'insertDescrController'
            templateUrl: 'modules/views/volume-insert-descr/insert.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

insertDescrApp.controller 'insertDescrController', (volumeInsertDescrFactory, deFactory, $scope, $sce, $rootScope, $state, $filter, modalFactory, $stateParams, searchDeService, searchFactory) ->
  $rootScope.pageTitle = "Insert description"

  $scope.insertDescription = {}
  $scope.allClosed = true
  $scope.createPlaceMode = false
  $scope.aes = 0
  $scope.des = 0

  $scope.switchAllPanels = () ->
    $scope.allClosed = !$scope.allClosed
    for section in $scope.sections
      section.open = !$scope.allClosed
  $scope.sections = []
  $scope.insertId = $scope.$stateParams.insertId or JSON.parse(localStorage.getItem('insertId'))

  searchDeService.getRepositories().then (resp) ->
    mainRepo = _.find resp, (o) -> o.repositoryId == '1'
    $scope.repository = mainRepo

  volumeInsertDescrFactory.findInsertDescription($scope.insertId).then (resp) ->
#    console.log 'findInsertDescription', resp
    volumeInsertDescrFactory.recordInsAction($scope.insertId, 'VIEW')
    if resp.status is "ok"
#      console.log resp.data
      $scope.insertDescription = resp.data
      $scope.insertDescription.data.startMonth = +$scope.insertDescription.data.startMonth
      $scope.insertDescription.data.endMonth = +$scope.insertDescription.data.endMonth

  $scope.uploadModifyInsertGuardia = (form) ->
    volumeInsertDescrFactory.uploadModifyInsertGuardia(form).then (resp) ->
#      console.log "uploadModifyInsertGuardia", resp

  $scope.countInsertAESandDES = (insertId) ->
    volumeInsertDescrFactory.countInsertAESandDES(+insertId).then (resp) ->
      $scope.aes = resp.data.totalAEs
      $scope.des = resp.data.totalDEs

  $scope.findInsertAES = (insertId) ->
    volumeInsertDescrFactory.findInsertAES(insertId).then (resp) ->
#      console.log(resp)

  $scope.findInsertDES = (insertId) ->
    volumeInsertDescrFactory.findInsertDES(insertId).then (resp) ->
#      console.log(resp)

  $scope.findInsertGuardia = (insertId) ->
    volumeInsertDescrFactory.findInsertGuardia(insertId).then (resp) ->
#      console.log "findInsertGuardia", resp
      if resp.status is "ok"
        $scope.spine = resp.data

  $scope.searchAeDeByVolume = (type) ->
#    console.log('searchAeDeByVolume', type)
    if type == 'doc' then searchDe() else searchAe()

  searchDe = () ->
    insertSearchData = {
      collection:     $scope.insertDescription.data.collectionName,
      insert:         $scope.insertDescription.data.insertNo,
      isActiveFilter: if $scope.insertDescription.data.collectionId == 0 then false else true,
      searchSection:  'archivalLocationSearch',
      series:         null,
      type:           'archivalLocationAdvancedSearch',
      volume:         $scope.insertDescription.data.volumeNo
    }

    $scope.searchArray = [insertSearchData]

    searchDeService.getResults($scope.searchArray).then((resp) ->
      newResult = {}
      newResult.searchName = "Document Search"
      newResult.type = 'doc_advanced'
      newResult.results = resp.data
      newResult.searchArray = $scope.searchArray
      newResult.content = {
        searchType: newResult.searchName,
        where: '',
        title: 'Document Search'
      }
      $rootScope.$broadcast('new-result', newResult))

  searchAe = () ->
#    console.log('searchAe')
    params = {}
    params.owner = $rootScope.me.account
    params.repositoryId = $scope.repository.repositoryId
    params.collectionId = $scope.insertDescription.data.collectionId || ''
    params.seriesId = ''
    params.volumeId = $scope.insertDescription.data.volumeId
    params.insertId = $scope.insertDescription.data.insertId
    params.onlyMyArchivalEnties          = false
    params.everyoneElsesArchivalEntities = false
    params.allArchivalEntities           = true

    searchFactory.browseAe(params).then (resp) ->
      if resp.data
        newResult = {}
        newResult.searchName = "Upload search"
        newResult.type = 'ae'
        newResult.subtype =  "archival_search"
        newResult.results = resp.data.aentities
        newResult.content = {
          searchType: newResult.searchName,
          where: $scope.repository.repositoryName + ' ' + $scope.insertDescription.data.collectionName + ' ' + $scope.insertDescription.data.insertNo,
          title: 'Images search'
        }
        $rootScope.$broadcast('new-result', newResult)

  $scope.goToBAL = () ->
    $state.go('mia.browseArchive', { type: 'ins', id: $scope.insertId })

  $scope.countInsertAESandDES($scope.insertId)
  $scope.findInsertGuardia($scope.insertId)
