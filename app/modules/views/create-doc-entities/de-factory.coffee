deServiceApp = angular.module 'deServiceApp',[]

deServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
deServiceApp.factory 'deFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  documentBasePath = ENV.documentBasePath

  factory =
    #-------------------
    # Document Entity
    #-------------------
    updateDocumentPrivacy: (docId)->
      return $http.get(documentBasePath+'uploadDocumentPrivacy/'+docId).then (resp)->
        resp.data

    getAllDocuments:()->
      return $http.get(documentBasePath+'findDocuments/').then (resp) ->
        resp.data

    getMyDocuments: (page, perPage)->
      return $http.get(documentBasePath+'findUserDocuments' + '?page=' + page + '&perPage=' + perPage).then (resp) ->
        resp.data

    getDocumentsInVolume: (volumeId, page, perPage, query = '')->
      url = documentBasePath + 'findDocumentsInVolume/' + volumeId + '?page=' + page + '&perPage=' + perPage + '&notChildren=true'
      url = url + '&q=' + query if query.length
      return $http.get(url).then (resp) ->
        resp.data

    getDocumentsInInsert: (insertId, page, perPage, query = '')->
      url = documentBasePath + 'findDocumentsInInsert/' + insertId + '?page=' + page + '&perPage=' + perPage + '&notChildren=true'
      url = url + '&q=' + query if query.length
      return $http.get(url).then (resp) ->
        resp.data

    getDocumentListByFileId: (uploadFileId) ->
      return $http.get(documentBasePath+'findDocumentsByUploadFile/'+uploadFileId).then (resp) ->
        resp.data

    getUndeletedDocumentsByFileId: (uploadFileId) ->
      return $http.get(documentBasePath+'findNotDeletedDocuments/'+uploadFileId).then (resp) ->
        resp.data

    getDocumentEntityById: (documentId) ->
      return $http.get(documentBasePath+'findDocument/'+documentId).then (resp) ->
        resp.data

    getDocumentCat: () ->
      return $http.get(documentBasePath+'findDocumentTypologies').then (resp) ->
        resp.data

    getDocumentFields: (type) ->
      return $http.get(documentBasePath+'findDocumentFields/'+type).then (resp) ->
        resp.data

    createDe: (documentEntity) ->
      params = documentEntity
      return $http.post(documentBasePath+'createDE/', params ).then (resp) ->
        resp.data

    deleteDe: (documentId) ->
      return $http.get(documentBasePath+'deleteDE/'+documentId).then (resp) ->
        resp.data

    unDeleteDe: (documentId) ->
      return $http.get(documentBasePath+'unDeleteDE/'+documentId).then (resp) ->
        resp.data
    
    getPeople: (searchQuery)->
      return $http.get(documentBasePath+'findPeople/'+searchQuery).then (resp)->
        resp.data

    getPeopleById: (peopleId)->
      return $http.get(documentBasePath+'findPeopleById/'+peopleId).then (resp)->
        resp.data

    getPlaceById: (placeId)->
      return $http.get(documentBasePath+'findPlaceById/'+placeId).then (resp)->
        resp.data

    getPlace: (searchQuery)->
      return $http.get(documentBasePath+'findPlaces/'+searchQuery).then (resp)->
        resp.data

    getFolioByUploadFileId: (uploadFileId)->
      return $http.get(documentBasePath+'checkFolio/'+uploadFileId).then (resp)->
        resp.data

    saveFolio: (folios)->
      return $http.post(documentBasePath+'saveFolio/', folios).then (resp)->
        resp.data
    
    deleteFolio: (uploadFileId, folioId)->
      params={}
      params.folioId = folioId
      params.uploadFileId = uploadFileId
      return $http.post(documentBasePath+'deleteFolio/', params).then (resp)->
        resp.data

    getPlaceTypes: ()->
      return $http.get(documentBasePath+'findPlaceTypes/').then (resp)->
        resp.data


    createNewPeople: (person)->
      params = person
      return $http.post(documentBasePath+'addNewPeople/', params).then (resp)->
        resp.data

    createNewPlace: (place)->
      params = place
      return $http.post(documentBasePath+'addNewPlace/', params).then (resp)->
        resp.data

    saveTranscription: (transcriptions)->
      return $http.post(documentBasePath+'saveOrModifyTranscriptions', transcriptions).then (resp)->
        resp.data

    getAllTopics: ()->
      return $http.get(documentBasePath+'findTopics/').then (resp)->
        resp.data

    recordDocAction: (documentId, type) ->
      docInfo = {
        "recordType": "DE"
        "entryId": Number(documentId)
        "action": type
      }
#      console.log docInfo
      return $rootScope.$broadcast('recordDocAction', docInfo)

  return factory
