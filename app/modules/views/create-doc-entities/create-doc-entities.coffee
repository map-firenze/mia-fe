createDocEntApp = angular.module 'createDocEntApp',[
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'people'
    'places'
  ]


createDocEntApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.createDocEnt',
        url: '/create-document-entity/'
        params: { documentEntity:null }
        views:
          'content':
            controller: 'createDocEntController'
            templateUrl: 'modules/views/create-doc-entities/create-doc-entities.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'

]

createDocEntApp.controller 'createDocEntController', (deFactory, $scope, $sce, $rootScope, $state, $filter, modalFactory, $window, searchFactory) ->
  
  $rootScope.pageTitle = "Create Document"
  
  # searchFactory.getLastRecords().then (resp) ->
  #   console.log resp
  #   if resp.status is 'ok'
  #     $scope.lastRecords = resp.data
  #     console.log($scope.lastRecords)
  #   else
  #     $scope.lastRecords = {}

  $scope.initializeDE = ()->
    $scope.docCreated = undefined
    $scope.loadingProcess = false
    if $state.params.documentEntity
      $scope.documentEntity = $state.params.documentEntity
    else
      # $scope.documentEntity = {"uploadFiles": [{"uploadFileId": 91, "filePath": "img/manuscripts/image9-thumb.jpeg", "folios": [{"folioId": "", "uploadFileId": 92, "folioNumber": "123", "rectoverso": "recto"}]}]}
      $window.location.href = $window.location.protocol + '//' + $window.location.host + '/Mia/index.html#/mia/archive'
      #console.log($window.location.protocol + '' + $window.location.host + '' + $scope.lastRecords.lastUpload)
      #$window.location.href = $window.location.protocol + '//' + $window.location.host + '/Mia/index.html#/mia/archival-entity/' + $scope.lastRecords.lastUpload + '?page=1'
    
  $scope.initializeDE()
  $scope.typology=false
  $scope.datePanelstatus={}
  $scope.datePanelstatus.open=true
  $scope.showPossiblePeopleTable = []
  $scope.showPossiblePlaceTable = []
  $scope.limit=0
  $scope.months=[{'name':'January', 'id':1},
    {'name':'February', 'id':2},
    {'name':'March', 'id':3},
    {'name':'April', 'id':4},
    {'name':'May', 'id':5},
    {'name':'June', 'id':6},
    {'name':'July', 'id':7},
    {'name':'August', 'id':8},
    {'name':'September', 'id':9},
    {'name':'October', 'id':10},
    {'name':'November', 'id':11},
    {'name':'December', 'id':12}]
  deFactory.getDocumentCat().then (resp)->
    $scope.categories = resp.data.categories
    for cat in $scope.categories
      cat.displayName=cat.categoryName+' - '+cat.typologyName

  if $scope.documentEntity.uploadFiles[0].uploadFileId
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.documentEntity.uploadFiles[0].uploadFileId)
  else
    $scope.viewerAddress = $sce.trustAsResourceUrl('')

  $scope.changeLimit = (quantity) ->
    temp = $scope.limit + quantity
    if temp >=0 and temp+6<= $scope.documentEntity.uploadFiles.length
      $scope.limit = temp

  $scope.updateViewer = (uploadId) ->
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+uploadId)

  $scope.chooseDe = (deId)->
    $scope.select(deId)

  $scope.typologyChosen = (type)->
    #if it's not the first time i choose and i am changing category i've to ask the user if he wants to delete
    if $scope.documentEntity.category and ($scope.documentEntity.category isnt type.categoryName)
      pageTitle = $filter('translate')("editDE.CHANGETYPTITLE")
      messagePage = $filter('translate')("editDE.CHANGETYP")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
        #if he says yes i delete
        if resp
          makeFieldsForEdit(type)
        #otherwise i rollback
        else
          restoreTypology()
    else
      #if it is the first time i choose
      if $scope.documentEntity.category isnt type.categoryName
        makeFieldsForEdit(type)
      #if i jsut changed typology without changing category
      else
        $scope.documentEntity.typology = type.typologyName

  #rollback typology
  restoreTypology = ()->
    for cat in $scope.categories
      if cat.typologyName is $scope.documentEntity.typology
        $scope.typology = cat

  makeFieldsForEdit = (type) ->
    $scope.typology = {}
    $scope.typology = type
    $scope.deData = {}
    $scope.typologyFields = []
    $scope.initializeDE()
    $scope.documentEntity.category = type.categoryName
    $scope.documentEntity.typology = type.typologyName
    $scope.documentEntity.fields = []
    $scope.textFields = []
    $scope.peopleFields = []
    $scope.placesFields = []
    tempDate = $scope.documentEntity.date
    tempUpload = $scope.documentEntity.uploadFiles
    tempCat = $scope.documentEntity.category
    tempType = $scope.documentEntity.typology
    $scope.documentEntity = {}
    $scope.documentEntity.date = tempDate
    $scope.documentEntity.uploadFiles = tempUpload
    $scope.documentEntity.category = tempCat
    $scope.documentEntity.typology = tempType

    deFactory.getDocumentFields(type.categoryName.replace(/\s/g, "")).then (resp)->
      $scope.documentEntity.fields=resp.data.fields
      #scorro i field
      for field in $scope.documentEntity.fields
        #chiudo tutti i pannelli
        field.openPanel= false
        #faccio tre array uno per tipo di field
        if field.type is "people"
          $scope.peopleFields.push(field)
        if field.type is "place"
          $scope.placesFields.push(field)
        if field.type is "text"
          $scope.textFields.push(field)
      #levo i field dalla struttura. Aggiungerò dopo
      delete $scope.documentEntity.fields

  prepareField = (fieldsToAdd)->
    names = []
    values = []
    for field in fieldsToAdd
      if field.beName and field.value[0] and field.value[0].id
        name = field['beName']
        #creo un vettore di un vettore che contiene il nome del campo
        names.push(name)
        #creo un vettore di coppie valori (id e unsure)
        values.push(field['value'])
    #creo un oggetto {nome: {id: valore1, unsure:valore2}}
    # return objectToAdd = _.object(names, values)
    return objectToAdd = _.zipObject(names, values)

  $scope.createDe = () ->
    if $scope.docCreated
      toaster.pop('error', 'Document already created')
      return
    $scope.loadingProcess = true
    $scope.documentEntity.type = $scope.documentEntity.category.replace(/\s/g, "")
    $scope.documentEntity.category = $scope.documentEntity.category.replace(/\s/g, "")
    #ADD TO DE DOCUMENT PEOPLE PLACES AND OTHERS
    $scope.documentEntity = _.extend($scope.documentEntity, prepareField($scope.peopleFields))
    $scope.documentEntity = _.extend($scope.documentEntity, prepareField($scope.placesFields))
    $scope.documentEntity = _.extend($scope.documentEntity, prepareField($scope.textFields))

    # DATE VALUES
    $scope.documentEntity.date.docYear = Number($scope.documentEntity.date.docYear) or 0
    $scope.documentEntity.date.docMonth = Number($scope.documentEntity.date.docMonth) or 0
    $scope.documentEntity.date.docDay = Number($scope.documentEntity.date.docDay) or 0
    $scope.documentEntity.date.docModernYear = Number($scope.documentEntity.date.docModernYear) or 0
    $scope.documentEntity.date.docDateUncertain = Number($scope.documentEntity.date.docDateUncertain) or 0
    $scope.documentEntity.date.docUndated = Number($scope.documentEntity.date.docUndated) or 0
    $scope.documentEntity.date.docDateNotes = $scope.documentEntity.date.docDateNotes or ''

    #REAL REQUEST
    deFactory.createDe($scope.documentEntity).then (resp)->
      $scope.loadingProcess = false
      if resp.status is "ok"
        $scope.docCreated = resp.docId
#        deFactory.recordDocAction(resp.docId, 'CREATE')
        pageTitle = $filter('translate')("singleAentity.AFTERDETIT")
        messagePage = $filter('translate')("singleAentity.AFTERDEBODY")
        modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
          if resp
            $state.go('mia.modifyDocEnt', { 'docId':$scope.docCreated })
          else
            $state.go('mia.docentities')
