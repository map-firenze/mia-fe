singleuploadApp = angular.module 'singleuploadApp',[
    'miaApp'
    'translatorApp'
    'commonServiceApp'
    'ui.router'
    'modalUtils'
    'ui.bootstrap'
    'ngFileUpload'
    'ngDraggable'
    'ladda'
    'selectDeApp'
    'documentEntityApp'
    'ajoslin.promise-tracker'
    'replaceImgApp'
    'delImgApp'
    'rotateImgApp'
    'shareWithUsersApp'
    'actionsHistory'
    'commentsModule'
    'editMetaDataApp'
  ]

singleuploadApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.singleupload',
        url: '/archival-entity/:aeId?fileId&page&scrollTo&modal',
        reloadOnSearch: false,
        views:
          'content':
            controller: 'singleuploadController'
            templateUrl: 'modules/views/single-upload/single-upload.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

singleuploadApp.controller 'singleuploadController', (commonsFactory, userServiceFactory, searchFactory, deFactory, converterFactory, $scope, $state, $sce, $rootScope, Upload, $timeout, $window, $filter, modalFactory, $location, $anchorScroll, $q, volumeInsertDescrFactory) ->
  $rootScope.pageTitle = "Upload"
  $scope.getArchiveTracker = true
  $scope.fixUploadTracker = false
  $scope.editMetaLoader = false
  $scope.metadataEdited = false
  $scope.errorDeleting = false
  $scope.errorMess = ''
  $scope.folio1 = {}
  $scope.folio2 = {}
  $scope.isAdmin = false
  $scope.me = {}
  $scope.wrongFiles = []
  $scope.selectedImages = {}
  $scope.deletedObject = false
  $scope.notFoundInDB = false
  $scope.canChangePrivacy = false
  $scope.imageWithPrivacy = false
  $scope.imageIds = []

  $scope.random = Math.random()

  $scope.reload = ()->
    $state.reload()
    
  $scope.visibility = {}
  
  $scope.closeAllPanel = ()->
    $scope.visibility.showViewer = false
    $scope.visibility.showAddFile = false
    $scope.visibility.showMeta = false
    $scope.visibility.showUpload = false
    $scope.visibility.showReorder = false
    $scope.visibility.showReplaceFile = false
    $scope.visibility.showRotation = false

  $scope.setVisibility = (toOpen)->
    switch toOpen
      when "showViewer"
        $scope.visibility.showViewer = !$scope.visibility.showViewer
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showAddFile"
        $scope.visibility.showAddFile = !$scope.visibility.showAddFile
        $scope.visibility.showViewer = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showMeta"
        $scope.visibility.showMeta = !$scope.visibility.showMeta
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showUpload"
        $scope.visibility.showUpload = !$scope.visibility.showUpload
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showReorder"
        $scope.visibility.showReorder = !$scope.visibility.showReorder
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showRotation = false
      when "showRotation"
        $scope.visibility.showRotation = !$scope.visibility.showRotation
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false

  $scope.closeAllPanel()
  $scope.performingJobTracker = false

  uploadId = $scope.$stateParams.aeId
  uploadFileId = $scope.$stateParams.fileId || 0
  $scope.thumbNumb = 15
  pageNum = $scope.$stateParams.page || 1
  $scope.currentPage = $scope.$stateParams.page || 1

  initParams = () ->
    uploadId = $scope.$stateParams.aeId
    uploadFileId = $scope.$stateParams.fileId || 0
    $scope.thumbNumb = 15
    pageNum = $scope.$stateParams.page || 1
    $scope.currentPage = $scope.$stateParams.page || 1
  
  userServiceFactory.getMe().then (resp)->
    if resp.status is 'ok'
      $scope.me=resp.data[0]
      if _.find($scope.me.userGroups, (role) -> return role is "ADMINISTRATORS")
        $scope.isAdmin = true

  checkCanChangePrivacy = () ->
    $scope.canChangePrivacy = ($scope.archive.owner == $scope.me.account) || $rootScope.secAuthorize(['ADMINISTRATORS'])

  allImageWithPrivacy= () ->
    $scope.imageWithPrivacy = false
    if $scope.archive.thumbsFiles
      for i in [0...$scope.archive.thumbsFiles.length]
        if $scope.archive.thumbsFiles[i].canView == true
          $scope.imageWithPrivacy = true
    else
      return false

  getArchiveRequest = () ->
    commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage).then (resp) ->
      commonsFactory.recordUploadAction(uploadId, 'VIEW')
      $scope.archive = resp.aentity
      $scope.deletedObject = true if resp.aentity && resp.aentity.logicalDelete == 1
      $scope.notFoundInDB  = true if resp.aentity == null
      if $scope.archive
        checkCanChangePrivacy()
        allImageWithPrivacy()
        commonsFactory.getSharedImagesByArchivial(uploadId).then (resp) ->
          $scope.imageIds = resp.data
        $rootScope.pageTitle = "Upload: " + $scope.archive.aeName
        $scope.archive.privacy = checkPrivacy()
        $scope.totalItems = resp.count || 1
        converterFactory.getReport()
        $location.search('page', $scope.currentPage)

        if $scope.archive.thumbsFiles
          for i in [0...$scope.archive.thumbsFiles.length]
            if $scope.archive.thumbsFiles[i].canView == true
              uploadFileId = $scope.archive.thumbsFiles[i].uploadFileId unless uploadFileId
              break
          thumb = commonsFactory.findImageById($scope.archive.thumbsFiles, uploadFileId)
          if $scope.$stateParams.modal
            if $scope.$stateParams.modal == 'list'
              $scope.showDocumentList(thumb, true)
            if $scope.$stateParams.modal == 'edit'
              openEditFolioModal(thumb)
            $location.search('modal', null)
          $scope.openViewer(thumb)
          angular.forEach($scope.archive.thumbsFiles, (image)->
            deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
              insertFolioInfo(resp, image)
          )
      $scope.getArchiveTracker = false

  getArchive = () ->
    if uploadFileId && uploadFileId != 0
      commonsFactory.getPageByFileId(uploadId, uploadFileId, $scope.thumbNumb).then (respPage) ->
        if respPage.status == 'success' && respPage.data?.page
          $scope.currentPage = respPage.data?.page
        else
          uploadFileId = 0
          $scope.currentPage = 1
        getArchiveRequest()
    else
      getArchiveRequest()

# TODO: I left the old implementation, I do not know what the cycle on the 204 line was used for. It seems it is not needed in the new implementation
#  getArchive = () ->
#    commonsFactory.getMySingleupload(uploadId, 1000, pageNum).then (first) ->
#      $scope.deletedObject = true if first.aentity && first.aentity.logicalDelete == 1
#      $scope.notFoundInDB  = true if first.aentity == null
#      if first.aentity && first.aentity.thumbsFiles
#        $scope.firstImage = commonsFactory.findImageById(first.aentity.thumbsFiles, uploadFileId)
#        if !$scope.firstImage
#          uploadFileId = first.aentity.thumbsFiles[0].uploadFileId
#          if !first.aentity.thumbsFiles[0].canView
#            for i in [0...first.aentity.thumbsFiles.length]
#              if first.aentity.thumbsFiles[i].canView
#                uploadFileId = first.aentity.thumbsFiles[i].uploadFileId
#                break
#        else
#          pageNum = Math.trunc($scope.firstImage.imageOrder / $scope.thumbNumb) + 1
#          $scope.currentPage = pageNum
#      commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, pageNum).then (resp) ->
#        commonsFactory.recordUploadAction($state.params.aeId, 'VIEW')
#        $scope.archive = resp.aentity
##        console.log('---->>>', resp.aentity)
#        if $scope.archive
#          checkCanChangePrivacy()
#          allImageWithPrivacy()
#          commonsFactory.getSharedImagesByArchivial(uploadId).then (resp) ->
#            $scope.imageIds = resp.data
#          $scope.openViewer(commonsFactory.findImageById($scope.archive.thumbsFiles, uploadFileId)) if $scope.archive && $scope.archive.thumbsFiles
#          # console.log $scope.archive
#          # console.log $scope.archive.volume.volumeName
#          # console.log $scope.archive.insert.insertName
#          $rootScope.pageTitle = "Upload: " + $scope.archive.aeName
#          $scope.archive.privacy = checkPrivacy()
#          $scope.totalItems = $scope.archive.pagination.totalPagesForSingleAe * 10 || 1
#          converterFactory.getReport().then (resp) ->
#            # console.log resp
#          if $scope.archive.thumbsFiles
#            angular.forEach($scope.archive.thumbsFiles, (image)->
#              deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
#                insertFolioInfo(resp, image)
#              )
#        $scope.getArchiveTracker = false

  getArchive()

  $scope.$on('refresh-ae', (evt, args)->
    initParams()
    refreshAECurrentPage())

  refreshAECurrentPage = () ->
    $scope.getArchiveTracker = true
    commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage || 1).then (resp) ->
      $scope.archive = resp.aentity
      checkCanChangePrivacy()
      $scope.archive.privacy = checkPrivacy()
      if $scope.archive.thumbsFiles
        angular.forEach($scope.archive.thumbsFiles, (image)->
          deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
            insertFolioInfo(resp, image)
        )
      $scope.getArchiveTracker = false
      $location.search('page', $scope.currentPage)

  $scope.pageChanged = (pageNumb) ->
    $scope.currentPage = pageNumb
    refreshAECurrentPage()

  $scope.showDocumentList = (uploadFileIdForDe, showCreateButton)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-document-list/document-list.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"uploadFileId": uploadFileIdForDe.uploadFileId, "showCreateButton": showCreateButton}
      }
    ).then (selectedDeId) ->
      if not selectedDeId
        openEditFolioModal(uploadFileIdForDe)
      else
#        console.log selectedDeId
        $state.go('mia.modifyDocEnt', { 'docId':selectedDeId.documentId })

  $scope.isSharedImage = (image) ->
    return $scope.imageIds.indexOf(image.uploadFileId)
    
  ###########################
  # DOCUMENT ENTITY ROUTINE
  ###########################

  $scope.documentEntity = (uploadFileIdForDe)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-select-de/select-de.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"uploadFileId": uploadFileIdForDe.uploadFileId}
      }
    ).then (selectedDeId) ->
      if not selectedDeId
        openEditFolioModal(uploadFileIdForDe)
      else
#        console.log selectedDeId
        $state.go('mia.modifyDocEnt', { 'docId':selectedDeId.documentId })

  openEditFolioModal = (uploadFileIdForDe) ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-folio/edit-folio.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"uploadFileId": uploadFileIdForDe.uploadFileId, "uploadId":uploadId, "documentEntityId":null, "aeFile": uploadFileIdForDe}
      }
    ).then (documentEntity) ->
      doc = documentEntity
      $state.go('mia.createDocEnt', { 'documentEntity':doc })

  ###########################
  # DOCUMENT VIEWER ROUTINES
  ###########################

  $scope.openViewer = (image)->
    return unless image
    $scope.closeAllPanel()
    actual = _.find $scope.archive.thumbsFiles, (pic) -> pic.highlighted is true
    if actual
      actual.highlighted = false
    image.highlighted = true
    uploadFileId = image.uploadFileId
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+image.uploadFileId)
    $scope.setVisibility("showViewer")
    $location.search('fileId', image.uploadFileId)

  $scope.closeViewer = ()->
    $scope.setVisibility("showViewer")

  
  #########################
  # FIX UPLOAD ROUTINES
  #########################

  $scope.fixUpload = (uploadFileId) ->
    $scope.fixUploadTracker = true
    commonsFactory.fixUpload(uploadFileId).then (resp) ->
      $scope.fixUploadTracker = false
      $scope.getArchiveTracker = true
      if resp.status = "ok"
        commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage || 1).then (resp) ->
          $scope.archive = resp.aentity
          $scope.archive.privacy=checkPrivacy()
          $scope.getArchiveTracker = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("fixUpload.ERROR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp

  #########################
  # REPLACE IMGS ROUTINES
  #########################
  $scope.replaceModal = (uploadFileId)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-replace-img/replace-img.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFileId": uploadFileId
            "uploadId": $state.params.aeId
            }
      }
    ).then () ->
      getArchive()

  #########################
  # ROTATE IMGS ROUTINES
  #########################
  $scope.rotate = (uploadFile, degrees)->
    $location.search('fileId', uploadFile.uploadFileId)
    images = {}
    images[uploadFile.uploadFileId] = uploadFile
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-rotate-img/rotate-img.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFiles": images
            "degrees": degrees
            }
      }
    ).then () ->
      getArchive()

  #########################
  # REORDER ROUTINES
  #########################
  $scope.reorder =() ->
    $scope.setVisibility("showReorder")

  $scope.onDropComplete = (index, obj, evt, changePage) ->
    actual = _.find $scope.archive.thumbsFiles, (pic) -> pic.moved is true
    if actual
      actual.moved = false
    switch index
      when 'previousPage'
        index = $scope.archive.thumbsFiles[0].imageOrder-1
        toPage = -1
      when 'nextPage'
        index = $scope.archive.thumbsFiles[14].imageOrder+1
        toPage = 1
      else
        toPage = 0

    $scope.performingJobTracker = true
    commonsFactory.changeOrder(obj.uploadFileId, index).then (resp) ->
      if resp.status is "ok"
        $scope.currentPage = parseInt($scope.currentPage) + toPage
        commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage || 1).then (resp) ->
          $scope.archive = resp.aentity
          if $scope.archive.thumbsFiles
            angular.forEach($scope.archive.thumbsFiles, (image)->
              deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
                insertFolioInfo(resp, image)
              )
          movedPic = _.find $scope.archive.thumbsFiles, (pic) -> pic.uploadFileId is obj.uploadFileId
          if movedPic
            movedPic.moved = true
          $scope.performingJobTracker = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("modal.REORDERR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
  
  $scope.closeReorder =() ->
    $scope.setVisibility("showReorder")
    $scope.setVisibility("showViewer")

  #########################
  # SET PRIVACY ROUTINES
  #########################

  $scope.setPrivacy = (visibility) ->
  #  canSet = true
  #  for i in [0...$scope.archive.thumbsFiles.length]
  #    if($scope.archive.thumbsFiles[i].canSetPrivate == false)
  #      canSet = false
  #  if(canSet)
    $scope.performingJobTracker = true
    commonsFactory.setPrivacy(uploadId, visibility).then (resp) ->
      if $scope.archive.thumbsFiles.length != 1
        if resp.status is "ok"
          for image in $scope.archive.thumbsFiles
            image.privacy = resp.privacy
            $scope.archive.privacy = checkPrivacy()
            if(visibility == 1)
              if(image.canSetPrivate == false)
                pageTitle = $filter('translate')("modal.ERROR")
                messagePage = $filter('translate')("modal.PRIVACYERR3")
                modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
                # alert('folio  ' + image.folios[0].folioNumber + ' cannot be set to private')
          refreshAECurrentPage()
      if $scope.archive.thumbsFiles.length == 1
        for image in $scope.archive.thumbsFiles
          image.privacy = resp.privacy
          $scope.archive.privacy = checkPrivacy()
          if(visibility == 1)
            if(image.canSetPrivate == false)
              pageTitle = $filter('translate')("modal.ERROR")
              messagePage = $filter('translate')("modal.PRIVACYERR3")
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
              # alert('folio  ' + image.folios[0].folioNumber + ' cannot be set to private')
        refreshAECurrentPage()
      if resp.status is "ko"
        if resp.message == 'There are documents associated with this upload.'
          pageTitle = $filter('translate')("modal.ERROR")
          messagePage = $filter('translate')("modal.PRIVACYERR2")
        else
          pageTitle = $filter('translate')("modal.ERROR")
          messagePage = $filter('translate')("modal.PRIVACYERR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
      $scope.performingJobTracker = false
  #  else
  #    pageTitle = $filter('translate')("modal.ERROR")
  #    messagePage = $filter('translate')("modal.PRIVACYERR2")
  #    modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->

  $scope.setSingleDocPrivacy = (uploadFileId, privacyValue,canSetPrivate) ->
    if(canSetPrivate == false)
      pageTitle = $filter('translate')("modal.ERROR")
      messagePage = $filter('translate')("modal.PRIVACYERR1")
      modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
    else
      $scope.performingJobTracker = true
      commonsFactory.setSingleDocPrivacy(uploadFileId, privacyValue).then (resp)->
        if resp.status is "ok"
          findDocument(uploadFileId).privacy = resp.privacy
        if resp.status is "ko"
          if resp.message == 'There are documents associated with this image.'
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = $filter('translate')("modal.PRIVACYERR1")
          else
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = $filter('translate')("modal.PRIVACYERR")
          modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
        $scope.performingJobTracker = false
        $scope.archive.privacy = checkPrivacy()

  checkPrivacy = ()->
    publicAe=false
    if $scope.archive.thumbsFiles
      for thumbFile in $scope.archive.thumbsFiles
        if thumbFile.privacy is 0
          publicAe = true
    if publicAe
      return "Public"
    else
      return "Private"

  #########################
  # ADD FILE/S ROUTINES
  #########################

  $scope.$on('invalid-files', (evt, args)->
    $scope.wrongFiles = args
  )
  
  #NEW alai
  $scope.invalidImages = []

  $scope.validatePicFiles = (image) ->
    defer = $q.defer()
    fileType = image.name.split(".").pop()
    fileType = fileType.toLowerCase()
    if fileType != 'jpg'
      if fileType != 'tif'
        if fileType != 'tiff'
          if fileType != 'jpeg'
            image.$error = 'invalidType'
            $scope.invalidImages.push(image)
    return null

  $scope.getFiles = () ->
    document.getElementById("file-upload").click()

  $scope.cancelAddFiles = () ->
    $scope.wrongFiles = []
    $scope.picFiles = []
    $scope.setVisibility("showAddFile")
    
  $scope.addFiles = () ->
    $scope.setVisibility("showAddFile")
    commonsFactory.getLastUploadedFile(uploadId).then (resp) ->
      $scope.lastFile = resp.data.lastUpload.fileName

  $scope.finishUpload = () ->
    commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage || 1).then (resp) ->
      $scope.archive = resp.aentity
      $scope.currentPage = $scope.archive.pagination.totalPagesForSingleAe
#      $scope.pageChanged()
      $scope.showAddFile = false
#      $scope.getArchiveTracker = false
      #$scope.setVisibility("showAddFile")
      $scope.setVisibility("showViewer")
      $timeout -> getArchiveRequest()

  # multiple file upload
  $scope.addFileToAE = (files) ->
    $scope.uploading = true
    $scope.progress = 0
    $location.hash('top')
    $anchorScroll()
    if files and files.length
      #for file in files
        #alert(file.name)
        #file.safeName = file.name.replace(new RegExp("\\&", 'gi'), '_')
      $rootScope.isBusy = true
      Upload.upload(
        url: 'json/archive/addfilestoAE/'+uploadId
        data:
          files: files).then ((response) ->
            #file uploaded ok
            $scope.result = response.data
            $scope.uploading = false
            $rootScope.isBusy = false
            $scope.finishUpload()
          ), ((response) ->
            #file uploaded ko
            if response.status > 0
              $scope.errorMsg = response.status + ': ' + response.data
          ), (evt) ->
            #file uploaded progress
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
            $rootScope.$broadcast('progress', $scope.progress)
    # to do find a better solution to clean the uploaded files

  #############################
  # EDIT DOCUMENT META ROUTINES
  #############################

  $scope.editImageMetadata = (uploadFileId) ->
    $rootScope.VolumeID = $scope.archive.volume.volumeId
    $rootScope.InsertID = $scope.archive.insert?.insertId
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata.html'
      controller:'modalIstanceController'
      backdrop: 'static'
      resolve:{
        options:
          -> {
            "uploadFileId": uploadFileId,
            "volumeId": $scope.archive.volume.volumeId,
            "insertId": $scope.archive.insert?.insertId || null
          }
      }
    ).then (editImageMetadataIsChange) ->
      if editImageMetadataIsChange
        $state.reload()

  insertFolioInfo = (folioInfo, image) ->
    image.folios=[]
    if folioInfo.status is "ok"
      image.folios[0] = folioInfo.data[0]
      if folioInfo.data[1]
        image.folios[1] = folioInfo.data[1]
    else
      image.folios[0] = {}
      image.folios[0].folioId= ""
      image.folios[0].folioNumber = ""
      image.folios[0].uploadFileId = image.uploadFileId

      image.folios[1] = {}
      image.folios[1].folioId= ""
      image.folios[1].folioNumber = ""
      image.folios[1].uploadFileId = image.uploadFileId

  findDocument = (uploadId) ->
    for file in $scope.archive.thumbsFiles
      if file.uploadFileId is uploadId
        return file
        
  ####################################
  # DOWNLOAD AND DELETE FILE ROUTINES
  ####################################

  $scope.downloadDocument = (uploadFileId) ->
    commonsFactory.downloadDocument(uploadFileId).then (resp)->

  $scope.deleteDocument = (uploadFileId) ->
    if($scope.archive.thumbsFiles.length>1)
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = $filter('translate')("modal.DELETINGIMG")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (answer) ->
        if answer
          presentDocEn = []
          $scope.performingJobTracker = true
          commonsFactory.deleteDocument(uploadFileId).then (resp)->
            if resp.status is "ok"
              indexCounter=0
              for file in $scope.archive.thumbsFiles
                if file.uploadFileId is uploadFileId
                  $scope.archive.thumbsFiles.splice(indexCounter, 1)
                  break
                else
                  indexCounter++
              #if a document is deleted all the panels must be closed
                $scope.closeAllPanel()
            else
              pageTitle = $filter('translate')("modal.WARNING")
              messagePage = resp.message
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (confirmation) ->
                if resp.data.documentAE.documentIds.length
                  modalFactory.openModal(
                    templateUrl: 'modules/directives/modal-templates/modal-del-img/del-img.html'
                    controller:'modalIstanceController'
                    resolve:{
                      options:
                        -> {"documents": resp.data.documentAE}
                      }
                  ).then (selectedRepo) ->
            if not $scope.archive.thumbsFiles.length
              $state.go('mia.archive')
            $scope.performingJobTracker = false
    else
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = $filter('translate')("modal.LASTIMG")
      modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (answer) ->

  $scope.selectImage = (image, evt) ->
    if image.canView
      if $scope.visibility.showRotation
        if evt.shiftKey
          if typeof $scope.selectedImages[image.uploadFileId] != typeof undefined
            delete $scope.selectedImages[image.uploadFileId]
          else if JSON.stringify($scope.selectedImages).match(/"first":1/g)
            $scope.selectedImages[image.uploadFileId] = image
          else
            $scope.selectedImages[image.uploadFileId] = image
            $scope.selectedImages[image.uploadFileId].first = 1
        else
          if Object.keys($scope.selectedImages).length == 1
            if typeof $scope.selectedImages[image.uploadFileId] != typeof undefined
              delete $scope.selectedImages[image.uploadFileId]
            else
              $scope.selectedImages = {}
              $scope.selectedImages[image.uploadFileId] = image
          else
            $scope.selectedImages = {}
            $scope.selectedImages[image.uploadFileId] = image

  $scope.rotateSelectedImages = (degrees) ->
    if Object.values($scope.selectedImages).length == 0
      return

    uploadFileId = Object.values($scope.selectedImages)[0].uploadFileId
    $scope.$stateParams.fileId = "" + uploadFileId + ""
    #$location.search('fileId', uploadFile.uploadFileId)
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-rotate-img/rotate-img.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFiles": $scope.selectedImages
            "degrees": degrees
          }
      }
    ).then () ->
      getArchive()
#    console.log degrees

  $scope.selectAllImages = (images) ->
    $scope.selectedImages = {}
    images.forEach (image) ->
      if JSON.stringify($scope.selectedImages).match(/"first":1/g)
        image.first = 1
      $scope.selectedImages[image.uploadFileId] = image

  $scope.enableRotation = () ->
    $scope.selectedImages = {}
    $scope.setVisibility("showRotation")
    $scope.random = Math.random()

  $scope.disableRotation = () ->
    $scope.selectedImages = {}
    $scope.setVisibility("showRotation")
    $scope.setVisibility("showViewer")

  $scope.deleteAEntity = (uploadId) ->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = $filter('translate')("modal.DELETINGAE")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        commonsFactory.deleteAEntity(uploadId).then (resp)->
          if resp.status is "ok"
            $state.go('mia.archive')
          if resp.status is "ko"
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = resp.message
            modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
              return resp
          if resp.status is "w"
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = $filter('translate')("modal.DELAEERR") + ': ' +resp.message
            modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (confirmation) ->
              if resp.data.documentAE.documentIds.length
                modalFactory.openModal(
                  templateUrl: 'modules/directives/modal-templates/modal-del-img/del-img.html'
                  controller:'modalIstanceController'
                  resolve:{
                    options:
                      -> {"documents": resp.data.documentAE}
                    }
                ).then (selectedRepo) ->
          if not $scope.archive.thumbsFiles.length
            $state.go('mia.archive')
          $scope.performingJobTracker = false


  $scope.undeleteAEntity = (uploadId) ->
    commonsFactory.undeleteAEntity(uploadId).then (undeleteAEntityResp)->
      if undeleteAEntityResp.status is "ok"
        $scope.reload()
      else
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = undeleteAEntityResp.message
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp

  #########################
  # EDIT METADATA ROUTINES
  #########################
  $scope.editMeta = () ->
#    commonsFactory.recordUploadAction($state.params.aeId, 'EDIT')
    $scope.setVisibility("showMeta")
    $scope.archiveToEdit = angular.copy($scope.archive)

  $scope.dataEdited = ()->
    $scope.metadataEdited = true

  $scope.addRepo = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
      controller:'modalIstanceController'
    ).then (selectedRepo) ->
      if !angular.equals(selectedRepo, $scope.archiveToEdit.repository)
        $scope.archiveToEdit.repository = selectedRepo
        $scope.archiveToEdit.collection = {}
        $scope.archiveToEdit.series = {}
        $scope.archiveToEdit.volume = {}
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addCollection = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.repository.repositoryId}
        }
    ).then (selectedCollection) ->
#      console.log selectedCollection
      if $scope.archiveToEdit.collection.collectionId isnt selectedCollection.collectionId
        $scope.archiveToEdit.collection = selectedCollection
        $scope.archiveToEdit.series = {}
        $scope.archiveToEdit.volume = {}
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addVolume = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-volume/add-volume.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.collection.collectionId}
        }
    ).then (selectedVolume) ->
#      console.log('selectedVolume', selectedVolume)
#      if !angular.equals($scope.archiveToEdit.volume, selectedVolume)
      volumeInsertDescrFactory.findVolumeDescription(selectedVolume.volumeId).then (response) ->
        volumeData = response.data.data
        if volumeData
          selectedVolume.hasInserts = volumeData.hasInserts
          selectedVolume.aeCount = volumeData.aeCount
          $scope.archiveToEdit.volume = selectedVolume
          $scope.archiveToEdit.insert = {}
          $scope.metadataEdited = true

  $scope.addSeries = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.collection.collectionId}
        }
    ).then (selectedSeries) ->
      if !angular.equals($scope.archiveToEdit.series, selectedSeries)
        $scope.archiveToEdit.series = selectedSeries
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addInsert = () ->
#    console.log $scope.archiveToEdit
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-insert/add-insert.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "parentId": $scope.archiveToEdit.volume.volumeId
            "parentName": $scope.archiveToEdit.volume.volumeName
            "collection": $scope.archiveToEdit.collection
          }
        }
    ).then (selectedInsert) ->
      if !angular.equals($scope.archiveToEdit.insert, selectedInsert)
        $scope.archiveToEdit.insert = selectedInsert
        $scope.metadataEdited = true

  $scope.canISave= () ->
    return $scope.archiveToEdit.repository.repositoryName and
    $scope.archiveToEdit.collection.collectionName and
    $scope.archiveToEdit.volume.volumeName and
    $scope.metadataEdited and
    $scope.archiveToEdit.aeName and
    $scope.archiveToEdit.aeName.length<=254 and
    (($scope.archiveToEdit.volume.volumeName && !$scope.archiveToEdit.volume?.hasInserts && !$scope.archiveToEdit.volume?.aeCount > 0) || ($scope.archiveToEdit.volume.volumeName && $scope.archiveToEdit.volume?.aeCount > 0) || ($scope.archiveToEdit.insert.insertName && $scope.canChooseInsertAe()))

  $scope.canChooseInsertAe = () ->
    isStudyColl = $scope.archiveToEdit.collection?.studyCollection
    volume      = $scope.archiveToEdit.volume
    volHasIns   = volume?.hasInserts
    volHasAEs   = volume?.aeCount > 0

    return false if volHasAEs
    (isStudyColl && volHasIns && !volHasAEs) || (isStudyColl && !volHasIns && !volHasAEs) || (!isStudyColl && volHasIns && !volHasAEs) || (!isStudyColl && !volHasIns && !volHasAEs && $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS']))

  $scope.finishEditMeta = () ->
    $scope.editMetaLoader = true

    params = {}
    params.aeName = $scope.archiveToEdit.aeName or ''
    params.aeDescription = $scope.archiveToEdit.aeDescription or ''
    params.repository = {}
    params.repository = {}
    params.repository.repositoryId = $scope.archiveToEdit.repository.repositoryId or ''
    params.repository.repositoryName = $scope.archiveToEdit.repository.repositoryName or ''
    params.repository.repositoryAbbreviation = $scope.archiveToEdit.repository.repositoryAbbreviation or ''
    params.repository.repositoryDescription = $scope.archiveToEdit.repository.repositoryDescription or ''
    params.repository.repositoryLocation = $scope.archiveToEdit.repository.repositoryLocation or ''
    params.repository.repositoryCity = $scope.archiveToEdit.repository.repositoryCity or ''
    params.repository.repositoryCountry = $scope.archiveToEdit.repository.repositoryCountry or ''

    params.collection = {}
    params.collection.collectionId = $scope.archiveToEdit.collection.collectionId or ''
    params.collection.collectionName = $scope.archiveToEdit.collection.collectionName or ''
    params.collection.collectionAbbreviation = $scope.archiveToEdit.collection.collectionAbbreviation or ''
    params.collection.collectionDescription = $scope.archiveToEdit.collection.collectionDescription or ''

#    params.series = {}
#    params.series.seriesId = $scope.archiveToEdit.series.seriesId or ''
#    params.series.seriesName  = $scope.archiveToEdit.series.seriesName or ''
#    params.series.seriesSubtitle = $scope.archiveToEdit.series.seriesSubtitle or ''

    params.volume = {}
    params.volume.volumeId = $scope.archiveToEdit.volume.volumeId or ''
    params.volume.volumeName = $scope.archiveToEdit.volume.volumeName or ''

    params.insert = {}
    params.insert.insertId = $scope.archiveToEdit.insert.insertId or ''
    params.insert.insertName = $scope.archiveToEdit.insert.insertName or ''

    #$scope.showMeta = !$scope.showMeta
    commonsFactory.updateMetaDataAE(uploadId, params).then (resp) ->
      if resp.status is "ok"
        getArchive()
        $scope.editMetaLoader = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("modal.EDITMETAERR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp

  $scope.closeEditMeta = () ->
    $scope.setVisibility("showMeta")


  $scope.chooseUsersWithImage = (image) ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-share-with-users/share-with-users.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "imageId": image.uploadFileId
            "archiveId": $scope.archive.uploadId
            }
        }
    ).then (resp) ->
      getArchive()

  $scope.chooseUsers = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-share-with-users/share-with-users.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "imageId": ''
            "archiveId": $scope.archive.uploadId
            }
        }
    ).then (resp) ->
      getArchive()

  #----------------------------------------
  # Show Fullscreen
  #----------------------------------------
  $scope.transcriptions = []
  transWindow=undefined

  reloadPage=()->
    $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})

  extractDomain = (url) ->
    domain = undefined
    if url.indexOf('://') > -1
      domain = url.split('/')[2]
    else
      domain = url.split('/')[0]
    domain='http://'+domain

  $scope.openTranscriptor= () ->
    uploadFileId = $location.search().fileId
    workUrl = $state.href($state.current.name, $state.params, {absolute: true})
    transWindow=$window.open("#{extractDomain(workUrl)}/Mia/json/src/ShowArchivalEntityInManuscriptViewer/show?archivalEntityId=#{$scope.archive.uploadId}&fileId=#{uploadFileId}")
    transWindow.onbeforeunload=()->
      $timeout(reloadPage, 1000)

  $scope.synopsis = null
  $scope.gotoComments = () ->
    $location.hash('comments')
    $anchorScroll()

  $scope.goToPublicProfile = (user) ->
    $state.go('mia.publicProfile', { 'account': user  })

  $scope.canCreateDoc = (image) ->
    if(image.privacy == 1)
      if($scope.archive.owner == $scope.me.account)
        return true
      else
        return false
    else
      return true
  