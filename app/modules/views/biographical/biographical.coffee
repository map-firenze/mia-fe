bioPeopoleApp = angular.module 'bioPeopoleApp',[
  'bioPeopleServiceApp'
  'modalUtils'
  'uploadPortraitApp'
  'titles'
  'modParent'
  'personDocApp'
  'actionsHistory'
  'commentsModule'
  'addToCsApp'
]


bioPeopoleApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.bio-people',
        url: '/people/:id?scrollTo'
        reloadOnSearch: false
        views:
          'content':
            controller: 'bioPeopleController'
            templateUrl: 'modules/views/biographical/biographical.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'

]
bioPeopoleApp.controller 'bioPeopleController', (bioPeopleFactory, $sce, $scope, $rootScope, $state, $window, deFactory, $filter, modalFactory, $stateParams, userServiceFactory, alertify, $timeout, $location, $anchorScroll, csFactory, searchDeService, searchFactory) ->
  $rootScope.pageTitle = "Biographical Record"
  $rootScope.deTitle = ""
  $scope.id = $scope.$stateParams.id

  ##############################
  # panels and graphic variables
  ##############################
  $scope.allClosed = true
  $scope.editingPortrait = false
  $scope.portraitAuthor = ""
  $scope.portraitSubject = ""
  $scope.errors = {}
  $scope.switchAllPanels = () ->
    $scope.allClosed = !$scope.allClosed
    for section in $scope.sections
      section.open = !$scope.allClosed

  #page sections
  $scope.sections = []
  $scope.sections.push({"name":"Summary", "open":true, "edit":false})
  $scope.sections.push({"name":"Details", "open":false, "edit":false})
  $scope.sections.push({"name":"Names", "open":false, "edit":false})
  $scope.sections.push({"name":"Headquarters", "open":false, "edit":false})
  $scope.sections.push({"name":"Titles / Occupations", "open":false, "edit":false})
  $scope.sections.push({"name":"Parents", "open":false, "edit":false})
  $scope.sections.push({"name":"Children", "open":false, "edit":false})
  $scope.sections.push({"name":"Spouses", "open":false, "edit":false})
  $scope.sections.push({"name":"Research Notes", "open":false, "edit":false})

  $scope.editSection = []
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)
  $scope.editSection.push(false)

  $scope.months = [
    {'id': null, 'name':''},
    {'id': 1, 'name':'January'},
    {'id': 2, 'name':'February'},
    {'id': 3, 'name':'March'},
    {'id': 4, 'name':'April'},
    {'id': 5, 'name':'May'},
    {'id': 6, 'name':'June'},
    {'id': 7, 'name':'July'},
    {'id': 8, 'name':'August'},
    {'id': 9, 'name':'September'},
    {'id': 10, 'name':'October'},
    {'id': 11, 'name':'November'},
    {'id': 12, 'name':'December'}
  ]

  $scope.genders=[{'label':'Father', 'genre':'M', "active" : false},{'label':'Mother', 'genre':'F', "active" : false}]
  $scope.spousesJson=[{'label':'Husband', 'value':'h', "active" : false},{'label':'Wife', 'value':'w', "active" : false}]

  $scope.switchEdit = (sectionToEdit)->
    $scope.editSection[sectionToEdit]=!$scope.editSection[sectionToEdit]

  $scope.showDocDetails=false

  ###############
  # Edit Routines
  ###############

  $scope.uploadPortrait=()->
    modalScope = $scope.$new(true)
    modalScope.personId = $scope.id
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-upload-portrait/upload-portrait.html'
      controller:'modalIstanceController'
      scope: modalScope
    ).then (resp) ->
#      console.log resp.data
      $state.reload()

  $scope.editPortrait = ()->
    $scope.editingPortrait = !$scope.editingPortrait
    $scope.portraitAuthor = $scope.personPortraitDetails.portraitAuthor
    $scope.portraitSubject = $scope.personPortraitDetails.portraitSubject
#    console.log $scope.personPortraitDetails
  
  $scope.savePortrait = (portraitAuthor, portraitSubject)->
    bioPeopleFactory.editPortrait($scope.id, portraitAuthor, portraitSubject).then (resp)->
      $scope.editingPortrait = false
      $state.reload()

  $scope.switchEditSummary=()->
#    console.log $scope.personData
#    console.log oldDataSummary
    $scope.sections[0].edit=!$scope.sections[0].edit
    if $scope.sections[0].edit
      $scope.oldDataSummary=angular.copy($scope.personData)
    else
      $scope.personData=angular.copy($scope.oldDataSummary)

  $scope.saveSummary=()->
#    console.log $scope.personData
    bioPeopleFactory.editPerson($scope.personData).then (resp)->
      $scope.oldDataSummary={}
      $scope.sections[0].edit=false

  $scope.switchEditPersonal=()->
    $scope.sections[1].edit=!$scope.sections[1].edit
    if $scope.sections[1].edit
      $scope.oldDataPersonal=angular.copy($scope.personData)
    else
      $scope.personData=angular.copy($scope.oldDataPersonal)

  $scope.validateDates = (field) ->
    birthYear = $scope.personData.bornYear
    deathYear = $scope.personData.deathYear
    activeStart = $scope.personData.activeStart
    activeEnd = $scope.personData.activeEnd
    bornDay = $scope.personData.bornDay
    deathDay = $scope.personData.deathDay
    $scope.errors.validateErrorMessage = ''
    $scope.errors.personDataFormInvalid = false
    $scope.errors.birthDateInvalid = false
    $scope.errors.deathDateInvalid = false
    $scope.errors.activeStartInvalid = false
    $scope.errors.activeEndInvalid = false
    $scope.errors.birthDayInvalid = false
    $scope.errors.deathDayInvalid = false
    regexpPathern = /^\d+$/
    onlyNumberMsg = 'Field can only be a number'
    incorrentDayValue = 'Incorrect day value'

    # birth day is a number
    if field == 'bornDay' and !regexpPathern.exec(bornDay) and bornDay and bornDay != ''
      $scope.errors.birthDayInvalid = true
      $scope.errors.personDataFormInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # birth day is a < 1 and > 31
    if field == 'bornDay' and bornDay and bornDay != '' and (Number(bornDay) < 1 or Number(bornDay) > 31 )
      $scope.errors.birthDayInvalid = true
      $scope.errors.personDataFormInvalid = true
      $scope.errors.validateErrorMessage = incorrentDayValue
      return

    # birth year is a number
    if field == 'bornYear' and !regexpPathern.exec(birthYear) and birthYear and birthYear != ''
      $scope.errors.personDataFormInvalid = true
      $scope.errors.birthDateInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # birth > death (for birth field)
    if field == 'bornYear' and birthYear and birthYear != '' and deathYear and deathYear != '' and Number(birthYear) > Number(deathYear)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.birthDateInvalid = true
      $scope.errors.validateErrorMessage = 'Birth Year cannot be bigger than Death Year'
      return

    # death day is a number
    if field == 'deathDay' and !regexpPathern.exec(deathDay) and deathDay and deathDay != ''
      $scope.errors.personDataFormInvalid = true
      $scope.errors.deathDayInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # birth day is a < 1 and > 31
    if field == 'deathDay' and deathDay and deathDay != '' and (Number(deathDay) < 1 or Number(deathDay) > 31 )
      $scope.errors.deathDayInvalid = true
      $scope.errors.personDataFormInvalid = true
      $scope.errors.validateErrorMessage = incorrentDayValue
      return

    # death year is a number
    if field == 'deathYear' and !regexpPathern.exec(deathYear) and deathYear and deathYear != ''
      $scope.errors.personDataFormInvalid = true
      $scope.errors.deathDateInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # death < birth (for death field)
    if field == 'deathYear' and birthYear and birthYear != '' and deathYear and deathYear != '' and Number(deathYear) < Number(birthYear)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.deathDateInvalid = true
      $scope.errors.validateErrorMessage = 'Death Year cannot be less than Birth Year'
      return

    # actve start is a number
    if field == 'activeStart' and !regexpPathern.exec(activeStart) and activeStart and activeStart != ''
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeStartInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # actve start > active end (for active start)
    if field == 'activeStart' and activeStart and activeStart != '' and activeEnd and activeEnd != '' and Number(activeStart) > Number(activeEnd)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeStartInvalid = true
      $scope.errors.validateErrorMessage = 'Active Start cannot be bigger than Active End'
      return

    # active end is a number
    if field == 'activeEnd' and !regexpPathern.exec(activeEnd) and activeEnd and activeEnd != ''
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeEndInvalid = true
      $scope.errors.validateErrorMessage = onlyNumberMsg
      return

    # active end < active start (for active end)
    if field == 'activeEnd' and activeStart and activeStart != '' and activeEnd and activeEnd != '' and Number(activeEnd) < Number(activeStart)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeEndInvalid = true
      $scope.errors.validateErrorMessage = 'Active End cannot be less than Active Start'
      return

    # birth > active end (for birth field)
    if field == 'bornYear' and birthYear and birthYear != '' and activeEnd and activeEnd != '' and Number(birthYear) > Number(activeEnd)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.birthDateInvalid = true
      $scope.errors.validateErrorMessage = 'Birth Year cannot be bigger than Active End'
      return

    # active end < birth (for active end)
    if field == 'activeEnd' and birthYear and birthYear != '' and activeEnd and activeEnd != '' and Number(activeEnd) < Number(birthYear)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeEndInvalid = true
      $scope.errors.validateErrorMessage = 'Active End cannot be less than Birth Year'
      return

    # active start > death (for active start)
    if field == 'activeStart' and activeStart and activeStart != '' and deathYear and deathYear != '' and Number(activeStart) > Number(deathYear)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.activeStartInvalid = true
      $scope.errors.validateErrorMessage = 'Active Start cannot be bigger than Death Year'
      return

    # death > active start (for death)
    if field == 'deathYear' and activeStart and activeStart != '' and deathYear and deathYear != '' and Number(deathYear) < Number(activeStart)
      $scope.errors.personDataFormInvalid = true
      $scope.errors.deathDateInvalid = true
      $scope.errors.validateErrorMessage = 'Death Year cannot be less than Active Start'
      return

  $scope.savePersonal = () ->
    if $scope.bornPlace?
      $scope.personData.bornPlaceId = $scope.bornPlace.id
    else
      $scope.personData.bornPlaceId = null
    if $scope.deathPlace?
      $scope.personData.deathPlaceId = $scope.deathPlace.id
    else
      $scope.personData.deathPlaceId = null
    if $scope.errors.personDataFormInvalid
      alertify.alert("Please correct form errors and save the document again.")
      return
    bioPeopleFactory.editPerson($scope.personData).then (resp)->
      $scope.oldDataPersonal={}
      $scope.sections[1].edit=false
      if resp.status == 'ok'
#        bioPeopleFactory.recordPersonAction($scope.$stateParams.id, 'EDIT')
        initModule()
        alertify.alert("If you changed the \"FirstName\" or \"Family Name\" fields, please check the Names Section to remove the old ones which could be wrong or not longer necessary")

  $scope.realUndelete = () ->
    bioPeopleFactory.undeletePerson($scope.$stateParams.id).then (resp)->
      if resp.status is 'ok'
        alertify.alert 'Person successfully restored'
        $state.reload()
      else
        console.log('Some error occured during undelete person process')

  $scope.changePlace=(placeToChange)->
    console.log "prova"

  #################
  # Names Routines
  #################
  # names={}
  # names.edit=false
  # console.log(names.edit)

  $scope.namesEditing=false
  $scope.newNameVariant=false
  
  $scope.nameTypes=['Patronymic', 'Appellative', 'Family', 'Married', 'Given', 'SearchName', 'Maiden']
  backNames=[]

  $scope.editNames=(name) ->
    $scope.namesEditing=true
    $scope.newNameVariant=true
    name.edit=true
    backNames=angular.copy($scope.names)

  $scope.addNewName = () ->
    $scope.namesEditing=true
    $scope.newNameVariant=true
    newName = {}
    newName.nameType = ""
    newName.altName = ""
    newName.edit = true
    $scope.names.push(newName)

  $scope.cancelEditName = (name) ->
    $scope.namesEditing=false
    $scope.newNameVariant=false
    name.edit=false
    $scope.names=angular.copy(backNames)
    initModule()
  
  $scope.cancelEditNewName = (name) ->
    $scope.namesEditing=false
    $scope.newNameVariant=false
    name.edit=false
    $scope.names.splice(-1, 1)

  $scope.saveName = (name) ->
    if name.altNameId
      for oldName in backNames
        if oldName.altNameId is name.altNameId
          oldNameToChange=oldName
      bioPeopleFactory.editPersonName($scope.$stateParams.id, name, oldNameToChange).then (resp)->
    else
      bioPeopleFactory.addPersonName($scope.$stateParams.id, name).then (resp)->
    $scope.namesEditing=false
    $scope.newNameVariant=false
    name.edit=false

  $scope.deleteName = (name) ->
    bioPeopleFactory.deletePersonName($scope.$stateParams.id, name).then (resp)->
      $scope.namesEditing=false
      $scope.newNameVariant=false
      name.edit=false
      $state.reload()

  ########################
  # Headquarters Routines
  ########################

  $scope.headquarters = []
  $scope.addingHeadquarter = false
  $scope.newHeadYear = ''
  $scope.newHeadquarter = {}

  updateHeadquartersName = (personId)->
    bioPeopleFactory.getHeadquarters(personId).then (resp) ->
      $scope.headquarters = resp.data.headquarters
      angular.forEach($scope.headquarters, (headq)->
        deFactory.getPlaceById(headq.placeId).then (resp)->
          headq.feData=resp.data
      )
  
  $scope.deleteHeadquarter = (headQId)->
    bioPeopleFactory.deleteHeadquarter(headQId).then (resp)->
      updateHeadquartersName($scope.$stateParams.id)

  $scope.addHeadquarter = ()->
    $scope.addingHeadquarter = true

  $scope.cancelAddHeadquarter = ()->
    $scope.addingHeadquarter = false

  $scope.selectHeadquarter = (passed, place)->
    $scope.newHeadquarter.placeId = passed.temp.id

  $scope.saveHeadquarter = (year)->
    $scope.newHeadquarter.year = year
    $scope.newHeadquarter.personId = $scope.$stateParams.id
    bioPeopleFactory.addHeadquarter($scope.newHeadquarter).then (resp)->
      $scope.addingHeadquarter = false
      updateHeadquartersName($scope.$stateParams.id)


  #################
  # Titles Routines
  #################

  #set the preferred role on front end
  $scope.setPreferredRole = ()->
    angular.forEach($scope.titles, (singleTitle)->
      if singleTitle.titleOccId == $scope.preferredRole
        singleTitle.preferredRole = true
      else
        singleTitle.preferredRole = false
      )
  #this function is designed in order to assign automatically the preferred
  # in the case in which the prson had 2 titles and one of them was deleteChild
  # the remained title must be preferred role
  $scope.initializeTitles=(personTitles)->
    if personTitles
      $scope.titles=personTitles
      if ($scope.titles.length == 1 && $scope.firstTitle == false)
        $timeout ->
          unless $scope.titles[0].preferredRole
            oldTitle = $scope.titles[0]
            $scope.titles[0].preferredRole = true
            newTitle = $scope.titles[0]
            $scope.firstTitle = true
            $scope.saveTitle(newTitle, oldTitle)
      else
        $scope.firstTitle = false
    else
      $scope.titles = []
      $scope.firstTitle = true

  # set the poLink by instantiaiting a new object. The parameter title can be
  # be the new title added or an old title to be modified. The parameter params
  # is the poLink key in json object to pass to backend
  $scope.setPoLink=(title, params)->
    params.poLink.startUnsure="0"
    params.poLink.startApprox="0"
    params.poLink.endUnsure="0"
    params.poLink.endApprox="0"
    if (title.poLink != null && title.poLink != undefined)
      if title.poLink.startUnsure
        params.poLink.startUnsure = "1"
      if title.poLink.startApprox
        params.poLink.startApprox = "1"
      if title.poLink.endUnsure
        params.poLink.endUnsure = "1"
      if title.poLink.endApprox
        params.poLink.endApprox = "1"
    params.poLink.startYear = title.poLink?.startYear || null
    params.poLink.startMonth = title.poLink?.startMonth || null
    params.poLink.startDay = title.poLink?.startDay || null
    params.poLink.endYear = title.poLink?.endYear || null
    params.poLink.endMonth = title.poLink?.endMonth || null
    params.poLink.endDay = title.poLink?.endDay || null


  $scope.cancelTitle=(title)->
    title.editing=false
    $scope.titleToEdit.editing=false
    initModule()

  $scope.saveTitle=(newTitle, oldTitle)->
    #console.log newTitle
    #console.log oldTitle
    params={}
    params.poLink = {}
    params.personId = parseInt($state.params.id)
    params.newTitleAndOccupation = newTitle.titleOccId
    #setting preferredRole
    if oldTitle.preferredRole
      params.preferredRole = "1"
    else
      params.preferredRole = "0"
    #first title added automatucally is preferred role
    if $scope.firstTitle
      params.preferredRole = "1"
    #distinguish two cases of new title and modified title
    if oldTitle.titleOcc
      #console.log params
      $scope.setPoLink(oldTitle, params)
      params.oldTitleAndOccupation = oldTitle.titleOccId
      bioPeopleFactory.editPersonTitles(params).then (resp)->
    else
      #console.log params
      $scope.setPoLink(oldTitle, params)
      bioPeopleFactory.addPersonTitles(params).then (resp)->
    oldTitle.editing=false
    newTitle.editing=false
    $scope.titleToEdit.editing=false
    initModule()
    $state.reload()

  $scope.editTitle=(title)->
    angular.forEach($scope.titles, (singleTitle)->
      singleTitle.editing=false
      )
    title.editing=true
    $scope.titleToEdit=angular.copy(title)

  $scope.deleteTitle=(title)->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = $filter('translate')("modal.DELETINGTITLE")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        params={}
        params.personId=parseInt($state.params.id)
        params.titleAndOccupationToBeDeleted=title.titleOccId
        bioPeopleFactory.deletePersonTitles(params).then (resp)->
          initModule()
        $scope.titleToEdit.editing=false

  $scope.addTitle=()->
    newTitle = {}
    $scope.titles.push(newTitle)
    $scope.editTitle(newTitle)

  ##################
  # Person Routines
  ##################

  #In order to have only one mother and one father this function
  #manage different boolean attributes only on frontend
  $scope.setExistingParents=(parents)->
    $scope.existingFather = false
    $scope.existingMother = false
    $scope.existingGenders = {}
    $scope.existingGenders.M = false
    $scope.existingGenders.F = false
    $scope.genders[0].active = false
    $scope.genders[1].active = false
    for parent in parents
      $scope.existingGenders[parent.gender] = true
    if $scope.existingGenders.M
      $scope.existingFather = true
      $scope.genders[0].active = true
    if $scope.existingGenders.F
      $scope.existingMother = true
      $scope.genders[1].active = true

  $scope.cancelPerson=(person)->
    person.editing=false
    initModule()

  $scope.savePerson = (passed, person)->
    params={}
    params.documentId = $state.params.documentEntityId
    params.category = $scope.documentEntity.category
    params.type = passed.role.type.replace(/\s/g, "")
    if person.id
      params.idPeopleToDelete = person.id
    params.newPeople=passed.temp
    modDeFactory.editPerson(params).then (resp)->
      if resp.status is 'ok'
        getDocumentPeople()
        person.editPerson = false
    initModule()
    $state.reload()


  # Parents
  #This function helps the directive mod-parent
  # which is watching the parent gender to close the
  # the search window when user modify the choice of
  #parent to add
  $scope.editGender=(parent)->
    if parent.gender == "M"
      parent.gender = "F"
    else
      parent.gender = "M"

  $scope.editParent=(parent)->
    $scope.parentToEdit={}
    angular.forEach($scope.parents, (singleParent)->
      singleParent.editing=false
      )
    parent.editing=true
    $scope.parentToEdit=angular.copy(parent)

  #the following 3 functions are used in frontend in order
  # to distinguish the 3 cases
  $scope.addParent=()->
    newParent={}
    newParent.gender='F'
    $scope.parents.push(newParent)
    $scope.editParent(newParent)

  $scope.addFather=()->
    newParent={}
    newParent.gender='M'
    $scope.parents.push(newParent)
    $scope.editParent(newParent)

  $scope.addMother=()->
    newParent={}
    newParent.gender='F'
    $scope.parents.push(newParent)
    $scope.editParent(newParent)

  $scope.deleteParent=(parent)->
    params={}
    params.personId=parseInt($state.params.id)
    params.parentToBeDeleted=parent.parentId
    bioPeopleFactory.deletePersonParents(params).then (resp)->
      initModule()

  $scope.saveParent=(newParent, oldParent)->
    otherParent=undefined
    params={}
    params.personId = parseInt($state.params.id)
    if oldParent.parentId
      angular.forEach($scope.parents, (parent)->
        if parent.parentId isnt oldParent.parentId
          otherParent=parent.parentId
          )
      if oldParent.role is "Father"
        params.oldFatherId = oldParent.parentId
        params.newFatherId = newParent.id
        params.oldMotherId = otherParent
        params.newMotherId = otherParent
      else
        params.oldMotherId = oldParent.parentId
        params.newMotherId = newParent.id
        params.oldFatherId = otherParent
        params.newFatherId = otherParent
      bioPeopleFactory.editPersonParents(params).then (resp)->
    else
      if newParent.gender is "M"
        params.newFatherId=newParent.id
      else
        params.newMotherId=newParent.id
      bioPeopleFactory.addPersonParents(params).then (resp)->
    oldParent.editing=false
    initModule()
    $state.reload()

  # $scope.openParent = (person)->
  #   params = {}
  #   params.id = $scope.parent.parentId
  #   console.log($scope.parent.parentId)
    #$state.go 'mia.bio-people', params

   # Children
  $scope.saveChild=(newChild, oldChild)->
    alreadyPresent = false
    angular.forEach($scope.children, (singleChild)->
      if singleChild.id is newChild.temp.id
#        console.log singleChild
        alreadyPresent = true
      )
    if not alreadyPresent
      params={}
      params.personId = parseInt($state.params.id)
      params.newChildId=newChild.temp.id
      if oldChild.id
        params.oldChildId=oldChild.id
        bioPeopleFactory.editPersonChildren(params).then (resp)->
      else
        bioPeopleFactory.addPersonChildren(params).then (resp)->
      oldChild.editing=false
      initModule()
      $state.reload()
    else
      pageTitle = $filter('translate')("general.ERROR")
      messagePage = $filter('translate')("people.ALREADYIN")
      modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

  $scope.editChild=(child)->
    $scope.childToEdit={}
    angular.forEach($scope.children, (singleChild)->
      singleChild.editing=false
      )
    child.editing=true
    $scope.childToEdit=angular.copy(child)

  $scope.deleteChild=(child)->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = $filter('translate')("modal.DELETINGCHILD")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        params={}
        params.personId = parseInt($state.params.id)
        params.childToBeDeleted = child.id
        bioPeopleFactory.deletePersonChildren(params).then (resp)->
          initModule()

  $scope.addChild=()->
    newChild={}
    newChild.id=""
    $scope.children.push(newChild)
    $scope.editChild(newChild)

  # Spouses
  $scope.setSpouseGender=(gender)->
    if gender == "M"
      $scope.spousesJson[0].active = true
      $scope.spousesJson[1].active = false
    else
      $scope.spousesJson[1].active = true
      $scope.spousesJson[0].active = false

  $scope.editSpouse=(spouse)->
    $scope.spouseToEdit={}
    angular.forEach($scope.spouses, (singleSpouse)->
      singleSpouse.editing=false
      )
    spouse.editing=true
    $scope.spouseToEdit=angular.copy(spouse)

  $scope.deleteSpouse = (spouse)->
    params={}
    params.personId=parseInt($state.params.id)
    params.spouseToBeDeleted=spouse.personId
    bioPeopleFactory.deletePersonSpouses(params).then (resp)->
      initModule()

  $scope.addSpouse = ()->
    newSpouse={}
    newSpouse.personId=''
    if $scope.personData.gender == 'M'
      # newSpouse.spouseType='w'
    else
      # newSpouse.spouseType='h'
    $scope.spouses.push(newSpouse)
    $scope.editSpouse(newSpouse)

  $scope.saveSpouse=(newSpouse, oldSpouse)->
#    console.log newSpouse
    alreadyPresent = false
    angular.forEach($scope.spouses, (spouse)->
#      console.log spouse
#      console.log (spouse.personId is newSpouse.temp.id)
      if spouse.personId is newSpouse.temp.id
        alreadyPresent = true
      )
    if not alreadyPresent
      #console.log oldSpouse
      params={}
      params.personId = parseInt($state.params.id)
      params.newSpouse={}
      params.newSpouse.personId = newSpouse.temp.id
      params.newSpouse.spouseType = oldSpouse.spouseType
      if oldSpouse.personId
        #console.log "edit spouse"
        params.oldSpouse={}
        params.oldSpouse.personId=oldSpouse.personId
        params.oldSpouse.spouseType=oldSpouse.spouseType
        bioPeopleFactory.editPersonSpouses(params).then (resp)->
          #console.log resp
      else
        #console.log "new spouse"
        bioPeopleFactory.addPersonSpouses(params).then (resp)->
          #console.log resp
      #console.log params
      oldSpouse.editing=false
      initModule()
      # $state.reload()
    else
      pageTitle = $filter('translate')("general.ERROR")
      messagePage = $filter('translate')("people.ALREADYIN")
      modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

  $scope.hideAddBtnIfEdditing = (itemsArray) ->
    if itemsArray
      isSomeEditing = !itemsArray.some (spouse) -> spouse.editing == true
      return isSomeEditing
    return true
    
  ##################
  # Places Routines
  ##################
  $scope.saveBornPlace = (selectedPlace, previousPlace)->
    deFactory.getPlaceById(selectedPlace.temp.id).then (resp)->
      $scope.bornPlace={}
      $scope.bornPlace.id=selectedPlace.temp.id
      $scope.bornPlace.feData=resp.data

  $scope.saveDeathPlace = (selectedPlace, previousPlace)->
    deFactory.getPlaceById(selectedPlace.temp.id).then (resp)->
      $scope.deathPlace={}
      $scope.deathPlace.id=selectedPlace.temp.id
      $scope.deathPlace.feData=resp.data

  $scope.onChangeBornPlace = (placeName) ->
    if placeName == null or placeName == ''
      $scope.bornPlace = null

  $scope.onChangeDeathPlace = (placeName) ->
    if placeName == null or placeName == ''
      $scope.deathPlace = null

  ##################
  # Rel Doc Routines
  ##################

  $scope.selectDocs=(docType)->
    if $scope.selctedDocuments
      $scope.showDocDetails=false
      $scope.showDocDetails=true
      $scope.docToShow = $scope.selctedDocuments.filter((singleDoc)->
        return singleDoc.category is docType.name
        )

  $scope.openDoc=(row)->
#    console.log row
    params = {}
    #params.docId = row.entity.documentId
    params.docId = row.documentId
    $state.go 'mia.modifyDocEnt', params


  ##############################
  # Main Body
  ##############################

  $scope.realDelete = () ->
    if $scope.$stateParams.id
      csFactory.checkExistsInCs('BIO', $scope.$stateParams.id).then (resp) ->
        if resp.status == 'success' && resp.data.length
          messagePage = 'This biographical record is used in a Project. Are you sure you want to delete? If you deleted it people in the Project will not be able to see it anymore'
          deleteWarning(messagePage)
        else
          messagePage = $filter('translate')("personDoc.BODYPRE")
          deleteWarning(messagePage)

  deleteWarning = (messagePage) ->
    pageTitle = $filter('translate')("personDoc.TITLEPRE")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        bioPeopleFactory.deletePerson($scope.$stateParams.id).then (resp)->
#            console.log resp
          if resp.data and resp.data.length
            modalFactory.openModal(
              templateUrl: 'modules/directives/modal-templates/modal-person-doc/person-doc.html'
              controller:'modalIstanceController'
              resolve:{
                options:
                  -> {'docIds': resp.data}
                }
            ).then (resp) ->
          if resp.status is "w"
            pageTitle = $filter('translate')("personDoc.TITLEFAILED")
            messagePage = $filter('translate')("personDoc.BODYFAILAUTH")
            modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->
              console.log('done')
          if resp.status is "ok"
#              console.log ('persona cancellata')
            alertify.alert 'Person successfully deleted'
            $state.go('mia.welcome')

  $scope.deletePerson = ()->
    if $scope.$stateParams.id
      pageTitle = $filter('translate')("people.WARNING")
      messagePage = $filter('translate')("people.DELETING")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
        if resp
#          console.log 'delete confirmed'
          personId=$scope.$stateParams.id
          bioPeopleFactory.deletePerson(personId).then (resp)->
            console.log resp
        else
          console.log "Action Deleted"

  $scope.dates = (numb) ->
    selectMonth = ""
    angular.forEach($scope.months, (month) ->
      if month.id == numb
        selectMonth = month.name
    )
    selectMonth
    
  initModule=()->
    $scope.oldDataSummary={}
    $scope.oldDataPersonal={}
    if not $scope.$stateParams.id
      $scope.switchAllPanels()
      $scope.switchEditSummary()
    else
      personId=$scope.$stateParams.id
      $scope.personDataTable=[]
      bioPeopleFactory.getPerson(personId).then (resp)->
        bioPeopleFactory.recordPersonAction($scope.$stateParams.id, 'VIEW')
        $scope.personData=resp.data
#        console.log($scope.personData)
        $scope.setSpouseGender($scope.personData.gender)
        $scope.personId=$scope.personData.peopleId
        angular.forEach(resp.data, (value, key)->
          $scope.personDataTable.push({'propertyName':key, 'propertyValue':value})
          )
        if $scope.personData.bornPlaceId
          deFactory.getPlaceById($scope.personData.bornPlaceId).then (resp)->
            $scope.bornPlace={}
            $scope.bornPlace.id=$scope.personData.bornPlaceId
            $scope.bornPlace.feData=resp.data
        if $scope.personData.deathPlaceId
          deFactory.getPlaceById($scope.personData.deathPlaceId).then (resp)->
            $scope.deathPlace={}
            $scope.deathPlace.id=$scope.personData.deathPlaceId
            $scope.deathPlace.feData=resp.data
        bioPeopleFactory.getPersonPortraitDetails(personId).then (resp)->
          $scope.personPortraitDetails=resp.data
        bioPeopleFactory.getPersonName(personId).then (resp)->
          $scope.names=resp.data.personNames
        bioPeopleFactory.getPersonTitles(personId).then (resp)->
          $scope.initializeTitles(resp.data.tileOcc)
          $scope.preferredRole = resp.data.preferredRole
          $scope.setPreferredRole()
        bioPeopleFactory.getPersonParents(personId).then (resp)->
          if resp.data
            $scope.parents=resp.data.parents
            $scope.setExistingParents($scope.parents)
          else
            $scope.parents=[]
            $scope.setExistingParents($scope.parents)
          angular.forEach($scope.parents, (parent) ->
            if parent.gender is 'M'
              parent.role = 'Father'
            else
              parent.role = 'Mother'
            bioPeopleFactory.getPerson(parent.parentId).then (resp)->
              parent.name = resp.data.mapNameLf
            )
        bioPeopleFactory.getPersonChildren(personId).then (resp)->
          $scope.children=[]
          angular.forEach(resp.data.children, (child)->
            bioPeopleFactory.getPerson(child).then (resp)->
              temp={}
              temp.id=child
              temp.name = resp.data.mapNameLf
              $scope.children.push(temp)
            )
        bioPeopleFactory.getPersonSpouses(personId).then (resp) ->
          if resp.data
            $scope.spouses = resp.data.spouses
          else
            $scope.spouses = []
          angular.forEach($scope.spouses, (spouse) ->
            if spouse.spouseType is 'w'
              spouse.type='Wife'
            else
              spouse.type='Husband'
            bioPeopleFactory.getPerson(spouse.personId).then (resp) ->
              spouse.name = resp.data.mapNameLf
            )
        updateHeadquartersName(personId)

        # TODO: To be implemented
        bioPeopleFactory.countDoc(personId).then (resp)->
          $scope.totalDocNum=resp.data
          # console.log(resp.data)
        bioPeopleFactory.findDocPeople(personId).then (resp)->
          $scope.selectdDocuments=resp.data

        # Old do not use.
        # bioPeopleFactory.countCatFields(personId).then (resp)->
        #   $scope.roleInDoc=[]
        #   if resp.data
        #     _.map(resp.data.countDocumentFields, (value, key)->
        #       temp={}
        #       temp.name=key
        #       temp.count=0
        #       temp.docType=[]
        #       _.map(value, (count, role)->
        #         temp.count=temp.count+count
        #         temp2={}
        #         temp2.role=role
        #         temp2.count=count
        #         temp.docType.push(temp2)
        #         )
        #       $scope.roleInDoc.push(temp)
        #       )
  initModule()

  $scope.gotoComments = () ->
    $location.hash('comments')
    $anchorScroll()
  
  $scope.goToPublicProfile = (user) ->
    $state.go('mia.publicProfile', { 'account': user  })

  $scope.addToCS = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-to-cs/add-to-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {
            'recordType': 'BIO',
            'recordId':   $scope.id
          }
      }
    )

  $scope.findDocsRealtedToPerson = () ->
    searchDe()

  searchDe = () ->
    deRelatedToPerson = [{
        people:     [ $scope.id ],
        isActiveFilter: if $scope.id == 0 then false else true,
        type: 'peopleAdvancedSearch'
        searchSection:  'peopleSearch'
      }]

    $scope.searchArray = deRelatedToPerson
    
    searchDeService.getResults($scope.searchArray).then((resp) ->
      newResult = {}
      newResult.searchName = $scope.personData.mapNameLf
      newResult.type = 'doc_advanced'
      newResult.results = resp.data
      newResult.searchArray = prepareSearchArray($scope.searchArray[0])
      newResult.searchForm = createSearchForm($scope.personData)
      newResult.content = {
        searchType: 'Document Search',
        where: '',
        title: 'Document Search',
        countResult: $scope.totalDocNum
      }

      $rootScope.$broadcast('new-result', newResult))

  # To build correctly the result box of the right-sidebar a searchArray is needed with the right elements
  # in specific positions
  prepareSearchArray = (peopleSearch) ->
    searchArray = [
      { searchSection: "archivalLocationSearch", type: "archivalLocationAdvancedSearch", isActiveFilter: false },
      { searchSection: "categoryAndTypologySearch", type: "categoryAndTypologyAdvancedSearch", isActiveFilter: false },
      { searchSection: "transcriptionSearch", type: "transcriptionAdvancedSearch", isActiveFilter: false },
      { searchSection: "synopsisSearch", type: "synopsisAdvancedSearch", isActiveFilter: false },
      { searchSection: "placesSearch", type: "placesAdvancedSearch", isActiveFilter: false },
      peopleSearch,
      { searchSection: "topicsSearch", type: "topicsAdvancedSearch", isActiveFilter: false },
      { searchSection: "dateSearch", type: "dateAdvancedSearch", isActiveFilter: false }
      { searchSection: "documentOwnerSearch", type: "documentOwnerAdvancedSearch", isActiveFilter: false }
      { searchSection: "languagesSearch", type: "languagesAdvancedSearch", isActiveFilter: false }
    ]

    return searchArray

  createSearchForm = (personData) ->
    searchForm = {
      'people' : {
        searchSection: "peopleSearch",
        type: "peopleAdvancedSearch",
        isActiveFilter: true,
        # People elements inside searchForm.people is not perfectly equal to personData
        # so i recreate the object to match searchForm's people
        people: [
          {
            id:           personData.peopleId,
            unsure:       personData.unsure,
            mapNameLf:    personData.mapNameLf
            gender:       personData.gender,
            activeStart:  personData.activeStart,
            activeEnd:    personData.activeEnd,
            bornYear:     personData.bornYear,
            deathYear:    personData.deathYear,
            firstName:    personData.firstName,
            lastName:     personData.lastName,
            lastPrefix:   personData.lastPrefix,
            sucNum:       personData.sucNum
          }
        ]
      }
    }

    return searchForm

