bioPeopleServiceApp = angular.module 'bioPeopleServiceApp',[]

bioPeopleServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
bioPeopleServiceApp.factory 'bioPeopleFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV, $filter) ->
  bioPeopleBasePath = ENV.bioPeopleBasePath
  peopleBasePath = ENV.peopleBasePath
  docPath = ENV.documentBasePath
  bioDocBasePath = ENV.bioDocBasePath
  advancedSearchDeBasePath = ENV.advancedSearchDeBasePath

  factory =
    #-----------------------
    # Biographical services
    #-----------------------
    getPerson: (personId) ->
      return $http.get(bioPeopleBasePath+'findBiographicalPeople/'+personId).then (resp) ->
        resp.data
    
    getPersonPortraitDetails: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonPotraitDetails/'+personId).then (resp) ->
        resp.data

    editPerson: (person) ->
      return $http.post(bioPeopleBasePath+'modifyBiographicalPeople/', person).then (resp) ->
        resp.data

    deletePerson:(personId)->
      return $http.get(bioPeopleBasePath+'deletePerson/'+personId).then (resp) ->
        resp.data
    
    undeletePerson:(personId)->
      return $http.get(bioPeopleBasePath+'undeletePerson/'+personId).then (resp) ->
#        console.log(resp)
        resp.data

    #-----------------------
    # Count Doc Related to Person record services --
    #-----------------------

    countDoc: (personId) ->
      deRelatedToPerson = [{
        people:     [ personId ],
        isActiveFilter: if personId == 0 then false else true,
        type: 'peopleAdvancedSearch'
        searchSection:  'peopleSearch'
      }]

      deRelatedToPersonJSON=$filter("json")(deRelatedToPerson)

      return $http.post(advancedSearchDeBasePath+'partialRecordCount/', deRelatedToPerson).then (resp) ->
        resp.data
        # console.log(resp.data.data)
        return resp.data

    findDocPeople: (personId) ->
      deRelatedToPerson = [{
        people:     [ personId ],
        isActiveFilter: if personId == 0 then false else true,
        type: 'peopleAdvancedSearch'
        searchSection:  'peopleSearch'
      }]

      deRelatedToPersonJSON=$filter("json")(deRelatedToPerson)

      return $http.post(advancedSearchDeBasePath+'advancedSearchResults/0/20/docYear/asc', deRelatedToPerson).then (resp) ->
        resp.data

      # Request URL: http://localhost:2222/Mia/json/de/advancedsearch/advancedSearchResults/0/20/docYear/asc

    # --> Old services to do not use
    # countDoc: (personId) ->
    #   return $http.get(bioDocBasePath+'countAllDocumentsPeople/'+personId).then (resp) ->
    #     resp.data
    # findDocPeople: (personId) ->
    #   return $http.get(bioDocBasePath+'findDocumentsPeople/'+personId).then (resp) ->
    #     resp.data
    # countCatFields: (personId) ->
    #   return $http.get(bioDocBasePath+'countCategoryAndFieldsDocumentsPeople/'+personId).then (resp) ->
    #     resp.data


    #-------
    # Names
    #-------
    getPersonName: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonNames/'+personId).then (resp) ->
        resp.data
    editPersonName: (personId, name, oldName) ->
      delete oldName.edit
      params={}
      params.personId=personId
      params.oldPersonName=oldName
      params.newPersonName={}
      params.newPersonName.altNameId = name.altNameId
      params.newPersonName.altName = name.altName
      params.newPersonName.nameType = name.nameType
      return $http.post(bioPeopleBasePath+'modifyPersonName/', params).then (resp) ->
        resp.data

    editPortrait: (personId, portraitAuthor, portraitSubject) ->
      params = {}
      params.personId = personId
      params.portraitAuthor = portraitAuthor
      params.portraitSubject = portraitSubject
      return $http.post(bioPeopleBasePath+'modifyPersonPortraitDetails/', params).then (resp) ->
        resp.data

    uploadPortrait: (params) ->
      data = {
        "filesForm": { params },
        "personId": personId
      }
      return $http.post(bioPeopleBasePath+'uploadPortraitUser/', data).then (resp) ->
        resp.data

    datatoBlob: (dataurl, name, origSize, fileType) ->
      arr = dataurl.split(',')
      mime = arr[0].match(/:(.*?);/)[1]
      bstr = atob(arr[1])
      n = bstr.length
      u8arr = new Uint8Array(n)
      while n--
        u8arr[n] = bstr.charCodeAt(n)
      blob = new (window.Blob)([ u8arr ], type: fileType)
      blob.name = name
      blob.$ngfOrigSize = origSize
      blob

    addPersonName: (personId, name) ->
      params={}
      params.personId=personId
      params.newPersonName={}
      params.newPersonName.altName = name.altName
      params.newPersonName.nameType = name.nameType
      return $http.post(bioPeopleBasePath+'addPersonName/', params).then (resp) ->
        resp.data

    deletePersonName: (personId, name) ->
      delete name.edit
      params={}
      params.personId=personId
      params.altNameToBeDeleted=name.altNameId
      return $http.post(bioPeopleBasePath+'deletePersonName/', params).then (resp) ->
        resp.data

    #--------
    # Titles
    #--------
    getPeopleTitles: (queryStr)->
      return $http.get(docPath+'findTitleAndOccupation/'+queryStr).then (resp) ->
        resp.data

    getPersonTitles: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonTitlesAndOccupations/'+personId).then (resp) ->
        resp.data
    editPersonTitles: (params) ->
      return $http.post(bioPeopleBasePath+'modifyPersonTitleAndOccupation/', params).then (resp) ->
        resp.data
    addPersonTitles: (params) ->
      #console.log bioPeopleBasePath+'addPersonTitleAndOccupation/', params
      return $http.post(bioPeopleBasePath+'addPersonTitleAndOccupation/', params).then (resp) ->
        resp.data
    deletePersonTitles: (title) ->
      return $http.post(bioPeopleBasePath+'deletePersonTitleAndOccupation/', title).then (resp) ->
        resp.data
    #---------
    # Parents
    #---------
    getPersonParents: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonParents/'+personId).then (resp) ->
        resp.data
    editPersonParents: (parent) ->
      return $http.post(bioPeopleBasePath+'modifyPersonParents/', parent).then (resp) ->
        resp.data
    addPersonParents: (parent) ->
      return $http.post(bioPeopleBasePath+'addPersonParents/', parent).then (resp) ->
        resp.data
    deletePersonParents: (parent) ->
      return $http.post(bioPeopleBasePath+'deletePersonParents/', parent).then (resp) ->
        resp.data

    #----------
    # Children
    #----------
    getPersonChildren: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonChildren/'+personId).then (resp) ->
        resp.data
    editPersonChildren: (child) ->
      return $http.post(bioPeopleBasePath+'modifyPersonChild/', child).then (resp) ->
        resp.data
    addPersonChildren: (child) ->
      return $http.post(bioPeopleBasePath+'addPersonChild/', child).then (resp) ->
        resp.data
    deletePersonChildren: (child) ->
      return $http.post(bioPeopleBasePath+'deletePersonChild/', child).then (resp) ->
        resp.data

    #----------
    # Spouses
    #----------
    getPersonSpouses: (personId) ->
      return $http.get(bioPeopleBasePath+'findPersonSpouses/'+personId).then (resp) ->
        resp.data
    editPersonSpouses: (spouse) ->
#      console.log bioPeopleBasePath+'modifyPersonSpouse/', spouse
      return $http.post(bioPeopleBasePath+'modifyPersonSpouse/', spouse).then (resp) ->
        resp.data
    addPersonSpouses: (spouse) ->
      return $http.post(bioPeopleBasePath+'addPersonSpouse/', spouse).then (resp) ->
        resp.data
    deletePersonSpouses: (spouse) ->
      return $http.post(bioPeopleBasePath+'deletePersonSpouse/', spouse).then (resp) ->
        resp.data

    #------------
    # Headquarter
    #------------
    getHeadquarters: (personId) ->
      return $http.get(bioPeopleBasePath+'findHeadquarters/' + personId).then (resp) ->
        resp.data
    addHeadquarter: (headquarter) ->
      return $http.post(bioPeopleBasePath+'addHeadquarter/', headquarter).then (resp) ->
        resp.data
    deleteHeadquarter: (headQId) ->
      params = {}
      params.headquarterId = headQId
      return $http.post(bioPeopleBasePath+'deleteHeadquarter/', params).then (resp) ->
        resp.data

    recordPersonAction: (personId, type) ->
      personInfo = {
        "recordType": "BIO"
        "entryId": Number(personId)
        "action": type
      }
#      console.log personInfo
      return $rootScope.$broadcast('recordPersonAction', personInfo)
    
  return factory
