docentitiesApp = angular.module 'docentitiesApp',[
    'miaApp'
    'translatorApp'
    'commonServiceApp'
    'ui.router'
    'modalUtils'
    'ui.bootstrap'
    'ngFileUpload'
    'ngDraggable'
    'ladda'
    'ajoslin.promise-tracker'
  ]

docTranscription.filter 'replaceTagsTransMyDocs', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<newsHeader>/g, '[')
    text = text.replace(/<\/newsHeader>/g, '')
    text = text.replace(/<newsFrom>/g, '[')
    text = text.replace(/<\/newsFrom>/g, '')
    text = text.replace(/<from>/g, 'News From: ')
    text = text.replace(/<\/from>/g, '')
    if text.indexOf('fromUnsure') >= 0
      text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/fromUnsure>/g, '')
    text = text.replace(/<hub>/g, 'News Hub: ')
    text = text.replace(/<\/hub>/g, '')
    if text.indexOf('hubUnsure') >= 0
      text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/hubUnsure>/g, '')
    text = text.replace(/<date>/g, ' - Date: ')
    text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
    text = text.replace(/<\/date>/g, ']')
    text = text.replace(/<plTransit>/g, '[Place of Transit: ')
    text = text.replace(/<\/plTransit>/g, ']')
    text = text.replace(/<plTransitDate>/g, '- Date: ')
    text = text.replace(/<\/plTransitDate>/g, '')
    text = text.replace(/<transc>/g, '')
    text = text.replace(/<\/transc>/g, '')
    text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
    text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
    text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
    #line required for each occurence except the first one
    text = text.replace(/<position.*?<\/position>/g, '')
    # text = text.replace(/(\r\n|\n|\r)/, "")
    text

docTranscription.filter 'replaceTagsSynMyDocs', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<syn>/g, '')
    text = text.replace(/<\/syn>/g, '')
    text = text.replace(/<wordCount>/g, '[Avviso word count: ')
    text = text.replace(/<\/wordCount>/g, ']')
    text = text.replace(/<writtenPagesNo>/g, '[Written pages number: ')
    text = text.replace(/<\/writtenPagesNo>/g, ']')
    #console.log(text)
    text
    
docentitiesApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.docentities',
        url: '/document-entities?page',
        reloadOnSearch: false,
        views:
          'content':
            controller: 'docentitiesController'
            templateUrl: 'modules/views/doc-entities/doc-entities.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

docentitiesApp.controller 'docentitiesController', ($scope, $rootScope, deFactory, $state, userServiceFactory, $location, $timeout) ->
  $rootScope.pageTitle = "My Documents"
  $scope.documentEnts = []
  $scope.pagination   = {
    currentPage: 1,
    totalItems: 1,
    maxSize: 5,
    perPage: 5
  }

  $scope.loadingDocuments = false

  initParams = () ->
    $scope.pagination.currentPage = $scope.$stateParams.page || 1
    $location.search('page', $scope.pagination.currentPage)
  
#  userServiceFactory.getMe().then (resp)->
#    if resp.status is 'ok'
#      $scope.me=resp.data[0]

  getDocuments = () ->
    $scope.loadingDocuments = true
    deFactory.getMyDocuments($scope.pagination.currentPage, $scope.pagination.perPage).then (resp) ->
      $scope.loadingDocuments = false
      if !resp.data?
        $scope.noDocumentsFound = true
      else
        $scope.noDocumentsFound = false
        notDeletedDocs = _.filter(resp.data.documentEntities, { 'flgLogicalDelete': 0 })
        $scope.documentEnts = notDeletedDocs
        $scope.pagination.totalItems = resp.count

  initData = () ->
    initParams()
    $timeout -> getDocuments()

  initData()

  $scope.pageChange = () ->
    $location.search('page', $scope.pagination.currentPage)
    document.body.scrollTop = document.documentElement.scrollTop = 0 # because reloadOnSearch: false
    getDocuments()

  $scope.selectDe = (doc)->
    $state.go('mia.modifyDocEnt', { 'docId':doc })
