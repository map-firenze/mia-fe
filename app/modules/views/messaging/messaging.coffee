mailApp = angular.module 'mailApp', []

mailApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.mailbox',
        url: '/messaging?recipient'
        views:
          'content':
            controller: 'mailboxController'
            templateUrl: 'modules/views/messaging/messaging.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

mailApp.controller 'mailboxController', (commonsFactory, messagingFactory, searchDeService, $scope, $sce, $timeout, $compile, $rootScope, $state, alertify) ->
  $rootScope.pageTitle = "Mailbox"
  $scope.tabs = [{name: "inbox", value: true, active: true}, {name: "outbox", value: false, active:false}, {name: "compose", value: false, active: false}]
  $scope.msgTabs = []
  $scope.composeMessageData = {}
  $scope.selectedMsg = {}
  $scope.messageBody = {recipient: '', subject: '', msgText: ''}
  $scope.currentPages = {currentPageInbox: 1, currentPageOutbox: 1}
  $scope.recipient = ""
  $scope.subject = ""
  $scope.msgText = ""
  $scope.accountSelected = false
  $scope.elementsPerPage = 10
  $scope.currentAccount = JSON.parse(localStorage.getItem('miaMe'))
  $scope.sendMsgStatus = ""
  $scope.isAllInboxSelected = false
  $scope.isAllOutboxSelected = false
  $scope.noMessages = false
  $scope.loading = false
  $scope.userAccounts = []

  alertify.logPosition("bottom right")

  if $scope.$stateParams.recipient && $scope.$stateParams.recipient.length
    $scope.composeMessageData.to = $scope.$stateParams.recipient
    $scope.messageBody.recipient = $scope.$stateParams.recipient
    $scope.accountSelected = true
    _.forEach $scope.tabs, (tab) ->
      tab.value  = false
      tab.active = false
      if tab.name == "compose"
        tab.value  = true
        tab.active = true

  $scope.getUserAccount = (searchQuery) ->
    $scope.accountSelected = false
    if searchQuery.length == 0
      $scope.loading      = false
      $scope.userAccounts = []
    else if searchQuery.length >= 3
      $scope.loading = true
      searchDeService.getUserByName(searchQuery).then (resp)->
        $scope.loading = false
        if resp.length
          $scope.userAccounts = resp
        else
          $scope.account = null
  
  $scope.inboxPageChanged = ()->
#    console.log($scope.currentPages.currentPageInbox)
    firstNumberInbox = ($scope.currentPages.currentPageInbox - 1) * $scope.elementsPerPage + 1
    lastNumberInbox = $scope.currentPages.currentPageInbox * $scope.elementsPerPage + 1
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
  #    messagingFactory.getInboxMessages(firstNumberInbox, lastNumberInbox).then (resp)->
      messagingFactory.getInboxMessages(account, $scope.currentPages.currentPageInbox).then (resp)->
  #      console.log(resp.data)
        $scope.inboxMessages = resp.data.messages

  $scope.outboxPageChanged = ()->
#    console.log($scope.currentPages.currentPageOutbox)
    firstNumberOutbox = ($scope.currentPages.currentPageOutbox - 1) * $scope.elementsPerPage + 1
    lastNumberOutbox = $scope.currentPages.currentPageOutbox * $scope.elementsPerPage + 1
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
  #    messagingFactory.getOutboxMessages(firstNumberOutbox, lastNumberOutbox).then (resp)->
      messagingFactory.getOutboxMessages(account, $scope.currentPages.currentPageOutbox).then (resp)->
  #      console.log(resp.data)
        $scope.outboxMessages = resp.data.messages

  $scope.addAccount = (acc) ->
    console.log('addAccount', acc)
    $scope.composeMessageData.to = JSON.parse(JSON.stringify(acc.account))
    $scope.messageBody.recipient = acc.account
    $scope.accountSelected = true
  
  $scope.sendMessage = () ->
    return if !$scope.accountSelected || !$scope.messageBody.recipient.length
    $scope.composeMessageData.to = $scope.messageBody.recipient
    $scope.composeMessageData.messageSubject = $scope.messageBody.subject
    $scope.composeMessageData.messageText = $scope.messageBody.msgText
    $scope.composeMessageData.account = $scope.currentAccount.data[0].account
    messagingFactory.sendMessage($scope.composeMessageData).then (resp) ->
      if resp.status == 'ok'
        alertify.alert "Your message has been sent!"
        $scope.messageBody.recipient = ''
        $scope.messageBody.subject = ''
        $scope.messageBody.msgText = ''
        $scope.tabs[0].active = true
        $state.go('.', {recipient: undefined}, {notify: false}) if $scope.$stateParams.recipient && $scope.$stateParams.recipient.length

  $scope.selectMessage = (msg) ->
    $scope.selectedMsg = JSON.parse(JSON.stringify(msg))
    $scope.msgTabs.forEach((el) ->
      el.active = false)
    for i in [0...$scope.msgTabs.length]
      if $scope.msgTabs[i].messageId == $scope.selectedMsg.messageId
        $scope.msgTabs[i].active = true
        return
    $scope.selectedMsg.active = true
    $scope.msgTabs.push($scope.selectedMsg)
#    console.log($scope.selectedMsg)
    $scope.getSelectedMsg()

  $scope.closeThisMessage = (selectedMsg) ->
    index = $scope.msgTabs.indexOf(selectedMsg)
    if index > -1
      $scope.msgTabs.splice(index, 1)

  $scope.getSelectedMsg = () ->
    msgId = $scope.selectedMsg.messageId
    messagingFactory.getSelectedMessage(msgId).then (resp) ->
      $scope.selectedMsg.from = resp.data.from
      $scope.selectedMsg.messageSubject = resp.data.messageSubject
      $scope.selectedMsg.messageText = resp.data.messageText

  $scope.replyMessage = (msgTab) ->
    $scope.tabs[2].active = true
    if msgTab.from
      $scope.messageBody.recipient = msgTab.from
    else
      $scope.messageBody.recipient = msgTab.to
    $scope.accountSelected = true
    $scope.messageBody.subject = 'RE: ' + msgTab.messageSubject
    $scope.messageBody.msgText = "#{msgTab.sentDate}\n#{msgTab.messageText}"

  $scope.deleteMessage = (msgTab) ->
    msgId = msgTab.messageId
    messagingFactory.deleteMessage(msgId).then (resp) ->
#      console.log(resp)
      if resp.status == 'ok'
        $scope.inboxInit()
        $scope.outboxInit()
    $scope.closeMessage(msgTab)

  $scope.closeAllMessages = () ->
    $scope.msgTabs = []
    $scope.tabs[0].active = true

  $scope.closeThisMsg = (event, selectedMsgTab) ->
    event.stopPropagation()
    event.preventDefault()
    index = $scope.msgTabs.indexOf(selectedMsgTab)
    if index > -1
      $scope.msgTabs.splice(index, 1)
    if $scope.msgTabs.length
      $scope.msgTabs[Math.max(0, index - 1)].active = true
    else
      $scope.tabs[0].active = true
  
  $scope.changeTab = (idx) ->
    # if I'm on compose tab and I'm navigating to another tab
    if $scope.tabs[2].active == true && idx != 2
      $scope.closeComposeMessage(idx)
    else if $scope.tabs[0].active == true && idx == 0
      $scope.inboxInit()
    else
      $scope.tabs[idx].active = true

  $scope.closeComposeMessage = (idx) ->
    if $scope.messageBody.recipient ||  $scope.messageBody.subject || $scope.messageBody.msgText
      alertify.confirm( 'If you close this you will lose your message. Continue?', (->
        $scope.messageBody.recipient = ''
        $scope.messageBody.subject = ''
        $scope.messageBody.msgText = ''
        $scope.tabs[idx].active = true
        $scope.$apply()
        ))
    else
      $scope.tabs[idx].active = true

  $scope.closeMessage = (msgTab) ->
    index = $scope.msgTabs.indexOf(msgTab)
    if index > -1
      $scope.msgTabs.splice(index, 1)
    if $scope.msgTabs.length
      $scope.msgTabs[Math.max(0, index - 1)].active = true
    else
      $scope.tabs[0].active = true
  
  $scope.selectAllInboxMessages = () ->
    $scope.isAllInboxSelected = !$scope.isAllInboxSelected
    angular.forEach($scope.inboxMessages, (el) ->
      el.selected = $scope.isAllInboxSelected)

  $scope.inboxMsgSelected = () ->
    $scope.isAllInboxSelected = $scope.inboxMessages.every((el) ->
      el.selected)

  $scope.selectAllOutboxMessages = () ->
    $scope.isAllOutboxSelected = !$scope.isAllOutboxSelected
    angular.forEach($scope.outboxMessages, (el) ->
      el.selected = $scope.isAllOutboxSelected)

  $scope.outboxMsgSelected = () ->
    $scope.isAllOutboxSelected = $scope.outboxMessages.every((el) ->
      el.selected)

  $scope.deleteSelectedInboxMessages = () ->
    $scope.checkedInboxBoxes = $scope.getCheckedInbox()
    if !$scope.checkedInboxBoxes.length
      alertify.alert("You did not selected any messages to delete")
    else
      alertify.confirm("Are you sure you want to delete selected messages?", () ->
#        console.log('OK', $scope.checkedInboxBoxes)
        messagingFactory.multipleMessagesDelete($scope.checkedInboxBoxes).then (resp) ->
          if resp.status == 'ok'
            $scope.inboxInit()
            $state.reload()
      )
  
  $scope.deleteSelectedOutboxMessages = () ->
    $scope.checkedOutboxBoxes = $scope.getCheckedOutbox()
    if !$scope.checkedOutboxBoxes.length
      alertify.alert("You did not selected any messages to delete")
    else
      alertify.confirm("Are you sure you want to delete selected messages?", () ->
#        console.log('OK', $scope.checkedOutboxBoxes)
        messagingFactory.multipleMessagesDelete($scope.checkedOutboxBoxes).then (resp) ->
          if resp.status == 'ok'
            $scope.outboxInit()
            $state.reload()
      )
  
  # $scope.setSelectedInboxMessagesRead = () ->
  #   alertify.confirm("Are you sure you want to mark selected messages as read?", () ->
  #     # messagingFactory.setRead()
  #     console.log('OK')
  #   )

  $scope.getCheckedInbox = () ->
    inboxChecksChecked = []
    for i in [0...$scope.inboxMessages.length]
      if ($scope.inboxMessages[i].selected)
        inboxChecksChecked.push($scope.inboxMessages[i].messageId)
    return inboxChecksChecked

  $scope.getCheckedOutbox = () ->
    outboxChecksChecked = []
    for i in [0...$scope.outboxMessages.length]
      if ($scope.outboxMessages[i].selected)
        outboxChecksChecked.push($scope.outboxMessages[i].messageId)
    return outboxChecksChecked
  
  $scope.inboxInit = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      messagingFactory.getInboxMessages(account, $scope.currentPages.currentPageInbox).then (resp)->
        if resp.status == 'ok' && resp.data?
  #        console.log(resp.data)
          $scope.inboxMessages = resp.data.messages
          $scope.inboxMessages.forEach((el) ->
            el.selected = false)
          $scope.totalInboxMsgCount = resp.data.messTotalCount
        else
          $scope.inboxMessages = []
        if resp.data == null
          $scope.loading = false
          $scope.noInboxMessages = true

  $scope.outboxInit = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      messagingFactory.getOutboxMessages(account, $scope.currentPages.currentPageOutbox).then (resp)->
        if resp.status == 'ok' && resp.data?
  #        console.log(resp.data)
          $scope.outboxMessages = resp.data.messages
          $scope.outboxMessages.forEach((el) ->
            el.selected = false)
          $scope.totalOutboxMsgCount = resp.data.messTotalCount
        else
          $scope.outboxMessages = []
        if resp.data == null
          $scope.loading = false
          $scope.noOutboxMessages = true

  $scope.$watch('tabs[0].active', (newVal)->
    if newVal == true
      $scope.inboxInit()
  )

  $scope.$watch('tabs[1].active', (newVal)->
    if newVal == true
      $scope.outboxInit()
  )

  $scope.trustHtmlMessage = (text)->
    $sce.trustAsHtml(text)

  $scope.notifyCorrectShare = (accountTo, sender, url, shareType, mapDocId, docName)->
    $scope.composeMessageData.to = accountTo
    $scope.composeMessageData.messageSubject = "Your request has been accepted"
    if shareType == "document"
      $scope.composeMessageData.messageText = "<br/><strong>#{sender}</strong> has shared <strong>#{shareType}</strong> (mapDocId:#{mapDocId}) with you.<br /><br/><a href=\"#{url}\">CLICK HERE</a> to access it."
    else
      $scope.composeMessageData.messageText = "<br/><strong>#{sender}</strong> has shared <strong>#{shareType}</strong> with you.<br/><br/><a href=\"#{url}\">CLICK HERE</a> to access it."

    $scope.composeMessageData.account = $scope.currentAccount.data[0].account
    messagingFactory.sendMessage($scope.composeMessageData).then (resp) ->
      if resp.status == 'ok'
        #alertify.success("Your message has been sent!")
        alertify.alert "Document shared successfully with " + accountTo, ()->
        alertify.error 'Ok'

sendApprovalRequest = (url, account, sender, urlFile, shareType, mapDocId, docName)->
  xhr = new XMLHttpRequest()
  xhr.open("POST", url, true)
  xhr.setRequestHeader("Content-Type", "application/json")
  xhr.onreadystatechange = ()->
    if this.readyState == XMLHttpRequest.DONE && this.status == 200
      angular.element(document.getElementById('ui-view')).scope().notifyCorrectShare(account, sender, urlFile, shareType, mapDocId, docName)

  xhr.send(JSON.stringify([account]))
