messagingApp = angular.module 'messagingApp', []

messagingApp.factory 'messagingFactory', ($http, $timeout) ->

  factory =
#    getMessagesCountInbox: () ->
#      return $http.get('json/messaging/getInboxMessages/'+account+'/0/1').then (resp)->
#        resp.data
#    getMessagesCountOutbox: () ->
#      return $http.get('json/messaging/getOutboxMessages/'+account+'/0/1').then (resp)->
#        resp.data
    getInboxMessages: (account, page, perPage=10) ->
#      return $http.get('json/messaging/getInboxMessages/'+account+'/'+firstNumber+'/'+lastNumber).then (resp)->
      return $http.get('json/messaging/getInboxMessages/'+account, {params: {page: page, perPage: perPage}}).then (resp)->
        resp.data
    getOutboxMessages: (account, page, perPage=10) ->
#      return $http.get('json/messaging/getOutboxMessages/'+account+'/'+firstNum+'/'+lastNum).then (resp)->
      return $http.get('json/messaging/getOutboxMessages/'+account, {params: {page: page, perPage: perPage}}).then (resp)->
        resp.data
    sendMessage: (msg) ->
      data = JSON.stringify(msg)
      return $http.post('json/messaging/send', data).then (resp)->
        resp.data
    sendMessageToAll: (msg) ->
      data = JSON.stringify(msg)
      return $http.post('json/messaging/sendToAll', data).then (resp)->
        resp.data
    deleteMessage: (msgId) ->
      return $http.get('json/messaging/delete/'+msgId).then (resp)->
        resp.data
    multipleMessagesDelete: (msgIds) ->
      idsList = {messagesId: msgIds}
      return $http.post('json/messaging/deleteMultipleMessages/', idsList).then (resp)->
        resp.data
    getSelectedMessage: (msgId) ->
      return $http.get('json/messaging/getMessage/'+msgId).then (resp)->
        resp.data
    # setRead: (msgId) ->
    #   return $http.get('json/messaging/setRead/'+msgId).then (resp)->
    #     resp.data

  return factory