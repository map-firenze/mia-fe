docAttCollApp = angular.module 'docAttCollApp', [
  'ui.bootstrap'
#    'ajoslin.promise-tracker'
#    'modPeople'
#    'modPlace'
#    'modTopic'
#    'biblioRef'
#    'docAeInfo'
#    'docSysInfo'
#    'docSummary'
#    'docTranscription'
#    'docSynopsis'
#    'deRelatedPeople'
#    'deSinglePlace'
#    'addCollationApp'
]

docAttCollApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider

      .state 'mia.docAttColl',
#        url: '/doc-att-coll/:collId?parentId&childId'
        url: '/doc-att-coll/:collId'
        views:
          'content':
            controller: 'docAttCollController'
            templateUrl: 'modules/views/doc-att-collation/doc-att-collation.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

docAttCollApp.controller 'docAttCollController', (deFactory, modDeFactory, $scope, $sce, $rootScope, $http, $state, $stateParams, $filter, modalFactory, commonsFactory, $window, $timeout, alertify) ->
  $rootScope.pageTitle = 'DOCUMENT RECONSTRUCTION'

#  console.log('-----$stateParams------', $scope.$stateParams)

  $scope.collation = undefined
  $scope.parentDe  = undefined
  $scope.childDe   = undefined

  $scope.parCurrentImageOrFileId = null
  $scope.chCurrentImageOrFileId  = null
  $scope.parCurrentFolios = []
  $scope.chCurrentFolios  = []

  $scope.months = [
    {'name': '',           'id': 0}
    {'name': 'January',    'id': 1},
    {'name': 'February',   'id': 2},
    {'name': 'March',      'id': 3},
    {'name': 'April',      'id': 4},
    {'name': 'May',        'id': 5},
    {'name': 'June',       'id': 6},
    {'name': 'July',       'id': 7},
    {'name': 'August',     'id': 8},
    {'name': 'September',  'id': 9},
    {'name': 'October',    'id': 10},
    {'name': 'November',   'id': 11},
    {'name': 'December',   'id': 12}
  ]

  initParentDocument = () ->
    #############################
    # Prepare parent DE
    #############################
    $scope.parentDe = $scope.collation.parentDocument
    $scope.parentCategory = $scope.parentDe.category.replace(/([A-Z])/g, ' $1').replace(/^./, (str)-> return str.toUpperCase())

    $scope.parLastShownImageIndex = if $scope.parentDe.uploadFiles.length <= 6 then $scope.parentDe.uploadFiles.length else 6

    angular.forEach($scope.parentDe.uploadFiles, (uploadFile) ->
      angular.forEach(uploadFile.folios, (singleFolio) ->
        singleFolio.transcriptionOrder = uploadFile.imageOrder
        $scope.parCurrentFolios.push(singleFolio)
      )
    )
    $scope.parCurrentFolios.sort(compareTranscriptions)

    if $scope.parentDe.uploadFiles[0].uploadFileId
      selectImage($scope.parentDe.uploadFiles[0], 'parent')
      $scope.updateViewer($scope.parentDe.uploadFiles[0], 'parent')
      $scope.parViewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.parentDe.uploadFiles[0].uploadFileId)
    else
      $scope.parViewerAddress = $sce.trustAsResourceUrl('')

    modDeFactory.findDocumentPeople($scope.parentDe.documentId, $scope.parentDe.category.replace(/\s/g, "")).then (resp)->
      $scope.parentPeoples = resp.data.arrayPeople

      angular.forEach($scope.parentPeoples, (role)->
        angular.forEach(role.peoples, (person)->
          deFactory.getPeopleById(person.id).then (resp)->
            person.feData = resp.data
        )
      )

    # Note: with parallel loading previewer may work not correctly
    $timeout ->
      initChildDocument()
    , 500

  initChildDocument = () ->
    #############################
    # Prepare child DE
    #############################
    $scope.childDe = $scope.collation.childDocument
    $scope.clildCategory = $scope.childDe.category.replace(/([A-Z])/g, ' $1').replace(/^./, (str)-> return str.toUpperCase())

    $scope.chLastShownImageIndex = if $scope.childDe.uploadFiles.length <= 6 then $scope.childDe.uploadFiles.length else 6

    angular.forEach($scope.childDe.uploadFiles, (uploadFile) ->
      angular.forEach(uploadFile.folios, (singleFolio) ->
        singleFolio.transcriptionOrder = uploadFile.imageOrder
        $scope.chCurrentFolios.push(singleFolio)
      )
    )
    $scope.chCurrentFolios.sort(compareTranscriptions)

    if $scope.childDe.uploadFiles[0].uploadFileId
      selectImage($scope.childDe.uploadFiles[0], 'child')
      $scope.updateViewer($scope.childDe.uploadFiles[0], 'child')
      $scope.chViewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.childDe.uploadFiles[0].uploadFileId)
    else
      $scope.chViewerAddress = $sce.trustAsResourceUrl('')

    modDeFactory.findDocumentPeople($scope.childDe.documentId, $scope.childDe.category.replace(/\s/g, "")).then (resp)->
      $scope.clildPeoples = resp.data.arrayPeople

      angular.forEach($scope.clildPeoples, (role)->
        angular.forEach(role.peoples, (person)->
          deFactory.getPeopleById(person.id).then (resp)->
            person.feData = resp.data
        )
      )

  initCollation = () ->
#    console.log('initCollation')
    $http.get('json/collation/attachment/' + $scope.$stateParams.collId).then (resp) ->
#      console.log(resp.data.data)
      data = resp.data.data
      return unless data
      return if data.type == 'LACUNA'
      $scope.collation = data
      initParentDocument()
#      initChildDocument()

  initCollation()

  $scope.deleteCollation = () ->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = 'Are you sure you want to delete this Reconstruction?  (This does not delete the individual documents)'
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        $http.delete('json/collation/' + $scope.collation.id).then (resp) ->
          $state.go 'mia.welcome'

  compareTranscriptions = (currentEl, nextEl) ->
    if Number(currentEl.transcriptionOrder) < Number(nextEl.transcriptionOrder)
      return -1
    if Number(currentEl.transcriptionOrder) > Number(nextEl.transcriptionOrder)
      return 1
    return 0

#  extractDomain = (url) ->
#    domain = undefined
#    if url.indexOf('://') > -1
#      domain = url.split('/')[2]
#    else
#      domain = url.split('/')[0]
#    domain='http://'+domain
#
#  $scope.openTranscriptor = () ->
#    workUrl = $state.href($state.current.name, $state.params, {absolute: true})
#    transWindow=$window.open(extractDomain(workUrl)+'/Mia/json/src/ShowDocumentInManuscriptViewer/show?documentEntityId='+$scope.parentDe.documentId+'&showTranscription=true&fileId=' + $scope.currentImageOrFileId)
#    transWindow.onbeforeunload=()->
#      $timeout(reloadPage, 1000)
#
#  reloadPage = () ->
#    $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})



#  $scope.$on('deTitle-edited', (evt, args)->
#    $scope.basic.deTitle = args
#    $rootScope.deTitle = args
#    saveBasic()
#  )
#
#  #panels and graphic variables
#  $scope.allClosed = true
#  $scope.switchAllPanels = () ->
#    $scope.allClosed = !$scope.allClosed
#    for section in $scope.sections
#      section.open = !$scope.allClosed
#
#  #page sections
#  $scope.sections = []
#  $scope.sections.push({"name":"Transcription", "open":false})
#  $scope.sections.push({"name":"Document Details", "open":false})
#  $scope.sections.push({"name":"People", "open":false})
#  $scope.sections.push({"name":"Places", "open":false})
#  $scope.sections.push({"name":"Topics", "open":false})
#
  #edition bool
#  $scope.editingTranscription = false
  #Pagination
#  $scope.limit = 0
#  $scope.changeLimit = (quantity) ->
#    temp = $scope.limit + quantity
#    $scope.lastShownImageIndex = temp + 6
#    if temp >= 0 and temp + 6 <= $scope.parentDe.uploadFiles.length
#      $scope.limit = temp

#  $scope.$on('focus', (evt, args) ->
#    if !$scope.editingTranscription
#      imageFound = _.find($scope.parentDe.uploadFiles, (pic) ->
#        pic.uploadFileId is parseInt(args))
#      if imageFound
#        indexImage = _.indexOf($scope.parentDe.uploadFiles, imageFound)
#        if indexImage < 6
#          $scope.limit = 0
#        else
#          $scope.limit = indexImage - 5
#        $scope.lastShownImageIndex = indexImage + 1
#        $scope.updateViewer(imageFound)
#    else
#      $rootScope.$broadcast('editApprove', false)
#      $scope.messageAlert = $filter('translate')("editDE.TRANSCLOSETEXTAREAMESSAGECHANGEIMAGE")
#      alertify.alert $scope.messageAlert
#  )
#
#  $rootScope.$on('editingTranscription', (evnt, args) ->
#    $scope.editingTranscription = args
#  )
#
  $scope.updateViewer = (image, type) ->
    if type == 'parent'
      $scope.parCurrentImageOrFileId = null
      angular.forEach($scope.parCurrentFolios, (singleFolio) ->
        if singleFolio.uploadFileId == image.uploadFileId
          $rootScope.$broadcast('uploadIdMatch', singleFolio.uploadFileId)
      )
      if image.uploadFileId?
        $scope.parCurrentImageOrFileId = image.uploadFileId
      else
        $scope.parCurrentImageOrFileId = image.uploadedFileId

      $scope.parViewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/" + $scope.parCurrentImageOrFileId)
      selectImage(image, type)
      $rootScope.$emit('imageSelected', image)
    else
      $scope.chCurrentImageOrFileId = null
      angular.forEach($scope.chCurrentFolios, (singleFolio) ->
        if singleFolio.uploadFileId == image.uploadFileId
          $rootScope.$broadcast('uploadIdMatch', singleFolio.uploadFileId)
      )
      if image.uploadFileId?
        $scope.chCurrentImageOrFileId = image.uploadFileId
      else
        $scope.chCurrentImageOrFileId = image.uploadedFileId

      $scope.chViewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/" + $scope.chCurrentImageOrFileId)
      selectImage(image, type)
      $rootScope.$emit('imageSelected', image)

#
#  $rootScope.$on('editCurrentTranscription', (evt, data) ->
#    $scope.updateViewer(data)
#  )
#
  selectImage = (image, type) ->
    if type == 'parent'
      actual = _.find $scope.parentDe.uploadFiles, (pic) -> pic.highlighted is true
    else
      actual = _.find $scope.childDe.uploadFiles, (pic) -> pic.highlighted is true
#    $rootScope.$broadcast('editApprove', true)
    if actual
      actual.highlighted = false
    image.highlighted = true
#
#  $scope.addPicToDe = () ->
#    modalFactory.openModal(
#      templateUrl: 'modules/directives/modal-templates/modal-edit-folio/edit-folio.html'
#      controller:'modalIstanceController'
#      resolve:{
#        options:
#          -> {"uploadFileId": $scope.documentEntity.uploadFiles[0].uploadFileId, "uploadId":$scope.documentData.uploadId, "documentEntityId":$scope.documentEntity.documentId }
#      }
#    ).then (documentEntity) ->
#      params = {}
#      params.documentId = documentEntity.documentId
#      params.modifyImagesInDe = []
#      for image in documentEntity.uploadFiles
#        params.modifyImagesInDe.push(image.uploadFileId)
#      modDeFactory.editDocImages(params).then (resp) ->
#        if (resp.status is 'ok')
#          $state.reload()
#
#  #----------------------------------------
#  # Edit Transcription
#  #----------------------------------------
#  $scope.transcriptions = []
#  transWindow=undefined
#
#  reloadPage=()->
#    $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})
#
#  extractDomain = (url) ->
#    domain = undefined
#    if url.indexOf('://') > -1
#      domain = url.split('/')[2]
#    else
#      domain = url.split('/')[0]
#    domain='http://'+domain
#
#  $scope.openTranscriptor= () ->
#    workUrl = $state.href($state.current.name, $state.params, {absolute: true})
#    transWindow=$window.open(extractDomain(workUrl)+'/Mia/json/src/ShowDocumentInManuscriptViewer/show?documentEntityId='+$scope.documentEntity.documentId+'&showTranscription=true&fileId=' + $scope.currentImageOrFileId)
#    transWindow.onbeforeunload=()->
#      $timeout(reloadPage, 1000)
#
#  $scope.synopsis = null
#  #----------------------------------------
#  # Bibliographical References
#  #----------------------------------------
#  $scope.biblioRef = []
#  $scope.canAddBiblio = true
#
#  getBiblioRef = () ->
#    modDeFactory.getBiblioRef($state.params.docId).then (resp)->
#      if resp.status is 'ok'
#        $scope.biblioRef = resp.data.arrayBiblio.biblios
#
#  $scope.addNewBiblioRef = () ->
#    temp = {}
#    temp.biblioId = null
#    temp.name = ""
#    temp.link = ""
#    $scope.biblioRef.push(temp)
#
#  $scope.cancelBiblioRef = (biblio)->
#    $scope.biblioRef.splice($scope.biblioRef.indexOf(biblio),1)
#
#  $scope.$on('editing-biblio', (event, args)->
#    $scope.canAddBiblio = false
#    )
#
#  $scope.$on('editing-biblio-end', (event, args)->
#    $scope.canAddBiblio = true
#    )
#
#  #----------------------------------------
#  # Utilities
#  #----------------------------------------
#  findFieldTypeInDoc = (fieldList)->
#    correctFields = []
#    fields = _.keys($scope.documentEntity)
#    for field in fields
#      for elem in fieldList
#        if elem.beName is field
#          for singleField in $scope.documentEntity[field]
#            singleField.realName = elem.name
#          correctFields.push($scope.documentEntity[field])
#    return correctFields
#
#  #----------------------------------------
#  # Main body
#  #----------------------------------------
#
#  #fields
#  getDocumentFields = () ->
#    deFactory.getDocumentFields($scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
#      fieldsToBePresent=resp.data.fields
#      for field in fieldsToBePresent
#        field.openPanel= false
#        if field.type is "text"
#          $scope.textFields.push(field)
#      $scope.documentEntity.text = findFieldTypeInDoc($scope.textFields)
#
#  compareTranscriptions = (currentEl, nextEl) ->
#    if Number(currentEl.transcriptionOrder) < Number(nextEl.transcriptionOrder)
#      return -1
#    if Number(currentEl.transcriptionOrder) > Number(nextEl.transcriptionOrder)
#      return 1
#    return 0
#
#  getTranscription = () ->
#    modDeFactory.getTranscription($scope.$stateParams.docId).then (resp)->
#      actualTranscriptions = []
#      if resp.status
#        #$scope.transcriptions = resp.data.transcriptions or "No transcription inserted yet"
#        if resp.data != null
#          $scope.transcriptions = resp.data.transcriptions
#        else
#          $scope.transcriptions = []
#        $scope.documentEntity.uploadFiles.forEach((singleFile) ->
#          foundAssignedTranscription = -1
#          foundTranscription = false
#          $scope.transcriptions.forEach((singleTranscription, i) ->
#            if +singleTranscription.uploadedFileId == +singleFile.uploadFileId
#              singleTranscription.transcriptionOrder = singleFile.imageOrder
#              singleTranscription.folioNumber = singleFile.folios.map((folio) ->
#                if folio.rectoverso
#                  localRectoVerso = folio.rectoverso
#                else
#                  localRectoVerso = ''
#                if folio.folioNumber != null then folio.folioNumber + ' ' + localRectoVerso else 'Not Numbered'
#              ).join(" - ")
#              foundTranscription = true
#              foundAssignedTranscription = i
#              actualTranscriptions.push(singleTranscription)
#          )
#          if !foundTranscription
#            actualTranscriptions.splice(foundAssignedTranscription + 1, 0, {
#              folioNumber: singleFile.folios.map((folio) ->
#                if folio.rectoverso
#                  newLocalRectoVerso = folio.rectoverso
#                else
#                  newLocalRectoVerso = ''
#                if folio.folioNumber != null then folio.folioNumber + ' ' + newLocalRectoVerso else 'Not Numbered'
#              ).join(" - ")
#              documentId: singleFile.documentId || $scope.$stateParams.docId
#              transcriptionOrder: singleFile.imageOrder
#              transcription: ""
#              docTranscriptionId: null
#              uploadedFileId: singleFile.uploadFileId
#            })
#        )
#        $scope.transcriptions = actualTranscriptions
#        $scope.transcriptions.sort(compareTranscriptions)
#
#  getSynopsis = () ->
#    modDeFactory.getSynopsis($scope.$stateParams.docId).then (resp)->
#      if resp.status is "ok"
#        #$scope.synopsis = resp.data.synopsis.generalNotesSynopsis or "No synopsis added"
#        $scope.synopsis = resp.data.synopsis.generalNotesSynopsis
#
#  getBasicDesc = () ->
#    modDeFactory.getBasicDescription($scope.$stateParams.docId).then (resp)->
#      if resp.status is "ok"
#        $scope.basic = resp.data.basicDescription
#        $scope.basic.docYear= "" if $scope.basic.docYear is 0
#        $scope.basic.docDay= "" if $scope.basic.docDay is 0
#        $scope.basic.docModernYear= "" if $scope.basic.docModernYear is 0
#        $scope.basic.deTitle = "" if $scope.basic.deTitle is 0
#
#  initDe = () ->
#    $scope.textFields = []
#    fieldsToBePresent = []
#    $scope.typologies = []
#    $scope.currentFolios = []
#    $scope.documentEntity = {}
#    $scope.documentData = {}
#    $scope.allFolios = {}
#    #just for development TODO Delete
#    if not $scope.$stateParams.docId
#      $scope.$stateParams.docId = 1
#
#    if $scope.$stateParams.docId
#      deFactory.getDocumentEntityById($scope.$stateParams.docId).then (resp)->
#        deFactory.recordDocAction($scope.$stateParams.docId, 'VIEW')
#        $scope.documentEntity = resp.data.documentEntity
#        $scope.lastShownImageIndex = if $scope.documentEntity.uploadFiles.length <= 6 then $scope.documentEntity.uploadFiles.length else 6
#        $rootScope.pageTitle = "Document: "
#        $rootScope.deTitle = $scope.documentEntity.deTitle
#
#        $scope.getAllFolios = () ->
#          angular.forEach($scope.documentEntity.uploadFiles, (uploadFile) ->
#            angular.forEach(uploadFile.folios, (singleFolio) ->
#              singleFolio.transcriptionOrder = uploadFile.imageOrder
#              $scope.currentFolios.push(singleFolio)
#            )
#          )
#          $scope.currentFolios.sort(compareTranscriptions)
#
#        $scope.getAllFolios()
#
#        if $scope.documentEntity.uploadFiles[0].uploadFileId
#          selectImage($scope.documentEntity.uploadFiles[0])
#          $scope.updateViewer($scope.documentEntity.uploadFiles[0])
#          $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.documentEntity.uploadFiles[0].uploadFileId)
#        else
#          $scope.viewerAddress = $sce.trustAsResourceUrl('')
#        #----------------------------------------
#        # Upload
#        #----------------------------------------
#        commonsFactory.getMySingleuploadByFileId($scope.documentEntity.uploadFiles[0].uploadFileId,12,1).then (resp)->
#          $scope.documentData = resp.aentity
#        #----------------------------------------
#        # Typologies available
#        #----------------------------------------
#        modDeFactory.findDocTypologies($scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
#          $scope.typologies = resp.data.typologies
#
#        getDocumentPeople()
#        getDocumentPlace()
#        getDocumentTopics()
#        getRelDoc()
#        getBiblioRef()
#        getDocumentLanguages()
#        getAllLanguages()
#        getDocumentFields()
#        delete $scope.documentEntity.transcriptions
#        delete $scope.documentEntity.date
#        getTranscription()
#        getSynopsis()
#        getBasicDesc()
#  initDe()
