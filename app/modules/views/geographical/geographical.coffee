bioPlaceApp = angular.module 'bioPlaceApp',[
  'bioPlaceServiceApp'
  'singlePlace'
  'placeSysInfo'
  'delGeoApp'
  'actionsHistory'
  'commentsModule'
  'addToCsApp'
]


bioPlaceApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.bio-place',
        url: '/place/:id?scrollTo'
        reloadOnSearch: false
        views:
          'content':
            controller: 'bioPlaceController'
            templateUrl: 'modules/views/geographical/geographical.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'

]

bioPlaceApp.controller 'bioPlaceController', ($scope, $rootScope, $timeout, $state, bioPlaceFactory, $filter, modalFactory, alertify, $location, $anchorScroll, csFactory) ->
  $rootScope.pageTitle = "Place Record"
  $rootScope.deTitle = ""
  
  $scope.placeId = $state.params.id
  $scope.title = ""
  $scope.placeNameFull = ""
  $scope.placeType = ""

  ##############################
  # panels and graphic variables
  ##############################
  $scope.allClosed = true
  $scope.createPlaceMode = false

  $scope.addingNewVariant = false

  $scope.switchAllPanels = () ->
    $scope.allClosed = !$scope.allClosed
    for section in $scope.sections
      section.open = !$scope.allClosed

  #page sections
  $scope.sections = []
  $scope.sections.push({"name":"Summary", "open":true, "edit":false})
  $scope.sections.push({"name":"Details", "open":false, "edit":false})
  $scope.sections.push({"name":"Name and variants", "open":false, "edit":false})
  $scope.sections.push({"name":"Hierarchy", "open":false, "edit":false})

  $scope.switchEdit = (sectionToEdit)->
    sectionToEdit.edit=!sectionToEdit.edit

  $scope.saveDetails = (section)->
    $scope.switchEdit(section)

  $scope.switchEditDetails = (section)->
    $scope.switchEdit(section)

  ##############################
  # Summary and details
  ##############################

  $scope.switchEditSummary = () ->
    if ($state.params.id)
      temp.placeName = angular.copy($scope.placeData.placeName)
      temp.plType = angular.copy($scope.placeData.plType)
    $scope.sections[0].edit = true

  $scope.cancelSummary = () ->
    $scope.placeData.placeName = angular.copy(temp.placeName)
    $scope.placeData.plType = angular.copy(temp.plType)
    $scope.sections[0].edit = false

  $scope.saveSummary = () ->
    if $scope.$stateParams.id
      if temp.placeName != $scope.placeData.placeName
        $scope.placeData.placeNameFull = $scope.placeData.placeNameFull.replace(temp.placeName, $scope.placeData.placeName)
        $scope.placeData.plNameFullPlType = $scope.placeData.plNameFullPlType.replace(temp.placeName, $scope.placeData.placeName)

      params = {}
      params.placeId = $scope.placeData.placeId
      params.placeNameFull = $scope.placeData.placeNameFull
      params.plNameFullPlType = $scope.placeData.plNameFullPlType
      params.placeName = $scope.placeData.placeName
      params.termAccent = $scope.placeData.termAccent
      params.plType = $scope.placeData.plType
      params.prefFlag = $scope.placeData.prefFlag
      params.plParentPlaceAllId = $scope.placeData.plParentPlaceAllId
      params.plParent = $scope.placeData.plParent
      params.placesMemo = $scope.placeData.placesMemo
      params.dateCreated = $scope.placeData.dateCreated
      params.lastUpdate = $scope.placeData.lastUpdate
      params.createdBy = $scope.placeData.createdBy
      params.lastUpdateBy = $scope.placeData.lastUpdateBy

      $scope.title = angular.copy($scope.placeData.placeName)
      $scope.placeNameFull = angular.copy($scope.placeData.placeNameFull)
      $scope.placeType = angular.copy($scope.placeData.plType)

      bioPlaceFactory.modifyPlaceDetails($scope.$stateParams.id, params).then (resp) ->
#        bioPlaceFactory.recordPlaceAction($scope.$stateParams.id, 'EDIT')
        $scope.sections[0].edit = false
        $scope.sections[1].edit = false
        $state.go($state.current, {'id':$scope.$stateParams.id}, {reload: true})
    else
      params = {}
      params.placeName = $scope.placeData.placeName
      params.placeNameWithAccent = $scope.placeData.termAccent
      params.plType = $scope.placeData.plType
      params.prefFlag = 'P'
      params.placeParentId = $scope.placeData.plParentPlaceAllId
      params.placeNotes = $scope.placeData.placesMemo
      bioPlaceFactory.addNewPlace(params).then (resp) ->
#        bioPlaceFactory.recordPlaceAction($scope.$stateParams.id, 'CREATE')
#        console.log resp.placeId
        $state.go($state.current, {'id':resp.placeId}, {reload: true})

  $scope.switchDetails = () ->
    temp.placeName = angular.copy($scope.placeData.placeName)
    temp.plParent = angular.copy($scope.placeData.plParent)
    temp.placesMemo = angular.copy($scope.placeData.placesMemo)
    temp.plType = angular.copy($scope.placeData.plType)
#    console.log $scope.sections
    $scope.sections[1].edit = true

  $scope.cancelDetails = () ->
    $scope.placeData.plParent = temp.plParent
    $scope.placeData.placesMemo = temp.placesMemo
    $scope.placeData.plType = temp.plType
    $scope.sections[1].edit = false

  ##############################
  # Summary and details
  ##############################
  tempName = {}
  $scope.switchName=(givenIndex)->
    $scope.names[givenIndex].edit=!$scope.names[givenIndex].edit

  $scope.saveVariantName = (givenIndex)->
    $scope.addingNewVariant = false
    params={}
    params.newPlaceNameVariant={}
    params.newPlaceNameVariant.placeAllId = $scope.names[givenIndex].placeAllId
    params.newPlaceNameVariant.placeName = $scope.names[givenIndex].placeName
    if $scope.names[givenIndex].placeAllId
#      console.log params
      bioPlaceFactory.editPlaceNames(params).then (resp) ->
#        console.log resp
        $scope.names[givenIndex].edit=false
        $rootScope.variantSaved = true
        $state.reload()
    else
      params.placeAllId=$scope.$stateParams.id
      bioPlaceFactory.addPlaceNames(params).then (resp) ->
#        console.log resp
        $scope.names[givenIndex].edit=false
        $rootScope.variantSaved = true
        $state.reload()

  $scope.deleteVariantName = (givenIndex)->
    bioPlaceFactory.deletePlaceNames($scope.names[givenIndex].placeAllId).then (resp) ->
#      console.log resp
      $scope.names.splice(givenIndex, 1)
      $rootScope.variantSaved = false

  if $rootScope.variantSaved
    $scope.sections[2].open = true
  else
    $scope.sections[2].open = false

  $scope.addVariantName = ()->
    $scope.addingNewVariant = true

    newName={}
    newName.placeAllId=null
    newName.placeName=''
    newName.edit=true
    if (!$scope.names)
      $scope.names = []
    $scope.names.push(newName)

  $scope.switchEditName = (givenIndex)->
    tempName = angular.copy($scope.names[givenIndex])
    $scope.names[givenIndex].edit=true

  $scope.cancelEditName = (givenIndex)->
    $scope.addingNewVariant = false
    $scope.names.splice(givenIndex, 1)


  ##############################
  # Delete and Undelete
  ##############################

  $scope.deletePlace = ()->
    if $scope.$stateParams.id
      csFactory.checkExistsInCs('GEO', $scope.$stateParams.id).then (resp) ->
        if resp.status == 'success' && resp.data.length
          messagePage = 'This place record is used in a Project. Are you sure you want to delete? If you deleted it people in the Project will not be able to see it anymore'
          deleteWarning(messagePage)
        else
          messagePage = $filter('translate')("places.DELETING")
          deleteWarning(messagePage)

  deleteWarning = (messagePage) ->
    pageTitle = $filter('translate')("places.WARNING")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
#          console.log 'delete confirmed'
        placeId = $scope.$stateParams.id
        bioPlaceFactory.deletePlace(placeId).then (resp) ->
          if resp.status == 'ok'
            alertify.alert 'Place successfully deleted'
            $state.reload()
          else modalFactory.openModal({
            templateUrl: 'modules/directives/modal-templates/modal-del-geo/del-geo.html'
            controller: 'modalIstanceController'
            resolve: {
              options: -> { 'documents': resp.data } }
            }).then (resp) ->
      else
        document.getElementById("focusOffed").blur()
#          console.log "Action Deleted"

  $scope.undeletePlace = () ->
    placeId = $scope.$stateParams.id
    bioPlaceFactory.undeletePlace(placeId).then (resp) ->
      if resp.status == 'ok'
        alertify.alert 'Place successfully restored'
#        console.log resp
        $state.reload()
        
  ##############################
  # Parent
  ##############################
  $scope.saveParent = (passed, place)->
#    console.log(passed)
    bioPlaceFactory.findPlaceDetails(passed.temp.id).then (parent)->
#      console.log(parent)
      $scope.parent = parent.data
      $scope.placeData.plParent = parent.data.placeNameFull
      $scope.placeData.plParentPlaceAllId = passed.temp.id
      # if $scope.placeId
      #   $scope.sections[1].edit = false

  $scope.cancelNewParent = (passed, place)->
    $scope.sections[1].edit = false

  initNewPlace = () ->
    $scope.placeData = {}
    $scope.placeData.createdBy = ""
    $scope.placeData.dateCreated = ""
    $scope.placeData.lastUpdate = ""
    $scope.placeData.lastUpdateBy = ""
    $scope.placeData.logicalDelete = ""
    $scope.placeData.plNameFullPlType = ""
    $scope.placeData.plParent = ""
    $scope.placeData.plParentPlaceAllId = ""
    $scope.placeData.plType = ""
    $scope.placeData.placeId = ""
    $scope.placeData.placeName = ""
    $scope.placeData.placeNameFull = ""
    $scope.placeData.placesMemo = ""
    $scope.placeData.prefFlag = ""
    $scope.placeData.termAccent = ""
    $scope.placeData.placeNameWithAccent = ""
    $scope.placeData.placeParentId = ""
    $scope.placeData.unsure = ""
    $scope.switchAllPanels()
    $scope.switchEditSummary()
    $scope.sections[1].open = true
    $scope.sections[1].edit = true

  getPlace = () ->
    placeId=$scope.$stateParams.id
    $scope.placeDataTable=[]
    bioPlaceFactory.findPlaceDetails(placeId).then (placeDetails)->
      bioPlaceFactory.recordPlaceAction($scope.$stateParams.id, 'VIEW')
      $scope.placeData = placeDetails.data

      $scope.title = angular.copy($scope.placeData.placeName)
      $scope.placeNameFull = angular.copy($scope.placeData.placeNameFull)
      $scope.placeType = angular.copy($scope.placeData.plType)

      bioPlaceFactory.findPlaceDetails($scope.placeData.plParentPlaceAllId).then (parent)->
        $scope.parent = parent.data
      bioPlaceFactory.getPlaceNames(placeId).then (variant)->
        if variant.data
          $scope.names=variant.data.placeNameVariants
          for name in $scope.names
            name.edit=false
          for i in [0...$scope.names.length]
            if $scope.names[i].prefFlag == 'P'
              primalFlag = $scope.names.splice(i, 1)
              $scope.names.unshift(primalFlag[0])
              break
        else
          $scope.names = []
  
  getPlaceTypes = () ->
    bioPlaceFactory.getPlaceTypes().then (resp)->
      $scope.placeTypes = resp.data

  ##############################
  # Main Body
  ##############################
  temp = {}
  $scope.oldDataSummary={}
  $scope.oldDataPersonal={}
  getPlaceTypes()
  if not $scope.$stateParams.id
    initNewPlace()
  else
    getPlace()

  $scope.$on('createPlaceMode', (evt, data) -> $scope.createPlaceMode = true)

  $scope.gotoComments = () ->
    $location.hash('comments')
    $anchorScroll()

  $scope.addToCS = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-to-cs/add-to-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {
            'recordType': 'GEO',
            'recordId':   $scope.placeId
          }
      }
    )
