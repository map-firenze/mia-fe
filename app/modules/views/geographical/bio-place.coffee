bioPlaceServiceApp = angular.module 'bioPlaceServiceApp',[]

bioPlaceServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
bioPlaceServiceApp.factory 'bioPlaceFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  geoBasePath = ENV.geoBasePath
  documentBasePath = ENV.documentBasePath

  factory =
    #-----------------------------
    # Biographical place services
    #-----------------------------
    addNewPlace: (params) ->
      return $http.post(documentBasePath+'addNewPlace/', params).then (resp) ->
        resp.data

    deletePlace: (placeId) ->
      return $http.get(geoBasePath+'deletePlace/'+placeId).then (resp) ->
        resp.data
    
    undeletePlace: (placeId) ->
      return $http.get(geoBasePath+'undeletePlace/'+placeId).then (resp) ->
        resp.data

    findPlaceDetails: (placeId)->
      return $http.get(geoBasePath+'findGeographicalPlace/'+placeId).then (resp) ->
        resp.data

    modifyPlaceDetails: (placeId, params)->
      return $http.post(geoBasePath+'modifyGeographicalPlace/', params).then (resp) ->
        resp.data

    #-----------------------------
    # Names
    #-----------------------------
    getPlaceNames: (placeId)->
      return $http.get(geoBasePath+'findPlaceNameVariantsById/'+placeId).then (resp) ->
        resp.data

    editPlaceNames: (params)->
      return $http.post(geoBasePath+'modifyPlaceNameVariant/', params).then (resp) ->
        resp.data

    addPlaceNames: (params)->
      return $http.post(geoBasePath+'addPlaceNameVariant/', params).then (resp) ->
        resp.data

    deletePlaceNames: (placeId)->
      return $http.get(geoBasePath+'deletePlaceNameVariant/'+placeId).then (resp) ->
        resp.data

    getPlaceTypes: ()->
      return $http.get('json/document/findPlaceTypes').then (resp) ->
        resp.data

    recordPlaceAction: (placeId, type) ->
      placeInfo = {
        "recordType": "GEO"
        "entryId": Number(placeId)
        "action": type
      }
#      console.log placeInfo
      return $rootScope.$broadcast('recordPlaceAction', placeInfo)

  return factory
