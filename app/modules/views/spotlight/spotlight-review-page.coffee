spotlightReviewPage = angular.module 'spotlightReviewPage', ['ui.router', 'spotlightDownloadMaterial', 'spotlightRejectMotivation', 'spotlightModifyRequest', 'spotlightRejectPeerReview']

spotlightReviewPage.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.spotlispotlightReviewPageghtPage',
        url: '/document-entity/:docId/spotlight/:id/review'
        views:
          'content':
            controller: 'spotlightPageController'
            templateUrl: 'modules/views/spotlight/spotlight-page.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

spotlightReviewPage.controller 'spotlightReviewPageController', ($scope, $window, $http, $stateParams, modalFactory, $rootScope) ->

  $rootScope.pageTitle = "Review Submission"

  $http.get("json/discovery/find/"+$stateParams.id).then (resp) ->
    if resp.data.status == 'ok'
      $scope.errorMsg = undefined
      $scope.discovery = resp.data.data.discovery

      if $scope.discovery.status == "Published"
        $rootScope.pageTitle = "Publication"

      initLinks($scope.discovery)
    else
      console.log(resp.data)
      $scope.errorMsg = resp.data.message

  initLinks = (discovery) ->
    $scope.links = []
    for id in discovery.links
      currentLink = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+id
      $scope.links = $scope.links.concat([currentLink])

  $scope.downloadMaterialTobeSent = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-download-material-modal.html'
      controller:'modalIstanceController'
    )

  $scope.peerReviewReject = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-reject-peerReview.html'
      controller:'modalIstanceController'
    )

  $scope.publishAsDiscovery = () ->
    pageTitle = "Publish"
    messagePage = "Are you sure you want to publish this document?"
    modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (modalResponse) ->
      if modalResponse
        $http.put("json/discovery/"+$scope.discovery.id+"/publish/").then (resp) ->
          if resp.data.status == 'ok'
            $scope.errorMsg = undefined
            $scope.discovery = resp.data.data.discovery
            window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/discovery/' + $scope.discovery.id
          else
            $scope.errorMsg = resp.data.message

  # $scope.approve = () ->
  #   pageTitle = "Review completed"
  #   messagePage = ""
  #   modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (modalResponse) ->
  #     if modalResponse
  #       $http.put("json/discovery/"+$scope.discovery.id+"/approve/").then (resp) ->
  #         if resp.data.status == 'ok'
  #           $scope.errorMsg = undefined
  #           $scope.discovery = resp.data.data.discovery
  #         else
  #           $scope.errorMsg = resp.data.message
  $scope.approve = () ->
    $http.put("json/discovery/"+$scope.discovery.id+"/approve/").then (resp) ->
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        $scope.discovery = resp.data.data.discovery
      else
        $scope.errorMsg = resp.data.message

  $scope.reject = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-reject-motivation.html'
      controller:'modalIstanceController'
    )

  $scope.requestModify = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-modify-request.html'
      controller:'modalIstanceController'
    )

  $scope.getFileUrl = (file) ->
    return window.location.protocol + '//' + window.location.host + '/Mia/json/discovery/' + $scope.discovery.id + '/download/' + file.id

  
  $scope.deleteSpotlight = () ->
    pageTitle = "Delete"
    messagePage = "This will delete the submission. Are you sure you want to proceed?"
    modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (resp) ->
      if resp
        $http.delete('json/discovery/' + $scope.discovery.id).then (resp) ->
          if  resp.data.status == 'ok'
            $scope.errorMsg = undefined
            window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/welcome'
          else
            $scope.errorMsg = resp.data.message
