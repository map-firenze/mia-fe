spotlightDetailsPage = angular.module 'spotlightDetailsPage', ['ui.router', 'spotlightReviewPage']

spotlightDetailsPage.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.spotlightReviewPage',
        url: '/spotlight-review-staff/:id'
        views:
          'content':
            controller: 'spotlightReviewPageController'
            templateUrl: 'modules/views/spotlight/spotlight-review-page.html'
          'sidebar':
              controller: 'sidebarController'
              templateUrl: 'modules/sidebar/sidebar.html'

    $stateProvider
      .state 'mia.spotlightDiscovery',
        url: '/discovery/:id'
        views:
          'content':
            controller: 'spotlightReviewPageController'
            templateUrl: 'modules/views/spotlight/spotlight-review-page.html'
          'sidebar':
              controller: 'sidebarController'
              templateUrl: 'modules/sidebar/sidebar.html'
]

spotlightDetailsPage.controller 'spotlightDetailsPageController', ($scope, $http, $stateParams, modalFactory, ENV, $rootScope) ->

  $rootScope.pageTitle = "Submission for Review"

  $http.get("json/discovery/find/"+$stateParams.id).then (resp) ->
    if resp.data.status == 'ok'
      $scope.errorMsg = undefined
      $scope.discovery = resp.data.data.discovery
      initLinks($scope.discovery)
    else
      console.log(resp.data)
      $scope.errorMsg = resp.data.message
  
  initLinks = (discovery) ->
    $scope.links = []
    for id in discovery.links
      currentLink = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+id
      $scope.links = $scope.links.concat([currentLink])

  $scope.waitForPeerReview = () ->
    console.log("Waiting for peer review")

  $scope.deleteSpotlight = () ->
    pageTitle = "Delete Submission"
    if $rootScope.me.account == $scope.discovery.owner
      messagePage = "This action will delete your submission. Are you sure you want to proceed?"
    else
      messagePage = "This action will delete the user's submission. Are you sure you want to proceed?"
    modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (resp) ->
      if resp
        $http.delete('json/discovery/' + $scope.discovery.id).then (resp) ->
          if  resp.data.status == 'ok'
            $scope.errorMsg = undefined
            window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/welcome'
          else
            $scope.errorMsg = resp.data.message

  $scope.getFileUrl = (file) ->
    return window.location.protocol + '//' + window.location.host + '/Mia/json/discovery/' + $scope.discovery.id + '/download/' + file.id