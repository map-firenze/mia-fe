spotlightPage = angular.module 'spotlightPage', ['ui.router', 'spotlightDetailsPage', 'ngFileUpload']

spotlightPage.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.spotlightPage',
        url: '/document-entity/:docId/spotlight/:id'
        views:
          'content':
            controller: 'spotlightDetailsPageController'
            templateUrl: 'modules/views/spotlight/spotlight-details-page.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

spotlightPage.controller 'spotlightPageController', ($scope, $http, modalFactory, $stateParams, Upload, $rootScope) ->

  $scope.titleReadOnly = false
  $scope.motivationReadOnly = false
  $rootScope.pageTitle = "Create short notice for review"

  initLinks = (discovery) ->
    $scope.links = []
    for id in discovery.links
      currentLink = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+id
      $scope.links = $scope.links.concat([currentLink])

  initModule = ()  ->
    if $stateParams.id == undefined
      $http.get("json/discovery/default?documentId="+$stateParams.docId).then (resp) ->
        if resp.data.status == 'ok'
          $scope.errorMsg = undefined
          $scope.discovery = resp.data.data.discovery
          $scope.discovery.uploads = []
          $scope.discovery.links = []
          $scope.review = false
        else
          $scope.errorMsg = resp.data.message
    else
      $http.get("json/discovery/find/"+$stateParams.id).then (resp) ->
        if resp.data.status == 'ok'
          $scope.errorMsg = undefined
          $scope.discovery = resp.data.data.discovery
          $scope.titleReadOnly = true
          $scope.motivationReadOnly = true
          $scope.formData.motivationEdit = $scope.discovery.whyHistImp
          $scope.formData.titleEdit = $scope.discovery.title
          $scope.review = true
          initLinks($scope.discovery)
        else
          console.log(resp.data)
          $scope.errorMsg = resp.data.message

  initModule()
  
  $scope.saveTitle = () ->
    $scope.titleReadOnly = true
    $scope.discovery.title = $scope.formData.titleEdit
  
  $scope.cancelTitle = () ->
    if $scope.discovery.title != null
      $scope.titleReadOnly = true
      $scope.formData.titleEdit = $scope.discovery.title

  $scope.editTitle = () ->
    $scope.titleReadOnly = false

  $scope.saveMotivation = () ->
    $scope.motivationReadOnly = true
    $scope.discovery.whyHistImp = $scope.formData.motivationEdit

  $scope.cancelMotivation = () ->
    if $scope.discovery.whyHistImp != null
      $scope.motivationReadOnly = true
      $scope.formData.motivationEdit = $scope.discovery.whyHistImp

  $scope.editMotivation = () ->
    $scope.motivationReadOnly = false

  extractId = (link) ->
    start = link.lastIndexOf("/") + 1
    end = link.length
    return link.substring(start, end)

  $scope.addLink = (link) ->
    if $scope.links == undefined
      $scope.links = []
    
    if !isNaN(extractId(link)) && $scope.discovery.links.includes(parseInt(extractId(link))) == false
      $http.get("json/document/findDocument/"+extractId(link)).then (resp) ->
        if resp.data.status == 'ok'
          $scope.errorMsg = undefined
          $scope.discovery.links = $scope.discovery.links.concat([resp.data.data.documentEntity.documentId])
          $scope.links = $scope.links.concat([link])
        else
          $scope.errorMsg = resp.data.message

    $scope.link = undefined

  $scope.sendToPeerReview = (formData) ->
    pageTitle = "Send to Peer Review"
    messagePage = "Are you sure your submission is correct? You will not be able to edit after submission."
    modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (modalResponse) ->
      if modalResponse
        $scope.discovery.documentId = $stateParams.docId

        msg = "Send to Peer Review: Thank you for submitting a short notice for publication. It is now awaiting Peer Review. You will be notified within 3 to 5 weeks of the outcome. "
        if $scope.review == false
          $http.post("json/discovery/add", $scope.discovery).then (postResp) ->
            if postResp.data
              modalFactory.openMessageModal(pageTitle, msg, false, "md").then (modalResp2) ->
                if modalResp2 == false
                  window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+$stateParams.docId+'/spotlight/'+postResp.data.data.discovery.id
        else
          $http.put("json/discovery/"+$scope.discovery.id+"/edit/", $scope.discovery).then (postResp) ->
            if postResp.data
              modalFactory.openMessageModal(pageTitle, msg, false, "md").then (modalResp2) ->
                if modalResp2 == false
                  window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+$stateParams.docId+'/spotlight/'+postResp.data.data.discovery.id

  uploadFile = (file, desc) ->
    Upload.upload(
      url: 'json/discovery/upload'
      data:
        files: [file]).then ((response) ->
          response.data.data.upload.descriptiveTitle = desc
          $scope.discovery.uploads = $scope.discovery.uploads.concat(response.data.data.upload)
          $scope.file = null
        )

  $scope.chooseFile = () ->
    document.getElementById("spotlight-file-upload").click()

  $scope.addFile = () ->
    if $scope.file != undefined and $scope.file != null and !($scope.file.name in $scope.discovery.uploads.map (f) -> f.filename)
      selectedDesc='File for Spotlight'
      uploadFile($scope.file, selectedDesc)

  $scope.getFileUrl = (file) ->
    if $scope.discovery.id != null
      return window.location.protocol + '//' + window.location.host + '/Mia/json/discovery/' + $scope.discovery.id + '/download/' + file.id

  $scope.removeUpload = (idx, file) ->
    $scope.discovery.uploads[idx].deleted = true

  $scope.removeLink = (idx) ->
    $scope.links.splice(idx, 1)
    $scope.discovery.links.splice(idx, 1)

  initTrix = () ->
    Trix.config.textAttributes.underline = {
      style: { "textDecoration": "underline" },
      inheritable: true,
      parser: (element) ->
        style = window.getComputedStyle(element)
        return style.textDecoration == "underline"
    }
    
    buttonHTML = '<button type="button" class="underline" data-trix-attribute="underline" title="underline">U</button>'
    groupElement = Trix.config.toolbar.content.querySelector(".text_tools")
    groupElement.insertAdjacentHTML("beforeend", buttonHTML)

  initTrix()