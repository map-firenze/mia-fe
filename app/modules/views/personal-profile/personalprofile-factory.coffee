personalProfileServiceApp = angular.module 'personalProfileService',[]

personalProfileServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
]
adminServiceApp.factory 'personalProfileFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  personalProfileBasePath = ENV.personalProfileBasePath # _basePath = 'json/', personalProfileBasePath: _basePath + 'userprofile/'
  userBasePath = ENV.userBasePath                       # userBasePath: _basePath + 'user/'
  factory =

    #-------------------
    # personal profile services
    #-------------------
    getUser: () ->
      return $http.get(personalProfileBasePath+'findUser/').then (resp) ->
        resp.data
    modifyUser: (params) ->
      return $http.post(personalProfileBasePath+'modifyUserProfile/', params).then (resp) ->
        resp.data
    changePass: (params) ->
      return $http.post(personalProfileBasePath+'changeUserPassword/', params).then (resp) ->
        resp.data
    searchCountries: (searchQuery) ->
      return $http.get(personalProfileBasePath+'findCountries/'+searchQuery).then (resp) ->
        resp.data
    uploadPhoto: (params) ->
      return $http.post('json/userprofile/uploadPortraitUser', params).then (resp) ->
        resp.data
    uploadPhotoTest: (params) ->
      return $http.post('json/test/JsonTest/uploadTest/',params).then (resp) ->
        resp.data
    datatoBlob: (dataurl, name, origSize, fileType) ->
      arr = dataurl.split(',')
      mime = fileType || arr[0].match(/:(.*?);/)[1]
      bstr = atob(arr[1])
      n = bstr.length
      u8arr = new Uint8Array(n)
      while n--
        u8arr[n] = bstr.charCodeAt(n)
      blob = new (window.Blob)([ u8arr ], type: fileType)
      blob.name = name
      blob.$ngfOrigSize = origSize
      console.log(arr, mime, blob)
      blob

    #-------------------
    # public profile services
    #-------------------
    findMiaUser: (account) ->
      return $http.get(userBasePath + 'findMiaUser/' + account).then (resp) ->
        console.log('===findMiaUser==>>>>', resp)

  return factory
