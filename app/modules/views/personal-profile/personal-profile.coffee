personalApp = angular.module 'personalProfileApp',[
  'personalProfileService'
  'changePassApp'
  'modalUtils'
  'uploadPhotoApp'
  'uploadCVApp'
  'ngFileUpload'
  'translatorAppEn'
]


personalApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.personalprofile',
        url: '/personal-profile'
        views:
          'content':
            controller: 'personalController'
            templateUrl: 'modules/views/personal-profile/personal-profile.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

personalApp.controller 'personalController', (personalProfileFactory, modalFactory, $scope, $rootScope, $state, $window) ->
  $rootScope.pageTitle = "Personal Profile"
  $scope.titles = [{"label":"Mrs."},{"label":"Mr."},{"label":"Miss"},{"label":"Dr."},{"label":"Professor"}]
  $scope.editing = false
  $scope.searchTableOpesn = false
  $scope.loading= false
  $scope.searchQuery= ""
  $scope.countryError = false

  $scope.selectCountry= (item)->
    $scope.editUser.country = item.code
    $scope.editUser.countryName = item.name
    $scope.searchTableOpen = false
    $scope.countryError = false

  $scope.getCountries= ()->
    $scope.loading = true
    $scope.searchTableOpen = true
    if $scope.editUser.country?.length == 0
      $scope.searchTableOpen = false
      $scope.countryError = false
      $scope.loading = false
    else if $scope.editUser.country?.length == 2
      personalProfileFactory.searchCountries($scope.editUser.country).then (resp)->
        $scope.results=resp.data
        $scope.loading = false
        findObj = _.find $scope.results, (o) -> o.code == $scope.editUser.country.toUpperCase()
        $scope.countryError = _.isUndefined(findObj)
        return
      , (data) ->
        $scope.loading = false
        console.log 'error'
        return
    else
      $scope.loading = false
      $scope.countryError = true

  $scope.resetSearch=()->
    $scope.searchTableOpen = false

  $scope.createGroupList=(groups)->
    filteredGroups = _.remove(groups, (n) ->
      n != "GUESTS"
    )
    $scope.groupList = filteredGroups.join(', ')

  $scope.editProfile=()->
    $scope.searchTableOpen = false
    $scope.changingPass = false
    $scope.editing = !$scope.editing
    $scope.editUser = angular.copy($scope.user)

  $scope.uploadPhoto=()->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-upload-photo/upload-photo.html'
      controller:'modalIstanceController'
    ).then (updatedUser) ->
      photoPath = updatedUser.photoPath + '&' + new Date().getTime() # NOTE: always returns the same image path '/Mia/user/ShowPortraitUser.do?account=adminalexey', temporary solution for the trigger $scope.user
      $scope.user.photoPath = photoPath
      # $window.location.reload()

  $scope.uploadCV=()->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-upload-cv/upload-cv.html'
      controller:'modalIstanceController'
      ).then (updatedUser) ->
        $scope.user = updatedUser
        $window.location.reload()

  $scope.changePassword=()->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-change-password/change-password.html'
      controller:'modalIstanceController'
      )
    modalInstance.account = $scope.user.account

  $scope.cancel=()->
    $scope.editing = false

  $scope.save=()->
    params={}
    params.affiliation      = $scope.editUser.affiliation
    params.city             = $scope.editUser.city
    params.country          = $scope.editUser.country
    params.email            = $scope.editUser.email
    params.firstName        = $scope.editUser.firstName
    params.address          = $scope.editUser.address
    params.lastName         = $scope.editUser.lastName
    params.title            = $scope.editUser.title
    params.interests        = $scope.editUser.interests
    params.academiaEduLink  = $scope.editUser.academiaEduLink
    params.mailNotification = $scope.editUser.mailNotification
    params.hideActivities   = $scope.editUser.hideActivities
    params.hidePublications = $scope.editUser.hidePublications
    params.hideStatistics   = $scope.editUser.hideStatistics

    personalProfileFactory.modifyUser(params).then (resp)->
      if resp.status == "ok"
        $scope.editing = false
        # NOTE: backend doesn't return the updated object
        initModule().then () ->
          $rootScope.me.email     = $scope.user.email
          $rootScope.me.firstName = $scope.user.firstName
          $rootScope.me.lastName  = $scope.user.lastName

  initModule=()->
    personalProfileFactory.getUser().then (resp)->
      if resp.data
        $scope.user = resp.data
        $scope.createGroupList($scope.user.userGroups)

  initModule()
