newsFactoryApp = angular.module 'newsFactoryApp',[]

newsFactoryApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
]
newsFactoryApp.factory 'newsFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  newsBasePath = ENV.newsBasePath
  updateNum = 20

  factory =
    findDailyNews: () ->
      return $http.get(newsBasePath+'getDailyUpdates/').then (resp)->
        resp.data
    findWeeklyNews: () ->
      return $http.get(newsBasePath+'getWeeklyUpdates/').then (resp)->
        resp.data
    findMonthlyNews: () ->
      return $http.get(newsBasePath+'getMonthlyUpdates/').then (resp)->
        resp.data
    findRecentNews: () ->
      return $http.get(newsBasePath+'getUpdates/'+updateNum).then (resp)->
        resp.data
    updateNumberOfNews: (newUpdNum) ->
      updateNum = newUpdNum
      return updateNum

    getLastRecords: (account) ->
      return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
        resp.data

  return factory
