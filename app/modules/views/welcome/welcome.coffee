welcomeApp = angular.module 'welcomeApp', ['newsApp', 'homeLastRecordApp']

welcomeApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.welcome',
        url: '/welcome'
        views:
          'content':
            controller: 'welcomeController'
            templateUrl: 'modules/views/welcome/welcome.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

welcomeApp.controller 'welcomeController', (newsFactory, personalProfileFactory, $scope, $rootScope, $timeout, $http) ->
  $rootScope.pageTitle  = null
  $scope.noAe           = true
  $scope.isAdmin        = false
  $scope.lastRecords    = null
  $scope.lastRecord     = null
  $scope.loadingLastRec = true
  $scope.startBroadcast = false

#  commonsFactory.getMyArchive(10, 6, 1).then (resp)->
#    if resp.aentities.length
#      $scope.noAe = false

#  personalProfileFactory.getUser().then (resp)->
#    if resp.data
#      for item in resp.data.userGroups
#        if item == "ADMINISTRATORS"
#          $scope.isAdmin = true

  $scope.$on 'get-me', (evt, arg) ->
    # NOTE: $timeout not working in FF, waiting $rootScope.me
    return if !$scope.lastRecords
    $scope.startBroadcast = true
    getLastRecords()

  $timeout ->
    return if !$rootScope.me
    return if $scope.startBroadcast
    getLastRecords()

  $scope.getAccount = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      $scope.user = me.data[0]

  $scope.getAccount()

  getLastRecords = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      newsFactory.getLastRecords(account).then (resp) ->
        $scope.lastRecords = resp.data
        $scope.prepareLastRecordsObjs()
        $rootScope.$broadcast('receiveLastRecord', resp)

  $scope.prepareLastRecordsObjs = () ->
    $scope.loadingLastRec = true
    collectionLastObjs = []

    if $scope.lastRecords.lastUpload && $scope.lastRecords.lastUploadDate
      singleObj = {'id': $scope.lastRecords.lastUpload, type: 'lastUpload', 'date': $scope.lastRecords.lastUploadDate}
      collectionLastObjs.push(singleObj)

    if $scope.lastRecords.lastDocument && $scope.lastRecords.lastDocumentDate
      singleObj = {'id': $scope.lastRecords.lastDocument, type: 'lastDocument', 'date': $scope.lastRecords.lastDocumentDate}
      collectionLastObjs.push(singleObj)

    if $scope.lastRecords.lastPerson && $scope.lastRecords.lastPersonDate
      singleObj = {'id': $scope.lastRecords.lastPerson, type: 'lastPerson', 'date': $scope.lastRecords.lastPersonDate}
      collectionLastObjs.push(singleObj)

    if $scope.lastRecords.lastPlace && $scope.lastRecords.lastPlaceDate
      singleObj = {'id': $scope.lastRecords.lastPlace, type: 'lastPlace', 'date': $scope.lastRecords.lastPlaceDate}
      collectionLastObjs.push(singleObj)

    if $scope.lastRecords.lastVolumeOrInsert && $scope.lastRecords.lastVolumeOrInsert.id && $scope.lastRecords.lastVolumeOrInsert.date
      singleObj = {'id': $scope.lastRecords.lastVolumeOrInsert.id, type: 'lastVolumeOrInsert', 'date': $scope.lastRecords.lastVolumeOrInsert.date}
      collectionLastObjs.push(singleObj)

    if collectionLastObjs.length == 0
      $scope.lastRecord     = null
      $scope.loadingLastRec = false
    else if collectionLastObjs.length == 1
      $scope.getLastRecordByObj(collectionLastObjs[0])
    else
      lastObj = _.orderBy(collectionLastObjs, ['date'], ['desc'])
      $scope.getLastRecordByObj(lastObj[0])




  $scope.isObjectOwnerOrAdmin = (object) ->
    result = (object.data.privacy == 0 || $scope.user.account == object.data.owner|| $scope.user.userGroups?.includes('ADMINISTRATORS'))
    return result

  $scope.getLastRecordByObj = (obj) ->
    if obj.type == 'lastDocument'
      $scope.getLastDocument().then (resp) ->
        if ($scope.isObjectOwnerOrAdmin(resp))
          $scope.lastRecord = resp
        else
          $scope.lastRecords.lastDocument = null
          $scope.prepareLastRecordsObjs()

    if obj.type == 'lastPerson'
      $scope.getLastPerson().then (resp) ->
        $scope.lastRecord = resp

    if obj.type == 'lastPlace'
      $scope.getLastPlace().then (resp) ->
        $scope.lastRecord = resp

    if obj.type == 'lastUpload'
      $scope.getLastUpload().then (resp) ->
        $scope.lastRecord = resp

    if obj.type == 'lastVolumeOrInsert'
      if $scope.lastRecords.lastVolumeOrInsert.type == 'Volume'
        $scope.getLastVolume().then (resp) ->
          $scope.lastRecord = resp
      else if $scope.lastRecords.lastVolumeOrInsert.type == 'Insert'
        $scope.getLastInsert().then (resp) ->
          $scope.lastRecord = resp
    $timeout -> $scope.loadingLastRec = false

  $scope.getLastDocument = () ->
    data = {type: 'document', data: null}
    return data unless $scope.lastRecords.lastDocument
    $http.get('json/document/findDocument/' + $scope.lastRecords.lastDocument).then (resp) ->
      if resp.data.status == 'ok'
        data.data = resp.data.data.documentEntity
      return data

  $scope.getLastPerson = () ->
    data = {type: 'people', data: null}
    return data unless $scope.lastRecords.lastPerson
    $http.get('json/biographical/findBiographicalPeople/' + $scope.lastRecords.lastPerson).then (resp) ->
      if resp.data.status == 'ok'
        data.data = resp.data.data
      return data

  $scope.getLastPlace = () ->
    data = {type: 'place', data: null}
    return data unless $scope.lastRecords.lastPlace
    $http.get('json/geographical/findGeographicalPlace/' + $scope.lastRecords.lastPlace).then (resp) ->
      if resp.data.status == 'ok'
        data.data = resp.data.data
      return data

  $scope.getLastUpload = () ->
    data = {type: 'ae', data: null}
    return data unless $scope.lastRecords.lastUpload
    $http.get('json/archive/getArchives/aentity/' + $scope.lastRecords.lastUpload + '/7/1').then (resp) ->
      if resp.data.aentity
        data.data = resp.data.aentity
      return data

  $scope.getLastVolume = () ->
    data = {type: 'volume', data: null}
    return data unless $scope.lastRecords.lastVolumeOrInsert && $scope.lastRecords.lastVolumeOrInsert.type == 'Volume'
    volumeInsertDescrFactory.findVolumeDescription($scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
      if resp.status == 'ok'
        data.data = resp.data.data
        volumeInsertDescrFactory.findVolumeSpine($scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
          data.data.spine = resp.data
          return data

  $scope.getLastInsert = () ->
    data = {type: 'insert', data: null}
    return data unless $scope.lastRecords.lastVolumeOrInsert && $scope.lastRecords.lastVolumeOrInsert.type == 'Insert'
    volumeInsertDescrFactory.findInsertDescription($scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
      if resp.status == 'ok'
        data.data = resp.data.data
        volumeInsertDescrFactory.findInsertGuardia($scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
          data.data.spine = resp.data
          return data