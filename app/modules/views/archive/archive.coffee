archiveApp = angular.module 'archiveApp',[
    'miaApp'
    'translatorApp'
    'commonServiceApp'
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'ngDraggable'
  ]


archiveApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.archive',
        url: '/archive'
        views:
          'content':
            controller: 'archiveController'
            templateUrl: 'modules/views/archive/archive.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

archiveApp.controller 'archiveController', (commonsFactory, $scope, $rootScope, $state, $window) ->
  #data for pagination
  $rootScope.pageTitle = "My Images"
  $scope.pagination = {
    maxSize : 1,
    bigTotalItems: 1,
    bigCurrentPage: 1,
    itemsPerPage : 10
  }
  thumbNumb = 6
  
  $scope.getArchiveTracker = true
  $scope.fixUploadTracker = false
  $scope.noArchiveFound = false

  commonsFactory.getMyArchive($scope.pagination.itemsPerPage, thumbNumb, 1).then (resp) ->
#    console.log('total ae read')
    $scope.myArchives = resp
    if $scope.myArchives.aentities.length
      $scope.pagination.bigTotalItems = $scope.myArchives.count || 1
      for archive in $scope.myArchives.aentities
        publicAe=false
        if archive.thumbsFiles
          for thumbFile in archive.thumbsFiles
            if thumbFile.privacy is 0
              publicAe = true
        if publicAe
          archive.privacy = "Public"
        else
          archive.privacy = "Private"
    else
      $scope.noArchiveFound = true
    $scope.getArchiveTracker = false

  $scope.pageChanged = ()->
#    console.log('visito pagina', $scope.pagination.bigCurrentPage)
    $scope.getArchiveTracker = true
    commonsFactory.getMyArchive($scope.pagination.itemsPerPage, thumbNumb, $scope.pagination.bigCurrentPage).then (resp) ->
      if $scope.myArchives.aentities.length
        $scope.myArchives = resp
      else
        $scope.noArchiveFound = true
      $scope.getArchiveTracker = false

  $scope.gotoSingle = (uploadId, uploadFileId) ->
    params = {}
    params.aeId = uploadId
    params.fileId = uploadFileId || 0
    params.page = 1
    $state.go 'mia.singleupload', params

  $scope.reload = () ->
    $window.location.reload()

  $scope.$on 'upload-finished', (event, args) ->
    setTimeout (->
      $scope.reload()
      return
    ), 1000
    return
    setTimeout (->
      $scope.reload()
      return
    ), 4000
    return
  $scope.fixUpload = (uploadFileId) ->
    $scope.fixUploadTracker = true
    commonsFactory.fixUpload(uploadFileId).then (resp) ->
      $scope.fixUploadTracker = false
      if resp.status = "ok"
        commonsFactory.getMyArchive($scope.aeNuber, thumbNumb, $scope.pagination.bigCurrentPage).then (resp) ->
          if $scope.myArchives.aentities.length
            $scope.myArchives = resp
          else
            $scope.noArchiveFound = true
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("fixUpload.ERROR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp
