pubProfileApp = angular.module 'publicProfileApp',[
  'personalProfileService'
]

pubProfileApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.publicProfile',
        url: '/public-profile/:account'
        views:
          'content':
            controller: 'pubProfileController'
            templateUrl: 'modules/views/public-profile/public-profile.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

pubProfileApp.controller 'pubProfileController', ($scope, $rootScope, $state, $http, personalProfileFactory, searchFactory, searchDeService) ->
  $rootScope.pageTitle = "Public Profile"
  $scope.user          = undefined
  $scope.loadingData   = false
  $scope.userStatistics = null
  $scope.joinDate = null
  $scope.lastUpdateDate = null
  $scope.joinDateMonth = null
  $scope.lastUpdateDateMonth = null
  # TODO: move to factory later

#  $http.get('json/user/findMiaUser/' + $state.params.account).then (resp) ->
#    console.log('===findMiaUser==>>>>', resp)
#
#  $http.get('json/user/findSocialInfo/' + $state.params.account).then (resp) ->
#    console.log('===findSocialInfo==>>>>', resp)
#
#  $http.get('json/user/findDiscoveries/' + $state.params.account).then (resp) ->
#    console.log('===findDiscoveries==>>>>', resp)
#
#  $http.get('json/user/findRecentActivities/' + $state.params.account).then (resp) ->
#    console.log('===findRecentActivities==>>>>', resp)
#
#  $http.get('json/userprofile/statistics/' + $state.params.account).then (resp) ->
#    console.log('===statistics==>>>>', resp)



  $scope.showSpotlightReview = (discovery) ->
    if discovery.status == 'Published' or discovery.owner == $rootScope.me.account
      window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/discovery/'+discovery.id
  
  initModule=()->
    $scope.loadingData = true
    $http.get('json/user/findMiaUser/' + $state.params.account).then (resp) ->
      # console.log('===findMiaUser==>>>>', resp)
      $scope.loadingData = false
      if resp.data.data
        $scope.user = resp.data.data

    $scope.loadingData = true
    $http.get('json/discovery/findUserDiscoveries?account='+$state.params.account).then (resp) ->
      $scope.loadingData = false
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        $scope.discoveries = resp.data.data.discoveries
        $scope.nodiscoveries = angular.equals($scope.discoveries, {}) || angular.equals($scope.discoveries, [])
      else
        $scope.errorMsg = resp.data.message
      
    $http.get('json/userprofile/getUserStatistics?account=' + $state.params.account).then (resp) ->
      $scope.userStatistics = resp.data.data
      if $scope.userStatistics?
        $scope.joinDate = new Date($scope.userStatistics.joinDate)
        $scope.lastUpdateDate = new Date($scope.userStatistics.lastUpdateDate)

        option = { month: 'long'}
        $scope.joinDateMonth = Intl.DateTimeFormat('en-US', option).format($scope.joinDate)
        $scope.lastUpdateDateMonth = Intl.DateTimeFormat('en-US', option).format($scope.lastUpdateDate)
    
#    personalProfileFactory.getUser().then (resp)->
#      $scope.loadingData = false
#      console.log('======', resp)
#      if resp.data
#        $scope.user = resp.data

  initModule()

  $scope.addMessage = (discovery) ->
    if discovery.status == 'Published'
      return false
    
    return true

  ##############################################################################
  ################################## UPLOADS ###################################
  ##############################################################################
  $scope.getUserUploads = () ->
    params = prepareUserUploadsQueryParams()

    $rootScope.$broadcast('start-selected-search')

    searchFactory.searchAeWord(params).then (response)->
      newResult  = prepareUserUploadsResults(params, response)
      
      $rootScope.$broadcast('new-result', newResult)

  prepareUserUploadsQueryParams = () ->
    params = {}
    params.owner = $scope.user.account
    params.matchSearchWords = ''
    params.partialSearchWords = ''
    params.searchType = 'AE_OWNER'
    params.onlyMyArchivalEnties = true
    params.everyoneElsesArchivalEntities = false
    params.allArchivalEntities = false
  
    params

  prepareUserUploadsResults = (params, response) ->
    newResult = {}
    newResult.searchName = $scope.user.account + ' uploads'
    newResult.type ='ae'
    newResult.subtype ='simple_search'
    newResult.results = response.data.aentities

    newResult.content = {
      searchType: newResult.searchName,
      where: 'in <b>uploads</b>',
      where2: 'Owner',
      title: 'Uploads',
      matchSearchWords: params.matchSearchWords,
      partialSearchWords: params.partialSearchWords,
      searchIn: 'Owner',
      countResult: response.data?.count || 0,
      params: params
    }
    
    newResult

  ##############################################################################
  ################################# DOCUMENTS ##################################
  ##############################################################################
  $scope.getUserDocuments = () ->
    userAccount = $scope.user.account
    
    userDocumentSearchData = prepareUserDocumentSearchData(userAccount)
    searchArray = prepareSearchArray(userDocumentSearchData)

    searchForm = createSearchForm(userAccount)

    searchDeService.getPartialRecordsCount(searchArray).then (count) ->
      recordsCount = count

      searchDeService.getResults(searchArray).then (response) ->
        newResult = prepareUserDocumentsResults({
          'userAccount': userAccount,
          'searchArray': searchArray,
          'searchForm': searchForm,
          'recordsCount': recordsCount
          'results': response.data
        })

        $rootScope.$broadcast('new-result', newResult)

  prepareUserDocumentSearchData = (userAccount) ->
    userDocumentSearchData = {
      searchSection: "documentOwnerSearch"
      type: "documentOwnerAdvancedSearch"
      isActiveFilter: true
      editType: "owner"
      account: userAccount
    }

    userDocumentSearchData

  # To build correctly the result box of the right-sidebar a searchArray is needed with the right elements
  # in specific positions
  prepareSearchArray = (userDocumentSearchData) ->
    searchArray = [
      { searchSection: "archivalLocationSearch", type: "archivalLocationAdvancedSearch", isActiveFilter: false },
      { searchSection: "categoryAndTypologySearch", type: "categoryAndTypologyAdvancedSearch", isActiveFilter: false },
      { searchSection: "transcriptionSearch", type: "transcriptionAdvancedSearch", isActiveFilter: false },
      { searchSection: "synopsisSearch", type: "synopsisAdvancedSearch", isActiveFilter: false },
      { searchSection: "placesSearch", type: "placesAdvancedSearch", isActiveFilter: false },
      { searchSection: "peopleSearch", type: "peopleAdvancedSearch", isActiveFilter: false },
      { searchSection: "topicsSearch", type: "topicsAdvancedSearch", isActiveFilter: false },
      { searchSection: "dateSearch", type: "dateAdvancedSearch", isActiveFilter: false },
      userDocumentSearchData,
      { searchSection: "languagesSearch", type: "languagesAdvancedSearch", isActiveFilter: false }
    ]

    searchArray

  createSearchForm = (userAccount) ->
    searchForm = {
      owner: {
        searchSection: "documentOwnerSearch"
        type: "documentOwnerAdvancedSearch"
        isActiveFilter: true
        editType: "owner"
        account: userAccount
      }
    }

    searchForm

  prepareUserDocumentsResults = (data) ->
    newResult = {}
    newResult.searchName = $scope.user.account + ' documents'
    newResult.type = 'doc_advanced'
    newResult.results = data.results
    newResult.searchArray = data.searchArray
    newResult.searchForm = data.searchForm
    newResult.content = {
      searchType: 'Document Search',
      where: '',
      title: 'Document Search',
      countResult: data.recordsCount
    }

    newResult

  ##############################################################################
  ############################# BIOGRAHICAL RECORDS ############################
  ##############################################################################
  $scope.getUserBiographicalRecords = () ->
    params = {
      'matchSearchWords' : '',
      'partialSearchWords': '',
      'searchType': [''],
      'owner': $scope.user.account
    }
    
    searchFactory.searchPeopleWord(params).then (response) ->
      newResult = preparePeopleResults(
        {
          'userAccounts': $scope.user.account,
          'recordsCount': response.count,
          'params': params,
          'recordsCount': response.count,
          'results': response.data.data
        })
      
      $rootScope.$broadcast('new-result', newResult)

  preparePeopleResults = (data) ->
    newResult = {}
    newResult.searchName = $scope.user.account + ' biographical records'
    newResult.searchTooltip = 'Biographical records by <b>' + data.userAccount + '</b>'
    newResult.type =  'people'
    newResult.subtype =  'simple_search'
    newResult.content = {
      searchType: 'owner',
      title: 'Biographical Records',
      matchSearchWords: '',
      partialSearchWords: '',
      searchIn: 'All Name Types',
      countResult: data.recordsCount,
      params: data.params
    }
    newResult.results = data.results

    newResult

  ##############################################################################
  ############################# GEOGRAHICAL RECORDS ############################
  ##############################################################################
  $scope.getUserGeographicalRecords = () ->
    params = {
      'matchSearchWords' : '',
      'partialSearchWords': '',
      'owner': $scope.user.account
    }

    searchFactory.searchPlaceWord2(params).then (response) ->
      newResult = preparePlaceResults(
        {
          'userAccounts': $scope.user.account,
          'recordsCount': response.count,
          'params': params,
          'recordsCount': response.count,
          'results': response.data.data
        })

      $rootScope.$broadcast('new-result', newResult)

  preparePlaceResults = (data) ->
    newResult = {}
    newResult.searchName = $scope.user.account + ' geographical records'
    newResult.searchTooltip = 'Geographical records by <b>' + data.userAccount + '</b>'
    newResult.type =  'place'
    newResult.subtype =  'simple_search'
    newResult.content = {
      searchType: 'owner',
      title: 'Geographical Records',
      matchSearchWords: '',
      partialSearchWords: '',
      searchIn: 'All Name Types',
      countResult: data.recordsCount,
      params: data.params
    }
    newResult.results = data.results

    newResult
  