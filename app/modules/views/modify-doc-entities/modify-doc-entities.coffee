modifyDocEntApp = angular.module 'modifyDocEntApp',[
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'modPeople'
    'modPlace'
    'modTopic'
    'biblioRef'
    'docAeInfo'
    'docSysInfo'
    'docViewer'
    'docSummary'
    'docTranscription'
    'docSynopsis'
    'docChineseSynopsis'
    'deRelatedPeople'
    'deSinglePlace'
    'actionsHistoryApp'
    'actionsHistory'
    'addCollationApp'
    'commentsModule'
    'preventLossChangesApp'
    'addToCsApp'
  ]

modifyDocEntApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider

      .state 'mia.modifyDocEnt',
        url: '/document-entity/:docId?scrollTo'
        params: { documentEntityId:null }
        reloadOnSearch: false
        views:
          'content':
            controller: 'modifyDocEntController'
            templateUrl: 'modules/views/modify-doc-entities/modify-doc-entities.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

modifyDocEntApp.controller 'modifyDocEntController', (messagingFactory, ENV, deFactory, modDeFactory, $scope, $sce, $rootScope, $state, $stateParams, $filter, modalFactory, commonsFactory, $window, $timeout, alertify, $http, $location, $anchorScroll) ->
  $rootScope.pageTitle = "Modify Document "
  
  $scope.currentImageOrFileId = null
  $scope.chineseSynopsis = null
  $scope.projectName = null

  $scope.$on('deTitle-edited', (evt, args)->
    $scope.basic.deTitle = args
    $rootScope.deTitle = args
    saveBasic()
  )

  # show only one alert in time
  $scope.alertIsShowed = false

  $scope.showAlert = (message) ->
    if !$scope.alertIsShowed
      $scope.alertIsShowed = true
      alertify.alert(message).then () ->
        $scope.alertIsShowed = false

  #panels and graphic variables
  $scope.allClosed = true
  $scope.switchAllPanels = () ->
    $scope.allClosed = !$scope.allClosed
    for section in $scope.sections
      section.open = !$scope.allClosed

  #page sections
  $scope.sections = []
  $scope.sections.push({"name":"Transcription and Synopsis", "open":false})
  $scope.sections.push({"name":"Document Details", "open":false})
  $scope.sections.push({"name":"People", "open":false})
  $scope.sections.push({"name":"Places", "open":false})
  $scope.sections.push({"name":"Topics", "open":false})

  #edition bool
  $scope.editingTranscription = false
  #Pagination
  $scope.limit = 0
  $scope.changeLimit = (quantity) ->
    temp = $scope.limit + quantity
    $scope.lastShownImageIndex = temp + 6
    if temp >= 0 and temp + 6 <= $scope.documentEntity.uploadFiles.length
      $scope.limit = temp

  $scope.$on('focus', (evt, args) ->
    if !$scope.editingTranscription
      imageFound = _.find($scope.documentEntity.uploadFiles, (pic) ->
        pic.uploadFileId is parseInt(args))
      if imageFound
        indexImage = _.indexOf($scope.documentEntity.uploadFiles, imageFound)
        if indexImage < 6
          $scope.limit = 0
        else
          $scope.limit = indexImage - 5
        $scope.lastShownImageIndex = indexImage + 1
        $scope.updateViewer(imageFound)
    else
      $rootScope.$broadcast('editApprove', false)
      $scope.messageAlert = $filter('translate')("editDE.TRANSCLOSETEXTAREAMESSAGECHANGEIMAGE")
      $scope.showAlert($scope.messageAlert)
  )

  $rootScope.$on('editingTranscription', (evnt, args) ->
    $scope.editingTranscription = args
  )

  $scope.updateViewer = (image) ->
    if image.canView
      if $scope.editingTranscription
        $scope.messageAlert = $filter('translate')("editDE.TRANSCLOSETEXTAREAMESSAGECHANGEIMAGE")
        $scope.showAlert($scope.messageAlert)
      else
        $scope.currentImageOrFileId = null
        angular.forEach($scope.currentFolios, (singleFolio) ->
          if singleFolio.uploadFileId == image.uploadFileId
            $rootScope.$broadcast('uploadIdMatch', singleFolio.uploadFileId)
        )
        if image.uploadFileId?
          $scope.currentImageOrFileId = image.uploadFileId
        else
          $scope.currentImageOrFileId = image.uploadedFileId

      $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/" + $scope.currentImageOrFileId)
      selectImage(image)
      $rootScope.$emit('imageSelected', image)
  
  $rootScope.$on('editCurrentTranscription', (evt, data) ->
    $scope.updateViewer(data)
  )

  selectImage = (image) ->
    if $scope.editingTranscription == false
      actual = _.find $scope.documentEntity.uploadFiles, (pic) -> pic.highlighted is true
      $rootScope.$broadcast('editApprove', true)
      if actual
        actual.highlighted = false
      image.highlighted = true
    else $rootScope.$broadcast('editApprove', false)

  $scope.addPicToDe = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-folio/edit-folio.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"uploadFileId": $scope.documentEntity.uploadFiles[0].uploadFileId, "uploadId":$scope.documentData.uploadId, "documentEntityId":$scope.documentEntity.documentId }
      }
    ).then (documentEntity) ->
      params = {}
      params.documentId = documentEntity.documentId
      params.modifyImagesInDe = []
      for image in documentEntity.uploadFiles
        params.modifyImagesInDe.push(image.uploadFileId)
      modDeFactory.editDocImages(params).then (resp) ->
        if (resp.status is 'ok')
          $state.reload()

  #----------------------------------------
  # Edit Transcription
  #----------------------------------------
  $scope.transcriptions = []
  transWindow=undefined

  reloadPage=()->
    $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})

  extractDomain = (url) ->
    domain = undefined
    if url.indexOf('://') > -1
      domain = url.split('/')[2]
    else
      domain = url.split('/')[0]
    domain='http://'+domain

  $scope.openTranscriptor= () ->
    workUrl = $state.href($state.current.name, $state.params, {absolute: true})
    transWindow=$window.open(extractDomain(workUrl)+'/Mia/json/src/ShowDocumentInManuscriptViewer/show?documentEntityId='+$scope.documentEntity.documentId+'&showTranscription=true&fileId=' + $scope.currentImageOrFileId)
    transWindow.onbeforeunload=()->
      $timeout(reloadPage, 1000)

  $scope.synopsis = null
  #----------------------------------------
  # Bibliographical References
  #----------------------------------------
  $scope.biblioRef = []
  $scope.canAddBiblio = true

  getBiblioRef = () ->
    modDeFactory.getBiblioRef($state.params.docId).then (resp)->
      if resp.status is 'ok'
        $scope.biblioRef = resp.data.arrayBiblio.biblios

  $scope.addNewBiblioRef = () ->
    temp = {}
    temp.biblioId = null
    temp.name = ""
    temp.link = ""
    $scope.biblioRef.push(temp)

  $scope.cancelBiblioRef = (biblio)->
    $scope.biblioRef.splice($scope.biblioRef.indexOf(biblio),1)

  $scope.$on('editing-biblio', (event, args)->
    $scope.canAddBiblio = false
    $scope.emitDeEditEvent('biblioRef', true)
    )

  $scope.$on('editing-biblio-end', (event, args)->
    $scope.canAddBiblio = true
    $scope.emitDeEditEvent('biblioRef', false)
    )

  #----------------------------------------
  # Related documents
  #----------------------------------------
  $scope.editingRelDoc = false
  $scope.related = []

  $scope.switchRelDoc = ()->
    $scope.editingRelDoc = !$scope.editingRelDoc

  $scope.addRelDoc = ()->
    $scope.related.push({'value':0})
    $scope.editingRelDoc = true

  $scope.saveRelDoc = ()->
#    console.log $scope.related
    params={}
    params.documentId=$scope.$stateParams.docId
    params.relatedDocs=[]
    for doc in $scope.related
      params.relatedDocs.push(doc.value)
    $scope.editingRelDoc = false
    modDeFactory.saveRelatedDocuments(params).then (resp)->
      if resp.status is "ok"
        console.log "saved related doc"
      else
        console.log "unsaved releted documents"


  getRelDoc=()->
    modDeFactory.getRelatedDocuments($scope.$stateParams.docId).then (resp)->
      $scope.related=[]
      if resp.data.relatedDocs.relatedDocs
        for doc in resp.data.relatedDocs.relatedDocs
          $scope.related.push({'value':doc})

  #----------------------------------------
  # Edit Basic Description
  #----------------------------------------
  $scope.editingType = false
  $scope.editingMYear = false
  $scope.editingDateUnc = false
  $scope.basic = {}

  saveBasic = ()->
    $scope.basic.docDateUncertain = Number($scope.basic.docDateUncertain) or 0
    $scope.basic.docUndated = Number($scope.basic.docUndated) or 0
    $scope.basic.docYear = Number($scope.basic.docYear) or 0
    $scope.basic.docMonth = Number($scope.basic.docMonth) or 0
    $scope.basic.docDay = Number($scope.basic.docDay) or 0
    $scope.basic.docModernYear = Number($scope.basic.docModernYear) or 0
    console.log($scope.basic)
    modDeFactory.saveBasicDescription($scope.basic).then (resp)->

  $scope.editBasic = (field)->
    switch field
      when "type"
        $scope.editingType = true
        $scope.emitDeEditEvent('basicDeDetails_type', true)
      when "moYear"
        $scope.editingMYear = true
        $scope.emitDeEditEvent('basicDeDetails_moYear', true)
      when "dateUnc"
        $scope.editingDateUnc = true
        $scope.emitDeEditEvent('basicDeDetails_dateUnc', true)
      when "docUndate"
        $scope.editingDocUndate = true
        $scope.emitDeEditEvent('basicDeDetails_docUndate', true)
      when "date"
        $scope.editingDate = true
        $scope.emitDeEditEvent('basicDeDetails_date', true)
      when "note"
        $scope.editingNote = true
        $scope.emitDeEditEvent('basicDeDetails_note', true)

  $scope.saveBasic = (field)->
    switch field
      when "type"
        $scope.editingType = false
        $scope.emitDeEditEvent('basicDeDetails_type', false)
      when "moYear"
        $scope.editingMYear = false
        $scope.emitDeEditEvent('basicDeDetails_moYear', false)
      when "dateUnc"
        $scope.editingDateUnc = false
        $scope.emitDeEditEvent('basicDeDetails_dateUnc', false)
      when "docUndate"
        $scope.editingDocUndate = false
        $scope.emitDeEditEvent('basicDeDetails_docUndate', false)
      when "date"
        $scope.editingDate = false
        $scope.emitDeEditEvent('basicDeDetails_date', false)
      when "note"
        $scope.editingNote = false
        $scope.emitDeEditEvent('basicDeDetails_note', false)
    saveBasic()

  #-----------
  # Languages
  #-----------
  $scope.docLang=[]
  $scope.allLang=[]
  $scope.editingLang = false

  getDocumentLanguages = ()->
    modDeFactory.getDocLang($scope.$stateParams.docId).then (resp)->
      $scope.docLang=[]
      if resp.data.documentLanguage.languages
        for lang in resp.data.documentLanguage.languages
          $scope.docLang.push(lang.id)


  getAllLanguages = ()->
    modDeFactory.getAllLang().then (resp)->
      $scope.allLang=resp.data.languages

  $scope.addLang=()->
    $scope.editingLang=true
    $scope.docLang.push('')

  $scope.switchEditLang=()->
    $scope.emitDeEditEvent('deLanguages', !$scope.editingLang)
    $scope.editingLang=!$scope.editingLang
    getDocumentLanguages() if !$scope.editingLang

  $scope.saveLang=()->
    $scope.emitDeEditEvent('deLanguages', false)
    $scope.editingLang=false
    params={}
    params.documentId=$scope.$stateParams.docId
    params.languages=[]
    angular.forEach($scope.docLang, (lang)->
      language={}
      language.id=lang
      angular.forEach($scope.allLang, (beLang)->
        if beLang.id is lang
          language.language=beLang.language
        )
      params.languages.push(language)
      )
    modDeFactory.editDocLang(params).then (resp)->

  $scope.langFilter = (selectedLang) ->
    filter = (currentOption) ->
      return true if (currentOption.id == selectedLang)
      return (!$scope.docLang.includes(currentOption.id))
    return filter

  $scope.deleteLang=(lang)->
    $scope.docLang.splice($scope.docLang.indexOf(lang), 1)

  #----------------------------------------
  # People
  #----------------------------------------
  $scope.peopleFields = []
  $scope.people = []

  $scope.isEditingPeople=(peopleToChek)->
    result=true
    angular.forEach((peopleToChek), (person)->
      if person.editPerson
        result=false
      )
    return result

  getDocumentPeople = () ->
    modDeFactory.findDocumentPeople($scope.$stateParams.docId, $scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
      $scope.people = resp.data.arrayPeople

      angular.forEach($scope.people, (role)->
        angular.forEach(role.peoples, (person)->
          deFactory.getPeopleById(person.id).then (resp)->
            person.feData = resp.data
          )
        )
  $scope.editPerson = (person)->
    person.edit = true

  $scope.selectPerson = (person)->
    person.edit = false

  $scope.openPerson = (person)->
    params = {}
    params.id = person.id
    $state.go 'mia.bio-people', params

  $scope.savePerson = (passed, person)->
    alreadyPresent = false
    #console.log('passed ', passed)
    #console.log('person ', person)
    if $scope.people
      for role in $scope.people
        if role.peoples
          for singlePerson in role.peoples
            #console.log('persona presente id ', singlePerson.id)
            if singlePerson.id and (singlePerson.id is passed.temp.id)
              alreadyPresent = true
      modDeFactory.getRelatedPeople($state.params.docId).then (resp)->
#        console.log(resp)
        if resp.status is 'ok'
          for relPeople in resp.data.people
            #console.log('persona presente id ', relPeople.id)
            if relPeople.id is passed.temp.id
              alreadyPresent = true
        # if not alreadyPresent
        params = {}
        params.documentId = $scope.$stateParams.docId
        params.category = $scope.documentEntity.category
        params.type = passed.role.type.replace(/\s/g, "")
        if person.id
          params.idPeopleToDelete = person.id
        params.newPeople = passed.temp
#        console.log params
        modDeFactory.editPerson(params).then (resp) ->
#          console.log(resp)
          if resp.status is 'ok'
            getDocumentPeople()
            person.editPerson = false
        # else
        #   pageTitle = $filter('translate')("general.ERROR")
        #   messagePage = $filter('translate')("people.ALREADYIN")
        #   modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

  $scope.addNewPerson = (role)->
    newPerson={}
    newPerson.id = undefined
    newPerson.unsure = 's'
    newPerson.editPerson = true
    $scope.emitDeEditEvent('dePersonEdit_'+ role.type, true)
    if !role.peoples
      role.peoples = []
    role.peoples.push(newPerson)

  $scope.cancelNewPeople = (person, passedRole)->
    angular.forEach($scope.people, (role)->
      if role is passedRole
        role.peoples.splice(role.peoples.indexOf(person),1)
      )

  $scope.deletePerson = (person, role)->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = $filter('translate')("modal.DELETINGPERSONDOC")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        params={}
        params.documentId = $scope.$stateParams.docId
        params.category = $scope.documentEntity.category
        params.type = role.type.replace(/\s/g, "")
        params.idPeopleToDelete = person.id
        modDeFactory.deletePerson(params).then (resp)->
          if resp.status is 'ok'
            getDocumentPeople()

  $scope.isThereSender=()->
    result=false
    angular.forEach($scope.people, (role)->
      if (role.type is 'Senders') and role.peoples
        result=true
      )
    return result

  $scope.isThereRecipient=()->
    result=false
    angular.forEach($scope.people, (role)->
      if (role.type is 'Recipient') and role.peoples
        result=true
      )
    return result

  #----------------------------------------
  # Place
  #----------------------------------------
  $scope.placesFields = []
  $scope.places = []

  $scope.isEditingPlace=(placeToChek)->
    resp=true
    angular.forEach((placeToChek), (place)->
      if place.editPlace == true
        resp=false
      )
    return resp

  $scope.isProducerPlace=(role)->
    resp=false
    if role.places
      if ((role.places.length==1) and (role.type is 'Sender Place') or (role.type is 'Place of Creation') or (role.type is 'Place of Issue') or (role.type is 'Place of Compilation') or (role.type is 'Quartiere/Quarter'))
        resp=true
    return resp



  getDocumentPlace = () ->
    modDeFactory.findDocumentPlace($scope.$stateParams.docId, $scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
      $scope.places = resp.data.arrayPlace
      angular.forEach($scope.places, (role)->
        angular.forEach(role.places, (place)->
          deFactory.getPlaceById(place.id).then (resp)->
            place.feData = resp.data
          )
        )


  $scope.modifyPlace = {}

  $scope.editPlace = (place, role)->
#    place.edit = true
    console.log(role)
    $scope.modifyPlace = angular.copy(place)
    $scope.emitDeEditEvent('dePlaceEdit_'+ role.type, true)
    place.editPlace = !place.editPlace

  $scope.cancelNewPlace = (place, passedRole)->
    angular.forEach($scope.places, (role)->
      if role is passedRole
        role.places.splice(role.places.indexOf(place),1)
      )

  $scope.savePlace = (passed, place)->
    params={}
    params.documentId = $scope.$stateParams.docId
    params.category = $scope.documentEntity.category
    params.type = passed.role.type.replace(/\s/g, "")
    params.newPlace=passed.temp
    params.idPlaceToDelete = place.id if place?.id
    modDeFactory.addDocumentPlace(params).then (resp)->
      if resp.status is 'ok'
        getDocumentPlace()
        place.editPlace = false
#    if place.id
#      params.idPlaceToDelete = place.id
#      modDeFactory.editDocumentPlace(params).then (resp)->
#        if resp.status is 'ok'
#          getDocumentPlace()
#          place.editPlace = false
#    else
#      modDeFactory.addDocumentPlace(params).then (resp)->
#        if resp.status is 'ok'
#          getDocumentPlace()
#          place.editPlace = false

  $scope.deletePlace = (place, role)->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = $filter('translate')("modal.DELETINGPLACEDOC")
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        params={}
        params.documentId = $scope.$stateParams.docId
        params.category = $scope.documentEntity.category
        params.type = role.type.replace(/\s/g, "")
        params.idPlaceToDelete = place.id or null
        modDeFactory.deleteDocumentPlace(params).then (resp)->
          if resp.status is 'ok'
            getDocumentPlace()

  $scope.openPlace = (place) ->
    params = {}
    params.id = place.id
    # params.idName= row.placeNameId
    $state.go 'mia.bio-place', params

  $scope.addNewPlace = (role)->
    newPlace={}
    newPlace.id = undefined
    newPlace.unsure = undefined
    newPlace.editPlace = true
    $scope.emitDeEditEvent('dePlaceEdit_'+ role.type, true)
    if not role.places
      role.places = []
    role.places.push(newPlace)


  #----------------------------------------
  # Document Topics
  #----------------------------------------
  $scope.canAddTopic = true
  $scope.deTopics = []
  $scope.allTopics = []

  getDocumentTopics = () ->
    modDeFactory.getDocumentTopics($scope.$stateParams.docId).then (resp)->
      if resp.status is "ok"
        $scope.deTopics = resp.data.topicPlace.topicPlaces
      deFactory.getAllTopics().then (resp)->
        $scope.allTopics = resp.data.topics
        angular.forEach($scope.deTopics, (singleTop)->
          singleTop.isNew = false
          deFactory.getPlaceById(singleTop.placeId).then (resp)->
            singleTop.fePlaceName = resp.data.placeName
        )

  $scope.editTopic = (topic)->
    topic.editTopic = true
    $scope.emitDeEditEvent('deTopicsEdit', true)

  $scope.addNewTopic = ()->
    newTopic={}
    newTopic.placeId = null
    newTopic.topicListId = null
    newTopic.editTopic = true
    newTopic.isNew = true
    $scope.emitDeEditEvent('deTopicsEdit', true)
    $scope.deTopics.push(newTopic)

  $scope.refreshTopics=()->
    getDocumentTopics()

  $scope.cancelNewTopic = (topic)->
    $scope.deTopics.splice($scope.deTopics.indexOf(topic), 1) if topic.isNew

  $scope.$on('editing-topic', (event, args)->
    $scope.canAddTopic = false
    )

  $scope.$on('editing-topic-end', (event, args)->
    $scope.canAddTopic = true
    )

  #----------------------------------------
  # Utilities
  #----------------------------------------
  findFieldTypeInDoc = (fieldList)->
    correctFields = []
    fields = _.keys($scope.documentEntity)
    for field in fields
      for elem in fieldList
        if elem.beName is field && $scope.documentEntity[field]
          for singleField in $scope.documentEntity[field]
            singleField.realName = elem.name
          correctFields.push($scope.documentEntity[field])
    return correctFields

  #----------------------------------------
  # Main body
  #----------------------------------------

  #fields
  getDocumentFields = () ->
    deFactory.getDocumentFields($scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
      fieldsToBePresent=resp.data.fields
      for field in fieldsToBePresent
        field.openPanel= false
        if field.type is "text"
          $scope.textFields.push(field)
      $scope.documentEntity.text = findFieldTypeInDoc($scope.textFields)

  compareTranscriptions = (currentEl, nextEl) ->
    if Number(currentEl.transcriptionOrder) < Number(nextEl.transcriptionOrder)
      return -1
    if Number(currentEl.transcriptionOrder) > Number(nextEl.transcriptionOrder)
      return 1
    return 0
  
  getTranscription = () ->
    modDeFactory.getTranscription($scope.$stateParams.docId).then (resp)->
      actualTranscriptions = []
      if resp.status
        #$scope.transcriptions = resp.data.transcriptions or "No transcription inserted yet"
        if resp.data != null
          $scope.transcriptions = resp.data.transcriptions
        else
          $scope.transcriptions = []
        $scope.documentEntity.uploadFiles.forEach((singleFile) ->
          foundAssignedTranscription = -1
          foundTranscription = false
          $scope.transcriptions.forEach((singleTranscription, i) ->
            if +singleTranscription.uploadedFileId == +singleFile.uploadFileId
              singleTranscription.transcriptionOrder = singleFile.imageOrder
              singleTranscription.folioNumber = singleFile.folios.map((folio) ->
                if folio.rectoverso
                  localRectoVerso = folio.rectoverso
                else
                  localRectoVerso = ''
                if folio.folioNumber != null then folio.folioNumber + ' ' + localRectoVerso else 'Not Numbered'
              ).join(" - ")
              foundTranscription = true
              foundAssignedTranscription = i
              actualTranscriptions.push(singleTranscription)
          )
          if !foundTranscription
            actualTranscriptions.splice(foundAssignedTranscription + 1, 0, {
              folioNumber: singleFile.folios.map((folio) ->
                if folio.rectoverso
                  newLocalRectoVerso = folio.rectoverso
                else
                  newLocalRectoVerso = ''
                if folio.folioNumber != null then folio.folioNumber + ' ' + newLocalRectoVerso else 'Not Numbered'
              ).join(" - ")
              documentId: singleFile.documentId || $scope.$stateParams.docId
              transcriptionOrder: singleFile.imageOrder
              transcription: ""
              docTranscriptionId: null
              uploadedFileId: singleFile.uploadFileId
            })
        )
        $scope.transcriptions = actualTranscriptions
        $scope.transcriptions.sort(compareTranscriptions)

  getSynopsis = () ->
    modDeFactory.getSynopsis($scope.$stateParams.docId).then (resp)->
      if resp.status is "ok"
        #$scope.synopsis = resp.data.synopsis.generalNotesSynopsis or "No synopsis added"
        $scope.synopsis = resp.data.synopsis.generalNotesSynopsis

  getChineseSynopsis = () ->
    modDeFactory.getChineseSynopsis($scope.$stateParams.docId).then (response) ->
      if response.status is "ok"
        $scope.chineseSynopsis = response.data.chineseSynopsis.chineseSynopsis

  getBasicDesc = () ->
    modDeFactory.getBasicDescription($scope.$stateParams.docId).then (resp)->
      if resp.status is "ok"
        $scope.basic = resp.data.basicDescription
        $scope.basic.docYear= "" if $scope.basic.docYear is 0
        $scope.basic.docDay= "" if $scope.basic.docDay is 0
        $scope.basic.docModernYear= "" if $scope.basic.docModernYear is 0
        $scope.basic.deTitle = "" if $scope.basic.deTitle is 0

  initDe = () ->
    $scope.textFields = []
    fieldsToBePresent = []
    $scope.typologies = []
    $scope.currentFolios = []
    $scope.filesView = []
    $scope.documentEntity = {}
    $scope.documentData = {}
    $scope.allFolios = {}
    #just for development TODO Delete
    if not $scope.$stateParams.docId
      $scope.$stateParams.docId = 1
    
    if $location.path().indexOf('document-entity') > -1
      $scope.spotlightPub = 'false'
      console.log($scope.spotlightPub)
    else
      $scope.spotlightPub = 'true'
      console.log($scope.spotlightPub)
      $scope.$stateParams.docId = $scope.discovery.documentId
      console.log($scope.discovery.documentId)

    if $scope.$stateParams.docId
      deFactory.getDocumentEntityById($scope.$stateParams.docId).then (resp)->
        deFactory.recordDocAction($scope.$stateParams.docId, 'VIEW')
        $scope.documentEntity = resp.data.documentEntity
        $scope.lastShownImageIndex = if $scope.documentEntity.uploadFiles.length <= 6 then $scope.documentEntity.uploadFiles.length else 6

        $rootScope.canModifyTitle = $scope.documentEntity.canModify
        if $scope.documentEntity.deTitle && (!$scope.documentEntity.flgLogicalDelete || $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS']))
          $rootScope.pageTitle = "Document: "
          $rootScope.deTitle = $scope.documentEntity.deTitle
        else
          $rootScope.pageTitle = "Document"

        for i in [0...$scope.documentEntity.uploadFiles.length]
          if $scope.documentEntity.uploadFiles[i].canView == true
            $scope.filesView.push($scope.documentEntity.uploadFiles[i])

        $scope.getAllFolios = () ->
          angular.forEach($scope.documentEntity.uploadFiles, (uploadFile) ->
            angular.forEach(uploadFile.folios, (singleFolio) ->
              singleFolio.transcriptionOrder = uploadFile.imageOrder
              $scope.currentFolios.push(singleFolio)
            )
          )
          $scope.currentFolios.sort(compareTranscriptions)
        
        $scope.getAllFolios()

        if $scope.filesView.length > 0 && $scope.filesView[0].uploadFileId
          selectImage($scope.filesView[0])
          $scope.updateViewer($scope.filesView[0])
          $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.filesView[0].uploadFileId)
        else
          $scope.viewerAddress = $sce.trustAsResourceUrl('')
        #----------------------------------------
        # Upload
        #----------------------------------------
        if $scope.filesView.length > 0
          commonsFactory.getMySingleuploadByFileId($scope.filesView[0].uploadFileId,12,1).then (resp)->
            $scope.documentData = resp.aentity
        #----------------------------------------
        # Typologies available
        #----------------------------------------
        modDeFactory.findDocTypologies($scope.documentEntity.category.replace(/\s/g, "")).then (resp)->
          $scope.typologies = resp.data.typologies

        getDocumentPeople()
        getDocumentPlace()
        getDocumentTopics()
        getRelDoc()
        getBiblioRef()
        getDocumentLanguages()
        getAllLanguages()
        getDocumentFields()
        delete $scope.documentEntity.transcriptions
        delete $scope.documentEntity.date
        getTranscription()
        getSynopsis()
        getChineseSynopsis()
        getBasicDesc()

        commonsFactory.getApplicationProperty('project.name').then (response) ->
          $scope.projectName = response


  initDe()

  $scope.addToCollation = () ->
    $scope.emitDeEditEvent('addCollation', true)
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-collation/add-collation.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {'document': $scope.documentEntity}
      }
    )

  $rootScope.$on('createLacuna', (event, args) ->
    if $scope.documentEntity.documentId == args.documentId
      $scope.documentEntity.hasLacuna = true
      $scope.documentEntity.lacuna = args.lacunaData.childDocument.lacuna
  )

  $scope.goToBAL = (volId, insId) ->
    data = {fileId: undefined, page: undefined, per: undefined}
    data.type = if insId then 'ins' else 'vol'
    data.id   = if insId then insId else volId
    $state.go 'mia.browseArchLocation', data

  $scope.deleteLacuna = (lacuna) ->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = 'Are you sure you want to delete this Reconstruction?  (This does not delete the individual documents)'
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
      if resp
        $http.delete('json/collation/' + lacuna.id).then (resp) ->
          if resp.status == 204
            $scope.documentEntity.lacuna = null
            $scope.documentEntity.hasLacuna = false

  $rootScope.$on('updateDePrivacy', (event, args) ->
    $scope.documentEntity.privacy = args.privacy if $scope.documentEntity.documentId == args.documentId
  )

  $scope.gotoComments = () ->
    $location.hash('comments')
    $anchorScroll()

  ####################################
  #   prevent navigation if document entity has editing modifications that have not being saved
  ####################################
  $scope.isBeingEdit = []

  $window.onbeforeunload = (event) ->
    if $scope.isBeingEdit.length > 0
      return ''
    else
      return null

  $scope.$on('$stateChangeStart', (event, toState, toParams) ->
    if $scope.isBeingEdit.length > 0
      event.preventDefault()
      $scope.unsavedChangesDialog(event, toState, toParams).then (ignoreChanges) ->
        if ignoreChanges
          $state.go(toState.name, toParams)
    return

  )

  $scope.unsavedChangesDialog = () ->
    return new Promise (resolve, reject) ->
      if $scope.isBeingEdit.length > 0
        modalFactory.openModal(
          templateUrl: 'modules/directives/modal-templates/mod-prevent-loss-changes/prevent-loss-changes.html'
          controller:'modalIstanceController'
          resolve:{ options: () -> {'isBeingEdit': $scope.isBeingEdit}}
        ).then (ignoreChanges) ->
          resolve(ignoreChanges)
          if ignoreChanges
            $scope.isBeingEdit = []
      else resolve(true)
  $rootScope.unsavedChangesDialog = $scope.unsavedChangesDialog


  # document edition observation
  $scope.$on('documentEntityIsBeingEdited', (evt, data) ->
    if data.isEdit && ($scope.isBeingEdit.some (field) -> field == data.field)
      return
    if data.isEdit
      $scope.isBeingEdit.push(data.field)
    else
      $scope.isBeingEdit = $scope.isBeingEdit.filter (field) -> field != data.field
  )

  $scope.$on('documentEntity_dropChanges', (event, data)->
    if data
      $scope.isBeingEdit = []
  )

  $scope.emitDeEditEvent = (field, isEdit) ->
    $scope.$broadcast('documentEntityIsBeingEdited', {field: field, isEdit: isEdit})

  $scope.addToCS = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-to-cs/add-to-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {
            'recordType': 'DE',
            'recordId':   $scope.documentEntity.documentId
          }
      }
    )

  $scope.requestAccess = (row) ->
    $scope.user = JSON.parse(localStorage.getItem('miaMe')).data[0]
    alertify.confirm( 'The content you are trying to access is private. Would you like to request access to it?', (->
      # open modal for request
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-request-access/request-access.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"uploadId": row.documentId, "owner": row.owner}
          }
      ).then (data) ->
        deUrl = "#{window.location.protocol}//#{window.location.host}/Mia/index.html#/mia/document-entity/#{data.uploadId}"
        shareUrl = "#{ENV.archiveBasePath}shareWithUsersByDocument/#{data.uploadId}/false"

        composeMessageData = {}
        composeMessageData.to = data.recipient
        composeMessageData.messageSubject = "ACCESS CONTENT REQUEST"
        #composeMessageData.messageText = "<br/>#{$scope.user.firstName} #{$scope.user.lastName} (#{$scope.user.account}) - asked to access your private content:<br/><br/>" + data.msg + "<br/><a href=#{deUrl}>#{deUrl}</a><br/>To give him/her access click <a onClick=\"sendApprovalRequest('#{shareUrl}', '#{$scope.user.account}', '#{data.recipient}', '#{deUrl}', 'document', '#{row.documentId}', '#{row.deTitle}')\">here</button></a> otherwise ignore the request"
        composeMessageData.messageText = "<br/>#{$scope.user.firstName} #{$scope.user.lastName} asked to access your private content.<br/><br/>Request Message:<br/>-<br/>" + data.msg + "<br/>-<br/>The document can be found here: <br/><a href=#{deUrl}>#{deUrl}</a><br/><br/><a onClick=\"sendApprovalRequest('#{shareUrl}', '#{$scope.user.account}', '#{data.recipient}', '#{deUrl}', 'document', '#{row.documentId}', '#{row.deTitle}')\">CLICK HERE TO APPROVE THE REQUEST</button></a><br/>Otherwise ignore or delete this message."
        composeMessageData.account = $scope.user.account
        messagingFactory.sendMessage(composeMessageData).then (resp) ->
          if resp.status == 'ok'
            alertify.success("Your message has been sent!")
      ), ->
    alertify.error 'Abort')
