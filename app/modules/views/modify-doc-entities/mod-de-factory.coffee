modDeServiceApp = angular.module 'modDeServiceApp',[]

modDeServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
modDeServiceApp.factory 'modDeFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  modDeBasePath = ENV.modDeBasePath
  documentBasePath = ENV.documentBasePath
  peopleDeBasePath = ENV.peopleDeBasePath
  placeDeBasePath = ENV.placeDeBasePath
  topicBasePath = ENV.topicBasePath
  biblioBasePath = ENV.biblioBasePath
  addPeopleBasePath = ENV.addPeopleBasePath
  factory =

    ##############
    # Trascription
    ##############
    getTranscription: (docId) ->
      return $http.get(modDeBasePath+'findDocumentTranscriptions/'+docId).then (resp) ->
        resp.data

    editTranscription: (transcriptions) ->
      return $http.post(modDeBasePath+'modifyTranscriptions/', transcriptions).then (resp)->
        resp.data

    addTranscription: (transcriptions) ->
      return $http.post(documentBasePath+'saveOrModifyTranscriptions/', transcriptions).then (resp)->
        resp.data

    ##############
    # Synopsis
    ##############
    getSynopsis: (docId)->
      return $http.get(modDeBasePath+'findSynopsis/'+docId).then (resp)->
        resp.data

    getChineseSynopsis: (documentId)->
      return $http.get(modDeBasePath+'findChineseSynopsis/' + documentId).then (response)->
        response.data

    saveSynopsis: (synopsis)->
      return $http.post(modDeBasePath+'modifySynopsis/', synopsis).then (resp)->
        resp.data

    saveChineseSynopsis: (synopsis)->
      return $http.post(modDeBasePath+'modifyChineseSynopsis/', synopsis).then (resp)->
        resp.data

    ##############
    # Description
    ##############
    getBasicDescription: (docId)->
      return $http.get(modDeBasePath+'/findBasicDescription/'+docId).then (resp)->
        resp.data

    saveBasicDescription: (description)->
      return $http.post(modDeBasePath+'modifyBasicDescription/', description).then (resp)->
        resp.data

    ##############
    # Typology
    ##############
    findDocTypologies: (category)->
      return $http.get(documentBasePath+'findDocumentTypologiesByCategory/'+category).then (resp)->
        resp.data

    ##############
    # Languages
    ##############
    getAllLang: () ->
      return $http.get(documentBasePath+'findLanguages/').then (resp) ->
        resp.data

    getDocLang: (docId) ->
      return $http.get(modDeBasePath+'findDocumentLanguages/'+docId).then (resp)->
        resp.data

    editDocLang: (params) ->
      return $http.post(modDeBasePath+'modifyDocumentLanguages/', params).then (resp)->
        resp.data

    ##############
    # People
    ##############
    findDocumentPeople: (docId, cat) ->
      return $http.get(peopleDeBasePath+'findDocumentPeople/'+docId+'/'+cat).then (resp)->
        resp.data

    getRelatedPeople: (docId) ->
      return $http.get(documentBasePath+'findPeopleRelatedtoDocument/'+docId).then (resp)->
        resp.data
    
    addRelatedPeople: (person) ->
      return $http.post(documentBasePath+'addPeopleRelatedToDocument/', person).then (resp)->
        resp.data

    editRelatedPeople: (person) ->
      return $http.post(documentBasePath+'modifyPeopleRelatedToDocument/', person).then (resp)->
        resp.data
    
    deleteRelatedPeople: (person) ->
      return $http.post(documentBasePath+'deletePeopleRelatedToDocument/', person).then (resp)->
        resp.data
    

    deletePerson: (params)->
      return $http.post(peopleDeBasePath+'deletePeopleForDoc/', params).then (resp)->
        resp.data

    editPerson: (params)->
      return $http.post(peopleDeBasePath+'modifyPeopleForDoc/', params).then (resp)->
        resp.data

    addPerson: (params)->
      return $http.post(addPeopleBasePath+'addPeopleForDoc/', params).then (resp)->
        resp.data

    ##############
    # Place
    ##############
    findDocumentPlace: (docId, cat) ->
      return $http.get(placeDeBasePath+'findDocumentPlace/'+docId+'/'+cat).then (resp)->
        resp.data

    deleteDocumentPlace: (params)->
      return $http.post(placeDeBasePath+'deletePlaceForDoc/', params).then (resp)->
        resp.data

    editDocumentPlace: (params)->
      return $http.post(placeDeBasePath+'modifyPlaceForDoc/', params).then (resp)->
        resp.data

    addDocumentPlace: (params)->
      return $http.post(placeDeBasePath+'addPlaceForDoc/', params).then (resp)->
        resp.data
    ##############
    # Topics
    ##############
    getDocumentTopics: (docId) ->
      return $http.get(topicBasePath+'findTopicPlaceForDoc/'+docId).then (resp)->
        resp.data

    editDocumentTopic: (params) ->
      return $http.post(topicBasePath+'modifyTopicPlaceForDoc/', params).then (resp)->
        resp.data

    deleteDocumentTopic: (params) ->
      return $http.post(topicBasePath+'deleteTopicPlaceForDoc/', params).then (resp)->
        resp.data

    addDocumentTopic: (params) ->
      return $http.post(topicBasePath+'addTopicPlaceForDoc/', params).then (resp)->
        resp.data
   
    ##############
    # Biblio references
    ##############
    getBiblioRef: (docId) ->
      return $http.get(biblioBasePath+'findDocumentBiblios/'+docId).then (resp)->
        resp.data

    editBiblioRef: (params) ->
      return $http.post(biblioBasePath+'modifyBiblioForDoc/', params).then (resp)->
        resp.data

    deleteBiblioRef: (params) ->
      return $http.post(biblioBasePath+'deleteBiblioForDoc/', params).then (resp)->
        resp.data

    addBiblioRef: (params) ->
      return $http.post(biblioBasePath+'addBiblioForDoc/', params).then (resp)->
        resp.data
    ##############
    # Related documents
    ##############
    getRelatedDocuments: (docId) ->
      return $http.get(modDeBasePath+'findRelatedDocuments/'+docId).then (resp)->
        resp.data

    saveRelatedDocuments: (params) ->
      return $http.post(modDeBasePath+'modifyRelatedDocuments/', params).then (resp)->
        resp.data
        
    ##############
    # De images
    ##############
    editDocImages: (params) ->
      return $http.post(documentBasePath+'modifyImages/', params).then (resp)->
        resp.data
  return factory
