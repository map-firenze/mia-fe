browseArchiveApp = angular.module 'browseArchiveApp',[
    'miaApp'
    'translatorApp'
    'commonServiceApp'
    'ui.router'
    'ui.bootstrap'
    'ajoslin.promise-tracker'
    'ngDraggable'
    'browseArchiveFactoryApp'
  ]


browseArchiveApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.browseArchive',
        url: '/browse-selected-location?type&id&page'
        views:
          'content':
            controller: 'browseArchiveController'
            templateUrl: 'modules/views/browse-archive/browse-archive.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

browseArchiveApp.controller 'browseArchiveController', (commonsFactory, $scope, $rootScope, $state, $window, $http, volumeInsertDescrFactory, searchDeService, browseArchiveFactory, $location, $timeout) ->
  $rootScope.pageTitle  = "Browse Volume/Insert"
  $scope.repository     = undefined
  $scope.currentObj     = {type: '', id: '', data: null}
  $scope.numberedImages = []
  $scope.notNumberedAes = []
  $scope.thumbCount     = 6
  $scope.loading        = false
  $scope.loadingBAAL    = true
  $scope.user = {}
  $scope.pagination     = {
    currentPage: 1,
    totalItems: 1,
    maxSize: 5,
    perPage: 5
  }

  initMainRepo = () ->
    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      $scope.repository = mainRepo

  initParams = () ->
    $scope.pagination.currentPage = $scope.$stateParams.page || 1
    $location.search('page', $scope.pagination.currentPage)

  initUserData = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      $scope.user = me.data[0]

  setCurrentObj = () ->
    $scope.currentObj.type = $scope.$stateParams.type
    $scope.currentObj.id   = $scope.$stateParams.id

  initData = () ->
    initMainRepo()
    initParams()
    initUserData()
    return unless $scope.$stateParams.type && $scope.$stateParams.id
    setCurrentObj()

    if $scope.currentObj.type == 'vol'
      # TODO: need $q.all
      $scope.loading = true
      volumeInsertDescrFactory.findVolumeDescription($scope.currentObj.id).then (resp) ->
        $scope.currentObj.data = resp.data.data

      browseArchiveFactory.findVolumeWithNumbImages($scope.currentObj.id, 1, $scope.thumbCount).then (resp) ->
        if resp.data.thumbsFiles
          $scope.numberedImages = resp.data.thumbsFiles.filter (image) -> $scope.numberedImagesPrivacyFilter(image)
          console.log($scope.numberedImages)
        $scope.loadingBAAL    = false


      browseArchiveFactory.findAeWithNotNumbImagesByVol($scope.currentObj.id, $scope.pagination.currentPage, $scope.pagination.perPage).then (resp) ->
        $scope.loading = false
        if resp.data
          $scope.notNumberedAes = resp.data.aentities if resp.data.aentities
          $scope.pagination.totalItems = resp.data.count
          definitionPrivacy($scope.notNumberedAes)

    else if $scope.currentObj.type == 'ins'
      # TODO: need $q.all
      $scope.loading = true
      volumeInsertDescrFactory.findInsertDescription($scope.currentObj.id).then (resp) ->
        $scope.currentObj.data = resp.data.data

      browseArchiveFactory.findInsertWithNumbImages($scope.currentObj.id, 1, $scope.thumbCount).then (resp) ->
        if resp.data.thumbsFiles
          $scope.numberedImages = resp.data.thumbsFiles.filter (image) -> $scope.numberedImagesPrivacyFilter(image)
          console.log($scope.numberedImages)
        $scope.loadingBAAL    = false

      browseArchiveFactory.findAeWithNotNumbImagesByIns($scope.currentObj.id, $scope.pagination.currentPage, $scope.pagination.perPage).then (resp) ->
        $scope.loading = false
        if resp.data
          if resp.data.aentities
            $scope.notNumberedAes = resp.data.aentities.filter (ae) -> $scope.noNumbAesPrivacyFilter(ae)
          $scope.pagination.totalItems = resp.data.count
          definitionPrivacy($scope.notNumberedAes)

  initData()

  $scope.pageChange = () ->
    $location.search('page', $scope.pagination.currentPage)
    # TODO: need separate methods
    $timeout -> initData()

  $scope.gotoSingle = (uploadId, uploadFileId) ->
    params = {}
    params.aeId = uploadId
    params.fileId = uploadFileId || 0
    params.page = 1
    $state.go 'mia.singleupload', params

  $scope.gotoBrowseArcLoc = () ->
    $state.go 'mia.browseArchLocation', {type: $scope.currentObj.type, id: $scope.currentObj.id}

  definitionPrivacy = (aes) ->
    for archive in aes
      publicAe = true
      if archive.thumbsFiles
        for thumbFile in archive.thumbsFiles
          if thumbFile.privacy is 1
            publicAe = false
            break
      if publicAe
        archive.privacy = "Public"
      else
        archive.privacy = "Private"

  $scope.noNumbAesPrivacyFilter = (ae) ->
    if $scope.isObjectOwnerOrAdmin(ae.owner, ae.privacy)
      return true
    if (ae.thumbsFiles.some (files) -> files.privacy == 0 || files.privacy == null)
      return true
    return false

  $scope.numberedImagesPrivacyFilter = (image) ->
    return $scope.isObjectOwnerOrAdmin(image.owner, image.privacy)

  $scope.noNumbImagePrivacyFilter = (ae) ->
    return (image) ->
      return $scope.isObjectOwnerOrAdmin(ae.owner, image.privacy)

  $scope.isObjectOwnerOrAdmin = (owner, privacy) ->
    return $scope.user.account == owner || $scope.user.userGroups?.includes('ADMINISTRATORS') || privacy == 0
