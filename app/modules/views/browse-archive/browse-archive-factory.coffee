browseArchiveFactoryApp = angular.module 'browseArchiveFactoryApp', []

browseArchiveFactoryApp.factory 'browseArchiveFactory', ($http, ENV) ->
  bacPath = ENV.bacPath

  factory =

    findVolumeWithNumbImagesByFolio: (volId, folioNumber, perPage) ->
      $http.get(bacPath + 'findVolumeWithImages/' + volId + '?numbered=true&folioNumber=' + folioNumber + '&perPage=' + perPage).then (resp) ->
        resp

    findVolumeWithNumbImages: (volId, page, perPage) ->
      $http.get(bacPath + 'findVolumeWithImages/' + volId + '?numbered=true&page=' + page + '&perPage=' + perPage).then (resp) ->
        resp
##    TODO: not use
#    findVolumeWithNotNumbImages: (volId, perPage) ->
#      $http.get(bacPath + 'findVolumeWithImages/' + volId + '?numbered=false&page=1&perPage=' + perPage).then (resp) ->
#        resp

    findAeWithNotNumbImagesByVol: (volId, page, perPage) ->
      $http.get(bacPath + 'findVolumeAEsWithNotYetNumberedFolios/' + volId + '?page=' + page + '&perPage=' + perPage).then (resp) ->
        resp

    findInsertWithNumbImagesByFolio: (insId, folioNumber, perPage) ->
      $http.get(bacPath + 'findInsertWithImages/' + insId + '?numbered=true&folioNumber=' + folioNumber + '&perPage=' + perPage).then (resp) ->
        resp

    findInsertWithNumbImages: (insId, page, perPage) ->
      $http.get(bacPath + 'findInsertWithImages/' + insId + '?numbered=true&page=' + page + '&perPage=' + perPage).then (resp) ->
        resp
##    TODO: not use
#    findInsertWithNotNumbImages: (insId, page, perPage) ->
#      $http.get(bacPath + 'findInsertWithImages/' + insId + '?numbered=false&page=' + page + '&perPage=' + perPage).then (resp) ->
#        resp

    findAeWithNotNumbImagesByIns: (insId, page, perPage) ->
      $http.get(bacPath + 'findInsertAEsWithNotYetNumberedFolios/' + insId + '?page=' + page + '&perPage=' + perPage).then (resp) ->
        resp

  return factory
