browseArchLocationApp = angular.module 'browseArchLocationApp',[
    'miaApp'
    'translatorApp'
    'commonServiceApp'
    'ui.router'
    'modalUtils'
    'ui.bootstrap'
    'ngFileUpload'
    'ngDraggable'
    'ladda'
    'selectDeApp'
    'documentEntityApp'
    'ajoslin.promise-tracker'
    'replaceImgApp'
    'delImgApp'
    'rotateImgApp'
    'diffVersImageApp'
    'modCollationListApp'
    'browseArchiveFactoryApp'
    'imageCsListApp'
  ]

browseArchLocationApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.browseArchLocation',
        url: '/browse-archival-location?type&id&fileId&page&per',
        reloadOnSearch: false,
        views:
          'content':
            controller: 'browseArchLocationController'
            templateUrl: 'modules/views/browse-archive-loc/browse-archive-loc.html'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
]

browseArchLocationApp.controller 'browseArchLocationController', (commonsFactory, userServiceFactory, alertify, searchFactory, deFactory, converterFactory, $scope, $state, $sce, $rootScope, Upload, $timeout, $filter, modalFactory, $location, $window, $anchorScroll, $q, searchDeService, volumeInsertDescrFactory, browseArchiveFactory) ->
  $rootScope.pageTitle = "Browse Volume/Insert"
#  $scope.getArchiveTracker = true
  $scope.fixUploadTracker = false
  $scope.editMetaLoader = false
  $scope.metadataEdited = false
  $scope.errorDeleting = false
  $scope.errorMess = ''
  $scope.folio1 = {}
  $scope.folio2 = {}
  $scope.isAdmin = false
  $scope.me = {}
  $scope.wrongFiles = []
  $scope.selectedImages = {}
  $scope.lacunaTemplate = 'modules/views/browse-archive-loc/popover-template.html'

  $scope.random = Math.random()
  $scope.loadingFolioNumber = false
  $scope.goToFolioNumber = ''
  $scope.perPages = [
    {value: 15, name: '15'},
    {value: 30, name: '30'}
  ]

  $scope.visibility = {}

  $scope.repository     = undefined
  $scope.currentObj     = {type: '', id: '', data: null}
  $scope.numberedImages = []
  $scope.loadingImags   = false
  $scope.selectedImage  = null
  $scope.pagination     = {
    currentPage: 1,
    totalItems: 1,
    maxSize: 5,
    perPage: $scope.perPages[0]
  }

  initMainRepo = () ->
    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      $scope.repository = mainRepo

  setCurrentObj = () ->
    $scope.currentObj.type = $scope.$stateParams.type
    $scope.currentObj.id   = $scope.$stateParams.id

  initParams = () ->
    $scope.pagination.currentPage = $scope.$stateParams.page || 1

    if $scope.$stateParams.per
      per = _.find $scope.perPages, value: _.toInteger($scope.$stateParams.per)
      $scope.pagination.perPage = per || $scope.perPages[0]

    $location.search('page', $scope.pagination.currentPage)
    $location.search('per', $scope.pagination.perPage.value)

  loadImages = () ->
    if $scope.currentObj.type == 'vol'
      $scope.loadingImags = true
      volumeInsertDescrFactory.findVolumeDescription($scope.currentObj.id).then (resp) ->
        $scope.currentObj.data = resp.data.data

      browseArchiveFactory.findVolumeWithNumbImages($scope.currentObj.id, $scope.pagination.currentPage, $scope.pagination.perPage.value).then (resp) ->
        $scope.loadingImags = false
        if resp.data
          $scope.numberedImages        = resp.data.thumbsFiles
          $scope.pagination.totalItems = resp.data.count

          if $scope.$stateParams.fileId && $scope.numberedImages.length
            selectedImage = _.find $scope.numberedImages, uploadFileId: _.toInteger($scope.$stateParams.fileId) || $scope.numberedImages[0]
            $scope.selectedImage = selectedImage
            $scope.openViewer($scope.selectedImage)
          else
            $scope.selectedImage = $scope.numberedImages[0]
            $scope.openViewer($scope.selectedImage)

    else if $scope.currentObj.type == 'ins'
      $scope.loadingImags = true
      volumeInsertDescrFactory.findInsertDescription($scope.currentObj.id).then (resp) ->
        $scope.currentObj.data = resp.data.data

      browseArchiveFactory.findInsertWithNumbImages($scope.currentObj.id, $scope.pagination.currentPage, $scope.pagination.perPage.value).then (resp) ->
        $scope.loadingImags = false
        if resp.data
          $scope.numberedImages        = resp.data.thumbsFiles
          $scope.pagination.totalItems = resp.data.count

          if $scope.$stateParams.fileId && $scope.numberedImages.length
            selectedImage = _.find($scope.numberedImages, uploadFileId: _.toInteger($scope.$stateParams.fileId)) || $scope.numberedImages[0]
            $scope.selectedImage = selectedImage
            $scope.openViewer($scope.selectedImage)
          else
            $scope.selectedImage = $scope.numberedImages[0]
            $scope.openViewer($scope.selectedImage)

  initData = () ->
    initMainRepo()
    initParams()
    return unless $scope.$stateParams.type && $scope.$stateParams.id
    setCurrentObj()
    loadImages()

  initData()

  $scope.goToFolio = (goToFolioNumber) ->
    if $scope.numberedImages
      for thumbFile in $scope.numberedImages
        for folio in thumbFile.folios
          if folio.folioNumber == goToFolioNumber
            selectedImage = thumbFile
            break
        if selectedImage
          break
    if selectedImage
      $scope.selectedImage = selectedImage
      $scope.openViewer($scope.selectedImage)

    else if $scope.currentObj.type == 'vol'
      $scope.loadingFolioNumber = true
      browseArchiveFactory.findVolumeWithNumbImagesByFolio($scope.currentObj.id, goToFolioNumber, $scope.pagination.perPage.value).then (resp) ->
        $scope.loadingFolioNumber = false
        if resp.data
          if resp.data.status is "ko"
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = $filter('translate')("modal.FOLIOMISSING")
            modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          else
            $scope.numberedImages         = resp.data.thumbsFiles
            $scope.pagination.totalItems  = resp.data.count
            $scope.pagination.currentPage = resp.data.currentPage
            for thumbFile in $scope.numberedImages
              for folio in thumbFile.folios
                if folio.folioNumber == goToFolioNumber
                  selectedImage = thumbFile
                  break
              if selectedImage
                break
            if selectedImage
              $scope.selectedImage = selectedImage
              $scope.openViewer($scope.selectedImage)

    else if $scope.currentObj.type == 'ins'
      $scope.loadingFolioNumber = true
      browseArchiveFactory.findInsertWithNumbImagesByFolio($scope.currentObj.id, goToFolioNumber, $scope.pagination.perPage.value).then (resp) ->
        $scope.loadingFolioNumber = false
        if resp.data
          if resp.data.status is "ko"
            pageTitle = $filter('translate')("modal.ERROR")
            messagePage = $filter('translate')("modal.FOLIOMISSING")
            modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          else
            $scope.numberedImages         = resp.data.thumbsFiles
            $scope.pagination.totalItems  = resp.data.count
            $scope.pagination.currentPage = resp.data.currentPage
            for thumbFile in $scope.numberedImages
              for folio in thumbFile.folios
                if folio.folioNumber == goToFolioNumber
                  selectedImage = thumbFile
                  break
              if selectedImage
                break
            if selectedImage
              $scope.selectedImage = selectedImage
              $scope.openViewer($scope.selectedImage)

  $scope.changePerPage = () ->
    $location.search('per', $scope.pagination.perPage.value)
    loadImages()

  $scope.pageChange = () ->
    $location.search('page', $scope.pagination.currentPage)
    loadImages()

  $scope.goToAe = (image, modal) ->
    params = {}
    params.aeId   = image.uploadId
    params.fileId = image.uploadFileId
    params.modal = modal
##    params.page = 1
    $state.go 'mia.singleupload', params

  ###########################
  # Show different versions of the same image
  ###########################

  $scope.showDiffVersions = (image)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-diff-versions-of-image/diff-versions-of-image.html'
      controller:'modalIstanceController'
      resolve: {
        options:
          -> {"image": image}
      }
    )

  ###########################
  # Show child documents
  ###########################

  $scope.showCollationList = (image)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-collation-list/modal-collation-list.html'
      controller:'modalIstanceController'
      resolve: {
        options:
          -> {"image": image}
      }
    )

  $scope.goToFromPopup = (image, type) ->
    collation = image.collations[0]
    if type == 'doc'
      $state.go('mia.modifyDocEnt', { 'docId': collation.childDocumentId })
    else if type == 'bal'
      data = {fileId: undefined, page: undefined, per: undefined}
      data.type = if collation.parentInsertId then 'ins' else 'vol'
      data.id   = if collation.parentInsertId then collation.parentInsertId else collation.parentVolumeId
      $state.go 'mia.browseArchLocation', data, {reload: true}

  $scope.showUnsureDesc = (collation) ->
    pageTitle = 'Unsure Description'
    message   = collation.description
    modalFactory.openMessageModal(pageTitle, message, false)

  ###########################
  # Show case studies list
  ###########################

  $scope.showCsList = (image)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-image-cs-list/image-cs-list.html'
      controller:'modalIstanceController'
      resolve: {
        options:
          -> {"image": image}
      }
    )

  ###########################
  # OLD LOGIC WITHOUT ARCHIVE
  ###########################

  $scope.closeAllPanel = ()->
    $scope.visibility.showViewer = false
    $scope.visibility.showAddFile = false
    $scope.visibility.showMeta = false
    $scope.visibility.showUpload = false
    $scope.visibility.showReorder = false
    $scope.visibility.showReplaceFile = false
    $scope.visibility.showRotation = false

  $scope.setVisibility = (toOpen)->
    switch toOpen
      when "showViewer"
        $scope.visibility.showViewer = !$scope.visibility.showViewer
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showAddFile"
        $scope.visibility.showAddFile = !$scope.visibility.showAddFile
        $scope.visibility.showViewer = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showMeta"
        $scope.visibility.showMeta = !$scope.visibility.showMeta
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showUpload"
        $scope.visibility.showUpload = !$scope.visibility.showUpload
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showReorder = false
        $scope.visibility.showRotation = false
      when "showReorder"
        $scope.visibility.showReorder = !$scope.visibility.showReorder
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showRotation = false
      when "showRotation"
        $scope.visibility.showRotation = !$scope.visibility.showRotation
        $scope.visibility.showViewer = false
        $scope.visibility.showAddFile = false
        $scope.visibility.showMeta = false
        $scope.visibility.showUpload = false
        $scope.visibility.showReorder = false

  $scope.closeAllPanel()
  $scope.performingJobTracker = false

#  uploadId = $scope.$stateParams.aeId
#  uploadFileId = $scope.$stateParams.fileId || 0
#  $scope.thumbNumb = 15
#  pageNum = $scope.$stateParams.page || 1
#  $scope.currentPage = $scope.$stateParams.page || 1

#  initParams = () ->
#    uploadId = $scope.$stateParams.aeId
#    uploadFileId = $scope.$stateParams.fileId || 0
#    $scope.thumbNumb = 15
#    pageNum = $scope.$stateParams.page || 1
#    $scope.currentPage = $scope.$stateParams.page || 1
  
  userServiceFactory.getMe().then (resp)->
    if resp.status is 'ok'
      $scope.me=resp.data[0]
      if _.find($scope.me.userGroups, (role) -> return role is "ADMINISTRATORS")
        $scope.isAdmin = true

#  getArchive = () ->
#    commonsFactory.getMySingleupload(uploadId, 1000, pageNum).then (first) ->
#      $scope.firstImage = commonsFactory.findImageById(first.aentity.thumbsFiles, uploadFileId)
#      if !$scope.firstImage
#        uploadFileId = first.aentity.thumbsFiles[0].uploadFileId
#      else
#        pageNum = Math.trunc($scope.firstImage.imageOrder / $scope.thumbNumb) + 1
#        $scope.currentPage = pageNum
#      commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, pageNum).then (resp) ->
#        commonsFactory.recordUploadAction($state.params.aeId, 'VIEW')
#        $scope.archive = resp.aentity
##        $scope.openViewer(commonsFactory.findImageById($scope.archive.thumbsFiles, uploadFileId))
#        # console.log $scope.archive
#        # console.log $scope.archive.volume.volumeName
#        # console.log $scope.archive.insert.insertName
#        $scope.archive.privacy = checkPrivacy()
#        $scope.totalItems = $scope.archive.pagination.totalPagesForSingleAe * 10 || 1
#        converterFactory.getReport().then (resp) ->
#          # console.log resp
#        angular.forEach($scope.archive.thumbsFiles, (image)->
#          deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
#            insertFolioInfo(resp, image)
#          )
##        $scope.getArchiveTracker = false

#  getArchive()

  $scope.$on('refresh-ae', (evt, args)->
#    initParams()
    refreshAECurrentPage())

  refreshAECurrentPage = () ->
#    $scope.getArchiveTracker = true
    commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage).then (resp) ->
      $scope.archive = resp.aentity
      $scope.archive.privacy = checkPrivacy()
      angular.forEach($scope.archive.thumbsFiles, (image)->
        deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
          insertFolioInfo(resp, image)
        )
#      $scope.getArchiveTracker = false
      $location.search('page', $scope.currentPage)

  $scope.pageChanged = ()->
    refreshAECurrentPage()

  $scope.showDocumentList = (uploadFileIdForDe)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-document-list/document-list.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"uploadFileId": uploadFileIdForDe}
      }
    ).then (selectedDeId) ->
      if selectedDeId
        $state.go('mia.modifyDocEnt', { 'docId':selectedDeId.documentId })

  ###########################
  # DOCUMENT VIEWER ROUTINES
  ###########################

  $scope.$on('openInViewer', (evt, args)->
    $scope.openViewer(args)
  )

  $scope.openViewer = (image)->
    return unless image
    $scope.closeAllPanel()
    actual = _.find $scope.numberedImages, (pic) -> pic.highlighted is true
    if actual
      actual.highlighted = false
    image.highlighted = true
    uploadFileId = image.uploadFileId
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+image.uploadFileId)
    $scope.setVisibility("showViewer")
    $location.search('fileId', image.uploadFileId)

  $scope.closeViewer = ()->
    $scope.setVisibility("showViewer")

  
  #########################
  # FIX UPLOAD ROUTINES
  #########################

  $scope.fixUpload = (uploadFileId) ->
    $scope.fixUploadTracker = true
    commonsFactory.fixUpload(uploadFileId).then (resp) ->
      $scope.fixUploadTracker = false
#      $scope.getArchiveTracker = true
      if resp.status = "ok"
        commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage).then (resp) ->
          $scope.archive = resp.aentity
          $scope.archive.privacy=checkPrivacy()
#          $scope.getArchiveTracker = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("fixUpload.ERROR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp

  #########################
  # REPLACE IMGS ROUTINES
  #########################
  # $scope.replaceModal = (uploadFileId)->
  #   modalFactory.openModal(
  #     templateUrl: 'modules/directives/modal-templates/modal-replace-img/replace-img.html'
  #     controller:'modalIstanceController'
  #     resolve:{
  #       options:
  #         -> {
  #           "uploadFileId": uploadFileId
  #           "uploadId": $state.params.aeId
  #           }
  #     }
  #   ).then () ->
  #     getArchive()

  #########################
  # ROTATE IMGS ROUTINES
  #########################
  # $scope.rotate = (uploadFile, degrees)->
  #   $location.search('fileId', uploadFile.uploadFileId)
  #   images = {}
  #   images[uploadFile.uploadFileId] = uploadFile
  #   modalFactory.openModal(
  #     templateUrl: 'modules/directives/modal-templates/modal-rotate-img/rotate-img.html'
  #     controller:'modalIstanceController'
  #     resolve:{
  #       options:
  #         -> {
  #           "uploadFiles": images
  #           "degrees": degrees
  #           }
  #     }
  #   ).then () ->
  #     getArchive()

  $scope.onDropComplete = (index, obj, evt, changePage) ->
#    console.log('position to move the img', index)
    actual = _.find $scope.archive.thumbsFiles, (pic) -> pic.moved is true
    if actual
      actual.moved = false
    switch index
      when 'previousPage'
        index = $scope.archive.thumbsFiles[0].imageOrder-1
        toPage = -1
      when 'nextPage'
        index = $scope.archive.thumbsFiles[14].imageOrder+1
        toPage = 1
      else
        toPage = 0

    $scope.performingJobTracker = true
    commonsFactory.changeOrder(obj.uploadFileId, index).then (resp) ->
      if resp.status is "ok"
        $scope.currentPage = parseInt($scope.currentPage) + toPage
        commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage).then (resp) ->
          $scope.archive = resp.aentity
          angular.forEach($scope.archive.thumbsFiles, (image)->
            deFactory.getFolioByUploadFileId(image.uploadFileId).then (resp)->
              insertFolioInfo(resp, image)
            )
          movedPic = _.find $scope.archive.thumbsFiles, (pic) -> pic.uploadFileId is obj.uploadFileId
          if movedPic
            movedPic.moved = true
          $scope.performingJobTracker = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("modal.REORDERR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->

  # $scope.setSingleDocPrivacy = (uploadFileId, privacyValue) ->
  #   $scope.performingJobTracker = true
  #   commonsFactory.setSingleDocPrivacy(uploadFileId, privacyValue).then (resp)->
  #     if resp.status is "ok"
  #       findDocument(uploadFileId).privacy = resp.privacy
  #     if resp.status is "ko"
  #       pageTitle = $filter('translate')("modal.ERROR")
  #       messagePage = $filter('translate')("modal.PRIVACYERR")
  #       modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
  #     $scope.performingJobTracker = false
  #     $scope.archive.privacy = checkPrivacy()

  checkPrivacy = ()->
    publicAe = true
    if $scope.archive.thumbsFiles
      for thumbFile in $scope.archive.thumbsFiles
        if thumbFile.privacy is 1
          publicAe = false
          break
    if publicAe
      return "Public"
    else
      return "Private"

  #########################
  # ADD FILE/S ROUTINES
  #########################

  $scope.$on('invalid-files', (evt, args)->
    $scope.wrongFiles = args
  )
  
  #NEW alai
  $scope.invalidImages = []

  $scope.validatePicFiles = (image) ->
    defer = $q.defer()
    return null

  $scope.getFiles = () ->
    document.getElementById("file-upload").click()

  $scope.cancelAddFiles = () ->
    $scope.wrongFiles = []
    $scope.picFiles = []
    $scope.setVisibility("showAddFile")
    
  $scope.finishUpload = () ->
    commonsFactory.getMySingleupload(uploadId, $scope.thumbNumb, $scope.currentPage).then (resp) ->
      $scope.archive = resp.aentity
      $scope.currentPage = $scope.archive.pagination.totalPagesForSingleAe
      $scope.pageChanged()
      $scope.showAddFile = false
#      $scope.getArchiveTracker = false
      #$scope.setVisibility("showAddFile")
      $scope.setVisibility("showViewer")
#      getArchive()

  # multiple file upload
  $scope.addFileToAE = (files) ->
    $scope.uploading = true
    $scope.progress = 0
    $location.hash('top')
    $anchorScroll()
    if files and files.length
      #for file in files
        #alert(file.name)
        #file.safeName = file.name.replace(new RegExp("\\&", 'gi'), '_')
      $rootScope.isBusy = true
      Upload.upload(
        url: 'json/archive/addfilestoAE/'+uploadId
        data:
          files: files).then ((response) ->
            #file uploaded ok
            $scope.result = response.data
            $scope.uploading = false
            $rootScope.isBusy = false
            $scope.finishUpload()
          ), ((response) ->
            #file uploaded ko
            if response.status > 0
              $scope.errorMsg = response.status + ': ' + response.data
          ), (evt) ->
            #file uploaded progress
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
            $rootScope.$broadcast('progress', $scope.progress)
    # to do find a better solution to clean the uploaded files

  #############################
  # EDIT DOCUMENT META ROUTINES
  #############################

  $scope.$on('editMetaDaraDiffVers', (evt, args)->
    $scope.editImageMetadata(args)
  )

  $scope.editImageMetadata = (uploadFileId) ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFileId": uploadFileId,
            "volumeId": $scope.currentObj.data.volumeId,
            "insertId": $scope.currentObj.data.insertId || null
        }
      }
    ).then (editImageMetadataIsChange) ->
      if editImageMetadataIsChange
        $state.reload()

  insertFolioInfo = (folioInfo, image) ->
    image.folios=[]
    if folioInfo.status is "ok"
      image.folios[0] = folioInfo.data[0]
      if folioInfo.data[1]
        image.folios[1] = folioInfo.data[1]
    else
      image.folios[0] = {}
      image.folios[0].folioId= ""
      image.folios[0].folioNumber = ""
      image.folios[0].uploadFileId = image.uploadFileId

      image.folios[1] = {}
      image.folios[1].folioId= ""
      image.folios[1].folioNumber = ""
      image.folios[1].uploadFileId = image.uploadFileId

  findDocument = (uploadId) ->
    for file in $scope.numberedImages
      if file.uploadFileId is uploadId
        return file
        
  ####################################
  # DOWNLOAD AND DELETE FILE ROUTINES
  ####################################

  $scope.checkDownload = (owner, uploadFileId) ->
    if $scope.me.account == owner
      commonsFactory.downloadDocument(uploadFileId).then (resp)->
    else
      alertify.alert 'You are not allowed to download an image who belongs to someone else'

  $scope.downloadDocument = (uploadFileId) ->
    commonsFactory.downloadDocument(uploadFileId).then (resp)->

  # $scope.deleteDocument = (uploadFileId) ->
  #   if($scope.archive.thumbsFiles.length>1)
  #     pageTitle = $filter('translate')("modal.WARNING")
  #     messagePage = $filter('translate')("modal.DELETINGIMG")
  #     modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (answer) ->
  #       if answer
  #         presentDocEn = []
  #         $scope.performingJobTracker = true
  #         commonsFactory.deleteDocument(uploadFileId).then (resp)->
  #           if resp.status is "ok"
  #             indexCounter=0
  #             for file in $scope.archive.thumbsFiles
  #               if file.uploadFileId is uploadFileId
  #                 $scope.archive.thumbsFiles.splice(indexCounter, 1)
  #                 break
  #               else
  #                 indexCounter++
  #             #if a document is deleted all the panels must be closed
  #               $scope.closeAllPanel()
  #           else
  #             pageTitle = $filter('translate')("modal.ERROR")
  #             messagePage = $filter('translate')("modal.DELDOCERR") + ': ' +resp.message
  #             modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (confirmation) ->
  #               console.log(resp)
  #               if resp.data.documentAE.documentIds.length
  #                 modalFactory.openModal(
  #                   templateUrl: 'modules/directives/modal-templates/modal-del-img/del-img.html'
  #                   controller:'modalIstanceController'
  #                   resolve:{
  #                     options:
  #                       -> {"imageId": uploadFileId}
  #                     }
  #                 ).then (selectedRepo) ->
  #           if not $scope.archive.thumbsFiles.length
  #             $state.go('mia.archive')
  #           $scope.performingJobTracker = false
  #   else
  #     pageTitle = $filter('translate')("modal.WARNING")
  #     messagePage = $filter('translate')("modal.LASTIMG")
  #     modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (answer) ->

  $scope.selectImage = (image, evt) ->
    if $scope.visibility.showRotation
      if evt.shiftKey
        if typeof $scope.selectedImages[image.uploadFileId] != typeof undefined
          delete $scope.selectedImages[image.uploadFileId]
        else if JSON.stringify($scope.selectedImages).match(/"first":1/g)
          $scope.selectedImages[image.uploadFileId] = image
        else
          $scope.selectedImages[image.uploadFileId] = image
          $scope.selectedImages[image.uploadFileId].first = 1
      else
        if Object.keys($scope.selectedImages).length == 1
          if typeof $scope.selectedImages[image.uploadFileId] != typeof undefined
            delete $scope.selectedImages[image.uploadFileId]
          else
            $scope.selectedImages = {}
            $scope.selectedImages[image.uploadFileId] = image
        else
          $scope.selectedImages = {}
          $scope.selectedImages[image.uploadFileId] = image

  $scope.rotateSelectedImages = (degrees) ->
    #$location.search('fileId', uploadFile.uploadFileId)
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-rotate-img/rotate-img.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFiles": $scope.selectedImages
            "degrees": degrees
          }
      }
    ).then () ->
#      getArchive()
#    console.log degrees

  $scope.selectAllImages = (images) ->
#    console.log 1
    $scope.selectedImages = {}
    images.forEach (image) ->
      if JSON.stringify($scope.selectedImages).match(/"first":1/g)
        image.first = 1
      $scope.selectedImages[image.uploadFileId] = image

  #########################
  # EDIT METADATA ROUTINES
  #########################

  $scope.dataEdited = ()->
    $scope.metadataEdited = true

  $scope.addRepo = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
      controller:'modalIstanceController'
    ).then (selectedRepo) ->
      if !angular.equals(selectedRepo, $scope.archiveToEdit.repository)
        $scope.archiveToEdit.repository = selectedRepo
        $scope.archiveToEdit.collection = {}
        $scope.archiveToEdit.series = {}
        $scope.archiveToEdit.volume = {}
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addCollection = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.repository.repositoryId}
        }
    ).then (selectedCollection) ->
#      console.log selectedCollection
      if $scope.archiveToEdit.collection.collectionId isnt selectedCollection.collectionId
        $scope.archiveToEdit.collection = selectedCollection
        $scope.archiveToEdit.series = {}
        $scope.archiveToEdit.volume = {}
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addVolume = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-volume/add-volume.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.collection.collectionId}
        }
    ).then (selectedVolume) ->
      if !angular.equals($scope.archiveToEdit.volume, selectedVolume)
        $scope.archiveToEdit.volume = selectedVolume
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addSeries = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.collection.collectionId}
        }
    ).then (selectedSeries) ->
      if !angular.equals($scope.archiveToEdit.series, selectedSeries)
        $scope.archiveToEdit.series = selectedSeries
        $scope.archiveToEdit.insert = {}
        $scope.metadataEdited = true

  $scope.addInsert = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-insert/add-insert.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $scope.archiveToEdit.volume.volumeId}
        }
    ).then (selectedInsert) ->
      if !angular.equals($scope.archiveToEdit.insert, selectedInsert)
        $scope.archiveToEdit.insert = selectedInsert
        $scope.metadataEdited = true

  $scope.canISave= () ->
    return $scope.archiveToEdit.repository.repositoryName and
    $scope.archiveToEdit.collection.collectionName and
    $scope.archiveToEdit.volume.volumeName and
    $scope.metadataEdited and
    $scope.archiveToEdit.aeName and
    $scope.archiveToEdit.aeName.length<=254

  $scope.finishEditMeta = () ->
    $scope.editMetaLoader = true

    params = {}
    params.aeName = $scope.archiveToEdit.aeName or ''
    params.aeDescription = $scope.archiveToEdit.aeDescription or ''
    params.repository = {}
    params.repository = {}
    params.repository.repositoryId = $scope.archiveToEdit.repository.repositoryId or ''
    params.repository.repositoryName = $scope.archiveToEdit.repository.repositoryName or ''
    params.repository.repositoryAbbreviation = $scope.archiveToEdit.repository.repositoryAbbreviation or ''
    params.repository.repositoryDescription = $scope.archiveToEdit.repository.repositoryDescription or ''
    params.repository.repositoryLocation = $scope.archiveToEdit.repository.repositoryLocation or ''
    params.repository.repositoryCity = $scope.archiveToEdit.repository.repositoryCity or ''
    params.repository.repositoryCountry = $scope.archiveToEdit.repository.repositoryCountry or ''

    params.collection = {}
    params.collection.collectionId = $scope.archiveToEdit.collection.collectionId or ''
    params.collection.collectionName = $scope.archiveToEdit.collection.collectionName or ''
    params.collection.collectionAbbreviation = $scope.archiveToEdit.collection.collectionAbbreviation or ''
    params.collection.collectionDescription = $scope.archiveToEdit.collection.collectionDescription or ''

    params.series = {}
    params.series.seriesId = $scope.archiveToEdit.series.seriesId or ''
    params.series.seriesName  = $scope.archiveToEdit.series.seriesName or ''
    params.series.seriesSubtitle = $scope.archiveToEdit.series.seriesSubtitle or ''

    params.volume = {}
    params.volume.volumeId = $scope.archiveToEdit.volume.volumeId or ''
    params.volume.volumeName = $scope.archiveToEdit.volume.volumeName or ''

    params.insert = {}
    params.insert.insertId = $scope.archiveToEdit.insert.insertId or ''
    params.insert.insertName = $scope.archiveToEdit.insert.insertName or ''

    #$scope.showMeta = !$scope.showMeta
    commonsFactory.updateMetaDataAE(uploadId, params).then (resp) ->
      if resp.status is "ok"
#        getArchive()
        $scope.editMetaLoader = false
      if resp.status is "ko"
        pageTitle = $filter('translate')("modal.ERROR")
        messagePage = $filter('translate')("modal.EDITMETAERR")
        modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
          return resp

  $scope.closeEditMeta = () ->
    $scope.setVisibility("showMeta")

  $scope.numberedImagesPrivacyFilter = (image) ->
    return $scope.isObjectOwnerOrAdmin(image.owner, image.privacy)

  $scope.isObjectOwnerOrAdmin = (owner, privacy) ->
    return $scope.me.account == owner || $scope.me.userGroups?.includes('ADMINISTRATORS') || privacy == 0


  #----------------------------------------
  # Show Fullscreen
  #----------------------------------------
  $scope.transcriptions = []
  transWindow=undefined

  reloadPage=()->
    $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})

  extractDomain = (url) ->
    domain = undefined
    if url.indexOf('://') > -1
      domain = url.split('/')[2]
    else
      domain = url.split('/')[0]
    domain='http://'+domain

  $scope.openTranscriptor= () ->
    uploadFileId = $location.search().fileId
    #volumeId = $location.search().id
    volumeId = $scope.currentObj.data.volumeId
    workUrl = $state.href($state.current.name, $state.params, {absolute: true})
    transWindow=$window.open("#{extractDomain(workUrl)}/Mia/json/src/ShowArchivalLocationInManuscriptViewer/show?volumeId=#{volumeId}&fileId=#{uploadFileId}")
    transWindow.onbeforeunload=()->
      $timeout(reloadPage, 1000)

  $scope.synopsis = null
