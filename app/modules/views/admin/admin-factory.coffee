adminServiceApp = angular.module 'adminServiceApp',[]

adminServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
adminServiceApp.factory 'adminFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  adminBasePath = ENV.adminBasePath
  adminChangePassPath = ENV.adminChangePassPath

  factory =

    #-------------------
    # admin services
    #-------------------

    findUser: (params) ->
      return $http.get(adminBasePath+'findMiaUser/'+ params).then (resp) ->
        resp.data

    createUser: (params) ->
      return $http.post(adminBasePath+'createMiaUser/', params).then (resp) ->
        resp.data

    modifyUser: (params) ->
      return $http.post(adminBasePath+'modifyMiaUser/', params).then (resp) ->
        resp.data

    changePass: (params) ->
      return $http.post(adminChangePassPath+'changeUserPassword/', params).then (resp) ->
        resp.data

    searchUser: (account) ->
      return $http.get(adminBasePath+"getUsersByName/"+account).then (resp) ->
        resp.data

    findUserGroups: () ->
      return $http.get(adminBasePath+'findUserGroups/').then (resp) ->
        resp.data

  return factory
