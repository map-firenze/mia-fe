adminApp = angular.module 'adminApp',[
  'adminServiceApp'
  'personalProfileService'
  'changePassAdminApp'
  'modalUtils'
  'translatorAppEn'
]

adminApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.admin',
        url: '/admin'
        views:
          'content':
            controller: 'adminController'
            templateUrl: 'modules/views/admin/admin.html'

          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]

adminApp.controller 'adminController', (adminFactory, personalProfileFactory, messagingFactory, modalFactory, alertify, $scope, $rootScope, $state, $window) ->
  #
  #GENERAL DATA
  #

  #data for pagination
  $rootScope.pageTitle = "Users administration"
  $scope.tabs = [{"name":"search", "value":true}, {"name":"create", "value":false}, {"name":"modify", "value":false}, {"name":"compose", "value":false}]

  #useful variables
  $scope.searchUserAccount = undefined
  $scope.signedUser = undefined
  $scope.accountInfo = undefined
  $scope.showDate = undefined
  $scope.newUser = {}
  $scope.userToModify = {}
  $scope.possibleGroups = []
  $scope.composeMessageData = {}
  $scope.messageBody = {subject: '', msgText: ''}
  $scope.currentAccount = JSON.parse(localStorage.getItem('miaMe'))

  #booleans for FE routines
  $scope.isAdmin = false
  $scope.searchAccountTableOpen = false
  $scope.searchCountryTableOpen = false
  $scope.loading = false
  $scope.modifyMode = false
  $scope.editing = false
  $scope.modified = false
  $scope.saved = false

  #check if the signed user can have access to Admin page
  $scope.hide = true
  personalProfileFactory.getUser().then (resp)->
    if resp.data
      userData = resp.data
      $scope.signedUser = userData.account
      for item in userData.userGroups
        if item == "ADMINISTRATORS"
          $scope.isAdmin = true
          $scope.hide = false

  #create a list of informations on account for modify tab
  $scope.setAccountInfoList=()->
    temp = []
    if $scope.userToModify.active
      temp.push("Active")
    if $scope.userToModify.approved
      temp.push("Approved")
    if $scope.userToModify.locked
      temp.push("Locked")
    $scope.accountInfo = temp.join(', ')

  #concatenates values to obtain YYYY/MM/DD for modify tab
  $scope.setAccountDate=()->
    temp = []
    temp.push($scope.userToModify.accountExpirationYear)
    temp.push($scope.userToModify.accountExpirationMonth)
    temp.push($scope.userToModify.accountExpirationDay)
    $scope.showDate = temp.join('/')

  #
  #TABS ROUTINE
  #
  #Set all configuration for tabs navigation here
  #Tabs' names are: search, create, modify and compose
  $scope.activeTab=(tabName)->
    if tabName is "search"
      $scope.searchUserAccount = ""
      $scope.results = undefined
    if tabName is "create"
      $scope.initializeNewUser()
    angular.forEach($scope.tabs, (singleTab)->
      if singleTab.name is tabName
        singleTab.value = true
      else
        singleTab.value = false
      )

  #By clicking on create Tab initialize a new user
  $scope.initializeNewUser=()->
    $scope.initilializeGroups()
    $scope.confirmPassword = ""
    $scope.newUser.account = ""
    $scope.newUser.firstName = ""
    $scope.newUser.lastName = ""
    $scope.newUser.organization = ""
    $scope.newUser.city = ""
    $scope.newUser.country = ""
    $scope.newUser.email = ""
    $scope.newUser.password = ""
    $scope.newUser.userGroups = []

  #Leading to modify tab by clicking on search results on search tab
  $scope.setModifyTab = (result) ->
    $scope.editing = false
    $scope.activeTab("modify")
    $scope.modifyMode = true
    accountToModify = result.account
    adminFactory.findUser(accountToModify).then (resp)->
      if resp.data
        $scope.userToModify = resp.data
        $scope.initilializeGroups($scope.userToModify.userGroups)
        $scope.createGroupList($scope.userToModify.userGroups)
        $scope.setAccountInfoList()
        $scope.setAccountDate()

  #clicking on cancel button of modify tab
  $scope.cancelModifyMode=()->
    $scope.modifyMode = false
    $scope.userToModify = {}
    $scope.activeTab('search')
    $scope.modified = false

  #clicking on cancel button of create tab
  $scope.cancelSaveMode=()->
    newUser = {}
    $scope.activeTab('search')
    $scope.saved = false

  #clicking on modify button of modify tab
  $scope.switchEditing=()->
    if $scope.editing
      $scope.editing = false
    else
      $scope.editing = true

  #
  #SETTING AND CHECKING DATES
  #
  #initialize the expiration date within yearsToAdd years
  #date array has both seprated and backend format
  $scope.setDate=(yearsToAdd)->
    accountExpirationDate = new Date()
    nextYearDate = accountExpirationDate.getFullYear() + yearsToAdd
    accountExpirationDate.setFullYear(nextYearDate)
    ExpirationYear = nextYearDate+''
    ExpirationMonth = ('0'+(accountExpirationDate.getMonth()+1)).slice(-2)
    ExpirationDay = ('0'+accountExpirationDate.getDate()).slice(-2)
    date = []
    date.push(ExpirationYear)
    date.push(ExpirationMonth)
    date.push(ExpirationDay)
    date.push(ExpirationYear+'-'+ExpirationMonth+'-'+ExpirationDay+' 00:00:00')
    date

  #Check wether the expiration date is valid
  $scope.checkExpDate = (y,m,d)->
    insertedAccountExpirationDate = new Date(y,m-1,d)
    todayDate = new Date()
    if insertedAccountExpirationDate < todayDate
      return true

  #
  #HANDLING GROUPS
  #
  #make a list from from a user groups array
  $scope.createGroupList=(array)->
    $scope.stringListGroup = array.join(', ')

  #set the groups to which a user belongs to
  $scope.initilializeGroups=(existingGroups)->
    $scope.possibleGroups = []
    adminFactory.findUserGroups().then (resp)->
      angular.forEach(resp.data, (group)->
        temp = {}
        temp.name = group
        temp.value = false
        if existingGroups
          for item in existingGroups
            if item is group
              temp.value = true
        $scope.possibleGroups.push(temp)
        )

  #For an interactive updating of groups
  $scope.checkGroup = (user, groupName)->
    alreadyPresent = false
    if user.userGroups.length
      for presentGroup in user.userGroups
        if groupName is presentGroup
          user.userGroups.splice(user.userGroups.indexOf(groupName),1)
          alreadyPresent = true
      if not alreadyPresent
        user.userGroups.push(groupName)
    else
      user.userGroups.push(groupName)

  #
  #SEARCHING COUNTRY
  #
  #calls backend service and open a drop down menu of results
  $scope.getCountries=(user)->
    $scope.loading = true
    $scope.searchCountryTableOpen = true
    if user.country is undefined
      $scope.resetSearch()
    else
      if user.country.length is 0
        $scope.searchCountryTableOpen = true
      if user.country.length is 2
        personalProfileFactory.searchCountries(user.country).then (resp)->
          $scope.results = resp.data
          $scope.loading = false
          return
        , (data) ->
          $scope.loading = false
          console.log 'error'
          return
      else
        $scope.loading = false

  #country selection from drop down menu of results
  $scope.selectCountry= (user, item)->
    user.country = item.code
    $scope.searchCountryTableOpen = false

  #reset every country search
  $scope.resetSearch=()->
    $scope.searchCountryTableOpen = false
    $scope.loading = false

  #
  #BE SERVICES CALLED FROM FE SERVICES
  #

  ##SEARCH TAB
  
  #Searching user
  $scope.searchUser = (account)->
    $scope.loading = true
    if account is undefined || !account.length
      $scope.searchAccountTableOpen = false
      $scope.results = undefined
      $scope.loading = false
    else
      adminFactory.searchUser(account).then (resp)->
        $scope.searchAccountTableOpen = true
        $scope.results = resp.data
        $scope.loading = false
        return
      , (data) ->
        $scope.loading = false
        console.log 'error'
        return
  
  $scope.closeAccountTable = ()->
    $scope.searchAccountTableOpen = false

  ##MODIFY TAB
  #modify changes on userToModify
  $scope.updateUser = ()->
    $scope.userToModify.accountExpirationMonth = ('0'+$scope.userToModify.accountExpirationMonth).slice(-2)
    $scope.userToModify.accountExpirationDay = ('0'+$scope.userToModify.accountExpirationDay).slice(-2)
    adminFactory.modifyUser(preparePayload($scope.userToModify)).then (resp)->
      if resp.status is "ok"
        $scope.modified = true

  #Open a modal for changing the user's password
  $scope.changePassword=(account)->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-change-password-admin/change-password-admin.html'
      controller:'modalIstanceController'
      )
    modalInstance.account = account

  ##CREATE TAB
  #Creating a new account saving newUser present in create tab
  $scope.saveUser = ()->
    $scope.newUser.approvedBy = $scope.signedUser
    date = $scope.setDate(3)
    $scope.newUser.accountExpirationYear = date[0]
    $scope.newUser.accountExpirationMonth = date[1]
    $scope.newUser.accountExpirationDay = date[2]
    $scope.newUser.approved = "1"
    $scope.newUser.active = "1"
    $scope.newUser.locked = "0"
    $scope.newUser.mailhide = "1"
    adminFactory.createUser($scope.newUser).then (resp)->
      if resp.status is "ok"
        $scope.saved = true

  preparePayload = (userToModify) ->
    payload = angular.copy($scope.userToModify)
    
    # Remove field that are not present on BE to avoid 400 Error
    delete payload.portrait
    delete payload.portraitImageName
    delete payload.interests
    delete payload.photoPath

    payload

  ##COMPOSE TAB

  $scope.closeComposeMessage = (tabName) ->
    if $scope.messageBody.subject || $scope.messageBody.msgText
      alertify.confirm( 'If you close this you will lose your message. Continue?', (->
        $scope.messageBody.subject = ''
        $scope.messageBody.msgText = ''
        $scope.activeTab('search')
        $scope.$apply()
        ))
    else
      $scope.activeTab('search')

  $scope.sendMessage = () ->
    return if !$scope.messageBody.subject || !$scope.messageBody.msgText
    $scope.composeMessageData.messageSubject = $scope.messageBody.subject
    $scope.composeMessageData.messageText = $scope.messageBody.msgText
    $scope.composeMessageData.account = $scope.currentAccount.data[0].account
    messagingFactory.sendMessageToAll($scope.composeMessageData).then (resp) ->
      if resp.status == 'ok'
        alertify.alert "Your message has been sent!"
        $scope.messageBody.subject = ''
        $scope.messageBody.msgText = ''
        $scope.activeTab('search')
