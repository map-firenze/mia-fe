sidebarApp = angular.module('sidebarApp',[
  'ui.router'
  'ngAutocomplete'
  'ngAnimate'
  'toaster'
  'translatorApp'
  'translatorAppEn'
  'sidebarApp'
  'addRepoApp'
  'addCollectionApp'
  'addSeriesApp'
  'addVolumeSearchApp'
  'addInsertSearchApp'
  'addVolumeApp'
  'addInsertApp'
  'addDescApp'
  'docentitiesApp'
  'ajoslin.promise-tracker'
  'ngFileUpload'
  'modalUtils'
  'ui.bootstrap'
  'personalProfileService'
  'createAddVolumeApp'
  'createAddInsertApp'
  'ngLodash'
  'spotlightPendingReview'
  'ui.tree'
  'addFolderCsApp'
  'shareWithUsersApp'
  'csServiceApp'
  'editCsApp'
  'addLinkCsApp'
  'addBioGeoCsApp'
  'addFileCsApp'
])

sidebarApp.controller 'sidebarController', ($scope, $rootScope, $interval, $state, commonsFactory, promiseTracker, Upload, $q, $log, modalFactory, personalProfileFactory, $timeout, $filter, searchDeService, lodash, volumeInsertDescrFactory, $http, $location, $anchorScroll, csFactory, $window, toaster, $animate) ->
  #data model for upload form
  $scope.uploadFinished = false
  $scope.uploadLoader = false
  $scope.isAdmin = false

  $location.hash('')

  $scope.navSidebarIsActive = (paths, hash = false) ->
    #if path is string, convert to array
    if !Array.isArray(paths)
      paths = [paths]
    # for sidebar modal
    if $location.hash() == hash
      return true
    # for route without modal
    pathIsPresent = false
    for path in paths
      if $location.url().includes(path)
        pathIsPresent = true
    if pathIsPresent and $location.hash() == '' and !hash
      return true
    return false

  $scope.$on('invalid-files', (evt, args)->
    $scope.wrongFiles = args
  )

  updateForNewMessages = () ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      commonsFactory.checkNewMessages(account).then (resp) ->
        $scope.newMailsCount = if resp.data then resp.data.messTotalCount else 0

    
  updateForNewSpotlights = () ->
    commonsFactory.checkNewSpotlights().then (resp) ->
      $scope.spotlightReviewsCount = if resp.data then resp.data.count else 0

  $scope.$on 'get-me', (evt, arg) ->
    # Scheduled jobs (in milliseconds)
    updateForNewMessages()
    $interval updateForNewMessages, 40000 #40 seconds
    updateForNewSpotlights()
    $interval updateForNewSpotlights, 110000 #110 seconds

  #Debug
  #$scope.uploadFileStep = true

  uploadHandle = {}

  $scope.getErrorFiles = (erroredFiles) ->
    if $scope.totalFiles.length and $scope.eFile.length
      for eFile in $scope.erroredFiles
        for singleFile in $scope.totalFiles
          if singleFile.$ngfBlobUrl is eFile.$ngfBlobUrl
            eFile.name = singleFile.name
      return erroredFiles
    else
      return null

  #NEW alai
  $scope.invalidImages = []

  $scope.validatePicFiles = (image) ->
    defer = $q.defer()
    fileType = image.name.split(".").pop()
    fileType = fileType.toLowerCase()
    if fileType != 'jpg'
      if fileType != 'tif'
        if fileType != 'tiff'
          if fileType != 'jpeg'
            image.$error = 'invalidType'
            $scope.invalidImages.push(image)
    return null

  $scope.validateSelectedFile = (image) ->
    if image.name.length > 100
      return 'This file name is too long. Max 100 characters.'
    else
      return null

  $scope.addRepo = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
      controller:'modalIstanceController'
    ).then (selectedRepo) ->
      $rootScope.newUpload.metadata.repository = selectedRepo

  initializeArchive=()->
    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      $rootScope.newUpload.metadata.repository = mainRepo

  initializeArchive()

  $scope.delRepo = () ->
    $rootScope.newUpload.metadata.repository = undefined
    $rootScope.newUpload.metadata.collection = undefined
    $rootScope.newUpload.metadata.series = undefined
    $rootScope.newUpload.metadata.volume = undefined
    $rootScope.newUpload.metadata.insert = undefined

  $scope.delCollection = () ->
    $rootScope.newUpload.metadata.collection = undefined
    $rootScope.newUpload.metadata.series = undefined
    $rootScope.newUpload.metadata.volume = undefined
    $rootScope.newUpload.metadata.insert = undefined

  $scope.delSeries = () ->
    $rootScope.newUpload.metadata.series = undefined
    $rootScope.newUpload.metadata.volume = undefined
    $rootScope.newUpload.metadata.insert = undefined

  $scope.delVolume = () ->
    $rootScope.newUpload.metadata.volume = undefined
    $rootScope.newUpload.metadata.insert = undefined

  $scope.delInsert = () ->
    $rootScope.newUpload.metadata.insert = undefined

#    TODO: Filipp #236 - refactor this after add separate change observe service
  $scope.addCollection = () ->
    if $rootScope.unsavedChangesDialog
      $rootScope.unsavedChangesDialog().then (ignoreChanges) ->
        if ignoreChanges
          $scope.addCollectionModal()
    else
      $scope.addCollectionModal()

  $scope.addCollectionModal = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $rootScope.newUpload.metadata.repository.repositoryId}
        }
    ).then (selectedCollection) ->
#      console.log('selectedCollection', selectedCollection)
      $rootScope.newUpload.metadata.collection = selectedCollection
      $rootScope.newUpload.metadata.series = undefined
      $rootScope.newUpload.metadata.volume = undefined
      $rootScope.newUpload.metadata.insert = undefined

  $scope.addVolume = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-volume/add-volume.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $rootScope.newUpload.metadata.collection.collectionId}
        }
    ).then (selectedVolume) ->
      # TODO: temp solution
      volumeInsertDescrFactory.findVolumeDescription(selectedVolume.volumeId).then (response) ->
        volumeData = response.data.data
        if volumeData
          selectedVolume.hasInserts = volumeData.hasInserts
          selectedVolume.aeCount = volumeData.aeCount
          $rootScope.newUpload.metadata.volume = selectedVolume
#          console.log('root', $rootScope.newUpload.metadata)
          $rootScope.newUpload.metadata.insert = undefined

  $scope.addSeries = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {"parentId": $rootScope.newUpload.metadata.collection.collectionId}
        }
    ).then (selectedSeries) ->
      $rootScope.newUpload.metadata.series = selectedSeries

  $scope.addInsert = () ->
    $scope.data = {}
    # open modal
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-insert/add-insert.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "parentId": $rootScope.newUpload.metadata.volume.volumeId
            "parentName": $rootScope.newUpload.metadata.volume.volumeName
            "collection": $rootScope.newUpload.metadata.collection
          }
        }
    ).then (selectedInsert) ->
      $rootScope.newUpload.metadata.insert = selectedInsert

  $scope.canChooseInsert = () ->
    isStudyColl = $rootScope.newUpload.metadata.collection?.studyCollection
    volume      = $rootScope.newUpload.metadata.volume
    volHasIns   = volume?.hasInserts
    volHasAEs   = volume?.aeCount > 0

    return false if volHasAEs
    (isStudyColl && volHasIns && !volHasAEs) || (isStudyColl && !volHasIns && !volHasAEs) || (!isStudyColl && volHasIns && !volHasAEs) || (!isStudyColl && !volHasIns && !volHasAEs && $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS']))

  $scope.canNext = () ->
    isStudyColl = $rootScope.newUpload.metadata.collection?.studyCollection
    volume      = $rootScope.newUpload.metadata.volume
    volHasIns   = volume?.hasInserts
    volHasAEs   = volume?.aeCount > 0
    insert      = $rootScope.newUpload.metadata.insert

    return (volume && !volHasIns && !volHasAEs) || (volume && volHasAEs) || (insert && $scope.canChooseInsert())

  ##################
  # Upload routines
  ##################

  $scope.getFiles = () ->
    document.getElementById("file-upload").click()

  # multiple file upload
  $scope.uploadFiles = (files) ->
#    console.log(files)
    $scope.totalFiles = files
    if not $rootScope.newUpload.metadata.series
      $rootScope.newUpload.metadata.series = {}
      $rootScope.newUpload.metadata.series.seriesId = ''
      $rootScope.newUpload.metadata.series.seriesName = ''
      $rootScope.newUpload.metadata.series.seriesSubtitle = ''
    if not $rootScope.newUpload.metadata.insert
      $rootScope.newUpload.metadata.insert = {}
      $rootScope.newUpload.metadata.insert.insertId = ''
      $rootScope.newUpload.metadata.insert.insertName = ''
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-desc/add-desc.html'
      controller:'modalIstanceController'
    ).then (selectedDesc) ->
      $scope.uploading = true
      $scope.uploadLoader = true
      $rootScope.newUpload.metadata.aeDescription = selectedDesc
      $scope.uploadFinished = true
      params=$rootScope.newUpload.metadata
      if files and files.length
        $rootScope.isBusy = true
        uploadHandle = Upload.upload(
          url: 'json/upload/uploadWithMeta/'
          data:
            files: files
            params: params).then ((response) ->
              # console.log("Risposta server di upload")
              # console.log(response)
              $scope.result = response.data
              $scope.uploadLoader = false
              $scope.uploadDone()
              $rootScope.isBusy = false
#              console.log($scope.result)
              params = {}
              params.aeId = $scope.result['uploadImages-ok'].uploadInfoId
              $state.go('mia.singleupload', params, {reload: true})
            ), ((response) ->
              if response.status == 500
                $rootScope.$broadcast('progress', NaN)
              if response.status > 0
                $scope.errorMsg = response.status + ': ' + response.data
            ), (evt) ->
              $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
              $rootScope.$broadcast('progress', $scope.progress)
              
  #clean all the cached data
  $scope.uploadDone = () ->
    #clean everything
    $scope.uploadPanel = false
    $scope.picFiles = []
    $scope.progress = 0
    $rootScope.newUpload.metadata = {}
    $rootScope.newUpload.metadata.repository = undefined
    $rootScope.newUpload.metadata.collection = undefined
    $rootScope.newUpload.metadata.series = undefined
    $rootScope.newUpload.metadata.volume = undefined
    $rootScope.newUpload.metadata.insert = undefined
    initializeArchive()
    $rootScope.$broadcast('upload-finished')
    $scope.uploadFileStep = false

  ###################
  # Graphic routines
  ###################

  #decide if to the upload panel or not
  $scope.uploadPanel = false

  #decide if to the navigation panel or not
  $scope.navigationPanel = false

  #decide if to the case study panel or not
  $scope.csPanel = false

  #close the upload panel
  $scope.closeUploadPanel= () ->
    $scope.uploadPanel = false
    $scope.navigationPanel = false
    $location.hash('')

  #open the upload panel
  $scope.openUploadPanel= () ->
    $scope.uploadPanel = !$scope.uploadPanel
    $scope.navigationPanel = false
    $scope.csPanel = false
    
    if $scope.uploadPanel
      $location.hash('upload')
      $anchorScroll()
    else
      $location.hash('')

  ###################
  # NAVIGATION START
  ###################

  defaultNavigator =
    step: 'collections'
    selected:
      selectedRepositoryId: undefined
      selectedCollectionId: undefined
      selectedVolumeId:     undefined
      selectedInsertId:     undefined
    data:
      repositories: []
      collections:  []
      volumes:      []
      inserts:      []

  navigator            = JSON.parse(localStorage.getItem('navigator')) || defaultNavigator

  $scope.repositories  = navigator.data.repositories
  $scope.collections   = navigator.data.collections
  $scope.volumes       = navigator.data.volumes
  $scope.inserts       = navigator.data.inserts

  $scope.navigatorStep = navigator.step

  $scope.selectedRepositoryId = navigator.selected.selectedRepositoryId
  $scope.selectedCollectionId = navigator.selected.selectedCollectionId
  $scope.selectedVolumeId     = navigator.selected.selectedVolumeId
  $scope.selectedInsertId     = navigator.selected.selectedInsertId

  $scope.repositoryFilter = ''
  $scope.collectionFilter = ''
  $scope.volumeFilter     = ''
  $scope.insertFilter     = ''

  $scope.loadingCol = false
  $scope.loadingVol = false
  $scope.loadingIns = false

  # orders
#  $scope.colSortProp    = 'collectionId'
#  $scope.colSortReverse = false
#  $scope.volSortProp    = 'volumeName'
#  $scope.volSortReverse = false
#  $scope.insSortProp    = 'insertName'
#  $scope.insSortReverse = false

  $scope.openNanigationPanel = () ->
    $scope.navigationPanel = !$scope.navigationPanel
    $scope.uploadPanel = false
    $scope.csPanel = false

    if $scope.navigationPanel
      $location.hash('browse-navigator')
      $anchorScroll()
    else
      $location.hash('')

    # searchDeService.getRepositories().then (resp) ->
    #   $scope.repositories = resp

    if $scope.navigationPanel && !$scope.collections.length
      $scope.loadingCol = true
      searchDeService.getCollection($scope.selectedRepositoryId).then (resp) ->
        $scope.loadingCol = false
        $scope.collections = resp
        angular.forEach $scope.collections, (col) -> col.collectionId = parseFloat(col.collectionId)
        keepNavigatorData()

  #close the navigation panel
  $scope.closeNanigationPanel = () ->
    $scope.navigationPanel = false
    $location.hash('')

  #get repository collections when click on folder
  $scope.getRepositoryCollections = (rep) ->
    return if $scope.selectedRepositoryId == rep.repositoryId
    $scope.selectedRepositoryId = rep.repositoryId
    unless lodash.isUndefined($scope.selectedRepositoryId)
      $scope.loadingCol = true
      searchDeService.getCollection($scope.selectedRepositoryId).then (resp) ->
        $scope.loadingCol = false
        $scope.collections = resp
        keepNavigatorData()

  #get collection volumes when click on folder
  $scope.getCollectionVolumes = (col) ->
    $scope.navigatorStep = 'volumes_inserts'
    return if $scope.selectedCollectionId == col.collectionId
    $scope.selectedCollectionId = col.collectionId
    $scope.volumes = []
    $scope.inserts = []
    $scope.selectedVolumeId = undefined
    $scope.selectedInsertId = undefined
    unless lodash.isUndefined($scope.selectedCollectionId)
      $scope.loadingVol = true
      searchDeService.getVolumesWithAe($scope.selectedCollectionId).then (resp) ->
        $scope.loadingVol = false
        $scope.volumes = resp
        keepNavigatorData()

  #get volume inserts when click on folder
  $scope.getVolumeInserts = (vol) ->
    $scope.navigatorStep = 'volumes_inserts'
#    return if $scope.selectedVolumeId == vol.volumeId
    $scope.selectedVolumeId = vol.volumeId
    $scope.inserts = []
    $scope.selectedInsertId = undefined
    unless lodash.isUndefined($scope.selectedVolumeId)
      volumeInsertDescrFactory.findVolumeDescription($scope.selectedVolumeId).then (response) ->
        if response.data.data.hasInsertsWithAEs
          $scope.loadingIns = true
          searchDeService.getInsertsWithAe($scope.selectedVolumeId).then (resp) ->
            $scope.loadingIns = false
            $scope.inserts = resp
            keepNavigatorData()
        else
          $scope.inserts = []
          keepNavigatorData()
          $scope.closeNanigationPanel()
          $state.go 'mia.browseArchive', {type: 'vol', id: $scope.selectedVolumeId, page: 1}

  $scope.browseSelectedLocation = (ins) ->
    $scope.selectedInsertId = ins.insertId
    keepNavigatorData()
    $scope.closeNanigationPanel()
    $state.go 'mia.browseArchive', {type: 'ins', id: ins.insertId, page: 1}

  keepNavigatorData = () ->
    navigator =
      step: $scope.navigatorStep
      selected:
        selectedRepositoryId: $scope.selectedRepositoryId
        selectedCollectionId: $scope.selectedCollectionId
        selectedVolumeId:     $scope.selectedVolumeId
        selectedInsertId:     $scope.selectedInsertId
      data:
        repositories: $scope.repositories
        collections:  $scope.collections
        volumes:      $scope.volumes
        inserts:      $scope.inserts
    localStorage.setItem('navigator', JSON.stringify(navigator))

  $scope.clearNavigatorData = () ->
    localStorage.clear()
    
#  $scope.changeColOrder = (prop) ->
#    $scope.colSortProp    = prop
#    $scope.colSortReverse = !$scope.colSortReverse

#  $scope.changeVolOrder = () ->
#    $scope.volSortReverse = !$scope.volSortReverse

#  $scope.changeInsOrder = () ->
#    $scope.insSortReverse = !$scope.insSortReverse

  ###################
  # NAVIGATION END
  ###################

  ###################
  # CASE STUDY START
  ###################

  $scope.csStep = 'csList' # csList/csCreate
  $scope.newFolder = false

  $scope.newCs = {}

  $scope.caseStudies = []

  $rootScope.currentCs = JSON.parse(localStorage.getItem('cs')) || undefined
  $scope.previewItem   = undefined

  $scope.getCsList = () ->
    $scope.newCs = {}
    $rootScope.currentCs = undefined
    $scope.csStep = 'csList'
    localStorage.removeItem('cs')
    getCaseStudies()

  $scope.createCs = () ->
    $scope.csStep = 'csCreate'

  $scope.createNewCs = () ->
    csFactory.createNewCs($scope.newCs).then (resp) ->
#      console.log('new case_study', resp)
      csData = resp.data
      if resp.status == 'success' && csData
        $scope.newCs = {}
        $scope.caseStudies.unshift(csData)
        $rootScope.currentCs = csData
#        $rootScope.currentCs.folders = []
        # TODO: need fix
        $scope.selectCs($rootScope.currentCs)
        keepCsData()

  $scope.editCs = (e, cs) ->
    e.stopPropagation()
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-cs/edit-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {caseStudy: cs}
      }
    ).then (modCs) ->
#      console.log('modCs', modCs)
      index = _.findIndex $scope.caseStudies, id: cs.id
      $scope.caseStudies[index] = modCs
      keepCsData()

  $scope.deleteCs = (e, cs) ->
    e.stopPropagation()
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = 'Are you sure you want to delete this Project?'
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (agree) ->
      if agree
        csFactory.deleteCs(cs.id).then (resp) ->
          if resp.status == 204
            index = _.findIndex $scope.caseStudies, id: cs.id
            $scope.caseStudies.splice(index, 1)
          else if resp.status == 200 && resp.data?.status == 'error' && resp.data?.message
            toaster.pop('error', resp.data.message)
          else
            toaster.pop('error', 'Error! Try again later')

  $scope.selectCs = (cs) ->
    $rootScope.currentCs = cs
    csFactory.getCsFolders(cs.id).then (resp) ->
#      console.log('===folders==>>>>', resp)
      if resp.status == 'success'
        $rootScope.currentCs.folders = resp.data
        $rootScope.currentCs.token   = resp.token
        _.forEach $rootScope.currentCs.folders, (folder) -> folder.isOpen = false
        keepCsData()
      else
        toaster.pop('error', resp.message)
        $scope.getCsList()

  $scope.refreshCsFolders = () ->
    csFactory.getCsFolders($rootScope.currentCs.id).then (resp) ->
      if resp.status == 'success'
        $rootScope.currentCs.folders = resp.data
        $rootScope.currentCs.token   = resp.token
        _.forEach $rootScope.currentCs.folders, (folder) -> folder.isOpen = false
        keepCsData()
      else
        toaster.pop('error', resp.message)
        $scope.getCsList()

  $scope.toggleFolder = (folder) ->
    folder.isOpen = !folder.isOpen
    keepCsData()

  $scope.$on 'addToCsFolder', (e, data) ->
    return if $rootScope.currentCs.id != data.csId || !data.item
    csFolderIndex = _.findIndex $rootScope.currentCs.folders, id: data.folderId
    newItem       = data.item
    $rootScope.currentCs.folders[csFolderIndex].items.push(newItem)
    #TODO: need add orderBy after implement ordering for items
    keepCsData()

  $scope.$on 'openCs', (e, cs) ->
    return unless cs.documentId || cs.folderId
    $scope.newCs = {}
    $rootScope.currentCs = undefined
    $scope.csStep = 'csList'
    localStorage.removeItem('cs')

    csFactory.getCsList().then (resp) ->
      $scope.caseStudies = resp.data
      selectedCs = _.find $scope.caseStudies, id: cs.id
      return unless selectedCs
      $scope.csPanel = true
      $scope.uploadPanel = false
      $scope.navigationPanel = false
      $location.hash('cs')
      $anchorScroll()
      $rootScope.currentCs = selectedCs
      csFactory.getCsFolders(cs.id).then (resp) ->
        if resp.status == 'success'
          $rootScope.currentCs.folders = resp.data
          $rootScope.currentCs.token   = resp.token
          csFolder = _.find $rootScope.currentCs.folders, id: cs.folderId
          return unless csFolder
          _.map $rootScope.currentCs.folders, (folder) -> folder.isOpen = if folder.id == cs.folderId then true else false
          item = _.find csFolder.items, {entityType: 'DE', entity: {documentId: cs.documentId}}
          return unless item
          $timeout ->
            itemSelector = '#folder-item-' + item.id
            itemEl = angular.element(document.querySelector(itemSelector))
            return unless itemEl[0]
            itemEl[0].scrollIntoView()
            $scope.runner = $animate.removeClass(itemEl, 'highlight-cs-item')
            $scope.runner.finally -> itemEl.addClass('highlight-cs-item')
          keepCsData()
        else
          toaster.pop('error', resp.message)
          $scope.getCsList()

  $scope.addFolderCs = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-folder-cs/add-folder-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {caseStudy: $rootScope.currentCs}
      }
    ).then (newFolder) ->
#      console.log('addFolderCs resp', newFolder)
      $rootScope.currentCs.folders.unshift(newFolder)
      keepCsData()

  $scope.editFolderCs = (e, folder) ->
    e.stopPropagation()
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-folder-cs/add-folder-cs.html'
      controller:'modalIstanceController'
      size: 'md'
      resolve:{
        options:
          -> {caseStudy: $rootScope.currentCs, folder: folder}
      }
    ).then (modFolder) ->
#      console.log('modFolder', modFolder)
      index = _.findIndex $rootScope.currentCs.folders, id: modFolder.id
      $rootScope.currentCs.folders[index] = modFolder
      keepCsData()

  $scope.deleteFolderCs = (e, folder) ->
    e.stopPropagation()
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = 'Are you sure you want to delete this folder from the Project?'
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (agree) ->
      if agree
        csFactory.deleteFolder($rootScope.currentCs.id, folder.id).then (resp) ->
          console.log(resp)
          if resp.status == 204
            index = _.findIndex $rootScope.currentCs.folders, id: folder.id
            $rootScope.currentCs.folders.splice(index, 1)
            keepCsData()
          else if resp.status == 200 && resp.data?.status == 'error' && resp.data?.message
            toaster.pop('error', resp.data.message)
          else
            toaster.pop('error', 'Error! Try again later')

  $scope.deleteCsFolderItem = (folder, item, index) ->
    pageTitle = $filter('translate')("modal.WARNING")
    messagePage = 'Are you sure you want to delete this item from the Project?'
    modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (agree) ->
      if agree
        csFactory.deleteFolderItem($rootScope.currentCs.id, folder.id, item.id).then (resp) ->
          if resp.status == 204
            csFolderIndex = _.findIndex $rootScope.currentCs.folders, id: folder.id
            $rootScope.currentCs.folders[csFolderIndex].items.splice(index, 1)
            keepCsData()
          else if resp.status == 200 && resp.data?.status == 'error' && resp.data?.message
            toaster.pop('error', resp.data.message)
          else
            toaster.pop('error', 'Error! Try again later')

  $scope.chooseUsers = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-share-with-users/share-with-users.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "caseStudy": $rootScope.currentCs
        }
      }
    ).then (resp) ->

  $scope.addLinkCs = (folder) ->
#    console.log('---', folder)
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-link-cs/add-link-cs.html'
      controller:'modalIstanceController'
#      size: 'md'
      resolve:{
        options:
          -> {caseStudy: $rootScope.currentCs, folderId: folder.id}
      }
    ).then (newLink) ->
#      console.log('newLink resp', newLink)

  $scope.goToLink = (itemLink) ->
    if !itemLink.match(/^http[s]?:\/\//)
      itemLink = 'http://' + itemLink
    $window.open(itemLink, "_blank")
    return true

  $scope.addBioGeoCs = (folder) ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-bio-geo-cs/add-bio-geo-cs.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {caseStudy: $rootScope.currentCs, folderId: folder.id}
      }
    ).then (newLink) ->

  $scope.goToItem = (item) ->
    $state.go 'mia.modifyDocEnt', {docId: item.entity.documentId} if item.entityType == 'DE'
    $state.go 'mia.bio-people',   {id:    item.entity.peopleId}   if item.entityType == 'BIO'
    $state.go 'mia.bio-place',    {id:    item.entity.placeId}    if item.entityType == 'GEO'
    $scope.previewItem = undefined
    $scope.closeCsPanel()

  $scope.addFileCs = (folder) ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-add-file-cs/add-file-cs.html'
      controller:'modalIstanceController'
#      size: 'md'
      resolve:{
        options:
          -> {caseStudy: $rootScope.currentCs, folderId: folder.id}
      }
    ).then (newFile) ->

  $scope.downloadCsFile = (item) ->
    defer = $q.defer()
    $timeout((->
      $window.location = '/Mia' + item.entity.path
    ), 1000).then (->
      defer.resolve 'success'
    ), ->
      defer.reject 'error'
    defer.promise

  $scope.showItemPreview = (item) ->
    $scope.previewItem = item
    text = $scope.previewItem.entity.transcriptions[0].transcription
    if text != 'undefined' and text != null
      text = text.replace(/<newsHeader>/g, '[')
      text = text.replace(/<\/newsHeader>/g, '')
      text = text.replace(/<newsFrom>/g, '[')
      text = text.replace(/<\/newsFrom>/g, '')
      text = text.replace(/<from>/g, 'News From: ')
      text = text.replace(/<\/from>/g, '')
      if text.indexOf('fromUnsure') >= 0
        text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
        text = text.replace(/<\/fromUnsure>/g, '')
      text = text.replace(/<hub>/g, 'News Hub: ')
      text = text.replace(/<\/hub>/g, '')
      if text.indexOf('hubUnsure') >= 0
        text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
        text = text.replace(/<\/hubUnsure>/g, '')
      text = text.replace(/<date>/g, ' - Date: ')
      text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
      text = text.replace(/<\/date>/g, ']')
      text = text.replace(/<transc>/g, '')
      text = text.replace(/<\/transc>/g, '')
      text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
      text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
      text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
      #line required for each occurence except the first one
      text = text.replace(/<position.*?<\/position>/g, '')
      $scope.previewItem.entity.transcriptions[0].transcription = text
    $rootScope.$broadcast('preview-obj', null)
    console.log($scope.previewItem)

  $scope.closePreviewItem = () ->
    $scope.previewItem = undefined

  $scope.$on 'preview-obj-open', ->
    $scope.closePreviewItem()

  $scope.sharedNotebook = () ->
    $rootScope.$broadcast('shared-notebook')

  getCaseStudies = () ->
    csFactory.getCsList().then (resp) ->
      $scope.caseStudies = resp.data
      $timeout -> checkAccessRightsCurrentCs(resp.data)

  checkAccessRightsCurrentCs = (caseStudies) ->
    return if !$rootScope.currentCs || $rootScope.currentCs?.createdBy == $rootScope.me?.account
    cs = _.find caseStudies, id: $rootScope.currentCs.id
    unless cs
      toaster.pop('error', 'You do not have an access to view this Project')
      $scope.getCsList()

  $scope.openCsPanel = () ->
    $scope.csPanel = !$scope.csPanel
    $scope.uploadPanel = false
    $scope.navigationPanel = false

    if $scope.csPanel
      $location.hash('cs')
      $anchorScroll()
      getCaseStudies()
    else
      $location.hash('')
      $scope.previewItem = undefined

  # https://github.com/angular-ui-tree/angular-ui-tree
  $scope.treeOptions =
    accept: (sourceNodeScope, destNodesScope, destIndex) ->
      sourceIsFolder = sourceNodeScope.$element.hasClass('folder')
      sourceIsItem   = sourceNodeScope.$element.hasClass('item')
      destIsFolders  = destNodesScope.$element.hasClass('folder-list')
      destIsItems    = destNodesScope.$element.hasClass('item-list')
      return sourceIsFolder && destIsFolders || sourceIsItem && destIsItems

    beforeDrop: (event) ->
      $scope.oldCurrentCs = angular.copy($rootScope.currentCs)

    dropped: (event) ->
      return if event.source.nodesScope == event.dest.nodesScope && event.source.index == event.dest.index
      sourceIsFolder = event.source.nodeScope.$element.hasClass('folder')

      if sourceIsFolder
        data = {}
        data.type = 'FOLDER'
        data.entities = []

        # Use $timeout for getting actual items
        $timeout ->
          _.map $rootScope.currentCs.folders, (obj, i) -> data.entities.push({id: obj.id, position: i+1})
          updateOrderCs(data)
      else
#        sourceData     = event.source.nodesScope.$element.data()
#        sourceFolderId = sourceData.folderId
        destData       = event.dest.nodesScope.$element.data()
        destFolderId   = destData.folderId
        destFolder     = _.find $rootScope.currentCs.folders, id: destFolderId

        data           = {}
        data.type      = 'ITEM'
        data.folderId  = destFolderId
        data.entities  = []

        # Use $timeout for getting actual items
        $timeout ->
          _.map destFolder.items, (obj, i) -> data.entities.push({id: obj.id, position: i+1})
          updateOrderCs(data)


  updateOrderCs = (data) ->
    csFactory.reorder($rootScope.currentCs.id, data).then (resp) ->
      if resp.status == 'success'
        updatedFolders = resp.data

        if updatedFolders.length
          _.map updatedFolders, (folder) ->
            existFolder = _.find $scope.oldCurrentCs.folders, id: folder.id
            folder.isOpen = if existFolder then existFolder.isOpen else false

        $timeout ->
          $rootScope.currentCs.folders = updatedFolders
          keepCsData()
      else
        toaster.pop('error', resp.message)
        $rootScope.currentCs = $scope.oldCurrentCs
        keepCsData()

  #close the navigation panel
  $scope.closeCsPanel = () ->
    $scope.csPanel = false
    $location.hash('')

  keepCsData = () ->
    localStorage.setItem('cs', JSON.stringify($rootScope.currentCs)) if $rootScope.currentCs

  ###################
  # CASE STUDY END
  ###################

  #used to switch from metadata insert and file upload panels
  $scope.gotoUploadStep = () ->
    $log.debug "[Mia sidebar] meta data you are going to upload are"
    $scope.uploadFileStep = true

  # for debug purpose
  # $scope.gotoUploadStep()

  $scope.gotoMetadataStep = () ->
    $scope.uploadFileStep = false

  $scope.showSpotlightPendingReviews = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-pending-review.html'
      controller:'modalIstanceController'
    )

  $scope.isAuthorized = () ->
    $rootScope.secAuthorize(['ADMINISTRATORS', 'SPOTLIGHT_COORDINATORS'])