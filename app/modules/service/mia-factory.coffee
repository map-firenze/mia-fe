commonServiceApp = angular.module 'commonServiceApp',[
  'angular-growl'
  'utilsApp'
]

commonServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
commonServiceApp.factory 'commonsFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  archiveBasePath = ENV.archiveBasePath
  userBasePath = ENV.userBasePath
  applicationPropertiesPath = ENV.applicationProperties

  me = JSON.parse(localStorage.getItem('miaMe'))
  if me and me.status is 'ok'
    if me.data[0].userGroups.indexOf('ADMINISTRATORS') > -1
      $rootScope.isAdmin = true
    else
      $rootScope.isAdmin = false

  factory =
    #----------------------
    # Main archive services
    #----------------------
    getMyArchive: (aeNuber, thumbNumb, pageNum) ->
      return $http.get(archiveBasePath+'getArchives/'+aeNuber+'/'+thumbNumb+'/'+pageNum).then (resp) ->
        resp.data
    #------------------------
    # Single archive services
    #------------------------
    getMySingleupload: (uploadId, thumbNumb, pageNum) ->
      return $http.get(archiveBasePath+'getArchives/aentity/'+uploadId+'/'+thumbNumb+'/'+pageNum).then (resp) ->
        resp.data

    #-----------------------------------
    # Single archive get page by file id
    #-----------------------------------
    getPageByFileId: (uploadId, fileId, perPage) ->
      return $http.get(archiveBasePath + 'findPageForFile?uploadId=' + uploadId + '&fileId=' + fileId + '&perPage=' + perPage).then (resp) ->
        resp.data

    #-----------------------------------
    # Single archive services by file id
    #-----------------------------------
    getMySingleuploadByFileId: (uploadFileId, thumbNumb, pageNum) ->
      return $http.get(archiveBasePath+'getArchives/aentityByUploadFile/'+uploadFileId+'/'+thumbNumb+'/'+pageNum).then (resp) ->
        resp.data

    fixUpload:(uploadId) ->
      return $http.get(archiveBasePath+'repairarchive/'+uploadId).then (resp) ->
        resp.data

    changeOrder:(uploadFileId, newOrderPlace) ->
      return $http.get(archiveBasePath+'orderFileinAE/'+uploadFileId+'/'+newOrderPlace).then (resp) ->
        resp.data
    
    rotate: (uploadFileId, direction) ->
      params = {}
      params.uploadFileIds = []
      if typeof uploadFileId == "object"
        params.uploadFileIds = uploadFileId
      else
        params.uploadFileIds.push(uploadFileId)
      params.rotateType = direction
      return $http.post(archiveBasePath+'rotateFilesInAE/', params).then (resp) ->
        resp.data
    
    setPrivacy:(uploadId, privacyValue) ->
      return $http.get(archiveBasePath+'setPrivacyUpload/'+uploadId+'/'+privacyValue).then (resp) ->
        resp.data

    setSingleDocPrivacy:(uploadId, privacyValue) ->
      return $http.get(archiveBasePath+'setPrivacyOneFile/'+uploadId+'/'+privacyValue).then (resp) ->
        resp.data

    downloadDocument: (docId) ->
      defer = $q.defer()
      $timeout((->
        $window.location = archiveBasePath+'downloadAFile/' + docId
      ), 1000).then (->
        defer.resolve 'success'
      ), ->
        defer.reject 'error'
      defer.promise

    updateMetaDataAE:(uploadId, params)->
      return $http.put(archiveBasePath+'updateMetaDataAE/'+uploadId, params).then (resp) ->
        resp.data

    deleteDocument: (uploadFileId) ->
      return $http.get(archiveBasePath+'deleteFileInAE/'+uploadFileId).then (resp) ->
        resp.data

    deleteAEntity: (uploadId) ->
      return $http.delete(archiveBasePath+'deleteAE/'+uploadId).then (resp) ->
        resp.data

    undeleteAEntity: (uploadId) ->
      return $http.post(archiveBasePath+'unDeleteAE/'+uploadId).then (resp) ->
        resp.data
    
    saveSharedUsers: (uploadId, imageId, users) ->
      return $http.post(archiveBasePath+'shareWithUsers/'+uploadId+'/'+imageId, users).then (resp) ->
        resp.data
    
    shareWithUsersByArchivial: (uploadId, users) ->
      return $http.post(archiveBasePath+'shareWithUsersByArchivial/'+uploadId, users).then (resp) ->
        resp.data
    
    shareWithUsersByDocument: (docId, users,replaceSharedUsers) ->
      return $http.post(archiveBasePath+'shareWithUsersByDocument/'+docId+'/'+replaceSharedUsers, users).then (resp) ->
        resp.data

    getSharedUsersByImageId: (uploadId, imageId) ->
      return $http.get(archiveBasePath+'getSharedUsers/'+uploadId+'/'+imageId).then (resp) ->
        resp.data
    
    getSharedUsersByDocumentId: (docId) ->
      return $http.get(archiveBasePath+'getSharedUsersByDocumentId/'+docId).then (resp) ->
        resp.data
    
    getSharedUsersByArchiveId: (uploadId) ->
      return $http.get(archiveBasePath+'getSharedUsersByArchivial/'+uploadId).then (resp) ->
        resp.data
    
    getSharedImagesByArchivial: (uploadId) ->
      return $http.get(archiveBasePath+'getSharedImagesByArchivial/'+uploadId).then (resp) ->
        resp.data
    
    getLastUploadedFile: (uploadId) ->
      return $http.get(archiveBasePath+'getLastUploadedFile/'+uploadId).then (resp) ->
        resp.data

    #--------------
    # Lang services
    #--------------
    getLatestLang: (userCode, loadingTracker) ->
      return $http.get('user/'+userCode+'/language/latest', {tracker: loadingTracker}).then (resp) ->
        resp.data
    saveCurrentLang: (userCode, currentLang) ->
      params = {}
      params.language = currentLang
      return $http.post('user/'+userCode+'/language/save', params).then (resp) ->
        resp.data

    #----------
    # Utilities
    #----------

    findAeById: (archives, uploadId) ->
      for archive in archives
        if archive.uploadId.toString() is uploadId.toString()
          return archive
      return false

    findImageById: (imageList, uploadFileId) ->
      for image in imageList
        if image.uploadFileId.toString() is uploadFileId.toString()
          return image
      return false # TODO: need fix this

    removeUnusedFields: (fieldArray)->
      fields = []
      for field in fieldArray
        if not angular.equals(field, "status") and
        not angular.equals(field, "message") and
        not angular.equals(field, "documentId") and
        not angular.equals(field, "typology") and
        not angular.equals(field, "privacy") and
        not angular.equals(field, "uploadFiles")
          fields.push(field)
      return fields
    
    checkNewMessages: (account) ->
      return $http.get('json/messaging/getNewInboxMessages/'+account, {params: {page: 1, perPage: 10}}).then (resp)->
        resp.data

    checkNewSpotlights: () ->
      return $http.get('json/discovery/newSpotlights').then (resp) ->
        resp.data

    recordUploadAction: (uploadId, type) ->
      uploadInfo = {
        "recordType": "AE"
        "entryId": Number(uploadId)
        "action": type
      }
#      console.log uploadInfo
      return $rootScope.$broadcast('recordUploadAction', uploadInfo)

    getApplicationProperty: (propertyName) ->
      return $http.get(applicationPropertiesPath + '?propertyName=' + propertyName).then (response) ->
        Object.values(response.data.data)[0]
        
  return factory

commonServiceApp.filter 'isEmpty', () ->
  return (obj) ->
    return if Object.keys(obj).length == 0 then true else false