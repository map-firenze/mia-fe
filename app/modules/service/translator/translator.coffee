translatorApp = angular.module 'translatorApp',[
  'pascalprecht.translate'
  'tmh.dynamicLocale'
  'commonServiceApp'
]

translatorApp.config ['$provide', '$translateProvider', 'tmhDynamicLocaleProvider'
  ($provide, $translateProvider, tmhDynamicLocaleProvider) ->
    $translateProvider.useSanitizeValueStrategy('escape')
    $translateProvider.translations 'en', {}
    tmhDynamicLocaleProvider.localeLocationPattern("js/i18n/angular-locale_{{locale}}.js")
    $translateProvider.preferredLanguage 'en'
]

translatorApp.factory ['$location', '$translate',
  ($location, $translate) ->
    factory =
      lang : (lang) ->

        if not lang
          lang =  $location.search().lang || $translate.use() || 'en'
        return "en" if lang is "en_UK"
        return lang

    return factory
]
translatorApp.controller 'translatorAppController', ($rootScope, $scope, $translate, $locale, tmhDynamicLocale, $location, $log, TranslatorAdapter, modalFactory, commonsFactory) ->
  $scope.languages = ["en"]

  $scope.$watch('currentLang', (newValue, oldValue) ->
    $log.debug '[Translator] Watch activated'
    $scope.setLanguage (newValue),
    true
  )

  commonsFactory.getLatestLang('121', $rootScope.latestLangTracker).then (resp) ->
    if resp.lang
      if resp.lang is "en-uk"
        $scope.currentLang = "en"
      else
        $scope.currentLang = resp.lang

  $scope.currentLang = $scope.currentLang || TranslatorAdapter.lang()

  $scope.getCurrentLang = () ->
    return $scope.currentLang

  $scope.setLanguage = (lang) ->
    $translate.use lang
    tmhDynamicLocale.set lang
    moment.locale lang
    $log.debug '[Translator] Language changed now set to ' + lang

  $scope.getStandardFormatCurrentLang = () ->
    if $translate.use() == "en"
      return "en-uk"
    else
      return $translate.use() || "en-uk"


  $scope.openLanguagesModal = (size) ->
    modalFactory.openModal(
      templateUrl: 'modules/translator/language-modal.html'
      controller: "modalInstanceController"
      size: size
      resolve:{
        options:
          -> {"items":$scope.languages, "initialSelected":$scope.currentLang}
      }
    ).then (selectedItem) ->
      $scope.currentLang = selectedItem
      #Save current lang
      commonsFactory.saveCurrentLang('121', $scope.getStandardFormatCurrentLang())
