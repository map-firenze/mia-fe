converterServiceApp = angular.module 'converterServiceApp',[]

converterServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
converterServiceApp.factory 'converterFactory', ($http, $q, ENV) ->
  converterBasePath = ENV.uploadBasePath

  factory =

    #-------------------
    # admin services
    #-------------------
    getStatus: (params) ->
      return $http.get(converterBasePath+'statusUpload/').then (resp) ->
        resp.data

    getReport: (params) ->
      return $http.get(converterBasePath+'getUploadReport/').then (resp) ->
        resp.data

  return factory
