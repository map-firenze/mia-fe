userServiceApp = angular.module 'userServiceApp',[]

userServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]

userServiceApp.factory 'userServiceFactory', ($http, ENV) ->
  userBasePath = ENV.userBasePath
  factory =
    getMe: () ->
      return $http.get(userBasePath+'me/').then (resp) ->
        localStorage.setItem('miaMe', JSON.stringify(resp.data))
        resp.data
    
    isMyUserLevel: (me, group) ->
      isRole = false
      angular.forEach(me.userGroups, (role)->
        if role is group
          isRole = true
        )
      return isRole
  
  return factory
          