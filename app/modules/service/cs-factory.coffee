csServiceApp = angular.module 'csServiceApp', []

csServiceApp.factory 'csFactory', ($http, ENV) ->
  csPath = ENV.csPath # _basePath + 'case_study/'

  factory =

    createNewCs: (data) ->
      return $http.post(csPath, data).then (resp) ->
        resp.data

    updateCs: (csId, data) ->
      return $http.put(csPath + csId, data).then (resp) ->
        resp.data

    deleteCs: (csId) ->
      return $http.delete(csPath + csId).then (resp) ->
        resp

    getCsList: () ->
      return $http.get(csPath).then (resp) ->
        resp.data

    getCsFolders: (csId) ->
      return $http.get(csPath + csId + '/folders').then (resp) ->
        resp.data

    getCsSharedUsers: (csId) ->
      return $http.get(csPath + csId + '/user_group').then (resp) ->
        resp.data

    addUsersToCs: (csId, data) ->
      return $http.post(csPath + csId + '/user_group', data).then (resp) ->
        resp.data

    addFolder: (csId, data) ->
      return $http.post(csPath + csId + '/folders', data).then (resp) ->
        resp.data

    editFolder: (csId, folderId, data) ->
      return $http.put(csPath + csId + '/folders/' + folderId, data).then (resp) ->
        resp.data

    deleteFolder: (csId, folderId) ->
      return $http.delete(csPath + csId + '/folders/' + folderId).then (resp) ->
        resp

    addToCsFolder: (csId, folderId, data) ->
      return $http.post(csPath + csId + '/folders/' + folderId + '/items', data).then (resp) ->
        resp.data

    deleteFolderItem: (csId, folderId, itemId) ->
      return $http.delete(csPath + csId + '/folders/' + folderId + '/items/' + itemId).then (resp) ->
        resp

    reorder: (csId, data) ->
      return $http.post(csPath + csId + '/reorder', data).then (resp) ->
        resp.data

    checkExistsInCs: (entityType, entityId) ->
      return $http.get(csPath + '/by_entity?id=' + entityId + '&type=' + entityType).then (resp) ->
        resp.data

  return factory
