uploadServiceApp = angular.module 'uploadServiceApp',[]

uploadServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
uploadServiceApp.factory 'uploadFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  uploadBasePath = ENV.uploadBasePath

  factory =
    
    #-------------------
    # getData for upload
    #-------------------
    getPossibleRepository: (value) ->
      return $http.get(uploadBasePath+'findRepository/'+value).then (resp) ->
        resp.data
    checkRepository: (repoName, repoLocation, repoAbbreviation) ->
      return $http.get(uploadBasePath+'checkRepository/'+repoName+'/'+repoLocation+'/'+repoAbbreviation+'/').then (resp) ->
        resp.data
    checkCollection: (collectionName, repoId) ->
      return $http.get(uploadBasePath+'checkCollection/'+collectionName+'/'+repoId).then (resp) ->
        resp.data
    getPossibleCollection: (searchQuery, idRepo) ->
      return $http.get(uploadBasePath+'findCollection/'+searchQuery+'/'+idRepo).then (resp) ->
        resp.data
    getPossibleSeries: (searchQuery, idColl) ->
      return $http.get(uploadBasePath+'findSeries/'+searchQuery+'/'+idColl).then (resp) ->
        resp.data
    getPossibleVolume: (searchQuery, idColl, limit = null) ->
      url = uploadBasePath+'findVolume/'+idColl+'?q='+searchQuery
      url += '&limit=' + limit if limit
      return $http.get(url).then (resp) ->
        resp.data
    getPossibleInsert: (searchQuery, idVolume) ->
      return $http.get(uploadBasePath+'findInsert/'+idVolume+'?q='+searchQuery).then (resp) ->
        resp.data
    getVolumesWithDE: (searchQuery, idColl) ->
      return $http.get(uploadBasePath + 'findVolume/' + idColl + '?withDE=true&q=' + searchQuery).then (resp) ->
        resp.data
    getInsertsWithDE: (searchQuery, idVolume) ->
      return $http.get(uploadBasePath + 'findInsert/' + idVolume + '?withDE=true&q=' + searchQuery).then (resp) ->
        resp.data
    getVolumesWithAE: (searchQuery, idColl) ->
      return $http.get(uploadBasePath + 'findVolume/' + idColl + '?withAE=true&q=' + searchQuery).then (resp) ->
        resp.data
    getInsertsWithAE: (searchQuery, idVolume) ->
      return $http.get(uploadBasePath + 'findInsert/' + idVolume + '?withAE=true&q=' + searchQuery).then (resp) ->
        resp.data
      
  return factory
