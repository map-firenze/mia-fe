folioNumb = angular.module 'folioNumb', []
folioNumb.directive 'folioNumbDir', (deFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/folio-numb/folio-numb.html'
  scope:
    thumb: '='

  link: (scope, elem, attr) ->

