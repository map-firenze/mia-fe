docViewer = angular.module 'docViewer', []
docViewer.directive 'docViewerDir', ($state, $stateParams, $window, $timeout, $sce) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-viewer/doc-viewer.html'
  scope:
    de: '='
    currentImgOrFileId: '='
    viewerAddr: '='
    lastShownImageIndx: '='

  link: (scope, elem, attr) ->

    extractDomain = (url) ->
      domain = undefined
      if url.indexOf('://') > -1
        domain = url.split('/')[2]
      else
        domain = url.split('/')[0]
      domain='http://'+domain

    scope.openTranscriptor = () ->
      workUrl = $state.href($state.current.name, $state.params, {absolute: true})
      transWindow=$window.open(extractDomain(workUrl)+'/Mia/json/src/ShowDocumentInManuscriptViewer/show?documentEntityId='+scope.de.documentId+'&showTranscription=true&fileId=' + scope.currentImgOrFileId)
      transWindow.onbeforeunload=()->
        $timeout(reloadPage, 1000)

    reloadPage = () ->
      $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true})

    scope.limit = 0
    scope.changeLimit = (quantity) ->
      temp = scope.limit + quantity
      scope.lastShownImageIndx = temp + 6
      if temp >= 0 and temp + 6 <= scope.de.uploadFiles.length
        scope.limit = temp

    scope.updateViewer = (image) ->
      scope.currentImgOrFileId = null
      if image.uploadFileId?
        scope.currentImgOrFileId = image.uploadFileId
      else
        scope.currentImgOrFileId = image.uploadedFileId

      scope.viewerAddr = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/" + scope.currentImgOrFileId)
      selectImage(image)

    selectImage = (image) ->
      actual = _.find scope.de.uploadFiles, (pic) -> pic.highlighted is true
      if actual
        actual.highlighted = false
      image.highlighted = true
