biblioRef = angular.module 'biblioRef', []
biblioRef.directive 'biblioRefDir', (modDeFactory, $state, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/biblio-ref/biblio-ref.html'
  scope:
    biblio: '='
    onCancel:'&'

  link: (scope, elem, attr) ->
    scope.editBiblio = false
    scope.canEditBiblio = true
    if not scope.biblio.biblioId
      scope.editBiblio = true
    oldBiblio = angular.copy(scope.biblio)

    scope.switchBiblio = () ->
      scope.editBiblio = not scope.editBiblio

    scope.$watch('editBiblio', (newVal)->
      if newVal
        scope.$root.$broadcast('editing-biblio')
      else
        scope.$root.$broadcast('editing-biblio-end')
    )

    scope.$on('editing-biblio', (event, args)->
      scope.canEditBiblio = false
      )
    scope.$on('editing-biblio-end', (event, args)->
      scope.canEditBiblio = true
      )
    scope.cancel= ()->
      scope.biblio = oldBiblio
      scope.editBiblio = false
      scope.$root.$broadcast('editing-biblio-end')
      if not scope.biblio.biblioId
        scope.onCancel({biblio:scope.biblio})


    scope.deleteBiblio=()->
      params={}
      params.documentId = $state.params.docId
      params.idBiblioToBeDeleted = scope.biblio.biblioId
      modDeFactory.deleteBiblioRef(params).then (resp) ->
        if resp.status is "ok"
          scope.onCancel({biblio:scope.biblio})

    scope.saveBiblio = () ->
      if not (scope.biblio.name is '' and scope.biblio.link is '')
        params = {}
        params.documentId = $state.params.docId
        params.newBiblio = {}
        params.newBiblio.biblioId = scope.biblio.biblioId || ""
        params.newBiblio.name = scope.biblio.name
        params.newBiblio.link = scope.biblio.link
        modDeFactory.addBiblioRef(params).then (resp) ->
          if resp.status is 'ok'
            scope.biblio.biblioId = resp.data.biblioId if resp.data
            scope.editBiblio = false
      else
        scope.cancel()
