browseVolume = angular.module 'browseVolume', ['searchServiceApp']
browseVolume.directive 'findVolDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/autocompleters/get-volume/get-volume.html'

  link: (scope, elem, attr) ->
    scope.closeTable = ()->
      scope.searchVolTableOpen = false
      
    scope.volOptions = {}

    scope.singleselect = attr.singleselect
    scope.volOptions.addplace = (attr.addvol == 'true')

    scope.volIsClicked = false

    scope.getVolumes = (searchQuery) ->
      if searchQuery.length == 0
        scope.searchVolTableOpen = false
      if searchQuery.length >= 3
        scope.loadingCircle = true
        scope.searchVolTableOpen = true
        scope.volIsClicked = false
        deFactory.getVolume(searchQuery).then (resp)->
          if resp.status is "ok"
            scope.volumes = resp.data.volumes
          else
            scope.volumes = null
          scope.loadingCircle = false

    scope.$on("volClearEvent", () ->
      scope.tempVolName = ""
      scope.volumes = [])

    scope.$on("volIsSelected", (event, volInfo) ->
      scope.tempVolName = volInfo.volName
      scope.volIsClicked = true)

    scope.openVol=(row)->
      scope.searchVolTableOpen=false
      scope.changeTab('volume')
      params = {}
      params.id = row.id
      # params.idName= row.placeNameId
      $state.go 'mia.bio-volume', params
    
    scope.usedVolumes = () ->
      scope.volOptions.addvol