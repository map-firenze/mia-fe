browsePeople = angular.module 'getUserAccountsModule', ['searchServiceApp', 'translatorAppEn']
browsePeople.directive 'getUserAccountsDir', (searchFactory, searchDeService, modalFactory, deFactory, titlesFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/autocompleters/get-user-accounts/get-user-accounts.html'

  link: (scope, elem, attr) ->
    
    scope.ownerIsClicked = false
    scope.loading = false

    scope.getUserByName = (searchQuery) ->
      if searchQuery
        if searchQuery.length < 3
          scope.loading = false
        if searchQuery.length >= 3
          scope.loading = true
          scope.ownerIsClicked = false
          searchDeService.getUserByName(searchQuery).then (resp)->
            if resp.length
              scope.owners = resp
            else
              scope.owners = null
            scope.loading = false

    scope.$on("ownersClearEvent", () ->
      scope.tempOwnerName = ""
      scope.owners = [])

    scope.$on("ownerIsSelected", (event, ownerInfo) ->
      scope.tempOwnerName = ownerInfo.firstName + ' ' + ownerInfo.lastName
      scope.tempOwnerName = "" if ownerInfo.firstName == undefined or ownerInfo.lastName == undefined
      scope.ownerIsClicked = true)
