browsePlace = angular.module 'findPlace', ['searchServiceApp']
browsePlace.directive 'findPlaceDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/autocompleters/get-places/get-places.html'

  link: (scope, elem, attr) ->
    scope.closeTable = ()->
      scope.searchPlaceTableOpen = false
      
    scope.placeOptions = {}

    scope.singleselect = attr.singleselect
    scope.placeOptions.addplace = (attr.addplace == 'true')

    scope.placeIsClicked = false

    scope.getPlaces = (searchQuery) ->
      scope.placeIsClicked = false
      if searchQuery.length == 0
        scope.searchPlaceTableOpen = false
      if searchQuery.length >= 3
        scope.loadingCircle = true
        scope.searchPlaceTableOpen = true
        deFactory.getPlace(searchQuery).then (resp)->
          if resp.status is "ok"
            scope.places = resp.data.places
          else
            scope.places = null
          scope.loadingCircle = false

    scope.$on("placesClearEvent", () ->
      scope.tempPlaceName = ""
      scope.places = [])

    scope.$on("topicsPlacesClearEvent", () ->
      scope.tempPlaceName = ""
      scope.places = [])

    scope.$on("placeIsSelected", (event, placeInfo) ->
      scope.tempPlaceName = placeInfo.placeName
      scope.placeIsClicked = true)

    scope.openPlace=(row)->
      scope.searchPlaceTableOpen=false
      scope.changeTab('place')
      params = {}
      params.id = row.id
      # params.idName= row.placeNameId
      $state.go 'mia.bio-place', params

    scope.addNewPlace = ()->
      if $state.includes('mia.bio-place')
        scope.$stateParams.id = ''
        $state.reload()
      else
        $state.go 'mia.bio-place'
    
    scope.usedPlaces = () ->
      scope.placeOptions.addplace