browsePeople = angular.module 'findPeople', ['searchServiceApp', 'translatorAppEn']
browsePeople.directive 'findPeopleDir', (searchFactory, modalFactory, deFactory, titlesFactory, toaster, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/autocompleters/get-people/get-people.html'

  link: (scope, elem, attr) ->
    scope.closeTable = ()->
      scope.searchTableOpen = false
    scope.loading = false
    scope.personOptions = {}

    scope.singleselect = attr.singleselect
    scope.personOptions.addperson = (attr.addperson == 'true')
    scope.personIsClicked = false

    scope.addNewPeople = ()->
      scope.searchTableOpen = false
      tempPeopleName = ""
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-person/add-person.html'
        controller:'modalIstanceController'
      ).then (resp) ->
    
    scope.getPeople = (searchQuery) ->
      scope.personIsClicked = false
      if searchQuery
        if searchQuery.length >= 3
          words = scope.countWords(searchQuery)
          if scope.countWords(searchQuery) >= 3
            toaster.pop('error', 'Queries with 3 or more words are not permitted')
            return

          scope.lastSearchQuery = searchQuery

          scope.loading = true
          scope.searchTableOpen = true
          deFactory.getPeople(searchQuery).then (resp)->
            if resp.status is "ok"
              if searchQuery == scope.lastSearchQuery || scope.lastSearchQuery == null
                scope.people = resp.data.people
                scope.loading = false
                scope.searchTableOpen = true
            else
              scope.people = null
              scope.loading = true
              scope.searchTableOpen = false

        else
          scope.people = null
          scope.loading = false
    
    scope.$on("peopleClearEvent", () ->
      scope.tempPeopleName = ""
      scope.people = [])

    scope.$on("personIsSelected", (event, personInfo) ->
      scope.tempPeopleName = personInfo.mapNameLf
      scope.personIsClicked = true)
    
    scope.usedPeople = () ->
      scope.personOptions.addperson

    scope.countWords = (searchQuery) ->
      return searchQuery.trim().split(' ').length