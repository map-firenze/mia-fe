newTitles = angular.module 'newTitles', ['addTitleApp', 'titlesServiceApp']
newTitles.directive 'newTitlesDir', (titlesFactory, modalFactory, $window) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/new-titles/new-titles.html'
  scope:
    title: "="
    onSelect: '&'
    onCancel:'&'
    fromSearch: '='
  link: (scope, elem, attr) ->
    scope.searchTableOpen = false
    scope.queryStr=""
    scope.selectedTitles = {}
    scope.loading = false
    scope.results = null
    scope.getTitles = ()->
      if scope.title.titleOcc.length is 3
        scope.loading = true
        scope.searchTableOpen = true
        titlesFactory.searchTitle(scope.title.titleOcc).then (resp)->
          if resp.data
            scope.results=resp.data.arrayTitleAndOccs
            scope.loading = false
            return
          else
            scope.loading = false
            console.log 'error'
            return
      else
        scope.loading = false


    scope.saveTitle = ()->
      scope.onSelect({params:scope.title})

    scope.cancel = ()->
      scope.onCancel()

    scope.addANewTitle = () ->
      scope.searchTableOpen = false
      scope.title =""
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/mod-new-titles/mod-new-titles.html'
        controller:'modalIstanceController'
      )

    scope.selectTitle = (selectedTitle)->
      scope.searchTableOpen = false
      scope.title =""
      titlesFactory.getTitle(selectedTitle.titleOccId).then (resp)->
        if resp
          modalFactory.openModal(
            templateUrl: 'modules/directives/modal-templates/mod-new-titles/mod-new-titles.html'
            controller:'modalIstanceController'
          )
          modalInstance.categoryId = resp.data.roleCatId.toString()
          modalInstance.name = selectedTitle.titleOcc
          modalInstance.occId = selectedTitle.titleOccId
          modalInstance.modify = true
