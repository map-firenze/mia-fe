docSummary = angular.module 'docSummary', [
    'ui.bootstrap']
docSummary.directive 'docSummaryDir', ($state, deFactory, modDeFactory, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-summary/doc-summary.html'
  
  link: (scope, elem, attr) ->
    scope.months=[
      {'name':'', 'id':0}
      {'name':'January', 'id':1},
      {'name':'February', 'id':2},
      {'name':'March', 'id':3},
      {'name':'April', 'id':4},
      {'name':'May', 'id':5},
      {'name':'June', 'id':6},
      {'name':'July', 'id':7},
      {'name':'August', 'id':8},
      {'name':'September', 'id':9},
      {'name':'October', 'id':10},
      {'name':'November', 'id':11},
      {'name':'December', 'id':12}]

    #show more less topics limit
    scope.showTopicsLimit = 1


    scope.$parent.$watch('deTopics', (newVal)->
      scope.showTopicsLimit = 1
    )

    if $state.params.docId
      deFactory.getDocumentEntityById($state.params.docId).then (resp)->
        scope.summary = resp.data.documentEntity
        scope.feCategory=scope.summary.category.replace(/([A-Z])/g, ' $1').replace(/^./, (str)-> return str.toUpperCase())

    #find topicTitle for document topics
    scope.findTopicTitle = (topicListId) ->
      topic = (scope.allTopics.find (topic) -> topic.topicId == topicListId)
      if topic
        return topic.topicTitle
    
    scope.getShowTopicsLimit = (deTopics) ->
      if deTopics.length > 0
        onlyAddedTopicsArray = (deTopics.filter (topic) -> topic.isNew == false)
        return onlyAddedTopicsArray.length
      return 0
