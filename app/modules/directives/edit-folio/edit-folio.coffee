editFolio = angular.module 'editFolio', []
editFolio.directive 'editFolioDir', ($filter, modalFactory, deFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/edit-folio/edit-folio.html'
  scope:
    folio1: '='
    folio2: '='
    doubleFolio: '='
    onCancel: '&'

  link: (scope, elem, attr) ->
    scrollingElement = (document.scrollingElement || document.body)
    scrollingElement.scrollTop = scrollingElement.scrollHeight
    
    scope.switchDoubleFolio = (value) ->
      scope.doubleFolio = value
      clearFolio2() unless scope.doubleFolio

    scope.cancel = () ->
      scope.onCancel()

    clearFolio2 = () ->
      scope.folio2 = {}

    scope.hasSameValues = () ->
      return false if _.isEmpty(scope.folio2)
      if scope.folio1.folioNumber && !scope.folio1.noNumb && !scope.folio1.rectoverso && scope.folio2.hasOwnProperty('folioNumber') && (!scope.folio2.hasOwnProperty('rectoverso') || scope.folio2.rectoverso == null)
        scope.folio1.folioNumber.toUpperCase() == scope.folio2.folioNumber.toUpperCase()
      else if scope.folio1.folioNumber && !scope.folio1.noNumb && scope.folio1.rectoverso && scope.folio2.hasOwnProperty('folioNumber') && scope.folio2.hasOwnProperty('rectoverso')
        scope.folio1.folioNumber.toUpperCase() == scope.folio2.folioNumber.toUpperCase() && scope.folio1.rectoverso == scope.folio2.rectoverso
      else
        false

    scope.determineFolio2 = () ->
      if scope.folio2.noNumb
        scope.folio2 = { noNumb: true }
      else
        clearFolio2()
    
    scope.saveFolio = () ->
      folios=[]
      if scope.folio1.noNumb
        scope.folio1.folioNumber = ""
        scope.folio1.rectoverso = ""
      if scope.doubleFolio && scope.folio2.noNumb
        scope.folio2.folioNumber = ""
        scope.folio2.rectoverso = ""
      if scope.doubleFolio
        scope.folio2.uploadFileId = scope.folio1.uploadFileId
      folios.push(scope.folio1)
      if scope.doubleFolio
        folios.push(scope.folio2)
      deFactory.deleteFolio(scope.folio1.uploadFileId, scope.folio1.folioId).then (resp) ->
        deFactory.deleteFolio(scope.folio1.uploadFileId, scope.folio2.folioId).then (resp) ->
          folios[0].folioId = ""
          folios[1]?.folioId = ""
          deFactory.saveFolio(folios).then (resp) ->
#            console.log(folios)
            if resp.status is "ok"
              pageTitle = $filter('translate')("modal.DONE")
              messagePage = $filter('translate')("modal.SAVEFOLIOOK")
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
                $state.reload()
            if resp.status is "ko"
              pageTitle = $filter('translate')("modal.ERROR")
              messagePage = $filter('translate')("modal.SAVEFOLIOERROR")
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
                console.log "problem in saving folio"
                console.log resp.message