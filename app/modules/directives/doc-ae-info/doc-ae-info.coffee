docAeInfo = angular.module 'docAeInfo', []
docAeInfo.directive 'docAeInfoDir', ($state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-ae-info/doc-ae-info.html'
  scope:
    info: '='
    deInfo: '='
    currentFolios: '='
    imageId: '='
    filesView: '='

  link: (scope, elem, attr) ->

    scope.goToAe = () ->
      params = {}
#      console.log(scope)
      params.aeId = scope.info.uploadInfoId
      params.fileId = scope.imageId
      #page parameter to be implemented - not easy
#      console.log(params)
      $state.go('mia.singleupload', params)
      