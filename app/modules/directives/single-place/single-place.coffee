singlePlace = angular.module 'singlePlace', [ 'addPlaceApp' ]
singlePlace.directive 'singlePlaceDir', (deFactory, modalFactory, searchDeService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/single-place/single-place.html'
  scope:
    place: '='
    placeModel: '=placeModel'
    role: '='
    onSelect: '&'
    onCancel: '&'

  link: (scope, elem, attr) ->
    scope.searchTableOpen = false
    scope.selected = false
    scope.loading = false

    if scope.place and scope.place.placeAllId
      isNewPlace = false
      scope.unsure = scope.place.unsure is 'u'
    else
      isNewPlace = true

    scope.selectedPlaces = {}

    if scope.place and scope.place.placeName
      scope.tempName = scope.place.placeNameFull

    temp = {}
    temp.id = undefined
    temp.unsure = undefined

    scope.closeTable = ()->
      scope.searchTableOpen = false

    scope.getPlaces = (searchQuery) ->
#      scope.placeModel = ""
      if searchQuery
        if searchQuery.length == 3
          scope.loading=true
          scope.searchTableOpen=true
          deFactory.getPlace(searchQuery).then (resp)->
            if resp.data
              scope.possiblePlaces = resp.data.places
            scope.loading=false

    scope.selectPlace = (place) ->
      if place.prefFlag is 'V'
        searchDeService.getPrincipalPlaces(place.id).then (response) ->
          scope.preparePlaceToSave(response)
          scope.savePlace()
      else
        scope.preparePlaceToSave(place)
        scope.savePlace()

    scope.preparePlaceToSave = (place) ->
      scope.selected = true
      scope.tempName = place.placeName
      temp.id = place.id
      scope.searchTableOpen = false
      scope.selectedPlaces = place
      scope.possiblePlaces = []

    scope.savePlace = ()->
      toPass={}
      toPass.role = scope.role
      if scope.selected
        toPass.temp = temp
      else
        toPass.temp = {}
        toPass.temp.id = scope.place.id
        toPass.temp.unsure = scope.place.unsure
      scope.onSelect({params:toPass})

    scope.addANewPlace = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.setUnsure = (unsure) ->
      scope.place.unsure = "u" if not unsure
      scope.place.unsure = "s" if unsure

    scope.cancel = ()->
      scope.place.editPlace = false
      if isNewPlace
        scope.onCancel({place:scope.place}, {role:scope.role})
