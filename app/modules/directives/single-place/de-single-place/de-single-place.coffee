deSinglePlace = angular.module 'deSinglePlace', [ 'addPlaceApp' ]
deSinglePlace.directive 'deSinglePlaceDir', (deFactory, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/single-place/de-single-place/de-single-place.html'
  scope:
    place: '='
    modifyPlace: '='
#    placeModel: '=placeModel'
    role: '='
    onSelect: '&'
    onCancel: '&'

  link: (scope, elem, attr) ->
    scope.searchTableOpen = false
    scope.selected = false
    scope.loading = false
    scope.unsurePlace = false

    if scope.place and scope.place.id
      isNewPlace = false
      scope.unsurePlace = scope.place.unsure is 'u'
      scope.tempPlace = scope.place.feData.placeName
    else
      isNewPlace = true
      scope.modifyPlace = {}
      scope.tempPlace = ''

    scope.selectedPlaces = {}

#    if scope.place and scope.place.placeName
#      scope.tempName = scope.place.placeName

    temp = {}
    temp.id = undefined
    temp.unsure = undefined

    scope.closeTable = ()->
      scope.searchTableOpen = false

    scope.getPlaces = (searchQuery) ->
      temp.id = undefined
      if searchQuery
        if searchQuery.length == 3
          scope.loading=true
          scope.searchTableOpen=true
          deFactory.getPlace(searchQuery).then (resp)->
            if resp.data
              scope.possiblePlaces = resp.data.places
            scope.loading=false
      else
        scope.searchTableOpen = false


    scope.selectPlace = (place) ->
      scope.selected = true
#      scope.modifyPlace.placeName = place.placeName
      scope.tempPlace = place.placeName
      scope.modifyPlace.unsure == "s"
      temp.id = place.id
      scope.searchTableOpen = false
      scope.selectedPlaces = place
      scope.possiblePlaces = []
      scope.unsurePlace = false
      temp.unsure = false
#      temp.unsure = if scope.unsurePlace then 'u' else 's'
#      scope.savePlace()


    scope.savePlace = ()->
      toPass={}
      toPass.role = scope.role
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'dePlaceEdit_' + scope.role.type, isEdit: false})
      if scope.selected
        toPass.temp = temp
        toPass.temp.unsure = if scope.unsurePlace then 'u' else 's'
      else
        toPass.temp = {}
        toPass.temp.id = scope.place.id
#        toPass.temp.unsure = scope.place.unsure
        toPass.temp.unsure = if scope.unsurePlace then 'u' else 's'
      scope.onSelect({params:toPass})

    scope.addANewPlace = () ->
      scope.modifyPlace.placeName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.cancel = ()->
      scope.place.editPlace = false
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'dePlaceEdit_' + scope.role.type, isEdit: false})
      if isNewPlace
        scope.onCancel({place:scope.place}, {role:scope.role})
