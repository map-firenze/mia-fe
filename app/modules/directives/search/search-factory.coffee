searchServiceApp = angular.module 'searchServiceApp',[]

searchServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
searchServiceApp.factory 'searchFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  searchBasePath = ENV.searchBasePath
  geoBasePath = ENV.geoBasePath

  $rootScope.$on('recordPlaceAction', (evt, placeInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', placeInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )
  
  $rootScope.$on('recordPersonAction', (evt, personInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', personInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )

  $rootScope.$on('recordUploadAction', (evt, uploadInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', uploadInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )

  $rootScope.$on('recordDocAction', (evt, docInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', docInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )

  $rootScope.$on('recordVolAction', (evt, volInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', volInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )

  $rootScope.$on('recordInsAction', (evt, insInfo) ->
    me = JSON.parse(localStorage.getItem('miaMe'))
    if me and me.status is 'ok'
      account = me.data[0].account
      return $http.post('json/historyLog/registerLogAction', insInfo).then () ->
        return $http.get('json/historyLog/getLastAccessedRecords/' + account).then (resp) ->
          $rootScope.$broadcast('receiveLastRecord', resp.data)
          return resp
  )

  factory =

    #-------------------
    # Search services
    #-------------------
    searchAeWord: (params, recordStart = 0, maxResult = 20, columnToOrderBy = 'aeName', orderType = 'asc') ->
      url = searchBasePath+'ae/wordSearch/' + recordStart + '/' + maxResult + '/' + columnToOrderBy + '/' + orderType
      return $http.post(url, params).then (resp) ->
        resp.data

    searchDocWord: (params, recordStart = 0, maxResult = 20, columnToOrderBy = 'docYear', orderType = 'asc') ->
      url = searchBasePath + 'de/wordSearch/' + recordStart + '/' + maxResult + '/' + columnToOrderBy + '/' + orderType
      return $http.post(url, params).then (resp) ->
        resp.data

    browseAe: (params) ->
      return $http.post(searchBasePath+'ae/archivalSearch/', params).then (resp) ->
        resp.data

    getPartialCount:(params)->
      return $http.post(searchBasePath+'ae/archivalSearchPartialCount/', params).then (resp) ->
        resp.data

    searchPeopleWord: (params, recordStart = 0, maxResult = 20, columnToOrderBy = 'MAPNameLF', orderType = 'asc') ->
      url = searchBasePath + 'people/wordSearch/' + recordStart + '/' + maxResult + '/' + columnToOrderBy + '/' + orderType
      return $http.post(url, params).then (resp) ->
        resp.data

    searchPlaceWord: (queryStr) ->
      return $http.get(geoBasePath+'findPlaceNameVariants/'+queryStr).then (resp) ->
        resp.data

    searchPlaceWord2: (params, recordStart = 0, maxResult = 20, columnToOrderBy = 'PLACENAMEFULL', orderType = 'asc') ->
      url = searchBasePath + 'places/wordSearch/' + recordStart + '/' + maxResult + '/' + columnToOrderBy + '/' + orderType
      return $http.post(url, params).then (resp) ->
        resp.data

    basicSearch: (queryStr) ->
      tokens = [].concat.apply [], queryStr.split('"').map (v, i) ->
        return if i%2 then '"' + v + '"' else v.split(' ')
      .filter(Boolean)
      params={}
      params.partialSearchWords = ''
      params.matchSearchWords = ''
      params.searchType = "ALL"

      tokens.forEach (token) ->
        if token.charAt(0) == '"'
          params.matchSearchWords += token.substr(1, token.length - 2) + ' '
        else
          params.partialSearchWords += token + ' '

      if params.matchSearchWords != ''
        params.matchSearchWords = params.matchSearchWords.substr(0, params.matchSearchWords.length - 1)

      if params.partialSearchWords != ''
        params.partialSearchWords = params.partialSearchWords.substr(0, params.partialSearchWords.length - 1)

#      console.log params
      return $http.post(searchBasePath + 'all/countWordSearch/', params).then (resp) ->
        resp.data

    getMyResearchHistory: (account, recordStart = 0, maxResult = 20, columnToOrderBy = 'dateAndTime', orderType = 'desc', type) ->
      url = 'json/historyLog/getMyResearchHistory/' + account + '/' + recordStart + '/' + maxResult + '/' + columnToOrderBy + '/' + orderType
      url += '?type=' + type if type
      return $http.get(url).then (resp) ->
        resp.data

    deleteMyResearchHistory: (account) ->
      return $http.post('json/historyLog/deleteMyResearchHistory/' + account).then (resp) ->
        resp.data

    getLastRecords: (account) ->
      return $http.get('json/historyLog/getLastAccessedRecords/'+account).then (resp) ->
        resp.data

    createAddVolume: (paramsVol) ->
      return $http.post('json/volumeInsertDescription/addVolume', paramsVol).then (resp) ->
        resp.data

    createAddInsert: (paramsIns) ->
#      console.log(paramsIns)
      return $http.post('json/volumeInsertDescription/addInsert', paramsIns).then (resp) ->
        resp.data

    findVolumeDescription: (volumeId) ->
      return $http.get('json/volumeInsertDescription/findVolume/' + volumeId + '/').then (resp) ->
        resp.data

    findInsertDescription: (insertId) ->
      return $http.get('json/volumeInsertDescription/findInsert/' + insertId + '/').then (resp) ->
        resp.data

  return factory
