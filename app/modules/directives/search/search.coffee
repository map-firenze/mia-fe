search = angular.module 'search', [
  'searchServiceApp'
  'ui.grid'
  'ngCookies'
  'newTitles'
  'searchArchive'
  'selectArchive'
  'browsePeople'
  'browsePlace'
  'searchPeople'
  'searchPlace'
  'searchDocument'
  'searchVol'
  'browseVolume'
  'createVol'
  'mapdocidSearch'
]
search.directive 'searchDir', (searchFactory, commonsFactory, $cookies, $rootScope, $http, $location, modalFactory, deFactory, titlesFactory, $state, $timeout, $stateParams) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/search.html'

  link: (scope, elem, attr) ->

    winloc = window.location
    scope.status=""
    scope.lastRecords = {}
    scope.archivalSearch = {}
    scope.isNewsFeedSearch = false

    scope.documentTabs = [
      { 'name': 'advancedSearch', 'value': true },
      { 'name': 'idSearch', 'value': false },
      { 'name': 'newsfeedSearch', 'value': false }
    ]

    scope.switchPanel = () ->
      scope.searchOpen = !scope.searchOpen

    scope.changeTab=(newTab)->
#      console.log newTab
      openNow=false
      if not scope.searchOpen
        scope.searchOpen = true
        openNow=true
      if (scope.status is newTab) and (not openNow)
        scope.searchOpen = false
      else
        scope.status = newTab
      if newTab is 'volume' or newTab is 'volume'
        $rootScope.$broadcast('resetVolumeTab')

    scope.searchVolTabSelected = () ->
      $rootScope.$broadcast('resetVolumeTab')

    scope.createVolTabSelected = () ->
      $rootScope.$broadcast('resetVolumeTab')

    scope.changeTab('ae')
    scope.searchOpen = false
    scope.titleToEdit = {}

    scope.addNewPlace = ()->
      openNow=false
      scope.searchOpen = false
      if $state.includes('mia.bio-place')
        scope.$stateParams.id = ''
        $state.reload()
      else
        $state.go 'mia.bio-place'

    $rootScope.$on('receiveLastRecord', (e, resp) ->
#      console.log 'receiveLastRecord', resp
      if resp.status is 'ok'
        scope.lastRecords = resp.data
      else
        scope.lastRecords = {}
    )

    $rootScope.$on '$stateChangeSuccess', ->
      scope.searchOpen = false

    scope.goToLastPlace = () ->
      $state.go 'mia.bio-place', {'id': scope.lastRecords.lastPlace}

    scope.goToLastPerson = () ->
      $state.go 'mia.bio-people', {'id': scope.lastRecords.lastPerson}

    scope.goToLastUpload = () ->
      winloc.href = winloc.protocol + '//' + winloc.host + '/Mia/index.html#/mia/archival-entity/' + scope.lastRecords.lastUpload + '?page=1'

    scope.goToLastDoc = () ->
      winloc.href = winloc.protocol + '//' + winloc.host + '/Mia/index.html#/mia/document-entity/' + scope.lastRecords.lastDocument

    scope.goToLastVolOrIns = () ->
      if scope.lastRecords.lastVolumeOrInsert.type == 'Volume'
        $state.go 'mia.volumeDescr', {'volumeId': scope.lastRecords.lastVolumeOrInsert.id}
      else if scope.lastRecords.lastVolumeOrInsert.type == 'Insert'
        $state.go 'mia.insertDescr', {'insertId': scope.lastRecords.lastVolumeOrInsert.id}

    scope.$on('show-advanced-search', () ->

      $timeout (->
        # Open panel and select document tab
        scope.searchOpen = true
        scope.status = 'de'

        scope.setDocumentsActiveTab('advancedSearch')
      )

      $rootScope.$broadcast('refine-search')
    )

    scope.$on('show-newsfeed-search', () ->

      $timeout (->
        # Open panel and select document tab
        scope.searchOpen = true
        scope.status = 'de'
        
        scope.setDocumentsActiveTab('newsfeedSearch')
      )
    )

    scope.setDocumentsActiveTab = (tabName) ->
      scope.isNewsFeedSearch = false

      if tabName == 'newsfeedSearch'
        scope.isNewsFeedSearch = true

      $rootScope.$broadcast('updateIsNewFeedSearch', scope.isNewsFeedSearch)

      angular.forEach(scope.documentTabs, (tab) ->
        if tab.name is tabName
          tab.value = true
        else
          tab.value = false
      )
