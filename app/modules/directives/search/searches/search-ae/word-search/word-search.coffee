searchArchive = angular.module 'searchArchive', ['searchServiceApp']
searchArchive.directive 'searchArchiveDir', (searchFactory, modalFactory, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-ae/word-search/word-search.html'

  link: (scope, elem, attr) ->
    scope.searchWhere = "AE_NAME_AND_DESCRIPTION"
    scope.searchType = 'all'
    scope.wordSearchAe = (query, where, type)->
      params = {}
      params.owner = scope.$root.me.account
      params.partialSearchWords = ''
      params.matchSearchWords = ''
      # params.searchString = query
      params.searchType = where
      params.onlyMyArchivalEnties = (type=='mine')
      params.everyoneElsesArchivalEntities = (type=='other')
      params.allArchivalEntities = (type=='all')

      # pip lup luoo "pippo"
      # matchSearchWords: "pippo"
      # partialSearchWords: "pip lup luoo"

      tokens = [].concat.apply [], query.split('"').map (v, i) ->
        return if i%2 then '"' + v + '"' else v.split(' ')
      .filter(Boolean)

      tokens.forEach (token) ->
        if token.charAt(0) == '"'
          params.matchSearchWords += token.substr(1, token.length - 2) + ' '
        else
          params.partialSearchWords += token + ' '

      if params.matchSearchWords != ''
        params.matchSearchWords = params.matchSearchWords.substr(0, params.matchSearchWords.length - 1)

      if params.partialSearchWords != ''
        params.partialSearchWords = params.partialSearchWords.substr(0, params.partialSearchWords.length - 1)
      
      searchFactory.searchAeWord(params).then (resp)->
        newResult = {}
        newResult.searchName = query
        place = 'All images'             if type=='all'
        place = 'Only my images'         if type=='mine'
        place = 'Everyone elses images'  if type=='other'
        whereS = 'Title and Description'            if where == 'AE_NAME_AND_DESCRIPTION'
        whereS = 'Title'                            if where == 'AE_NAME'
        whereS = 'Description'                      if where == 'AE_DESCRIPTION'
        newResult.content = {
          searchType: newResult.searchName,
          where: place,
          where2: whereS,
          title: 'Images Search',
          matchSearchWords: params.matchSearchWords,
          partialSearchWords: params.partialSearchWords,
          countResult: resp.data?.count || 0,
          params: params
        }
        newResult.type='ae'
        newResult.subtype='word_search'
        newResult.results=resp.data.aentities

        if newResult.results.length
          scope.$root.$broadcast('new-result', newResult)
        else
          pageTitle = $filter('translate')("modal.WARNING")
          messagePage = 'No results found!'
          modalFactory.openMessageModal(pageTitle, messagePage, false, "sm")
