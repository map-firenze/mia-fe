selectArchive = angular.module 'selectArchive', ['searchServiceApp']
selectArchive.directive 'selectArchiveDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state, $sce, searchDeService, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-ae/archival-search/archival-search.html'

  link: (scope, elem, attr) ->

    countResults = (archiveInfo, type) ->
      params = {}
      params.owner = scope.$root.me.account
      params.repositoryId = 1
      params.collectionId = archiveInfo.collection?.collectionId or ""
      params.seriesId = archiveInfo.series?.seriesId or ""
      params.volumeId = archiveInfo.volume?.volumeId or ""
      params.insertId = archiveInfo.insert?.insertId or ""
      params.onlyMyArchivalEnties = (type == 'other')
      params.everyoneElsesArchivalEntities = (type == 'mine')
      params.allArchivalEntities = (type == 'all')
      searchFactory.getPartialCount(params).then (resp) ->
        scope.showResults = true
        if resp.data
          scope.resultsNumber = resp.data.recordsCount

    scope.addRepo = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
        controller:'modalIstanceController'
      ).then (selectedRepo) ->
        scope.archivalSearch.repository = selectedRepo
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addCollection = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> { 'parentId': scope.archivalSearch.repository.repositoryId, 'fromSearch': true }
          }
      ).then (selectedCollection) ->
        scope.archivalSearch.collection = selectedCollection
        scope.archivalSearch.volume     = undefined
        scope.archivalSearch.insert     = undefined
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addVolume = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-volume/add-volume.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> { "parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true }
          }
      ).then (selectedVolume) ->
        scope.archivalSearch.volume = selectedVolume
        scope.archivalSearch.insert = undefined
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addSeries = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
        controller: 'modalIstanceController'
        resolve: {
          options:
            -> { "parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true }
          }
      ).then (selectedSeries) ->
        scope.archivalSearch.series = selectedSeries
        if searchType
          countResults(scope.archivalSearch, searchType)
    
    scope.addInsert = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-insert/add-insert.html'
        controller:'modalIstanceController'
        resolve: {
          options:
            -> { "parentId": scope.archivalSearch.volume.volumeId, 'fromSearch': true,
            "collection": scope.archivalSearch.collection,
            "parentName": scope.archivalSearch.volume.volumeName }
          }
      ).then (selectedInsert) ->
        scope.archivalSearch.insert = selectedInsert
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.browseAe = (archiveInfo, type) ->
      params = {}
      params.owner = scope.$root.me.account
      params.repositoryId = archiveInfo.repository.repositoryId
      params.collectionId = archiveInfo.collection?.collectionId or ""
      params.seriesId = archiveInfo.series?.seriesId or ""
      params.volumeId = archiveInfo.volume?.volumeId or ""
      params.insertId = archiveInfo.insert?.insertId or ""
      params.onlyMyArchivalEnties = ( type == 'other' )
      params.everyoneElsesArchivalEntities = (type == 'mine')
      params.allArchivalEntities = (type == 'all')
      place  = 'All images'            if type=='all'
      place  = 'Only my images'       if type=='mine'
      place  = 'Everyone elses images' if type=='other'
      whereS = {}
      whereS.repository = archiveInfo.repository
      whereS.collection = archiveInfo.collection
      whereS.series     = archiveInfo.series
      whereS.volume     = archiveInfo.volume
      whereS.insert     = archiveInfo.insert

      searchName = ''
      if archiveInfo.collection
        searchName = archiveInfo.collection.collectionAbbreviation
        searchName += ' - ' + archiveInfo.volume.volumeName if archiveInfo.volume
        searchName += ' - ' + archiveInfo.insert.insertName if archiveInfo.insert
      else
        searchName = archiveInfo.repository.repositoryName

      searchFactory.browseAe(params).then (resp) ->
        if resp.data
          newResult = {}
          newResult.searchName = searchName
          newResult.results = resp.data.aentities
          newResult.content = {
            searchType: newResult.searchName,
            where: place,
            where2: whereS,
            title: 'Images Search'
          }

          newResult.type = 'ae'
          newResult.subtype='archival_search'
          if newResult.results.length
            scope.$root.$broadcast('new-result', newResult)
          else
            pageTitle = $filter('translate')("modal.WARNING")
            messagePage = 'No results found!'
            modalFactory.openMessageModal(pageTitle, messagePage, false, "sm")

    scope.reset = () ->
      scope.archivalSearch.collection = undefined
      scope.archivalSearch.volume = undefined
      scope.archivalSearch.series = undefined
      scope.archivalSearch.insert = undefined
      scope.showResults = false
    
    scope.reset()

    #####################
    # Main
    #####################
    scope.searchAeType = 'all'
    scope.showResults = false

    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      scope.archivalSearch.repository = mainRepo
