mapdocidSearch = angular.module 'mapdocidSearch', [
  'searchServiceApp'
  'searchCountApp'
  'translatorAppEn']
mapdocidSearch.directive 'mapdocidSearchDir', ($http, $location, $state, alertify) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/mapdocid-search/mapdocid-search.html'

  link: (scope, elem, attr) ->
    scope.loading = true
    scope.dynamicPopover = {}
    scope.dynamicPopover.templateUrl = 'modules/right-sidebar/simple-popover-template.html'

    scope.mapDocIdSearch = ->
      if typeof scope.mapdocid == 'undefined' or scope.mapdocid == ''
        alertify.alert('Please enter mapdocid#')
        return
      if isNaN(scope.mapdocid)
        alertify.alert('Please enter a number not a string')
      else
        request = $http.get('json/document/findDocument/' + scope.mapdocid).then ((resp) ->
          scope.data = resp.data
          resp.data
          if resp.data.status == 'ok'
            if scope.mapdocid == $location.path().replace(/\/mia\/document-entity\//g, '')
              alertify.alert('You are already on this document file')
            else
              $state.go 'mia.modifyDocEnt', {'docId':    scope.mapdocid}
          else
            alertify.alert(resp.data.message)
        )
      return
