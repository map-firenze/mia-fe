createVol = angular.module 'createVol', ['searchServiceApp']
createVol.directive 'createVolDir', (searchFactory, modalFactory, deFactory, titlesFactory, $rootScope, $state, $sce, searchDeService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/create-add-vol/create-add-vol.html'

  link: (scope, elem, attr) ->

    countResults=(archiveInfo, type)->
      params={}
      params.owner=scope.$root.me.account
      params.repositoryId=1
      params.collectionId=archiveInfo.collection?.collectionId or ""
      params.seriesId=archiveInfo.series?.seriesId or ""
      params.volumeId=archiveInfo.volume?.volume or ""
      params.insertId=""
      params.onlyMyArchivalEnties = (type=='other')
      params.everyoneElsesArchivalEntities = (type=='mine')
      params.allArchivalEntities = (type=='all')
      searchFactory.getPartialCount(params).then (resp)->
        scope.showResults = true
        if resp.data
          scope.resultsNumber=resp.data.recordsCount

    scope.addRepo = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
        controller:'modalIstanceController'
      ).then (selectedRepo) ->
        scope.archivalSearch.repository = selectedRepo
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addCollection = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {'parentId': scope.archivalSearch.repository.repositoryId, 'fromSearch': true}
          }
      ).then (selectedCollection) ->
        scope.archivalSearch.collection = selectedCollection
        if searchType
          countResults(scope.archivalSearch, searchType)

    # scope.addVolume = () ->
    #   # open modal
    #   modalFactory.openModal(
    #     templateUrl: 'modules/directives/modal-templates/modal-create-add-volume/modal-create-add-volume.html'
    #     controller:'modalIstanceController'
    #   ).then (selectedVolume) ->
    #     scope.selectedVolume = selectedVolume

    # scope.addInsert = () ->
    #   # open modal
    #   modalFactory.openModal(
    #     templateUrl: 'modules/directives/modal-templates/modal-create-add-insert/modal-create-add-insert.html'
    #     controller:'modalIstanceController'
    #   ).then (selectedInsert) ->
    #     scope.selectedInsert = selectedInsert

    $rootScope.$on("volumeNoSaved", (evt, volumedata) ->
      scope.savedVolume = volumedata
    )

    scope.addVolume = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-create-add-volume/modal-create-add-volume.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.collection.collectionId}
          }
      ).then (selectedVolume) ->
        scope.archivalSearch.volume = selectedVolume
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addInsert = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-create-add-insert/modal-create-add-insert.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"collectionId": scope.archivalSearch.collection.collectionId
            "volumeNo": scope.savedVolume}
          }
      ).then (selectedInsert) ->
        scope.archivalSearch.insert = selectedInsert
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addSeries = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true}
          }
      ).then (selectedSeries) ->
        scope.archivalSearch.series = selectedSeries
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.browseAe = (archiveInfo, type)->
#      console.log(archiveInfo)
      params={}
      params.owner = scope.$root.me.account
      params.repositoryId = archiveInfo.repository.repositoryId
      params.collectionId = archiveInfo.collection?.collectionId or ""
      params.seriesId = archiveInfo.series?.seriesId or ""
      params.volumeId = archiveInfo.volume?.volume or ""
      params.insertId = ""
      params.onlyMyArchivalEnties = (type=='other')
      params.everyoneElsesArchivalEntities = (type=='mine')
      params.allArchivalEntities = (type=='all')
      searchFactory.browseAe(params).then (resp)->
        if resp.data
          newResult = {}
          newResult.searchName = angular.copy(archiveInfo.repository.repositoryName)
          newResult.type = 'ae'
          newResult.results = resp.data.aentities
          newResult.content = {
            searchType: newResult.searchName,
            where: archiveInfo.repository?.repositoryName + ' ' + archiveInfo.collection?.collectionName + ' ' + archiveInfo.series?.seriesName + ' ' + archiveInfo.volume?.volumeName,
            title: 'Images search'
          }
          scope.$root.$broadcast('new-result', newResult)

    scope.reset = ()->
      scope.archivalSearch.collection = undefined
      scope.archivalSearch.volume = undefined
      scope.archivalSearch.series = undefined
      scope.showResults = false
    
    scope.$on('resetVolumeTab', (evt) ->
      scope.archivalSearch.collection = undefined
      scope.archivalSearch.volume = undefined
      scope.archivalSearch.series = undefined
      scope.showResults = false
    )

    #####################
    # Main
    #####################
    scope.searchAeType = 'all'
    scope.showResults = false

    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      scope.archivalSearch.repository = mainRepo
