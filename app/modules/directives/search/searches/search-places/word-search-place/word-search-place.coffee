searchPlace = angular.module 'searchPlace', ['searchServiceApp']
searchPlace.directive 'searchPlaceDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state, $sce) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-places/word-search-place/word-search-place.html'

  link: (scope, elem, attr) ->
    scope.placeSearchWhere = "ALL_NAME_TYPES"

    scope.wordSearchPlace=(query, where)->
      searchFactory.searchPlaceWord(query).then (resp)->
        newResult={}
        newResult.searchName = angular.copy(query)
        newResult.type='place'
        newResult.results=resp.data
        placeToSearch = 'in all name types' if where=='ALL_NAME_TYPES'
        placeToSearch = 'in appellatives' if where=='APPELLATIVE'
        placeToSearch = 'in family names' if where=='FAMILY'
        placeToSearch = 'in given names' if where=='GIVEN'
        placeToSearch = 'in maiden names' if where=='MAIDEN'
        placeToSearch = 'in married' if where=='MARRIED'
        placeToSearch = 'in patronimics' if where=='PATRONIMIC'
        newResult.content = {
          searchType: newResult.searchName,
          where: placeToSearch,
          title: 'Place search'
        }
        scope.$root.$broadcast('new-result', newResult)

    scope.selectLocation=(place)->
      newResult={}
      newResult.searchName = angular.copy(place.placeName)
      newResult.type='place'
      newResult.results=[place]
      scope.searchPlaceTableOpen = false
      scope.$root.$broadcast('new-result', newResult)
      scope.changeTab('place')
