browsePlace = angular.module 'browsePlace', ['searchServiceApp']
browsePlace.directive 'browsePlaceDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state, $rootScope) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-places/search-place/search-place.html'

  link: (scope, elem, attr) ->
    scope.loading = false
    scope.closeTable = ()->
      scope.searchPlaceTableOpen = false

    scope.getPlaces = (searchQuery) ->
      if searchQuery.length == 0
        scope.searchPlaceTableOpen = false
      if searchQuery.length >= 3
        scope.loading=true
        scope.searchPlaceTableOpen = true
        deFactory.getPlace(searchQuery).then (resp)->
          if resp.status is "ok"
            scope.possiblePlaces = resp.data.places
          else
            scope.possiblePlaces = null
          scope.loading=false
      else
        scope.loading = false
        scope.searchPlaceTableOpen = false

    scope.openPlace=(row)->
      scope.searchTableOpen=false
      scope.changeTab('place')
      params = {}
      params.id = row.id
      # params.idName= row.placeNameId
      $state.go 'mia.bio-place', params

    scope.addNewPlace = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller:'modalIstanceController'
      ).then (resp) ->