basicSearch = angular.module 'basicSearch', [
  'searchServiceApp'
  'searchCountApp'
  'translatorAppEn']
basicSearch.directive 'basicSearchDir', (searchFactory, modalFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/basic-search/basic-search.html'

  link: (scope, elem, attr) ->
    scope.loading = true
    scope.dynamicPopover = {}
    scope.dynamicPopover.templateUrl = 'modules/right-sidebar/simple-popover-template.html'

    scope.basicSearch = (queryString) ->
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-search-count/search-count.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {'queryString': queryString}
          }
      ).then (selectedSearch) ->
        console.log('ricerca effettuata')

