searchVol = angular.module 'searchVol', ['searchServiceApp']
searchVol.directive 'searchVolDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state, $sce, volumeInsertDescrFactory, searchDeService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-vol/search-vol.html'

  link: (scope, elem, attr) ->

    countResults=(archiveInfo, type)->
      params={}
      params.owner=scope.$root.me.account
      params.repositoryId=1
      params.collectionId=archiveInfo.collection?.collectionId or ""
      params.seriesId=archiveInfo.series?.seriesId or ""
      params.volumeId=archiveInfo.volume?.volume or ""
      params.insertId=""
      params.onlyMyArchivalEnties = (type=='other')
      params.everyoneElsesArchivalEntities = (type=='mine')
      params.allArchivalEntities = (type=='all')
      searchFactory.getPartialCount(params).then (resp)->
        scope.showResults = true
        if resp.data
          scope.resultsNumber=resp.data.recordsCount

    scope.addRepo = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-repo/add-repo.html'
        controller:'modalIstanceController'
      ).then (selectedRepo) ->
        scope.archivalSearch.repository = selectedRepo
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addCollection = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-collection/add-collection.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {'parentId': scope.archivalSearch.repository.repositoryId, 'fromSearch': true}
          }
      ).then (selectedCollection) ->
        scope.archivalSearch.collection = selectedCollection
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addVolume = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-volume/add-volume.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true}
          }
      ).then (selectedVolume) ->
#        console.log 'addVolume', selectedVolume
        scope.archivalSearch.volume = selectedVolume
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addVolumeSearch = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-volume-search/add-volume-search.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true}
          }
      ).then (selectedVolume) ->
#        console.log 'addVolumeSearch', selectedVolume
        scope.archivalSearch.volume = selectedVolume
        volumeInsertDescrFactory.findVolumeDescription(scope.archivalSearch.volume.volumeId).then (resp) ->
          if resp.status is "ok"
            scope.archivalSearch.volume.hasInserts = resp.data.data.hasInserts
            scope.archivalSearch.volume.aeCount = resp.data.data.aeCount
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addInsertSearch = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-insert-search/add-insert-search.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.volume.volumeId, 'fromSearch': true, 'collection': scope.archivalSearch.collection, 'parentName': scope.archivalSearch.volume.volumeName, 'hasInserts': scope.archivalSearch.volume.hasInserts}
          }
      ).then (selectedInsert) ->
        scope.archivalSearch.insert = selectedInsert
        scope.archivalSearch.volume.hasInserts = true
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.addSeries = (searchType) ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-series/add-series.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {"parentId": scope.archivalSearch.collection.collectionId, 'fromSearch': true}
          }
      ).then (selectedSeries) ->
        scope.archivalSearch.series = selectedSeries
        if searchType
          countResults(scope.archivalSearch, searchType)

    scope.browseAe = (archiveInfo, type)->
#      console.log(archiveInfo)
      params={}
      params.owner = scope.$root.me.account
      params.repositoryId = archiveInfo.repository.repositoryId
      params.collectionId = archiveInfo.collection?.collectionId or ""
      params.seriesId = archiveInfo.series?.seriesId or ""
      params.volumeId = archiveInfo.volume?.volume or ""
      params.insertId = ""
      params.onlyMyArchivalEnties = (type=='other')
      params.everyoneElsesArchivalEntities = (type=='mine')
      params.allArchivalEntities = (type=='all')
      searchFactory.browseAe(params).then (resp)->
        if resp.data
          newResult = {}
          newResult.searchName = angular.copy(archiveInfo.repository.repositoryName)
          newResult.type = 'ae'
          newResult.results = resp.data.aentities
          newResult.content = {
            searchType: newResult.searchName,
            where: archiveInfo.repository?.repositoryName + ' ' + archiveInfo.collection?.collectionName + ' ' + archiveInfo.series?.seriesName + ' ' + archiveInfo.volume?.volumeName,
            title: 'Images search'
          }
          scope.$root.$broadcast('new-result', newResult)

    scope.reset = ()->
      scope.archivalSearch.collection = undefined
      scope.archivalSearch.volume = undefined
      scope.archivalSearch.series = undefined
      scope.showResults = false

    scope.$on('resetVolumeTab', (evt) ->
      scope.archivalSearch.collection = undefined
      scope.archivalSearch.volume = undefined
      scope.archivalSearch.series = undefined
      scope.showResults = false
    )

    scope.resetCollection = () ->
      scope.archivalSearch.collection=undefined
      scope.archivalSearch.volume=undefined
      scope.archivalSearch.insert=undefined

    scope.resetVolume = () ->
      scope.archivalSearch.volume=undefined
      scope.archivalSearch.insert=undefined

    scope.resetInsert = () ->
      scope.archivalSearch.insert=undefined

    scope.goToVolume = () ->
      $state.go("mia.volumeDescr", {'volumeId': scope.archivalSearch.volume.volumeId, 'collectionId': scope.archivalSearch.collection.collectionId})

    scope.goToInsert = () ->
      $state.go("mia.insertDescr", {'insertId': scope.archivalSearch.insert.insertId, 'collectionId': scope.archivalSearch.collection.collectionId})

    #####################
    # Main
    #####################
    scope.searchAeType = 'all'
    scope.showResults = false

    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      scope.archivalSearch.repository = mainRepo
