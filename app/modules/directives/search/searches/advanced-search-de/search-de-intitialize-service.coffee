searchDeModule.service 'searchDeInitializeService', ($http, ENV, searchDeService) ->

  getCollection = () ->
    return searchDeService.getCollection().then( (response) ->
      return response
    )

  selectionArchivalLocation = () ->
    return  {
      collection: { collectionId: 0, collectionName: '' },
      serie:      { seriesId: 0, seriesName: '' },
      volume:     { volumeId: 0, volumeName: '' },
      insert:     { insertId: 0, insertName: '' }
    }

  archivalLocation = () ->
    return {
      "searchSection": "archivalLocationSearch",
      "type": "archivalLocationAdvancedSearch",
      "isActiveFilter": false,
      "collection": "",  # stringa     obbligatorio
      "series": "",      # stringa     nullabile
      "volume": "",      # numerico    nullabile
      "insert": ""       # numerico??  nullabile??
    }

  selectionDocumentCategory = () ->
    return {
      category: "",
      typology: ""
    }

  documentCategory = () ->
    return {
      "searchSection": "categoryAndTypologySearch",
      "type": "categoryAndTypologyAdvancedSearch",
      "isActiveFilter": false,
      "category": null, # stringa     obbligatorio
      "typology": null  # stringa     nullabile
    }

  transcriptionsAndSynopsis = () ->
    return {
      transcription: "",
      synopsis: ""
    }

  transcription = () ->
    return {
      "searchSection": "transcriptionSearch",
      "type": "transcriptionAdvancedSearch",
      "isActiveFilter": false,
      "transcription": ""
    }

  synopsis = () ->
    return {
      "isActiveFilter": false,
      "searchSection": "synopsisSearch",
      "type": "synopsisAdvancedSearch",
      "synopsis": ""
    }

  selectionPeopleAndPlaces = () ->
    return {
      "people": [] ,
      "places": []
    }
    
  people = () ->
    return {
      "searchSection": "peopleSearch",
      "type": "peopleAdvancedSearch",
      "isActiveFilter": false,
      "people": []
    }

  places = () ->
    return  {
      "searchSection": "placesSearch",
      "type": "placesAdvancedSearch",
      "isActiveFilter": false,
      "places": []
    }

  topicAndRelatedPlaces = () ->
    return {
      "searchSection": "topicsSearch",
      "type": "topicsAdvancedSearch",
      "isActiveFilter": false,
      "topics":[]
    }
       
  owner = () ->
    return{
      "searchSection": "documentOwnerSearch",
      "type": "documentOwnerAdvancedSearch",
      "isActiveFilter": false,
      "editType": "owner",
      "account": ""
    }

  months = () ->
    [
      { num: '', name: '' },
      { num: 1, name: 'January' },
      { num: 2, name: 'February' },
      { num: 3, name: 'March' },
      { num: 4, name: 'April' },
      { num: 5, name: 'May' },
      { num: 6, name: 'June' },
      { num: 7, name: 'July' },
      { num: 8, name: 'August' },
      { num: 9, name: 'September' },
      { num: 10, name: 'October' },
      { num: 11, name: 'November' },
      { num: 12, name: 'December' }
    ]
  
  filterDate = () ->
    [
      { value: "from", label: "Written from" },
      { value: "before", label: "Written before" },
      { value: "between", label: "Written between" },
      { value: "in", label: "Written in/on" }
    ]

  dates = () ->
    return {
      "searchSection": "dateSearch",
      "type": "dateAdvancedSearch",
      "isActiveFilter": false,
      "dateFilterType": "",
      "dateYear": "",
      "dateMonth": "",
      "dateDay": "",
      "dateBYear": "",
      "dateBMonth": "",
      "dateBDay": ""
    }

  dateA = () ->
    return {
      filter: "in",
      year: "",
      month: "",
      day: ""
    }

  dateB = () ->
    return {
      year: "",
      month: "",
      day: ""
    }

  languages = () ->
    return {
      "searchSection": "languagesSearch",
      "type": "languagesAdvancedSearch",
      "isActiveFilter": false,
      "languages": []
    }
  

  service = {
    getCollection: getCollection,
    selectionArchivalLocation: selectionArchivalLocation,
    archivalLocation: archivalLocation,
    selectionDocumentCategory: selectionDocumentCategory,
    documentCategory: documentCategory,
    transcriptionsAndSynopsis: transcriptionsAndSynopsis,
    transcription: transcription,
    synopsis: synopsis,
    selectionPeopleAndPlaces: selectionPeopleAndPlaces,
    people: people,
    places: places,
    topicAndRelatedPlaces: topicAndRelatedPlaces,
    owner: owner,
    months: months,
    filterDate: filterDate,
    dates: dates,
    dateA: dateA,
    dateB: dateB,
    languages: languages,
  }

  return service