searchDeModule = angular.module 'searchDe', ['searchServiceApp','ui.bootstrap', 'translatorAppEn', 'findPeople', 'findPlace', 'getUserAccountsModule']

searchDeModule.directive 'searchDeDir', (searchFactory, modalFactory, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/advanced-search-de/search-de.html'

searchDeModule.controller 'search-de-controller', ($scope, searchDeInitializeService, searchDeService, $rootScope, modalFactory, $filter, $timeout) ->
  isOpen = false
  $scope.searchArray = []
  $scope.loadingProcess = false

  PEOPLE_SECTION = 3
  TOPICS_SECTION = 4

  $scope.sections = [
    {name: 'Archival Location', open: false},
    {name: 'Document Category', open: false},
    {name: 'Transcriptions and/or Synopsis', open: false},
    {name: 'People and Places', open: false},
    {name: 'Topics', open: false},
    {name: 'Date Range', open: false},
    {name: 'Languages', open: false},
    {name: 'Owner', open: false}
  ]

  # For detect last selected search
  $scope.selectedSearches = [
    {type: 'ArcLoc', selected: false, data: null, order: 0},
    {type: 'DocCat', selected: false, data: null, order: 1},
    {type: 'Trans',  selected: false, data: null, order: 2},
    {type: 'Syn',    selected: false, data: null, order: 3},
    {type: 'People', selected: false, data: null, order: 4},
    {type: 'Place',  selected: false, data: null, order: 5},
    {type: 'Top',    selected: false, data: null, order: 6},
    {type: 'DateR',  selected: false, data: null, order: 7},
    {type: 'Lang',   selected: false, data: null, order: 8},
    {type: 'Owner',  selected: false, data: null, order: 9}
  ]

  setSelectedData = (type, selected, data) ->
    search = _.find $scope.selectedSearches, type: type
    if search
      search.selected = selected
      search.data     = data

  $scope.init = () ->
    initMainRepo()
    initFilters()

  ################################# Watch last closed tab #################################
  $scope.$watch 'sections', (newVal, oldVal) ->
    lastClosedTab = _.find(oldVal, { open: true })

    return unless lastClosedTab

    switch lastClosedTab.name
      when 'Archival Location'
        $scope.initializeSelectionArchivalLocation()
        $scope.selectionAL.collection.collectionId = 0
        $scope.inputCollection = {}
        $scope.tempVolume = {}
        $scope.tempInsert = {}
      when 'Document Category'
        $scope.initializeSelectionDocumentCategory()
      when 'Transcriptions and/or Synopsis'
        $scope.initializeSelectionTranscriptionsAndSynopsis()
      when 'People and Places'
        initializeselectionPeopleAndPlaces()
        $scope.$broadcast("peopleClearEvent")
        $scope.$broadcast("placesClearEvent")
      when 'Topics'
        $scope.initializeSelectionTopicAndRelatedPlace()
        $scope.initializeSearchedTopicsAndRelatedPlaces()
        $scope.$broadcast("placesClearEvent")
      when 'Date Range'
        $scope.initializeDateA()
        $scope.initializeDateB()
      when 'Languages'
        $scope.selectedLanguage = { language: '' }
      when 'Owner'
        $scope.selectionOwner = { account: '', in: '' }
        $scope.$broadcast("ownersClearEvent")
  , true

  ##############################################################################
  ######################### ARCHIVAL LOCATION SECTION ##########################
  ##############################################################################

  ################################# INITIALIZE #################################
  $scope.selectedArchivalLocation = {}
  $scope.inputCollection = {}

  initMainRepo = () ->
    searchDeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      $scope.repository = mainRepo

  initArchivalLocation = () ->
    getCollection()
    # inizializza l'oggetto interno
    $scope.selectionAL =  $scope.initializeSelectionArchivalLocation()
    # inizzializza l'oggeto da spedire
    $scope.archivalLocation = searchDeInitializeService.archivalLocation()

  # collections
  $scope.collections =  []

  getCollection = () ->
    searchDeInitializeService.getCollection().then( (response) ->
      $scope.collections = response
    )
 
  # series
  $scope.series = []

  initializeSeries = () ->
    $scope.series = [{ "seriesId": 0, "seriesName": "" }]
  
  getSeries = () ->
    initializeSeries()
    searchDeService.getSeries($scope.selectionAL.collection.collectionId).then( (response) ->
      $scope.series = $scope.series.concat(response)
    , (error) ->
      console.log(error)
    )
    
  #volumes
  $scope.volumes = []
  initializeVolumes = () ->
    $scope.volumes = [{ "volumeId": 0, "volumeName": "" }]
  getVolumes = () ->
    initializeVolumes()
    searchDeService.getVolumes($scope.selectionAL.collection.collectionId).then( (response) ->
      $scope.volumes = $scope.volumes.concat(response)
    , (error) ->
      console.log(error)
    )

  #inserts
  $scope.inserts = []
  initializeInserts = () ->
    $scope.inserts = [{ "insertId": 0, "insertName": "" }]
  getInserts = () ->
    initializeInserts()
    searchDeService.getInserts($scope.selectionAL.volume.volumeId).then( (response) ->
      $scope.inserts = $scope.inserts.concat(response)
    , (error) ->
      console.log(error)
    )

  # selected value in archivalLocation (ArchivalLocation)
  $scope.selectionAL = {}
  $scope.initializeSelectionArchivalLocation = () ->
    $scope.selectionAL = searchDeInitializeService.selectionArchivalLocation()
    $scope.inputSelection = searchDeInitializeService.selectionArchivalLocation()

  $scope.onCollectionChange = (inputCollection) ->
    # se cambia la collezione selezionata, allora ricalcola le relative Serie e i Volumi e svuota i volumi
    if (inputCollection.collection.collectionId != 0) && inputCollection.collection.collectionId != $scope.selectionAL.collection.collectionId
      $scope.selectionAL = angular.copy(inputCollection)
      $scope.selectionAL.serie  = { seriesId: 0, seriesName: '' }
      $scope.selectionAL.volume = { volumeId: 0, volumeName: '' }
      $scope.selectionAL.insert = { insertId: 0, insertName: '' }
      $scope.tempVolume         = {}
      $scope.tempInsert         = {}
      getSeries()
      getVolumes()
      initializeInserts()
    # se seleziona la collezione vuota, svuota serie, volumi e inserti.
    else if (inputCollection.collection.collectionId == 0) && inputCollection.collection.collectionId != oldValue.collection.collectionId
      initializeSeries()
      initializeVolumes()
      initializeInserts()
    # se cambia il volume selezionato, allora ricalcola gli inserti
    else if (inputCollection.volume.volumeId != 0) && (inputCollection.volume.volumeId != oldValue.volume.volumeId)
      getInserts(inputCollection.volume)
    # se seleziona il volume vuoto, allora svuota gli inserti
    else if (inputCollection.volume.volumeId == 0) && (inputCollection.volume.volumeId != oldValue.volume.volumeId)
      initializeInserts()

  # remove this section from search and redo search
  $scope.removeAL = () ->
    $scope.archivalLocation = searchDeInitializeService.archivalLocation()
    setSelectedData('ArcLoc', false, null)
    createSearchArray()

  $scope.validArchLocForm = () ->
    $scope.selectionAL.collection != '' && $scope.selectionAL.collection.collectionId != 0

  # if form is valid update filter value
  $scope.checkFormAL = (isValid) ->
    return if !$scope.validArchLocForm()
    if isValid
      updateAL($scope.selectionAL)

  updateAL = (addedValue) ->
    $scope.loadingProcess = true
    # console.log(addedValue)
    #debugger
    $scope.archivalLocation.isActiveFilter = if (addedValue.collection.collectionId == 0) then false else true
    $scope.archivalLocation.collection = if (addedValue.collection.collectionId == 0) then null else addedValue.collection.collectionName
    $scope.archivalLocation.series = if (addedValue.serie.seriesId  == 0 ) then null else addedValue.serie.seriesName
    #$scope.archivalLocation.volume = if (addedValue.volume.volumeId == 0 ) then null else addedValue.volume
    if (addedValue.volume.volumeId == 0 )
      $scope.archivalLocation.volume = null
    else
      $scope.archivalLocation.volume = addedValue.volume.volumeName
      $scope.archivalLocation.insert = if (addedValue.insert.insertId == 0 ) then null else addedValue.insert.insertName
    modArchivalLocation = angular.copy($scope.archivalLocation)
    modArchivalLocation.collection = if (addedValue.collection.collectionId == 0) then null else addedValue.collection.collectionAbbreviation

    $scope.selectedArchivalLocation = angular.copy(addedValue)

    setSelectedData('ArcLoc', true, modArchivalLocation)
    createSearchArray()

  ##############################################################################
  ################### DOCUMENT CATEGORY AND TYPOLOGY SECTION ###################
  ##############################################################################

  ################################# INITIALIZE #################################
  
  initDocumentTypologies = () ->
    getDocumentTypologies()
    # inizializza l'oggetto interno
    $scope.initializeSelectionDocumentCategory()
    # inizzializza l'oggeto da spedire
    $scope.initializeDocumentCategory()

  documentTypologies = []
  categories =  {}
  $scope.categoriesNames = []
  $scope.typologiesOf = { "": [""] }
  # get all DocumenTypologies
  getDocumentTypologies = () ->
    searchDeService.getDocumentTypologies().then( (response) ->
      documentTypologies = response
      # categories
      for el in documentTypologies
        do (el) ->
          if el.categoryName not in Object.keys(categories)
            categories[el.categoryName] = el.type
            $scope.categoriesNames.push(el.categoryName)
            $scope.typologiesOf[el.categoryName] = [""]
          $scope.typologiesOf[el.categoryName].push(el.typologyName)
    )

  ############## SELECTED DOCUMENT CATEGORY AND TYPOLOGY SECTION ###############
  
  # selected value in Document Category
  $scope.selectionDocumentCategory = {}
  $scope.inputDocumentCategory = {}

  $scope.initializeSelectionDocumentCategory = () ->
    $scope.inputDocumentCategory = searchDeInitializeService.selectionDocumentCategory()

  ######### SEARCH FILTER FOR DOCUMENT CATEGORY AND TYPOLOGY SECTION ##########

  # filter value, used for search and to show to user last search
  $scope.documentCategory = {}
  $scope.initializeDocumentCategory = () ->
    $scope.documentCategory = searchDeInitializeService.documentCategory()

  # remove this section from search and redo search
  $scope.removeDocumentCategory = () ->
    $scope.initializeDocumentCategory()
    setSelectedData('DocCat', false, null)
    createSearchArray()

  # if form is valid update filter value
  $scope.checkFormDC = () ->
    updateDC($scope.selectionDocumentCategory)
    
  updateDC = (addedValue) ->
    $scope.loadingProcess = true
    $scope.documentCategory.isActiveFilter = if (addedValue.category == "")  then false else true
    $scope.documentCategory.category = if (addedValue.category == "") then null else JSON.parse(JSON.stringify(addedValue.category))
    $scope.documentCategory.typology = if (addedValue.typology == "") then null else JSON.parse(JSON.stringify(addedValue.typology))
    setSelectedData('DocCat', true, $scope.documentCategory)
    createSearchArray()

  $scope.onCategoryChange = (inputDocumentCategory) ->
    $scope.selectionDocumentCategory = angular.copy(inputDocumentCategory)
    $scope.selectionDocumentCategory.category = categories[inputDocumentCategory.category]

  ##############################################################################
  ######################## TRANSCRIPTION AND SYNOPSES ##########################
  ##############################################################################

  ################################# INITIALIZE #################################

  initTranscriptionsAndSynopsis = () ->
    # inizializza l'oggetto interno
    $scope.initializeSelectionTranscriptionsAndSynopsis()
    # inizializza gli oggetti da spedire
    $scope.initializeTranscriptions()
    $scope.initializeSynopsis()

  ################### SELECTED TRANSCRIPTIONS AND SYNOPSYS #####################

  $scope.checkFormTS = () ->
    if ($scope.selectionTS.transcription.length || $scope.selectionTS.synopsis.length)
      return true
    return false
  
  # initialize transcription and synopsis selected
  $scope.selectionTS = {}
  $scope.initializeSelectionTranscriptionsAndSynopsis = () ->
    $scope.selectionTS = searchDeInitializeService.transcriptionsAndSynopsis()

  ############### SEARCH FILTER FOR TRANSCRIPTIONS AND SYNOPSYS ################

  # initialize filter value for Transcription
  $scope.searchedTranscriptions = {}
  $scope.initializeTranscriptions = () ->
    $scope.searchedTranscriptions = searchDeInitializeService.transcription()
  
  # initialize filter value for Synopsis
  $scope.searchedSynopsis = {}
  $scope.initializeSynopsis = () ->
    $scope.searchedSynopsis = searchDeInitializeService.synopsis()
  
  # update filter for Trasciptions AND Synopsis
  $scope.updateTS = (bool) ->
    if (bool == true)
      $scope.loadingProcess = true
      $scope.searchedTranscriptions.isActiveFilter = if ($scope.selectionTS.transcription == "") then false else true
      $scope.searchedTranscriptions.transcription  = if ($scope.selectionTS.transcription == "") then null else JSON.parse(JSON.stringify($scope.selectionTS.transcription))
      $scope.searchedSynopsis.isActiveFilter = if ($scope.selectionTS.synopsis == "") then false else true
      $scope.searchedSynopsis.synopsis       = if ($scope.selectionTS.synopsis == "") then null else JSON.parse(JSON.stringify($scope.selectionTS.synopsis))
      setSelectedData('Trans', true, $scope.searchedTranscriptions) if $scope.selectionTS.transcription != ""
      setSelectedData('Syn',   true, $scope.searchedSynopsis)       if $scope.selectionTS.synopsis != ""
      createSearchArray()

  # remove Transcriptions section from search and redo search
  $scope.removeT = () ->
    $scope.initializeTranscriptions()
    setSelectedData('Trans', false, null)
    createSearchArray()

  # remove Synopsis section from search and redo search
  $scope.removeS = () ->
    $scope.initializeSynopsis()
    setSelectedData('Syn', false, null)
    createSearchArray()

  ##############################################################################
  ############################# PEOPLE AND PLACES ##############################
  ##############################################################################

  initPeopleAndPlaces = () ->
    # inizializza l'oggetto interno
    initializeselectionPeopleAndPlaces()
    # inizializza gli oggetti da spedire
    initializeSearchedPeople()
    initializeSearchedPlaces()

  # query string for people and person
  $scope.queryStringForPeopleAndPlaces = {
    "people": "",
    "places": ""
  }

  # on-change of query string we call getPople and clear selected people
  $scope.people = []
  $scope.getPeople = (val) ->
    return false if val.length < 3
    $scope.loadingPeople = true
    initializeSelectionPeople()
    searchDeService.getPeople(val).then( (response) ->
      $scope.people = response
      $scope.loadingPeople = false
    )
  
  # on-change of query string we call getPlaces and clear selected places
  $scope.places = []
  $scope.getPlaces = (val) ->
    return false if val.length < 3
    $scope.loadingPlaces = true
    initializeSelectionPlaces()
    searchDeService.getPlaces(val).then( (response) ->
      $scope.places = response
      $scope.loadingPlaces = false
    )

  ######################### SELECTED PEOPLE AND PLACES #########################

  #initialize People and person selected
  $scope.selectionPeopleAndPlaces = {}
  initializeselectionPeopleAndPlaces = () ->
    $scope.selectionPeopleAndPlaces = searchDeInitializeService.selectionPeopleAndPlaces()
  initializeSelectionPeople = () ->
    $scope.selectionPeopleAndPlaces.people = []
  initializeSelectionPlaces = () ->
    $scope.selectionPeopleAndPlaces.places = []

  #check person id in selected people
  $scope.checkIfThisPersonAreSelcted = (personToCheck) ->
    i = 0
    index = -1
    for person in $scope.selectionPeopleAndPlaces.people
      if person.id == personToCheck.id
        index = i
        break
      i += 1
    return index

  # for not duplicate code, we can use this instead check ELEM in ARR:
  $scope.checkIfThisAreSelctedInById = (objToCheck, arrWhereCheck) ->
    i = 0
    found = 0
    index = false
    for obj in arrWhereCheck
      if obj.id == objToCheck.id
        index = i
        found = 1
        break
      i += 1
    return [found, index]

  #add or remove person to selected people
  $scope.addOrRemovePersonToSelectedPeople = (person, singleselect) ->
    indexToRemove = $scope.checkIfThisPersonAreSelcted(person)
    if indexToRemove == -1
      # se non c'è, aggiungilo
      if (singleselect)
        $scope.selectionPeopleAndPlaces.people.length = 0
        $scope.selectionPeopleAndPlaces.people.push(person)
        $scope.$broadcast("personIsSelected", person)
      else
        $scope.$broadcast("personIsSelected", person)
        $scope.selectionPeopleAndPlaces.people = $scope.selectionPeopleAndPlaces.people.concat(person)
    else
      # se c'è, rimuovilo
      $scope.selectionPeopleAndPlaces.people.splice(indexToRemove, 1)
  
  #################### SEARCH FILTER FOR PEOPLE AND PLACES #####################

  # initialize filter value for People
  $scope.searchedPeople = {}
  initializeSearchedPeople = () ->
    $scope.searchedPeople = searchDeInitializeService.people()

  #initialize filter value for Places
  $scope.searchedPlaces = {}
  initializeSearchedPlaces = () ->
    # $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams, options) ->
    #   console.log(event)
    #   event.preventDefault()
    $scope.searchedPlaces = searchDeInitializeService.places()
  
  $scope.checkFormPeople = () ->
    updateSearchedPeople()

  $scope.checkFormPlaces = () ->
    updateSearchedPlaces()

  # update filter for People and Places ; use SON.parse(JSON.stringify for deep copy
  updateSearchedPeopleAndPlaces = () ->
    $scope.searchedPeople.isActiveFilter = if ($scope.selectionPeopleAndPlaces.people.length <= 0) then false else true
    #$scope.searchedPeople.people = if ($scope.selectionPeopleAndPlaces.people.length <= 0) then [] else JSON.parse(JSON.stringify($scope.selectionPeopleAndPlaces.people))
    $scope.searchedPeople.people = if ($scope.selectionPeopleAndPlaces.people.length <= 0) then [] else $scope.selectionPeopleAndPlaces.people
    $scope.searchedPlaces.isActiveFilter = if ($scope.selectionPeopleAndPlaces.places.length <= 0) then false else true
    #$scope.searchedPlaces.places = if ($scope.selectionPeopleAndPlaces.places.length <= 0) then [] else JSON.parse(JSON.stringify($scope.selectionPeopleAndPlaces.places))
    $scope.searchedPlaces.places = if ($scope.selectionPeopleAndPlaces.places.length <= 0) then [] else $scope.selectionPeopleAndPlaces.places
    createSearchArray()
  
  # update filter for People
  updateSearchedPeople = () ->
    $scope.loadingProcess = true
    $scope.searchedPeople.isActiveFilter = if ($scope.selectionPeopleAndPlaces.people.length <= 0) then false else true
    peopleArr = $scope.selectionPeopleAndPlaces.people.filter((el) ->
      for i in [0...$scope.searchedPeople.people.length]
        $scope.duplicatePersonError = false
        if (el.id == $scope.searchedPeople.people[i].id)
          $scope.duplicatePersonError = true
          return false
      return true)
    $scope.searchedPeople.people = $scope.searchedPeople.people.concat(peopleArr)
    $scope.selectionPeopleAndPlaces.people = []
    setSelectedData('People', true, $scope.searchedPeople)
    $scope.$broadcast("peopleClearEvent")
    createSearchArray()

  # update filter for places
  updateSearchedPlaces = () ->
    $scope.loadingProcess = true
    $scope.searchedPlaces.isActiveFilter = if ($scope.selectionPeopleAndPlaces.places.length <= 0) then false else true
    placesArr = $scope.selectionPeopleAndPlaces.places.filter((el) ->
      for i in [0...$scope.searchedPlaces.places.length]
        $scope.duplicatePlaceError = false
        if (el.id == $scope.searchedPlaces.places[i].id)
          $scope.duplicatePlaceError = true
          return false
      return true)
    $scope.searchedPlaces.places = $scope.searchedPlaces.places.concat(placesArr)
    $scope.selectionPeopleAndPlaces.places = []
    setSelectedData('Place', true, $scope.searchedPlaces)
    $scope.$broadcast("placesClearEvent")
    createSearchArray()

  # remove People section from search and redo search
  $scope.removeSearchedPeople = (e) ->
    e.preventDefault()
    initializeSearchedPeople()
    setSelectedData('People', false, null)
    createSearchArray()

  # remove Person from searched People and redo search
  $scope.removeSearchedPerson = (indexToRemove, e) ->
    e.preventDefault()
    #[found, indexToRemove] = $scope.checkIfThisAreSelctedInById(person, $scope.searchedPeople.people)
    $scope.searchedPeople.people.splice(indexToRemove, 1)
    if ($scope.searchedPeople.people.length <= 0)
      $scope.searchedPeople.isActiveFilter = false
      setSelectedData('People', false, null)
    else
      setSelectedData('People', true, $scope.searchedPeople)
    createSearchArray()

  # remove Places section from search and redo search
  $scope.removeSearchedPlaces = (e) ->
    e.preventDefault()
    initializeSearchedPlaces()
    setSelectedData('Place', false, null)
    createSearchArray()
  
  # remove single Place from searched Places and redo search
  $scope.removeSearchedPlace = (placeIndexToRemove, e) ->
    e.preventDefault()
    $scope.searchedPlaces.places.splice(placeIndexToRemove, 1)
    if ($scope.searchedPlaces.places.length <= 0)
      $scope.searchedPlaces.isActiveFilter = false
      setSelectedData('Place', false, null)
    else
      setSelectedData('Place', true, $scope.searchedPlaces)
    createSearchArray()

  #########################################################################
  ####################### TOPICS AND RELATED PLACES #######################
  #########################################################################

  initTopicsAndRelatedPlaces = () ->
    getTopics()
    $scope.topicsAndRelatedPlaces = searchDeInitializeService.topicAndRelatedPlaces()
    # inizializza l'oggetto interno
    $scope.initializeSelectionTopicAndRelatedPlace()
    # inizializza gli oggetti da spedire
    $scope.initializeSearchedTopicsAndRelatedPlaces()

  ###########################################################################

  # topics
  $scope.topics = []
  initializeTopics = () ->
    $scope.topics = [ { "topicTitle": "", "topicTitle": "" } ]

  getTopics = () ->
    initializeTopics()
    searchDeService.getTopics().then( (response) ->
      # $scope.topics = $scope.topics.concat(response)
      $scope.topics = response
    )
      
  # related places
  $scope.relatedPlaces = []

  # query string for related places
  $scope.queryStringForRelatedPlaces = {
    "place": ""
  }
  
  # on-change queryStringForRelatedPlaces we call getRelatedPlaces and clear selectedRelatedPlaces
  $scope.getRelatedPlaces = () ->
    if $scope.queryStringForRelatedPlaces.place.length  >= 3
      initializeSelectionRelatedPlaces()
      searchDeService.getPlaces($scope.queryStringForRelatedPlaces.place).then( (response) ->
        $scope.relatedPlaces = $scope.relatedPlaces.concat(response)
      )
    else
      $scope.relatedPlaces = []

  ################## SELECTED TOPIC AND RELATED PLACES ##################

  $scope.selectionTopicAndRelatedPlace = {}
  $scope.initializeSelectionTopicAndRelatedPlace = () ->
    $scope.selectionTopicAndRelatedPlace = {
      relatedtopic: {},
      relatedplace: {}
    }
  
  $scope.topicIsPresent = () ->
    typeof $scope.selectionTopicAndRelatedPlace.relatedtopic.topicId != 'undefined'
    
  initializeSelectionRelatedPlaces = () ->
    $scope.selectionTopicAndRelatedPlace.relatedplace = {}

  $scope.checkIfRelatedPlaceIsSelected = (relatedPlace) ->
    if ($scope.selectionTopicAndRelatedPlace.relatedplace && $scope.selectionTopicAndRelatedPlace.relatedplace.id) == relatedPlace.id then true else false

  $scope.selectThisRelatedPlace = (relatedPlace) ->
    #$scope.selectionTopicAndRelatedPlace.relatedplace = JSON.parse(JSON.stringify(relatedPlace))
    $scope.selectionTopicAndRelatedPlace.relatedplace = relatedPlace

  ############# SEARCH FILTER FOR TOPICS AND RELATED PLACES #############
  
  $scope.searchedTopicsAndRelatedPlaces = {}
  $scope.topicsAndRelatedPlaces = searchDeInitializeService.topicAndRelatedPlaces()
  $scope.searchRelatedPlaces = []

  $scope.initializeSearchedTopicsAndRelatedPlaces = () ->
    $scope.searchedTopicsAndRelatedPlaces = searchDeInitializeService.topicAndRelatedPlaces()

  # update filter and description value
  $scope.serchedTopic = ""
  $scope.serchedRelatedPlaceStringified = ""
  $scope.topicsAndPlaces = []
  $scope.searchedTopicsPlaces = []
  
  $scope.updateSearchedTopicsAndRelatedPlaces = () ->
    return unless $scope.topicIsPresent()
    
    $scope.selectionTopicAndRelatedPlace.relatedplace = $scope.searchedTopicsPlaces[0]
    $scope.addTopicAndPlace(angular.copy($scope.selectionTopicAndRelatedPlace))
    $scope.searchedTopicsPlaces.length = 0

  isNewTopic = (selectedTopic, existingTopics) ->
    result =  _.find existingTopics, (currentTopic) -> areSameTopic(selectedTopic, currentTopic)

    if result?
      return false
    return true

  areSameTopic = (topicA, topicB) ->
    if topicA.relatedtopic.topicId != topicB.relatedtopic.topicId
      return false
    if topicA.relatedplace?
      if !topicB.relatedPlace
        return false
      if topicA.relatedplace.id != topicB.relatedplace.id
        return false
    return true

  $scope.addTopicAndPlace = (topicToAdd) ->
    $scope.loadingProcess = true
    $scope.searchedTopicsAndRelatedPlaces.isActiveFilter = true
    $scope.topicsAndRelatedPlaces.isActiveFilter = true
    
    if !isNewTopic(topicToAdd, $scope.topicsAndPlaces)
      $scope.loadingProcess = false
      return
    
    $scope.topicsAndPlaces.push({
      'relatedtopic': topicToAdd.relatedtopic,
      'relatedplace': if topicToAdd.relatedplace? then topicToAdd.relatedplace else null
    })

    if topicToAdd.relatedplace?
      $scope.searchRelatedPlaces.push(topicToAdd.relatedplace)
    $scope.$broadcast("placesClearEvent")

    $scope.topicsAndRelatedPlaces.topics.push({
      "topicTitle": topicToAdd.relatedtopic.topicTitle.toString(),
      "topicId": topicToAdd.relatedtopic.topicId.toString(),
      "placeAllId": if topicToAdd.relatedplace? then topicToAdd.relatedplace.id.toString() else ""
    })

    if topicToAdd.relatedplace && topicToAdd.relatedplace.prefFlag == "V"
      searchDeService.getPrincipalPlaces(topicToAdd.relatedplace.id).then( (response) ->
        index = _.findIndex($scope.topicsAndRelatedPlaces.topics, {placeAllId : topicToAdd.relatedplace.id.toString()})
        $scope.topicsAndRelatedPlaces.topics[index].placeAllId = response.id.toString()
        topicToAdd.relatedplace.principalName = response.placeName.toString()
        setSelectedData('Top', true, $scope.topicsAndRelatedPlaces)
        createSearchArray()
        topicToAdd.relatedtopic = {}
        topicToAdd.relatedplace = {}
      )
    else
      setSelectedData('Top', true, $scope.topicsAndRelatedPlaces)
      createSearchArray()
      topicToAdd.relatedtopic = {}
      topicToAdd.relatedplace = {}

  $scope.removeTopicAndPlace = (index) ->
    $scope.topicsAndPlaces.splice(index, 1)
    $scope.topicsAndRelatedPlaces.topics.splice(index, 1)
    if ($scope.topicsAndPlaces.length < 1)
      setSelectedData('Top', false, null)
      $scope.initializeSearchedTopicsAndRelatedPlaces()
      $scope.topicsAndRelatedPlaces = searchDeInitializeService.topicAndRelatedPlaces()
    else
      setSelectedData('Top', true, $scope.topicsAndRelatedPlaces)
    createSearchArray()

  #########################################################################
  ########################## DATE RANGE SECTION ###########################
  #########################################################################

  $scope.today = new Date
  $scope.filterDate = searchDeInitializeService.filterDate()
  $scope.months = searchDeInitializeService.months()
  $scope.inputDayA = null
  $scope.inputDayB = null

  getNumberOfDays = (year, month) ->
    if $scope.isNotEmptyField(year) and $scope.isNotEmptyField(month)
      isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0))
      [31, (if isLeap then 29 else 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month - 1]
    else
      31

  $scope.onChangeDayDateA = (inputDayA) ->
    $scope.dateA.day = parseDay(inputDayA)

  $scope.onChangeDayDateB = (inputDayB) ->
    $scope.dateB.day = parseDay(inputDayB)

  parseDay = (inputDay) ->
    parsedValue = inputDay

    if isNaN(parsedValue)
      return ""
    else
      return parsedValue

  ############################# SELECTED DATES ############################

  $scope.dateA = {}
  $scope.initializeDateA = () ->
    $scope.dateA = searchDeInitializeService.dateA()

  $scope.dateB = {}
  $scope.initializeDateB = () ->
    $scope.dateB = searchDeInitializeService.dateB()

  $scope.numOfDayOfThismonthA = 31
  $scope.$watch "dateA", (newValue, oldValue) ->
    if newValue.year != oldValue.year || newValue.month != oldValue.month
      $scope.numOfDayOfThismonthA = getNumberOfDays(newValue.year, newValue.month)
  , true

  $scope.numOfDayOfThismonthB = 31
  $scope.$watch "dateB", (newValue, oldValue) ->
    if newValue.year != oldValue.year || newValue.month != oldValue.month
      $scope.numOfDayOfThismonthB = getNumberOfDays(newValue.year, newValue.month)
  , true

  ############################# SEARCHED DATES ############################

  initDateRange = () ->
    $scope.initializeSearchedDates()
    $scope.initializeDateA()

  $scope.searchedDates = {}
  $scope.initializeSearchedDates = () ->
    $scope.searchedDates = searchDeInitializeService.dates()
    setSelectedData('DateR', false, null) # no separate function to delete all objects

  $scope.dateRangeChecker = () ->
    switch $scope.dateA.filter
      when "from", "before"
        return $scope.validateDateQuery($scope.dateA)
      when "between"
        if ($scope.validateDateQuery($scope.dateA) && $scope.validateDateQuery($scope.dateB))
          dateA = createDate($scope.dateA)
          dateB = createDate($scope.dateB)
          return dateA < dateB
        else
          return false
      when "in"
        return $scope.validateDateQueryIn($scope.dateA)
      else
        return false

  $scope.validateDateQuery = (date) ->
    if not date?
      return false
    if $scope.isEmptyField(date.year)
      return false
    if $scope.isEmptyField(date.month) && $scope.isNotEmptyField(date.day)
      return false
    if $scope.isNotEmptyField(date.day)
      numberOfDays = getNumberOfDays(date.year, date.month)
      if date.day <= 0 || date.day > numberOfDays
        return false
    return true

  $scope.validateDateQueryIn = (date) ->
    if not date?
      return false
    if $scope.isEmptyField(date.year) && $scope.isEmptyField(date.month) && $scope.isEmptyField(date.day)
      return false
      
    if !$scope.isEmptyField(date.day)
      numberOfDays = getNumberOfDays(date.year, date.month)
      if $scope.isEmptyField(date.year) && date.month == 2
        numberOfDays = 29
      if date.day <= 0 || date.day > numberOfDays
        return false

    return true

  $scope.checkFormDate = () ->
    if ($scope.dateRangeChecker())
      $scope.loadingProcess = true
      updateDate()

  $scope.dateBIsNeeded = () ->
    $scope.dateA.filter == "between"

  $scope.showDateB = () ->
    $scope.searchedDates.dateFilterType == "between"
  
  updateDate = () ->
    $scope.initializeSearchedDates()
    $scope.searchedDates.dateFilterType =  if $scope.dateA.filter? then $scope.dateA.filter else "in"
    $scope.searchedDates.dateYear = $scope.dateA.year.toString() if $scope.dateA.year?
    $scope.searchedDates.dateMonth = $scope.dateA.month.toString() if $scope.dateA.month?
    $scope.searchedDates.dateDay = $scope.dateA.day.toString() if $scope.dateA.day?
    if $scope.dateBIsNeeded()
      $scope.searchedDates.dateBYear = $scope.dateB.year.toString() if $scope.dateB.year?
      $scope.searchedDates.dateBMonth = $scope.dateB.month.toString() if $scope.dateB.month?
      $scope.searchedDates.dateBDay = $scope.dateB.day.toString() if $scope.dateB.day?
    $scope.searchedDates.isActiveFilter = true
    setSelectedData('DateR', true, $scope.searchedDates)
    createSearchArray()

  $scope.removeDate = () ->
    $scope.initializeSearchedDates()
    createSearchArray()
    
  createDate = (dateInput) ->
    year = 0
    if !isNaN(parseInt(dateInput.year))
      year = dateInput.year

    monthIndex = 0
    if !isNaN(parseInt(dateInput.month))
      monthIndex = dateInput.month - 1

    day = 0
    if !isNaN(parseInt(dateInput.day))
      day = dateInput.day

    date = new Date()
    date.setFullYear(year)
    date.setMonth(monthIndex)
    date.setDate(day)
    date.setHours(0)
    date.setMinutes(0)
    date.setSeconds(0)

    return date

  ##############################################################################
  ######################### LANGUAGES SECTION ##########################
  ##############################################################################
  initLanguages = () ->
    # inizializza l'oggetto interno
    initializeLanguages()
    # inizializza la ricerca
    $scope.initializeSearchedLanguages()

  $scope.selectedLanguage ={language: ''}

  $scope.languages = []

  initializeLanguages = () ->
    searchDeService.getLanguages().then( (response) ->
      $scope.languages = (response)
    )

  ############ seached languages
  $scope.searchedLanguages = []
  $scope.initializeSearchedLanguages = () ->
    setSelectedData('Lang', false, null) # no separate function to delete all objects
    $scope.searchedLanguages = searchDeInitializeService.languages()
  
  $scope.addLanguages = () ->
    if ($scope.selectedLanguage.language not in $scope.searchedLanguages.languages && $scope.selectedLanguage.language != '')
      $scope.loadingProcess = true
      $scope.searchedLanguages.isActiveFilter=true
      # controllare che la lingua non sia gia nell array
      # crea array per partialcount
      $scope.searchedLanguages.languages = $scope.searchedLanguages.languages.concat($scope.selectedLanguage.language)
      $scope.selectedLanguage.language = ''
      setSelectedData('Lang', true, $scope.searchedLanguages)
      createSearchArray()
  
  $scope.removeSearchedLanguage = (indexToRemove) ->
    # cancella il linguaggio con indexToRemove
    $scope.searchedLanguages.languages.splice(indexToRemove, 1)
    # Se array diventa vuoto reinizializza tutto
    if $scope.searchedLanguages.languages.length == 0
      setSelectedData('Lang', false, null)
      $scope.initializeSearchedLanguages()
    else
      setSelectedData('Lang', true, $scope.searchedLanguages)
    createSearchArray()

  $scope.removeAllLanguages = () ->
    $scope.searchedLanguages.languages = []
    setSelectedData('Lang', false, null)
    $scope.initializeSearchedLanguages()
    createSearchArray()

  $scope.languageAlreadySelected = () ->
    $scope.selectedLanguage.language in $scope.searchedLanguages.languages

  #########################################################################
  ############################# OWNER SECTION #############################
  #########################################################################

  initOwner = () ->
    # inizializza l'oggetto interno
    $scope.initializeSearchedOwner()
    # inizializza gli oggetti da spedire
    initializeSelectionOwnerAccount()

  $scope.ownerIn = [
    { label: "Created by", value: "owner" },
    { label: "Last update by", value: "editor" }
  ]

  $scope.queryStringForOwner = { owner: "" }

  ########################### SEARCHED OWNER owners
  $scope.owners = []
  initializeOwner = () ->
    $scope.owners = [ { "account": "" }]

  $scope.getUserByName = (str) ->
    initializeOwner()
    initializeSelectionOwnerAccount()
    $scope.loadingOwner = true
    searchDeService.getUserByName(str).then( (response) ->
      $scope.owners = response
      $scope.loadingOwner = false
    )
  #### selected
  $scope.selectionOwner = { account: "" }
  initializeSelectionOwnerAccount = () ->
    $scope.selectionOwner.account = ""
  
  $scope.addOrRemoveOwnerToSelectedOwner = (owner) ->
    $scope.selectionOwner.account = owner.account
    $scope.$broadcast("ownerIsSelected", owner)
  
  $scope.checkIfThisOwnerAreSelected = (owner) ->
    owner.account == $scope.selectionOwner.account
    return "Created by" if $scope.searchedOwner.editType == "owner"
    return "Last update by"  if $scope.searchedOwner.editType == "editor"

  ############################# SEARCHED OWNER ############################
  $scope.searchedOwner = {}
  $scope.initializeSearchedOwner = () ->
    setSelectedData('Owner', false, null) # no separate function to delete all objects
    $scope.searchedOwner = searchDeInitializeService.owner()
    if $scope.selectionOwner.in?
#      console.log($scope.selectionOwner.in)
      $scope.searchedOwner.editType = $scope.selectionOwner.in
    else
      $scope.searchedOwner.editType = "owner"
#      console.log($scope.searchedOwner.editType)
    #createSearchArray()

  $scope.updateSearchedOwner = () ->
    if $scope.selectionOwner.account != ''
      $scope.loadingProcess = true
      return false if not $scope.selectionOwner["account"]?
      $scope.searchedOwner.account = JSON.parse(JSON.stringify($scope.selectionOwner.account))
      $scope.searchedOwner.isActiveFilter = true
      # console.log($scope.searchedOwner)
      if $scope.selectionOwner.in?
#        console.log($scope.selectionOwner.in)
        $scope.searchedOwner.editType = $scope.selectionOwner.in
      else
        $scope.searchedOwner.editType = "owner"
#        console.log($scope.searchedOwner.editType)
      $scope.$broadcast("ownersClearEvent")
      $scope.selectionOwner.in = ""
      setSelectedData('Owner', true, $scope.searchedOwner)
      createSearchArray()

  $scope.removeOwner = () ->
    $scope.initializeSearchedOwner()
    createSearchArray()

  #########################################################################
  ######################### SEARCH ARRAY SECTION ##########################
  #########################################################################

  ######## In questa sezione lavoro i dati per mandarli al server. ########

  # checkPlaceVariant = () ->
  #   if $scope.searchedPlaces.places
  #     for place in $scope.searchedPlaces.places
  #       if (place.principalId? and place.principalId not in $scope.editedSearchedPlaces.places)
  #         $scope.editedSearchedPlaces.places.push(place.principalId.toString())
  #       else if not place.principalId? and place.id not in $scope.editedSearchedPlaces.places
  #         $scope.editedSearchedPlaces.places.push(place.id.toString())
  #   return $scope.editedSearchedPlaces

  checkPlace = (place) ->
    if (place.principalId? and place.principalId not in $scope.editedSearchedPlaces.places)
      $scope.editedSearchedPlaces.places.push(place.principalId.toString())
    else if not place.principalId? and place.id not in $scope.editedSearchedPlaces.places
      $scope.editedSearchedPlaces.places.push(place.id.toString())


  editSearchedPlaces = () ->
    if $scope.searchedPlaces.places
      checkPlace(place) for place in $scope.searchedPlaces.places
    return $scope.editedSearchedPlaces

  createSearchArray = () ->
    # copio gli editedSearchedPlaces,
    $scope.editedSearchedPlaces = JSON.parse(JSON.stringify($scope.searchedPlaces))
    #$scope.editedSearchedPlaces = $scope.searchedPlaces
    $scope.editedSearchedPlaces.places = []
    editSearchedPlaces()
    # checkPlaceVariant() # da chiamare al posto di  editSearchedPlaces ma non è riutilizzabile per altri place

    # copio le editedSearchedPeople e le trasformo da array di numeri in stringhe
    editedSearchedPeople = JSON.parse(JSON.stringify($scope.searchedPeople))
    editedSearchedPeople.people = $scope.searchedPeople.people.map (people) -> people.id.toString() if $scope.searchedPeople.people

    # copio le editedArchivalLocation, creo una lista di soli ID di Volumi
    editedArchivalLocation = JSON.parse(JSON.stringify($scope.archivalLocation))
    copyEditedArchivalLocation = angular.copy(editedArchivalLocation)

    #editedArchivalLocation = $scope.archivalLocation
    #editedArchivalLocation.volume = $scope.archivalLocation.volume.volumeId if $scope.archivalLocation.volume
    # -- TODO -- do not write the insert field into the request payload (once the new service is ready please remove this line!!)
    # EUGENE leave the line below commented (thanks)
    # delete editedArchivalLocation.insert
    
    # copio editedSearchedTopicsAndRelatedPlaces
    editedSearchedTopicsAndRelatedPlaces = JSON.parse(JSON.stringify($scope.topicsAndRelatedPlaces))
    #editedSearchedTopicsAndRelatedPlaces = $scope.searchedTopicsAndRelatedPlaces

    $scope.placeList = $scope.editedSearchedPlaces.places
    $scope.searchArray = [
      editedArchivalLocation,
      $scope.documentCategory,
      $scope.searchedTranscriptions,
      $scope.searchedSynopsis,
      $scope.editedSearchedPlaces,
      editedSearchedPeople,
      editedSearchedTopicsAndRelatedPlaces,
      $scope.searchedDates,
      $scope.searchedOwner,
      $scope.searchedLanguages
    ]

    # TODO: delete this (copySearchArray and copyEditedArchivalLocation) after include insert in the search array
    $scope.copySearchArray    = angular.copy($scope.searchArray)
    $scope.copySearchArray[0] = copyEditedArchivalLocation

    $scope.lastSearchArray = angular.copy($scope.searchArray)

    # For newsFeedSearch skips results counting so no loading is needed
    if $scope.isNewsFeedSearch
      $scope.loadingProcess = false
      return

    searchDeService.getPartialRecordsCountWithQueryInfos($scope.searchArray).then(
      (response) ->
        if angular.equals($scope.lastSearchArray, response.query)
          $scope.countResult = response.partialRecordsCount
          $scope.loadingProcess = false
      , (error) ->
        $scope.countResult = "ERRORE"
    )
  
  $scope.getResults = () ->
    $scope.loadingProcess = true
    searchDeService.getResults($scope.searchArray, 0, 20).then (resp) ->
      $scope.copySearchArray[4].places = $scope.placeList
      newResult = {}
      newResult.searchName = setSearchName()
      newResult.type = 'doc_advanced'
      newResult.results = resp.data
      newResult.searchArray = $scope.copySearchArray
      newResult.content = {
        searchType: newResult.searchName,
        countResult: $scope.countResult
        where: '',
        title: 'Advanced Document Search'
        type: $scope.ifTranscriptionSearchIsActive()
      }
      newResult.searchForm = {
        archivalLocation: $scope.selectedArchivalLocation,
        dates: $scope.searchedDates,
        languages: $scope.searchedLanguages,
        owner: $scope.searchedOwner,
        people: $scope.searchedPeople,
        places: $scope.searchedPlaces,
        relatedPlaces: $scope.searchRelatedPlaces,
        synopsis: $scope.searchedSynopsis,
        topics: angular.copy($scope.topicsAndRelatedPlaces),
        transcriptions: $scope.searchedTranscriptions,
        documentCategory: $scope.selectionDocumentCategory
      }
      $scope.loadingProcess = false
      if newResult.results
        $rootScope.$broadcast('new-result', newResult)
      else
        pageTitle = $filter('translate')("modal.WARNING")
        messagePage = 'No results found!'
        modalFactory.openMessageModal(pageTitle, messagePage, false, "sm")

  $scope.ifTranscriptionSearchIsActive = () ->
    if $scope.searchedTranscriptions.isActiveFilter
      return "DE_TRANSCRIPTION"
    else
      return 'DE_SYNOPSIS'

  setSearchName = () ->
    orderedSearches = _.orderBy($scope.selectedSearches, ['order'], ['desc'])
    lastSearch      = _.filter(orderedSearches, (s) -> s.selected && s.data != null)[0]
    lastSearchName  = "Document Search"

    switch lastSearch.type
      when 'ArcLoc'
        lastSearchName = lastSearch.data.collection
        lastSearchName = lastSearchName + ' - ' + lastSearch.data.volume if lastSearch.data.volume
        lastSearchName = lastSearchName + ' - ' + lastSearch.data.insert if lastSearch.data.insert
      when 'DocCat'
        lastSearchName = lastSearch.data.category || lastSearch.data.typology
        lastSearchName = lastSearch.data.category + '/' + lastSearch.data.typology if lastSearch.data.category && lastSearch.data.typology
      when 'Trans'
        lastSearchName = lastSearch.data.transcription || 'Transcriptions'
      when 'Syn'
        lastSearchName = lastSearch.data.synopsis || 'Synopsis'
      when 'People'
        lastSearchName = lastSearch.data.people[lastSearch.data.people.length - 1].mapNameLf
      when 'Place'
        lastSearchName = lastSearch.data.places[lastSearch.data.places.length - 1].placeName
      when 'Top'
        lastSearchName = lastSearch.data.topics[lastSearch.data.topics.length - 1].topicTitle
      when 'DateR'
        lastSearchName = lastSearch.data.dateYear + '/' + lastSearch.data.dateMonth + '/' + lastSearch.data.dateDay
      when 'Lang'
        lastSearchName = lastSearch.data.languages.join('/')
      when 'Owner'
        lastSearchName = lastSearch.data.account

    lastSearchName.substring(0, 25)

  $scope.loading = false
  $scope.closeTable = ()->
    $scope.searchTableOpen1 = false
    $scope.searchTableOpen2 = false
  $scope.addNewPeople = ()->
    $scope.searchTableOpen1 = false
    $scope.searchTableOpen2 = false
  $scope.tempVolume = {}
  $scope.tempInsert = {}
    
  $scope.getVolumeFiltered = (searchQuery)->
    if searchQuery
      $scope.loadingVolumes=true
      $scope.searchTableOpen1=true
      searchDeService.getVolumesByName(searchQuery, $scope.selectionAL.collection.collectionId).then (response)->
        $scope.volumes = response
        $scope.loadingVolumes = false
    $scope.selectionAL.volume = {volumeId: 0, volumeName: ''}
    $scope.selectionAL.insert = {insertId: 0, insertName: ''}
    $scope.tempInsert = {}


  $scope.getInsertFiltered = (searchQuery)->
    if searchQuery
      $scope.loadingInserts=true
      $scope.searchTableOpen2=true
      searchDeService.getInsertsByName(searchQuery, $scope.selectionAL.volume.volumeId).then (response)->
        $scope.inserts = (response)
        $scope.loadingInserts = false
    else
      $scope.selectionAL.insert = {insertId: 0, insertName: ''}
  
  $scope.selectVolume=(volumeSelected)->
    $scope.tempVolume = { value : volumeSelected.volumeName }
    $scope.searchTableOpen1=false
    $scope.selectionAL.volume=volumeSelected

  $scope.selectInsert=(insertSelected)->
    $scope.tempInsert = { value : insertSelected.insertName }
    $scope.searchTableOpen2=false
    $scope.selectionAL.insert=insertSelected

  $scope.$on('refine-search', () ->
    searchArray = angular.copy($rootScope.selectedResult.searchArray)
    searchForm = angular.copy($rootScope.selectedResult.searchForm)

    if searchArray?
      refineSearch(searchArray, searchForm)
  )

  $scope.$on('updateIsNewFeedSearch', (event, value) ->
    $scope.isNewsFeedSearch = value
  )

  refineSearch = (searchArray, searchForm) ->
    $scope.isNewsFeedSearch = false

    $scope.init()
    for searchElement in searchArray
      if searchElement.isActiveFilter
        switch searchElement.searchSection
          when "archivalLocationSearch"
            $scope.tempVolume = {}
            $scope.tempInsert = {}
            $scope.removeAL()
            initializeSeries()
            initializeVolumes()
            initializeInserts()

            $scope.selectionAL = searchForm.archivalLocation
            updateAL($scope.selectionAL)

          when "categoryAndTypologySearch"
            $scope.removeDocumentCategory()
            $scope.selectionDocumentCategory = searchForm.documentCategory
            updateDC($scope.selectionDocumentCategory)

          when "synopsisSearch"
            $scope.removeS()
            $scope.selectionTS.synopsis = searchElement.synopsis
            $scope.searchedSynopsis = searchForm.synopsis
            $scope.updateTS(true)

          when "transcriptionSearch"
            $scope.removeT()
            $scope.selectionTS.transcription = searchElement.transcription
            $scope.searchedTranscriptions = searchForm.transcriptions
            $scope.updateTS(true)

          when "placesSearch"
            $scope.places = []
            $scope.selectionPeopleAndPlaces.places = searchForm.places.places
            updateSearchedPlaces()

          when "peopleSearch"
            $scope.people = []
            $scope.selectionPeopleAndPlaces.people = searchForm.people.people
            updateSearchedPeople()

          when "topicsSearch"
            $scope.topicsAndPlaces = []
            $scope.topicsAndRelatedPlaces.topics = []

            $scope.searchedTopicsPlaces = angular.copy(searchForm.relatedPlaces)

            if searchForm.topics.topics.length > 0
              for topic in searchForm.topics.topics
                topicToAdd = prepareTopicSelectionToAdd(topic, searchForm.relatedPlaces)
                $scope.addTopicAndPlace(topicToAdd)
          
          when "dateSearch"
            $scope.initializeSearchedDates()
            $scope.dateA.filter = searchElement.dateFilterType

            initDataDateA = {
              'year': searchElement.dateYear,
              'month': searchElement.dateMonth,
              'day': searchElement.dateDay
            }
            $scope.dateA = $scope.initializeDate($scope.dateA, initDataDateA)

            initDataDateB = {
              'year': searchElement.dateBYear,
              'month': searchElement.dateBMonth,
              'day': searchElement.dateBDay
            }
            $scope.dateB = $scope.initializeDate($scope.dateB, initDataDateB)
        
            updateDate()
          when "languagesSearch"
            $scope.removeSearchedLanguage()
            searchElement.languages.map (language) ->
              $scope.selectedLanguage.language = language
              $scope.addLanguages()
            

          when "documentOwnerSearch"
            $scope.initializeSearchedOwner()
            $scope.selectionOwner.account = searchForm.owner.account
            if searchForm.owner.editType == 'editor'
              $scope.selectionOwner.in = searchForm.owner.editType
            $scope.updateSearchedOwner()
            
          else
            console.log "invalid search section"
            
  prepareTopicSelectionToAdd = (topic, relatedPlaces) ->
    selection = { relatedtopic : topic }
    placeId = parseInt(topic.placeAllId)
    if topic.placeAllId != ""
      relatedplace = _.find(relatedPlaces, (relatedPlace) -> return relatedPlace.id == placeId)
      selection.relatedplace = relatedplace
    
    return selection


#########################################################################
################################# PLACES ################################
#########################################################################

#add or remove place to selected places
  $scope.addOrRemovePlaceToSelectedPlaces = (place, singleSelect) ->
    indexToRemove = $scope.checkIfThisPlaceAreSelcted(place)
    if indexToRemove == -1
      # se non c'è, aggiungilo
      if place.prefFlag == "V"
        searchDeService.getPrincipalPlaces(place.id).then( (response) ->
          if response
            place.principalName = response.placeName
            place.principalId = response.id
            
            addNewPlace(place, singleSelect)
        )
      else
        addNewPlace(place, singleSelect)
    else
      # se c'è, rimuovilo
      if $scope.sections[PEOPLE_SECTION].open
        $scope.selectionPeopleAndPlaces.places.splice(indexToRemove, 1)
      
      if $scope.sections[TOPICS_SECTION].open
        $scope.searchedTopicsPlaces.places.splice(indexToRemove, 1)

  #check PLACE id in selected PLACES
  $scope.checkIfThisPlaceAreSelcted = (placeToCheck) ->
    i = 0
    index = -1

    if $scope.sections[PEOPLE_SECTION].open
      for place in $scope.selectionPeopleAndPlaces.places
        if place.id == placeToCheck.id
          index = i
          break
        i += 1
    
    if $scope.sections[TOPICS_SECTION].open
      for place in $scope.searchedTopicsPlaces
        if place.id == placeToCheck.id
          index = i
          break
        i += 1
    return index

  addNewPlace = (place, singleSelect) ->
    if (singleSelect)
      if $scope.sections[PEOPLE_SECTION].open
        $scope.selectionPeopleAndPlaces.places.length = 0
        $scope.selectionPeopleAndPlaces.places.push(place)

      if $scope.sections[TOPICS_SECTION].open
        $scope.searchedTopicsPlaces.length = 0
        $scope.searchedTopicsPlaces.push(place)
    else
      if $scope.sections[PEOPLE_SECTION].open
        $scope.selectionPeopleAndPlaces.places = $scope.selectionPeopleAndPlaces.places.concat(place)
      
      if $scope.sections[TOPICS_SECTION].open
        $scope.searchedTopicsPlaces = $scope.searchedTopicsPlaces.concat(place)

    $scope.$broadcast("placeIsSelected", place)

  $scope.existsFilter = () ->
    return $scope.archivalLocation.isActiveFilter or \
    $scope.documentCategory.isActiveFilter or \
    $scope.searchedTranscriptions.isActiveFilter or \
    $scope.searchedSynopsis.isActiveFilter or \
    $scope.searchedPeople.isActiveFilter or \
    $scope.searchedPlaces.isActiveFilter or \
    $scope.topicsAndRelatedPlaces.isActiveFilter or \
    $scope.searchedDates.isActiveFilter or \
    $scope.searchedLanguages.isActiveFilter or \
    $scope.searchedOwner.isActiveFilter

  $scope.saveNewsfeedItem = () ->
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-save-filter/save-filter.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            'searchArray': angular.copy($scope.searchArray)
          }
        }
    ).then (response) ->
      if response == 'ok'
        initFilters()
        createSearchArray()

  initFilters = () ->
    initArchivalLocation()
    initDocumentTypologies()
    initTranscriptionsAndSynopsis()
    initPeopleAndPlaces()
    initTopicsAndRelatedPlaces()
    initDateRange()
    initLanguages()
    initOwner()

#########################################################################
############################# HELPER METHODS ############################
#########################################################################


  $scope.isEmptyField = (field) ->
    return not field? || field == ""

  $scope.isNotEmptyField = (field) ->
    return $scope.isEmptyField(field) == false

  $scope.initializeDate = (date, initData) ->
    if $scope.isNotEmptyField(initData.year)
      date.year = parseInt(initData.year)
    if $scope.isNotEmptyField(initData.month)
      date.month = parseInt(initData.month)
    if $scope.isNotEmptyField(initData.day)
      date.day = parseInt(initData.day)

    return date