titlesServiceApp = angular.module 'titlesServiceApp',[]

titlesServiceApp.config ['growlProvider'
  (growlProvider) ->
    growlProvider.globalTimeToLive(5000)
    #growlProvider.globalPosition('bottom-right')
]
titlesServiceApp.factory 'titlesFactory', ($http, $log, growl, $rootScope, $q, $timeout, $window, ENV) ->
  titlesBasePath = ENV.titlesBasePath
  searchBasePath = ENV.searchBasePath
  bioPeopleBasePath = ENV.bioPeopleBasePath

  factory =
    getTitle: (titleId) ->
      return $http.get(titlesBasePath+'findTitleAndOccupation/'+titleId).then (resp) ->
        resp.data
    getCategories: () ->
      return $http.get(titlesBasePath+'findRoleCategories/').then (resp)->
        resp.data
    searchTitle: (queryString) ->
      return $http.get(bioPeopleBasePath+'findTitlesAndOccs/'+queryString).then (resp)->
        resp.data
    createTitle: (params) ->
      return $http.post(titlesBasePath+'addNewTitleOccupation/', params).then (resp) ->
        resp.data
    modifyTitle: (params) ->
      return $http.post(titlesBasePath+'modifyTitleOccupation/', params).then (resp) ->
        resp.data
    deleteTitle: (queryString) ->
      return $http.get(titlesBasePath+'deleteTitleOccupation/'+queryString).then (resp) ->
        resp.data


  return factory
