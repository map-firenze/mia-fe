titles = angular.module 'titles', ['addTitleApp', 'titlesServiceApp']
titles.directive 'titlesDir', (bioPeopleFactory, titlesFactory, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-people/titles/titles.html'
  scope:
    title: "="
    onSelect: '&'
    onCancel:'&'
    fromSearch: '='
  link: (scope, elem, attr) ->
    scope.searchTableOpen = false
    scope.queryStr=""
    scope.selectedTitles = {}
    scope.loading = false
    scope.getTitles = ()->
      if scope.title.titleOcc.length is 3
        scope.loading = true
        scope.searchTableOpen = true
        titlesFactory.searchTitle(scope.title.titleOcc).then (resp)->
          scope.loading = false
          if resp.data && resp.data.arrayTitleAndOccs
            scope.results=resp.data.arrayTitleAndOccs
            return
        , (data) ->
          scope.loading = false
          console.log 'error'
          return
      else
        scope.loading = false


    scope.saveTitle = ()->
      scope.onSelect({params:scope.title})

    scope.cancel = ()->
      scope.onCancel()

    scope.addANewTitle = (i) ->
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/mod-new-titles/mod-new-titles.html'
        controller:'modalIstanceController'
      ).then (resp) ->
        if resp
          console.log "saved new title"
        #otherwise i rollback
        else
          console.log "cancelled"


    scope.selectTitle = (selectedTitle)->
#      console.log scope.isNew
      scope.title.poLink = selectedTitle.poLink
      scope.title.titleOcc = selectedTitle.titleOcc
      scope.title.titleOccId = selectedTitle.titleOccId
      scope.searchTableOpen = false
