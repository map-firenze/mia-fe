browsePeople = angular.module 'browsePeople', [
  'searchServiceApp'
  'translatorAppEn']

browsePeople.directive 'browsePeopleDir', (searchFactory, modalFactory, deFactory, titlesFactory, toaster, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-people/search-person/search-person.html'

  link: (scope, elem, attr) ->
    scope.lastSearchQuery = null

    scope.loading = false
    
    scope.closeTable = ()->
      scope.searchTableOpen = false
    scope.addNewPeople = ()->
      scope.searchTableOpen = false
      tempName = ""
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-person/add-person.html'
        controller:'modalIstanceController'
      ).then (resp) ->
    
    scope.getPeople = (searchQuery) ->
      if searchQuery
        if searchQuery.length >= 3
          if scope.countWords(searchQuery) >= 3
            toaster.pop('error', 'Queries with 3 or more words are not permitted')
            return
          
          scope.lastSearchQuery = searchQuery

          scope.loading=true
          scope.searchTableOpen=false
          deFactory.getPeople(searchQuery).then (resp)->
            if resp.status is "ok"
              if searchQuery == scope.lastSearchQuery || scope.lastSearchQuery == null
                scope.possiblePeople = resp.data.people
                scope.loading=false
                scope.searchTableOpen=true
            else
              scope.possiblePeople = null
              scope.loading = true
              scope.searchTableOpen = true

        else
          scope.loading = false
          scope.searchTableOpen = false

    scope.selectPerson=(person)->
      scope.searchTableOpen=false
      scope.changeTab('people')
      params = {}
      params.id = person.id
      $state.go 'mia.bio-people', params


    scope.countWords = (searchQuery) ->
      return searchQuery.trim().split(' ').length