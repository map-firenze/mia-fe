searchPeople = angular.module 'searchPeople', ['searchServiceApp']
searchPeople.directive 'searchPeopleDir', (searchFactory, modalFactory, deFactory, titlesFactory, $state, $sce) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-people/word-search-people/word-search-people.html'

  link: (scope, elem, attr) ->
    scope.peopleSearchWhere = "ALL_NAME_TYPES"
    scope.wordSearchPeople=(query, where)->
      params={}
      params.searchString=query
      params.searchType=[where]
      searchFactory.searchPeopleWord(params).then (resp)->
        newResult={}
        newResult.searchName = angular.copy(params.searchString)
        newResult.type='people'
        newResult.results=resp.data.data
        placeToSearch = 'all name types' if where=='ALL_NAME_TYPES'
        placeToSearch = 'appellatives' if where=='APPELLATIVE'
        placeToSearch = 'family names' if where=='FAMILY'
        placeToSearch = 'given names' if where=='GIVEN'
        placeToSearch = 'maiden names' if where=='MAIDEN'
        placeToSearch = 'married' if where=='MARRIED'
        placeToSearch = 'patronimics' if where=='PATRONIMIC'
        newResult.content = {
          searchType: newResult.searchName,
          where: placeToSearch,
          title: 'People search'
        }
        scope.$root.$broadcast('new-result', newResult)
