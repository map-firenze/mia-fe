searchPeModule.service 'searchPeService', ($http, ENV) ->
  uploadBasePath = ENV.uploadBasePath
  searchBasePath = ENV.searchBasePath
  documentBasePath = ENV.documentBasePath
  userBasePath = ENV.userBasePath
  advancedSearchPeBasePath = ENV.advancedSearchPeBasePath
  titleAndOccupationBasePath = ENV.titleAndOccupationBasePath
  findtitleAndOccupationBasePath = ENV.findtitleAndOccupationBasePath
  bioPeopleBasePath = ENV.bioPeopleBasePath


  getRepositories = () ->
    url = uploadBasePath+"findAllRepositories"
    return $http.get(url).then( (response) ->
      repositories = response.data.repositories
      return repositories
    , (error) ->
      console.log(error)
    )

  getCollection = (repositoryId=1) ->
    url = uploadBasePath+"findAllCollections/"+repositoryId
    return $http.get(url).then( (response) ->
      collection = response.data.collection
      return collection
    , (error) ->
      console.log(error)
    )

  getSeries = (collectionId) ->
    url = uploadBasePath+"findAllSeries/" + collectionId
    return $http.get(url).then( (response) ->
      series = response.data.series
      return series
    , (error) ->
      console.log(error)
    )

  getVolumes = (collectionId) ->
    url = uploadBasePath+"findAllVolumes/" + collectionId
    return $http.get(url).then( (response) ->
      volumes = response.data.volume
      return volumes
    , (error) ->
      console.log(error)
    )

  getVolumesWithAe = (collectionId) ->
    url = uploadBasePath+"findAllVolumes/" + collectionId + '?withAe=true'
    return $http.get(url).then( (response) ->
      volumes = response.data.volume
      return volumes
    , (error) ->
      console.log(error)
    )

  getVolumesByName = (name, collectionId) ->
    url = uploadBasePath+"findVolume/"+name+"/"+collectionId
    return $http.get(url).then( (response) ->
      volumes = response.data.volume
      return volumes
    , (error) ->
      console.log(error)
    )
  
  getInsertsByName = (name, volumeId) ->
    url = uploadBasePath+"findInsert/"+name+"/"+volumeId
    return $http.get(url).then( (response) ->
      inserts = response.data.insert
      return inserts
    , (error) ->
      console.log(error)
    )
  
  getInserts = (volumeId) ->
    url = uploadBasePath+"findAllInserts/" + volumeId
    return $http.get(url).then( (response) ->
      inserts = response.data.insert
      return inserts
    , (error) ->
      console.log(error)
    )

  getInsertsWithAe = (volumeId) ->
    url = uploadBasePath+"findAllInserts/" + volumeId + '?withAe=true'
    return $http.get(url).then( (response) ->
      inserts = response.data.insert
      return inserts
    , (error) ->
      console.log(error)
    )

  getPeople = (stringToSearch) ->
    url = documentBasePath+"findPeople/" + stringToSearch
    return $http.get(url).then( (response) ->
      if response.data.data
        people = response.data.data.people
      else
        people = []
    , (error) ->
      console.log(error)
    )
  
  getPlaces = (stringToSearch) ->
    url = documentBasePath+"findPlaces/" + stringToSearch
    return $http.get(url).then( (response) ->
      if response.data.data
        places = [{ placeName: "", id: null }]
        places = places.concat(response.data.data.places)
      else
        places = []
    , (error) ->
      console.log(error)
    )

  getPrincipalPlaces = (stringToSearch) ->
#    console.log(stringToSearch)
    url = documentBasePath+'findPrinicipalPlaceByPlaceId/' + stringToSearch
    return $http.get(url).then( (response) ->
      if response.data.data
        # console.log(response)
        idOfPricicipal = response.data.data
    , (error) ->
      console.log(error)
    )

  getDocumentTypologies = () ->
    url =  documentBasePath+"findDocumentTypologies"
    return $http.get(url).then( (response) ->
      if response.data.data.categories
        categories = response.data.data.categories
      else
        categories = []
    , (error) ->
      console.log(error)
    )
  
  getTopics = () ->
    url = documentBasePath+"findTopics"
    return $http.get(url).then( (response) ->
      if response.data.data
        # console.log(response)
        topics = response.data.data.topics
      else
        topics = []
    , (error) ->
      console.log (error)
    )
    

  getUserAccount = (stringToSearch) ->
    url = userBasePath+"getUsersAccount/" + stringToSearch
    return $http.get(url).then( (response) ->
      if response.data.data
        users = [{ account: "" }]
        users = users.concat(response.data.data)
      else
        user = []
    , (error) ->
    )
 
  getUserByName = (stringToSearch) ->
    url = userBasePath+"getUsersByName/" + stringToSearch
    return $http.get(url).then( (response) ->
      if response.data.data
        users = [{ account: "" }]
        #users = users.concat(response.data.data)
        users = response.data.data if response.data.data?
      else
        user = []
    , (error) ->
    )


  getPartialRecordsCount = (searchArray) ->
    url = advancedSearchPeBasePath+"partialRecordCount"
    return $http.post(url, searchArray).then(
      (response) ->
        if response.data.data?
          partialRecordsCount = response.data.data
        else
          0
      , (error) ->
        console.log("ERRORE: ", error)
    )

  getResults = (searchArray, recordStart = 0, maxResult = 20, columnToOrderBy = 'mapNameLf', orderType = 'asc') ->
    url = advancedSearchPeBasePath+"advancedSearchResults/" + recordStart + "/" + maxResult + "/" + columnToOrderBy + "/" + orderType
    return $http.post(url, searchArray).then(
      (response) ->
        response.data
      , (error) ->
        console.log("ERRORE: ", error)
    )

  getLanguages = () ->
    url = documentBasePath+"findLanguages/"
    return $http.get(url).then(
      (response) ->
        if response.data.data?
          languages = response.data.data.languages
          return languages
        else
          0
      , (error) ->
        console.log("ERRORE: ", error)
    )
  
  getRoleCategory = () ->
    url = titleAndOccupationBasePath
    return $http.get(url).then(
      (response) ->
        if response.data?
          roles = response.data.data
          return roles
        else
          0
      , (error) ->
        console.log("ERRORE: ", error)
    )
  
  getMatchTitleOccupation = (word) ->
    url = findtitleAndOccupationBasePath+"/" +word
    return $http.get(url).then(
      (response) ->
        if response.data?
          titles = response.data
          return titles
        else
          0
      , (error) ->
        console.log("ERRORE: ", error)
    )

  service = {
    getRepositories: getRepositories,
    getCollection: getCollection,
    getSeries: getSeries,
    getVolumes: getVolumes,
    getVolumesWithAe: getVolumesWithAe,
    getVolumesByName: getVolumesByName,
    getInsertsByName: getInsertsByName,
    getInserts: getInserts,
    getInsertsWithAe: getInsertsWithAe,
    getPeople: getPeople,
    getPlaces: getPlaces,
    getPrincipalPlaces: getPrincipalPlaces,
    getDocumentTypologies: getDocumentTypologies,
    getTopics: getTopics,
    getUserAccount: getUserAccount,
    getUserByName: getUserByName,
    getPartialRecordsCount: getPartialRecordsCount,
    getLanguages: getLanguages,
    getResults: getResults,
    getRoleCategory: getRoleCategory,
    getMatchTitleOccupation: getMatchTitleOccupation
  }

  return service