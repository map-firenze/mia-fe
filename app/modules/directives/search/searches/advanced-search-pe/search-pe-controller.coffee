searchPeModule = angular.module 'searchPe', ['searchServiceApp','ui.bootstrap', 'translatorAppEn', 'findPeople', 'findPlace', 'getUserAccountsModule']

searchPeModule.controller 'search-pe-controller', ($scope, searchPeInitializeService, searchPeService, $rootScope, modalFactory, $filter) ->
  isOpen = false

  $scope.searchArray = [
    searchPeInitializeService.namePartsSection(),
    searchPeInitializeService.initializeRoleTitleOccupation(),
    searchPeInitializeService.initializeWordTitleOccupation(),
    searchPeInitializeService.initializeMatchTitleOccupation(),
    searchPeInitializeService.initializeGenderPersonVitalStatistic(),
    searchPeInitializeService.initializeBirthDeathPersonVitalStatistic(),
    searchPeInitializeService.initializePersonVitalStatistic()
  ]

  $scope.loadingProcess = false

  filters = {
    namePartSection: searchPeInitializeService.namePartsSection()
    roleTitleOccupationSection: searchPeInitializeService.initializeRoleTitleOccupation()
    wordTitleOccupationSection: searchPeInitializeService.initializeWordTitleOccupation()
    matchTitleOccupationSection: searchPeInitializeService.initializeMatchTitleOccupation()
    genderPersonVitalStatisticSection: searchPeInitializeService.initializeGenderPersonVitalStatistic()
    personBirthDeathPlaceSection: searchPeInitializeService.initializeBirthDeathPersonVitalStatistic()
    personVitalStatistics: searchPeInitializeService.initializePersonVitalStatistic()
  }
  

  $scope.sections = [
    {name: 'Name Parts', open: false},
    {name: 'Title/Occupation', open: false},
    {name: 'Biographical Details', open: false},
  ]

  # For detect last selected search
  $scope.selectedSearches = [
    {type: 'ArcLoc', selected: false, data: null, order: 0},
    {type: 'DocCat', selected: false, data: null, order: 1},
    {type: 'Trans',  selected: false, data: null, order: 2},
    {type: 'Syn',    selected: false, data: null, order: 3},
    {type: 'People', selected: false, data: null, order: 4},
    {type: 'Place',  selected: false, data: null, order: 5},
    {type: 'Top',    selected: false, data: null, order: 6},
    {type: 'DateR',  selected: false, data: null, order: 7},
    {type: 'Lang',   selected: false, data: null, order: 8},
    {type: 'Owner',  selected: false, data: null, order: 9}
  ]

  $scope.dateA = {}
  $scope.dateB = {}

  setSelectedData = (type, selected, data) ->
    search = _.find $scope.selectedSearches, type: type
    if search
      search.selected = selected
      search.data     = data

  $scope.init = () ->
    initMainRepo()
    initNamePartsSection()
    initTitleOccupationSection()
    initPersonVitalStatistic()
  
  ################################# Init section code #################################
  initNamePartsSection = () ->
    #init search array
    $scope.namePartSections = []

    $scope.namePartSection = searchPeInitializeService.namePartsSection()
    #$scope.personDetailAndVisualStatisticsSection = searchPeInitializeService.initializePersonDetailAndVisualStatistics()

  initTitleOccupationSection = () ->
    $scope.rolesOccupationSections = []
    $scope.wordsTitleOccupationSection = []
    $scope.matchTitleOccupationsIsClicked = false
    $scope.initializeMatchTitleOccupationTitleOcTemp = {
      title : '',
      id : ''
    }

    $scope.roleTitleOccupationSection = initializeRoleTitleSection()
    $scope.wordTitleOccupationSection = initializeWordTitleSection()
    $scope.matchTitleOccupationSection = initializeMatchTitleSection()

    searchPeService.getRoleCategory().then( (response) ->
      $scope.occupations = (response)
    )
  
  initPersonVitalStatistic = () ->
    # $scope.genderPersonVitalStatisticSections = []
    $scope.personBirthDeathPlaceSections = []
    $scope.personVitalStatistics = []
    $scope.genderPlaceTemp = {
      title : '',
      id : ''
    }
    $scope.birthDeathPlaceTemp = {
      title : '',
      id : ''
    }
    $scope.genderPersonVitalStatisticSection = searchPeInitializeService.initializeGenderPersonVitalStatistic()
    $scope.personVitalStatistic = searchPeInitializeService.initializePersonVitalStatistic()
    $scope.personBirthDeathPlaceSection = searchPeInitializeService.initializeBirthDeathPersonVitalStatistic()

  $scope.checkFormRoleTitleOccupation = () ->
    role = JSON.parse($scope.roleTitleOccupationSection.role)
    if(!$scope.checkIsPresentRoleTitleOccupation(role, $scope.searchArray[1].roles))
      $scope.loadingProcess = true

      $scope.searchArray[1].roles.push(role)
      $scope.searchArray[1].isActiveFilter = true
      createSearchArray()
      
      clearRoleTitleOccupationSection()

  $scope.checkFormWordTitleOccupation = () ->
    role = angular.copy($scope.wordTitleOccupationSection.role)
    role.roleCatMinor = role.roleCatMinor.trim().toLowerCase()
    
    #if(!$scope.checkIsPresentWordTitleOccupation(role, $scope.searchArray[2].roles))
    $scope.loadingProcess = true

    $scope.searchArray[2].roles.push(role)
    $scope.searchArray[2].isActiveFilter = true
    createSearchArray()
    
    clearWordTitleOccupationSection()
   
  $scope.checkFormMatchTitleOccupation = () ->
    $scope.matchTitleOccupationSection.role.roleCateId = $scope.initializeMatchTitleOccupationTitleOcTemp.id
    $scope.matchTitleOccupationSection.role.roleCatMinor = $scope.initializeMatchTitleOccupationTitleOcTemp.title

    role = angular.copy($scope.matchTitleOccupationSection.role)
    
    $scope.searchArray[3].roles = [role]
    $scope.searchArray[3].isActiveFilter = true
    
    createSearchArray()

    clearMatchTitleOccupationSection()
  
  $scope.checkFormWord = () ->
    $scope.loadingProcess = true
    $scope.namePartSection.isActiveFilter = true
    $scope.searchArray[0] = angular.copy($scope.namePartSection)

    createSearchArray()
    clearNamePartSection()

  #####SECTION PERSON AND DETAIL STATISTICS
  $scope.months = searchPeInitializeService.months()
  
  $scope.checkFormGender = () ->
    if(!$scope.checkIsPresentGenderPersonVitalStatistic())
      $scope.loadingProcess = true
      $scope.genderPersonVitalStatisticSection.isActiveFilter = true
      $scope.searchArray[4] = angular.copy($scope.genderPersonVitalStatisticSection)

      filters.genderPersonVitalStatisticSection = angular.copy($scope.genderPersonVitalStatisticSection)

      createSearchArray()
      clearGenderPersonVitalStatisticSection()

  $scope.checkFormHeadquarters = () ->
    if(!$scope.checkIsPresentGenderHeadquartersPersonVitalStatistic())
      $scope.loadingProcess = true
      $scope.genderPersonVitalStatisticSection.isActiveFilter = true
      filters.genderPersonVitalStatisticSection = angular.copy($scope.genderPersonVitalStatisticSection)
      filters.genderPersonVitalStatisticSection.headquarter = $scope.genderPlaceTemp.id
      filters.genderPersonVitalStatisticSection.headquarterName = $scope.genderPlaceTemp.title

      $scope.searchArray[4] = angular.copy(filters.genderPersonVitalStatisticSection)

      createSearchArray()
      clearGenderPersonVitalStatisticSection()
  
  $scope.checkFormBirthDeathPlace = () ->
    if(!$scope.checkIsPresentBirthDeathStatistic())
      $scope.loadingProcess = true
      $scope.personBirthDeathPlaceSection.isActiveFilter = true
      searchValueOrig = $scope.personBirthDeathPlaceSection.searchType
      
      if($scope.personBirthDeathPlaceSection.searchType == 'birthAndDeathPlace')
        $scope.personBirthDeathPlaceSection.birthPlaceId = $scope.birthDeathPlaceTemp.id + ""
        $scope.personBirthDeathPlaceSection.birthPlaceName = $scope.birthDeathPlaceTemp.title

        $scope.personBirthDeathPlaceSection.deathPlaceId = $scope.birthDeathPlaceTemp.id + ""
        $scope.personBirthDeathPlaceSection.deathPlaceName = $scope.birthDeathPlaceTemp.title
      
      else if($scope.personBirthDeathPlaceSection.searchType == 'deathPlace')
        $scope.personBirthDeathPlaceSection.deathPlaceId = $scope.birthDeathPlaceTemp.id + ""
        $scope.personBirthDeathPlaceSection.deathPlaceName = $scope.birthDeathPlaceTemp.title

        $scope.personBirthDeathPlaceSection.birthPlaceId = ""
      else
        $scope.personBirthDeathPlaceSection.birthPlaceId = $scope.birthDeathPlaceTemp.id + ""
        $scope.personBirthDeathPlaceSection.birthPlaceName = $scope.birthDeathPlaceTemp.title

        $scope.personBirthDeathPlaceSection.deathPlaceId = ""
     
      $scope.searchArray[5] =  angular.copy($scope.personBirthDeathPlaceSection)

      createSearchArray()
      clearPersonBirthDeathPlaceSection()

  $scope.checkFormVitalStatisticSearch = () ->
    if(!$scope.checkIsPresentVitalStatisticSearch())
      if ($scope.dateARangeChecker())
        $scope.loadingProcess = true
        $scope.personVitalStatistic.isActiveFilter = true
        if ($scope.dateA.year)
          $scope.personVitalStatistic.year = $scope.dateA.year
        else
          $scope.personVitalStatistic.year = ''
        if ($scope.dateA.month)
          $scope.personVitalStatistic.month = $scope.dateA.month
        else
          $scope.personVitalStatistic.month = ''
        if ($scope.dateA.day)
          $scope.personVitalStatistic.day = $scope.dateA.day
        else
          $scope.personVitalStatistic.day = ''

        if ($scope.personVitalStatistic.searchType == "livedBetween")
          if(!$scope.checkIsPresentVitalStatisticSearchLivedBetween())
            if ($scope.dateB.year)
              $scope.personVitalStatistic.andYear = $scope.dateB.year
            else
              $scope.personVitalStatistic.andYear = ''
            if ($scope.dateB.month)
              $scope.personVitalStatistic.andMonth = $scope.dateB.month
            else
              $scope.personVitalStatistic.andMonth = ''
            if ($scope.dateB.day)
              $scope.personVitalStatistic.andDay = $scope.dateB.day
            else
              $scope.personVitalStatistic.andDay = $scope.dateB.day = ''

        filters.personVitalStatistics = angular.copy($scope.personVitalStatistic)
        $scope.searchArray[6] = angular.copy(filters.personVitalStatistics)
       
        createSearchArray()
        clearPersonVitalStatisticSection()
  
  $scope.checkFormVitalStatisticSearchLivedBetween = () ->
    if(!$scope.checkIsPresentVitalStatisticSearchLivedBetween())
      if ($scope.dateABRangeChecker())
        $scope.loadingProcess = true
        $scope.personVitalStatistic.isActiveFilter = true
        if ($scope.dateA.year)
          $scope.personVitalStatistic.year = $scope.dateA.year
        else
          $scope.personVitalStatistic.year = ''
        if ($scope.dateA.month)
          $scope.personVitalStatistic.month = $scope.dateA.month
        else
          $scope.personVitalStatistic.month = ''
        if ($scope.dateA.day)
          $scope.personVitalStatistic.day = $scope.dateA.day
        else
          $scope.personVitalStatistic.day = ''
        if ($scope.dateB.year)
          $scope.personVitalStatistic.andYear = $scope.dateB.year
        else
          $scope.personVitalStatistic.andYear = ''
        if ($scope.dateB.month)
          $scope.personVitalStatistic.andMonth = $scope.dateB.month
        else
          $scope.personVitalStatistic.andMonth = ''
        if ($scope.dateB.day)
          $scope.personVitalStatistic.andDay = $scope.dateB.day
        else
          $scope.personVitalStatistic.andDay = ''
        $scope.editPersonVitalStatisticLivedBetween = JSON.parse(JSON.stringify($scope.personVitalStatistic))
        $scope.searchArray.push(angular.copy($scope.editPersonVitalStatisticLivedBetween))
        $scope.personVitalStatistics.push(angular.copy($scope.editPersonVitalStatisticLivedBetween))

        filters.personVitalStatisticLivedBetween = angular.copy($scope.editPersonVitalStatisticLivedBetween)

        createSearchArray()
        clearPersonVitalStatisticSection()

  initializeRoleTitleSection = () ->
    return {
      "searchSection": "titleOccupationSearch",
      "type": "titleOccupationAdvancedSearch",
      "searchType":"roleCategory",
      "role": null
      "isActiveFilter":false
    }

  initializeWordTitleSection = () ->
    return {
      "searchSection": "titleOccupationSearch",
      "type": "titleOccupationAdvancedSearch",
      "searchType":"wordTitleOcc",
      "role": {
        "roleCateId": null,
        "roleCatMajor": null,
        "roleCatMinor": null,
      }
      "isActiveFilter":false
    }

  initializeMatchTitleSection = () ->
    return {
      "searchSection": "titleOccupationSearch",
      "type": "titleOccupationAdvancedSearch",
      "searchType":"matchTitleOcc",
      "role": {
        "roleCateId": null,
        "roleCatMajor": null,
        "roleCatMinor": null,
      }
      "isActiveFilter":false
    }

  createSearchArray = () ->
    searchPeService.getPartialRecordsCount($scope.searchArray).then(
      (response) ->
        $scope.countResult = response
        $scope.loadingProcess = false
      , (error) ->
        $scope.countResult = "ERRORE"
    )
  
  clearPersonBirthDeathPlaceSection = () ->
    $scope.personBirthDeathPlaceSection = searchPeInitializeService.initializeBirthDeathPersonVitalStatistic()
    $scope.personBirthDeathPlaceSection.isActiveFilter = true
    $scope.birthDeathPlaceTemp.title = ''
    $scope.birthDeathPlaceTemp.id = ''
  
  clearNamePartSection = () ->
    $scope.namePartSection.searchString = ''
    $scope.namePartSection.nameType = ''

  clearRoleTitleOccupationSection = () ->
    $scope.roleTitleOccupationSection.role = null

  clearWordTitleOccupationSection = () ->
    $scope.wordTitleOccupationSection = initializeWordTitleSection()

  clearMatchTitleOccupationSection = () ->
    $scope.matchTitleOccupationSection = initializeMatchTitleSection()
    $scope.initializeMatchTitleOccupationTitleOcTemp.id = ''
    $scope.initializeMatchTitleOccupationTitleOcTemp.title = ''

  clearPersonVitalStatisticSection = () ->
    $scope.initializeDateA()
    $scope.initializeDateB()
    $scope.personVitalStatistic.searchType =''
  
  clearGenderPersonVitalStatisticSection = () ->
    $scope.genderPersonVitalStatisticSection = searchPeInitializeService.initializeGenderPersonVitalStatistic()
    $scope.genderPersonVitalStatisticSection.isActiveFilter = true
    $scope.genderPlaceTemp.title = ''
    $scope.genderPlaceTemp.id = ''

  
  $scope.getMatchTitleOccupation = (searchQuery) ->
    $scope.matchTitleOccupationsIsClicked = false
    if searchQuery
      if searchQuery.length >= 3
        $scope.loading = true
        $scope.searchTableOpen = true
        searchPeService.getMatchTitleOccupation(searchQuery).then (resp)->
          if resp.status is "ok"
            $scope.matchTitleOccupations = resp.data.arrayTitleAndOccs
            $scope.searchTableOpen = true
          else
            $scope.matchTitleOccupations = null
          $scope.loading = false

  $scope.onClickGender = (value) ->
    if(value != 'X')
      $scope.genderPersonVitalStatisticSection = searchPeInitializeService.initializeGenderPersonVitalStatistic()
    $scope.genderPersonVitalStatisticSection.isActiveFilter = true
    $scope.genderPersonVitalStatisticSection.gender = value

  $scope.getPlaceFromString = (searchQuery) ->
    $scope.genderPlaceIsClicked = false
    if searchQuery
      if searchQuery.length >= 3
        $scope.loading = true
        $scope.searchGenderPlaceTableOpen = true
        searchPeService.getPlaces(searchQuery).then (resp)->
          $scope.placeGenderList = resp
          $scope.searchGenderPlaceTableOpen = true
          $scope.loading = false
  
  $scope.getBirthDeathPlaceFromString = (searchQuery) ->
    $scope.birthDeathPlaceIsClicked = false
    if searchQuery
      if searchQuery.length >= 3
        $scope.loading = true
        $scope.searchBirthDeathPlaceTableOpen = true
        searchPeService.getPlaces(searchQuery).then (resp)->
          $scope.placeBirthDeathPlaceList = resp
          $scope.searchBirthDeathPlaceTableOpen = true
          $scope.loading = false

  $scope.selectBirthDeathPlace = (place) ->
    $scope.birthDeathPlaceTemp.id = place.id
    $scope.birthDeathPlaceTemp.title = place.placeName
    $scope.searchBirthDeathPlaceTableOpen = false

  $scope.selectHeadquartersGender = (place) ->
    $scope.genderPlaceTemp.id = place.id
    $scope.genderPlaceTemp.title = place.placeName
    $scope.searchGenderPlaceTableOpen = false
  
  $scope.closeTable = ()->
    $scope.searchTableOpen = false
  $scope.loading = false

  $scope.selectRoleInMatchTitleOccupationList = (val) ->
    $scope.initializeMatchTitleOccupationTitleOcTemp.id = val.titleOccId
    $scope.initializeMatchTitleOccupationTitleOcTemp.title = val.titleOcc
    $scope.searchTableOpen = false
  
  
  $scope.changePersonVitalStatisticSearchType = (value) ->
    $scope.personVitalStatistic = searchPeInitializeService.initializePersonVitalStatistic()
    $scope.personVitalStatistic.searchType = value
    $scope.personVitalStatistic.isActiveFilter = true


  $scope.$on("peopleClearEvent", () ->
    $scope.initializeMatchTitleOccupationTitleOcTemp.id = ""
    $scope.initializeMatchTitleOccupationTitleOcTemp.title = ""
    $scope.matchTitleOccupations = [])

  $scope.$on("personIsSelected", (event, personInfo) ->
    $scope.initializeMatchTitleOccupationTitleOcTemp = personInfo.mapNameLf
    $scope.matchTitleOccupationsIsClicked = true)
  

  ############# REMOVE
  $scope.removePresentWordParts = () ->
    $scope.namePartSection = searchPeInitializeService.namePartsSection()

    $scope.searchArray[0].searchString = ''
    $scope.searchArray[0].isActiveFilter = false

    createSearchArray()
    
  $scope.removePresentRoleTitleOccupation = (index) ->
    $scope.roleTitleOccupationSection = initializeRoleTitleSection()

    $scope.searchArray[1].roles.splice(index, 1)
    if $scope.searchArray[1].roles.length == 0
      $scope.searchArray[1].isActiveFilter = false

    createSearchArray()
    
  $scope.removePresentWordTitleOccupation = (index) ->
    $scope.wordTitleOccupationSection = initializeWordTitleSection()

    $scope.searchArray[2].roles.splice(index, 1)
    if $scope.searchArray[2].roles.length == 0
      $scope.searchArray[2].isActiveFilter = false

    createSearchArray()
  
  $scope.removePresentMatchTitleOccupation = (index) ->
    $scope.matchsTitleOccupationSection = initializeMatchTitleSection()
    
    $scope.searchArray[3].roles = []
    $scope.searchArray[3].isActiveFilter = false

    createSearchArray()
  
  $scope.removePresentGenderPersonVitalStatistic = () ->
    filters.genderPersonVitalStatisticSection = searchPeInitializeService.initializeGenderPersonVitalStatistic()
    $scope.searchArray[4] = angular.copy(filters.genderPersonVitalStatisticSection)

    createSearchArray()

  $scope.removePresentDeathBirthPlaceSection = () ->
    filters.personBirthDeathPlaceSection = searchPeInitializeService.initializeBirthDeathPersonVitalStatistic()
    $scope.searchArray[5] = angular.copy(filters.personBirthDeathPlaceSection)
  
    createSearchArray()

  $scope.removePresentVitalStatisticSearch = () ->
    filters.personVitalStatistics =  searchPeInitializeService.initializePersonVitalStatistic()
    $scope.searchArray[6] = angular.copy(filters.personVitalStatistics)

    createSearchArray()


  ############# CHECK
  $scope.checkIsPresentWordParts = () ->
    for i in [0...$scope.searchArray.length]
      val =  $scope.searchArray[i]
      if(val.searchSection == 'namesSearch' && val.type == 'wordAdvancedSearch' && val.searchString == $scope.namePartSection.searchString && val.nameType == $scope.namePartSection.nameType)
        return true
    return false
  
  $scope.checkIsPresentRoleTitleOccupation = (role, roles) ->
    if roles.length == 0
      return false

    results = _.filter(roles, { 'roleCateId': role.roleCateId })
    
    return results.length > 0
  
  $scope.checkIsPresentMatchTitleOccupation = () ->
    if roles.length == 0
      return false

    results = _.filter(roles, { 'roleCateId': role.roleCateId })
    
    return results.length > 0
  
  $scope.checkIsPresentWordTitleOccupation = (role, roles) ->
    if roles.length == 0
      return false

    results = _.filter(roles, { 'roleCatMinor': role.roleCatMinor.trim().toLowerCase() })

    return results.length > 0
  
  $scope.checkIsPresentGenderPersonVitalStatistic = () ->
    for i in [0...$scope.searchArray.length]
      val =  $scope.searchArray[i]
      if(val.searchSection == 'personVitalStatisticsGenderSearch' && val.type == 'genderAdvancedSearch' && val.gender == $scope.genderPersonVitalStatisticSection.gender)
        return true
    return false
  
  $scope.checkIsPresentGenderHeadquartersPersonVitalStatistic = () ->
    for i in [0...$scope.searchArray.length]
      val =  $scope.searchArray[i]
      if(val.searchSection == 'personVitalStatisticsGenderSearch' && val.type == 'genderAdvancedSearch' && val.gender == $scope.genderPersonVitalStatisticSection.gender && val.headquarter == $scope.genderPlaceTemp.id)
        return true
    return false

  $scope.checkIsPresentBirthDeathStatistic = () ->
    for i in [0...$scope.searchArray.length]
      val =  $scope.searchArray[i]
      if(val.searchSection == 'personVitalStatisticsBirthDeathSearch' && val.type == 'personVitalStatisticsBirthDeathAdvancedSearch' && val.searchTypeOrigValue == $scope.personBirthDeathPlaceSection.searchType)
        if(val.searchTypeOrigValue == 'birthAndDeathPlace')
          if(val.birthPlaceId == $scope.birthDeathPlaceTemp.id && val.deathPlaceId == $scope.birthDeathPlaceTemp.id )
            return true
          else if($scope.searchTypeOrigValue == 'birthPlace')
            if(val.birthPlaceId == $scope.birthDeathPlaceTemp.id )
              return true
            else
              if(val.deathPlaceId == $scope.birthDeathPlaceTemp.id )
                return true
    return false
  
  $scope.checkIsPresentVitalStatisticSearch = () ->
    for i in [0...$scope.searchArray.length]
      valJson =  $scope.searchArray[i]
      if(valJson.searchSection == 'personVitalStatisticBADBLBBDOSearch' && valJson.type == 'personVitalStatisticBADBLBBDOAdvancedSearch' && valJson.searchType == $scope.personVitalStatistic.searchType && valJson.year == $scope.dateA.year && valJson.day == $scope.dateA.day && valJson.month == $scope.dateA.month)
        return true
    return false
  
  $scope.checkIsPresentVitalStatisticSearchLivedBetween = () ->
    for i in [0...$scope.searchArray.length]
      val =  $scope.searchArray[i]
      if(val.searchSection == 'personVitalStatisticBADBLBBDOSearch' && val.type == 'personVitalStatisticBADBLBBDOAdvancedSearch' && val.year == $scope.dateA.year && val.day == $scope.dateA.day && val.month == $scope.dateA.month && val.andYear == $scope.dateB.year && val.andMonth == $scope.dateB.month && val.andDay == $scope.dateB.day)
        return true
    return false

  $scope.getResults = () ->
    $scope.loadingProcess = true
    searchPeService.getResults($scope.searchArray, 0, 20).then (resp) ->

      newResult={}
      newResult.searchName = 'People'
      newResult.type='people'
      newResult.results=resp.data
      newResult.searchArray = angular.copy($scope.searchArray)
      newResult.content = {
        searchType: newResult.searchName,
        countResult: $scope.countResult,
        where: '',
        title: 'Advanced Search Person'
      }
      $scope.loadingProcess = false
      if newResult.results
        $rootScope.$broadcast('new-result', newResult)
      else
        pageTitle = $filter('translate')("modal.WARNING")
        messagePage = 'No results found!'
        modalFactory.openMessageModal(pageTitle, messagePage, false, "sm")
    
  $scope.thereAreResults = () ->
    for i in [0...$scope.searchArray.length]
      value =  $scope.searchArray[i]

      if value.isActiveFilter == true
        return true

    return false

  ##############################################################################

  $scope.clearFilters = () ->
    filters = {
      namePartSection: searchPeInitializeService.namePartsSection()
      roleTitleOccupationSection: searchPeInitializeService.initializeRoleTitleOccupation()
      wordTitleOccupationSection: searchPeInitializeService.initializeWordTitleOccupation()
      matchTitleOccupationSection: searchPeInitializeService.initializeMatchTitleOccupation()
      genderPersonVitalStatisticSection: searchPeInitializeService.initializeGenderPersonVitalStatistic()
      personBirthDeathPlaceSection: searchPeInitializeService.initializeBirthDeathPersonVitalStatistic()
      personVitalStatistics: searchPeInitializeService.initializePersonVitalStatistic()
    }

  ################################# Watch last closed tab #################################
  $scope.$watch 'sections', (newVal, oldVal) ->
    lastClosedTab = _.find(oldVal, { open: true })

    return unless lastClosedTab

    switch lastClosedTab.name
      when 'Biographical Details'
        $scope.initializeDateA()
        $scope.initializeDateB()
  , true


  initMainRepo = () ->
    searchPeService.getRepositories().then (resp) ->
      mainRepo = _.find resp, (o) -> o.repositoryId == '1'
      $scope.repository = mainRepo

    $scope.initializeDateA = () ->
      $scope.dateA = searchPeInitializeService.dateA()

    $scope.initializeDateB = () ->
      $scope.dateB = searchPeInitializeService.dateB()

  $scope.dateARangeChecker = () ->
    return dateValid($scope.dateA)

  $scope.dateABRangeChecker = () ->
    return dateValid($scope.dateA) && dateValid($scope.dateB)
  
  dateValid = (date) ->
    if not date?
      return false
    if isEmptyField(date.year)
      return false
    if isEmptyField(date.month) && isNotEmptyField(date.day)
      return false
    if isNotEmptyField(date.day)
      numberOfDays = getNumberOfDays(date.year, date.month)
      if date.day <= 0 || date.day > numberOfDays
        return false
    return true

  getNumberOfDays = (year, month) ->
    if $scope.isNotEmptyField(year) and $scope.isNotEmptyField(month)
      isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0))
      [31, (if isLeap then 29 else 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month - 1]
    else
      31

  isEmptyField = (field) ->
    return not field? || field == ""

  isNotEmptyField = (field) ->
    return isEmptyField(field) == false