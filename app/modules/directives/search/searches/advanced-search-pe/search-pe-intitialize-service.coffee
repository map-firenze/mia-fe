searchPeModule.service 'searchPeInitializeService', ($http, ENV, searchDeService) ->

  getCollection = () ->
    return searchDeService.getCollection().then( (response) ->
      return response
    )

  selectionArchivalLocation = () ->
    return  {
      collection: { collectionId: 0, collectionName: '' },
      serie:      { seriesId: 0, seriesName: '' },
      volume:     { volumeId: 0, volumeName: '' },
      insert:     { insertId: 0, insertName: '' }
    }

  archivalLocation = () ->
    return {
      "searchSection": "archivalLocationSearch",
      "type": "archivalLocationAdvancedSearch",
      "isActiveFilter": false,
      "collection": "",  # stringa     obbligatorio
      "series": "",      # stringa     nullabile
      "volume": "",      # numerico    nullabile
      "insert": ""       # numerico??  nullabile??
    }

  selectionDocumentCategory = () ->
    return {
      category: "",
      typology: ""
    }

  documentCategory = () ->
    return {
      "searchSection": "categoryAndTypologySearch",
      "type": "categoryAndTypologyAdvancedSearch",
      "isActiveFilter": false,
      "category": null, # stringa     obbligatorio
      "typology": null  # stringa     nullabile
    }

  transcriptionsAndSynopsis = () ->
    return {
      transcription: "",
      synopsis: ""
    }

  transcription = () ->
    return {
      "searchSection": "transcriptionSearch",
      "type": "transcriptionAdvancedSearch",
      "isActiveFilter": false,
      "transcription": ""
    }

  synopsis = () ->
    return {
      "isActiveFilter": false,
      "searchSection": "synopsisSearch",
      "type": "synopsisAdvancedSearch",
      "synopsis": ""
    }

  selectionPeopleAndPlaces = () ->
    return {
      "people": [] ,
      "places": []
    }
    
  people = () ->
    return {
      "searchSection": "peopleSearch",
      "type": "peopleAdvancedSearch",
      "isActiveFilter": false,
      "people": []
    }

  places = () ->
    return  {
      "searchSection": "placesSearch",
      "type": "placesAdvancedSearch",
      "isActiveFilter": false,
      "places": []
    }

  topicAndRelatedPlaces = () ->
    return {
      "searchSection": "topicsSearch",
      "type": "topicsAdvancedSearch",
      "isActiveFilter": false,
      "topics":[]
    }
       
  owner = () ->
    return{
      "searchSection": "documentOwnerSearch",
      "type": "documentOwnerAdvancedSearch",
      "isActiveFilter": false,
      "editType": "owner",
      "account": ""
    }

  months = () ->
    [
      { num: '', name: '' },
      { num: 1, name: 'January' },
      { num: 2, name: 'February' },
      { num: 3, name: 'March' },
      { num: 4, name: 'April' },
      { num: 5, name: 'May' },
      { num: 6, name: 'June' },
      { num: 7, name: 'July' },
      { num: 8, name: 'August' },
      { num: 9, name: 'September' },
      { num: 10, name: 'October' },
      { num: 11, name: 'November' },
      { num: 12, name: 'December' }
    ]
  
  filterDate = () ->
    [
      { value: "from", label: "Written from" },
      { value: "before", label: "Written before" },
      { value: "between", label: "Written between" },
      { value: "in", label: "Written in/on" }
    ]

  dates = () ->
    return {
      "searchSection": "dateSearch",
      "type": "dateAdvancedSearch",
      "isActiveFilter": false,
      "dateFilterType": "",
      "dateYear": "",
      "dateMonth": "",
      "dateDay": "",
      "dateBYear": "",
      "dateBMonth": "",
      "dateBDay": ""
    }

  dateA = () ->
    return {
      filter: "in",
      year: "",
      month: "",
      day: ""
    }

  dateB = () ->
    return {
      year: "",
      month: "",
      day: ""
    }

  languages = () ->
    return {
      "searchSection": "languagesSearch",
      "type": "languagesAdvancedSearch",
      "isActiveFilter": false,
      "languages": []
    }

  namePartsSection = () ->
    return {
      "searchSection":"namesSearch",
      "type":"wordAdvancedSearch",
      "isActiveFilter":false,
      "searchString": null ,
      "nameType": null
    }


  initializePersonDetailAndVisualStatistics = () ->
    return {
      "searchSection":"personVitalStatisticsSearch",
      "type":"headquartersAdvancedSearch",
      "isActiveFilter":false,
      "personId":null
    }


  initializeRoleTitleOccupation = () ->
    return {
      "searchSection": "titleOccupationSearch",
      "type": "titleOccupationAdvancedSearch",
      "searchType": "roleCategory",
      "roles": [],
      "isActiveFilter": false
    }

  initializeWordTitleOccupation = () ->
    return {
      "searchSection":"titleOccupationSearch",
      "type":"titleOccupationAdvancedSearch",
      "searchType":"wordTitleOcc",
      "roles": [],
      "isActiveFilter":false
    }
  
  initializeMatchTitleOccupation = () ->
    return {
      "searchSection": "titleOccupationSearch",
      "type": "titleOccupationAdvancedSearch",
      "searchType" : "matchTitleOcc",
      "roles": [],
      "isActiveFilter":false
    }

  initializeGenderPersonVitalStatistic = () ->
    return {
      "searchSection":"personVitalStatisticsGenderSearch",
      "type":"genderAdvancedSearch",
      "isActiveFilter":false,
      "gender": null,
      "headquarter": null
      "headquarterName": null
    }
  
  initializeBirthDeathPersonVitalStatistic = () ->
    return {
      "searchSection":"personVitalStatisticsBirthDeathSearch",
      "type":"personVitalStatisticsBirthDeathAdvancedSearch",
      "isActiveFilter":false,
      "searchType": null,
      "birthPlaceId":"",
      "deathPlaceId": "",
      "birthPlaceName":"",
      "deathPlaceName": ""
    }
  
  initializePersonVitalStatistic = () ->
    return {
      "searchSection":"personVitalStatisticBADBLBBDOSearch",
      "type":"personVitalStatisticBADBLBBDOAdvancedSearch",
      "isActiveFilter":false,
      "searchType": null,
      "year":null,
      "month":null,
      "day":null,
      "andYear":null,
      "andMonth":null,
      "andDay":null
    }

  initializePersonVitalStatisticLivedBetween = () ->
    return {
      "searchSection":"personVitalStatisticBADBLBBDOSearch",
      "type":"personVitalStatisticBADBLBBDOAdvancedSearch",
      "isActiveFilter":false,
      "searchType":"livedBetween",
      "year":null,
      "month":null,
      "day":null,
      "andYear":null,
      "andMonth":null,
      "andDay":null
    }

  service = {
    getCollection: getCollection,
    selectionArchivalLocation: selectionArchivalLocation,
    archivalLocation: archivalLocation,
    selectionDocumentCategory: selectionDocumentCategory,
    documentCategory: documentCategory,
    transcriptionsAndSynopsis: transcriptionsAndSynopsis,
    transcription: transcription,
    synopsis: synopsis,
    initializePersonVitalStatisticLivedBetween: initializePersonVitalStatisticLivedBetween,
    initializePersonVitalStatistic: initializePersonVitalStatistic,
    selectionPeopleAndPlaces: selectionPeopleAndPlaces,
    people: people,
    places: places,
    topicAndRelatedPlaces: topicAndRelatedPlaces,
    initializePersonDetailAndVisualStatistics: initializePersonDetailAndVisualStatistics,
    initializeRoleTitleOccupation: initializeRoleTitleOccupation,
    initializeMatchTitleOccupation: initializeMatchTitleOccupation,
    owner: owner,
    initializeBirthDeathPersonVitalStatistic: initializeBirthDeathPersonVitalStatistic,
    namePartsSection: namePartsSection,
    months: months,
    filterDate: filterDate,
    dates: dates,
    dateA: dateA,
    dateB: dateB,
    languages: languages,
    initializeWordTitleOccupation: initializeWordTitleOccupation,
    initializeGenderPersonVitalStatistic: initializeGenderPersonVitalStatistic
  }

  return service