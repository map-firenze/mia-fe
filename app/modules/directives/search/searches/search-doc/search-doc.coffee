searchDocument = angular.module 'searchDocument', ['searchServiceApp']
searchDocument.directive 'searchDocumentDir', (searchFactory, modalFactory, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/search/searches/search-doc/search-doc.html'

  link: (scope, elem, attr) ->
    scope.searchType = 'all'
    scope.searchWhere = 'DE_TRANSCRIPTION_AND_SYNOPSIS'
    scope.wordSearchDe = (query, where, type)->
      params = {}
      params.owner = scope.$root.me.account
      params.partialSearchWords = ''
      params.matchSearchWords = ''
      # params.searchString = query
      params.searchType = where
      params.onlyMyEntities = (type=='mine')
      params.everyoneElsesEntities = (type=='other')
      params.allEntities = (type=='all')

      # pip lup luoo "pippo"
      # matchSearchWords: "pippo"
      # partialSearchWords: "pip lup luoo"

      tokens = [].concat.apply [], query.split('"').map (v, i) ->
        return if i%2 then '"' + v + '"' else v.split(' ')
      .filter(Boolean)

      tokens.forEach (token) ->
        if token.charAt(0) == '"'
          params.matchSearchWords += token.substr(1, token.length - 2) + ' '
        else
          params.partialSearchWords += token + ' '

      if params.matchSearchWords != ''
        params.matchSearchWords = params.matchSearchWords.substr(0, params.matchSearchWords.length - 1)

      if params.partialSearchWords != ''
        params.partialSearchWords = params.partialSearchWords.substr(0, params.partialSearchWords.length - 1)

      searchFactory.searchDocWord(params).then (resp)->
        newResult = {}
        newResult.searchName = query
        place = 'All documents'               if type=='all'
        place = 'Only my documents'           if type=='mine'
        place = 'Everyone elses documents'    if type=='other'
        whereS = 'Transcription and synopsis' if where == 'DE_TRANSCRIPTION_AND_SYNOPSIS'
        whereS = 'Transcription'              if where == 'DE_TRANSCRIPTION'
        whereS = 'Synopsis'                   if where == 'DE_SYNOPSIS'
        newResult.searchTooltip = 'You have searched <b>' + newResult.searchName + '</b> for ' + whereS + ' ' + place
        newResult.content = {
          searchType: newResult.searchName,
          where: place,
          where2: whereS,
          title: 'Document search',
          matchSearchWords: params.matchSearchWords,
          partialSearchWords: params.partialSearchWords,
          countResult: resp.data?.count || 0,
          params: params
        }
        newResult.type='doc'
        newResult.subtype='word_search'
        if resp.data
          newResult.results = resp.data.docs
          if newResult.results.length
            scope.$root.$broadcast('new-result', newResult)
          else
            showWarning()
        else
          newResult.results = []
          showWarning()

    showWarning = () ->
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = 'No results found!'
      modalFactory.openMessageModal(pageTitle, messagePage, false, "sm")
