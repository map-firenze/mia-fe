docTranscription = angular.module 'docTranscription', []

# docTranscription.filter 'replace', [ ->
#   (input, from, to) ->
#       if input == undefined
#         return
#       regex = new RegExp(from, 'g')
#       input.replace regex, to
# ]

docTranscription.filter 'replaceTagsTrans', ->
  (text) ->
    if !text
      return text

    # hubUnsure = (text.match(/<hubUnsure>y/g) or []).length
    # i = 0
    # while i < hubUnsure
    #   text = text.replace("<hubUnsure>y", '-Unsure')
    #   text = text.replace("<\/hubUnsure>", '')
    #   i++
    # text
    # fromUnsure = (text.match(/<fromUnsure>y/g) or []).length
    # i = 0
    # while i < fromUnsure
    #   text = text.replace("<fromUnsure>y", '-Unsure')
    #   text = text.replace("<\/fromUnsure>", '')
    #   i++
    # text
    # dateUnsure = (text.match(/<dateUnsure>y/g) or []).length
    # i = 0
    # while i < dateUnsure
    #   text = text.replace("<dateUnsure>y", '-Unsure')
    #   text = text.replace("<\/dateUnsure>", ']')
    #   text = text.replace("<\/date>", '')
    #   i++

    text = text.replace(/<newsHeader>/g, '[')
    text = text.replace(/<\/newsHeader>/g, '')
    text = text.replace(/<newsFrom>/g, '[')
    text = text.replace(/<\/newsFrom>/g, '')
    text = text.replace(/<from>/g, 'News From: ')
    text = text.replace(/<\/from>/g, '')
    if text.indexOf('fromUnsure') >= 0
      text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/fromUnsure>/g, '')
    text = text.replace(/<hub>/g, 'News Hub: ')
    text = text.replace(/<\/hub>/g, '')
    if text.indexOf('hubUnsure') >= 0
      text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/hubUnsure>/g, '')
    text = text.replace(/<date>/g, ' - Date: ')
    text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
    text = text.replace(/<\/date>/g, ']')
    text = text.replace(/<plTransit>/g, '[Place of Transit: ')
    text = text.replace(/<\/plTransit>/g, ']')
    text = text.replace(/<plTransitDate>/g, ' - Date: ')
    text = text.replace(/<\/plTransitDate>/g, '')
    text = text.replace(/<transc>/g, '')
    text = text.replace(/<\/transc>/g, '')
    text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
    text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
    text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
    #line required for each occurence except the first one
    text = text.replace(/<position.*?<\/position>/g, '')
    # text = text.replace(/(\r\n|\n|\r)/, "")

    text

docTranscription.directive 'docTranscriptionDir', ($state, modDeFactory, deFactory, alertify, $rootScope, $location, $anchorScroll) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-transcription/doc-transcription.html'
  scope:
    transcriptions: '='
    images: '='
  
  link: (scope, elem, attr) ->
    scope.editingTranscription = false
    scope.editApprove = false
    scope.editedTrans = {}

    deFactory.getDocumentEntityById($state.params.docId).then (resp)->
      scope.summary = resp.data.documentEntity
      scope.feCategory=scope.summary.category.replace(/([A-Z])/g, ' $1').replace(/^./, (str)-> return str.toUpperCase())

    scope.focus = (imageId) ->
      scope.$root.$broadcast('focus', imageId)

    scope.$on('uploadIdMatch', (evt, data) ->
      scope.matchedUploadedId = data)

    scope.$on('editApprove', (evt, data) ->
      scope.editApprove = data)
  
    scope.editTranscription = (transcription, e) ->
      document.getElementById('transcriptionAligner').style.display = 'flex'
      document.getElementById('transcriptionAlignerRow').style.display = 'flex'
      document.getElementsByClassName('document-details')[0].style.height = '500px'
      scope.focus(transcription.uploadedFileId)
      console.log(scope.editApprove)
      if scope.editApprove
        scope.editedTrans = angular.copy(transcription)
        scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'transcription', isEdit: true})
        for trans in scope.transcriptions
          trans.editingTranscription = false
        transcription.editingTranscription = true

        $location.hash('tr_' + transcription.uploadedFileId + '_' + (if transcription.docTranscriptionId is null then '' else transcription.docTranscriptionId))
        $anchorScroll()

        $rootScope.$broadcast('editingTranscription', true)
        scope.$root.$on('imageSelected', (evt, data) -> )

    scope.editTranscriptionBIA = (transcription, e) ->
      # document.getElementById('transcriptionAligner').style.display = 'flex'
      # document.getElementById('transcriptionAlignerRow').style.display = 'flex'
      # document.getElementsByClassName('document-details')[0].style.height = '500px'
      # scope.focus(transcription.uploadedFileId)
      
      # fix for BIA documents
      scope.editApprove = true
      # console.log(transcription.uploadedFileId)
    
      if scope.editApprove
        scope.editedTrans = angular.copy(transcription)
        scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'transcription', isEdit: true})
        for trans in scope.transcriptions
          trans.editingTranscription = false
        transcription.editingTranscription = true

        #$location.hash('tr_' + transcription.uploadedFileId + '_' + (if transcription.docTranscriptionId is null then '' else transcription.docTranscriptionId))
        #$anchorScroll()

        $rootScope.$broadcast('editingTranscription', true)
        scope.$root.$on('imageSelected', (evt, data) -> )
        

    scope.changeMV = (transcription) ->
      scope.$root.$broadcast('editCurrentTranscription', transcription)
      scope.focus(transcription.uploadedFileId)

    scope.saveTranscription = (transcription) ->
      tempTransc = {}
      tempTransc.docTranscriptionId = transcription.docTranscriptionId || null
      tempTransc.documentId = $state.params.docId
      tempTransc.transcription = transcription.transcription
      tempTransc.uploadedFileId = transcription.uploadedFileId
      collectionToSend = []
      collectionToSend.push(tempTransc)
      if tempTransc.docTranscriptionId
        modDeFactory.editTranscription(collectionToSend).then (resp) ->
          stopEditing(transcription)
      else
        modDeFactory.addTranscription(collectionToSend).then (resp) ->
          if resp.status == 'ok' && resp.data.length
            transcription.docTranscriptionId = resp.data[0].docTranscriptionId
            stopEditing(transcription)
          if resp.status == 'w' && resp.data.length
            alertify.confirm 'A transcription already exists for this folio. Do you want to overwrite it?', (->
              transcription.docTranscriptionId = resp.data[0].docTranscriptionId
              scope.saveTranscription(transcription))
    
    stopEditing = (transcription) ->
      transcription.editingTranscription = false
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'transcription', isEdit: false})
      $rootScope.$broadcast('editingTranscription', false)
      document.getElementById('transcriptionAligner').style.display = 'none'
      document.getElementById('transcriptionAlignerRow').style.display = 'none'
      document.getElementsByClassName('document-details')[0].style.height = 'auto'

    scope.closeTranscription = (transcription) ->
      alertify.confirm 'Are you sure you want to discard your changes?', (->
        transcription.transcription = scope.editedTrans.transcription
        scope.editedTrans = {}
        stopEditing(transcription)
        scope.$apply()), ->
      alertify.error 'Cancel'

      


  

