actionsHistory = angular.module 'actionsHistory', []
actionsHistory.directive 'actionsHistoryDir', (modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/actions-history/actions-history.html'
  scope:
    recordId: '='
    recordType: '='

  link: (scope, elem, attr) ->

    scope.showActionsHistory = () ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/mod-actions-history/actions-history.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {'recordId': scope.recordId, 'recordType': scope.recordType}
        }
      )

