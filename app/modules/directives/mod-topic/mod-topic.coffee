modTopic = angular.module 'modTopic', []
modTopic.directive 'modTopicDir', (deFactory, $state, $filter, modalFactory, commonsFactory, $window, $timeout, modDeFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/mod-topic/mod-topic.html'
  scope:
    topic: "="
    allTopics: "="
    reloadTopic:"&"
    onCancel:"&"
  link: (scope, elem, attr) ->
    scope.loading = false
    scope.modifyTopic = {}
    if scope.topic.topicListId
      oldTopic=angular.copy(scope.topic)


    scope.$watch('topic.editTopic', (newVal)->
      if newVal
        scope.$root.$broadcast('editing-topic')
      else
        scope.$root.$broadcast('editing-topic-end')
    )

    scope.topicFilter = (topic) ->
      return topic.topicId == scope.topic.topicListId

    scope.searchTableOpen = false
    scope.selectedTopic = {}

    # deFactory.getAllTopics().then (resp)->
    #   scope.allTopics = resp.data.topics

    scope.getPlaces = (searchQuery) ->
      scope.modifyTopic.placeId = null
      if searchQuery
        if searchQuery.length == 3
          scope.loading = true
          scope.searchTableOpen=true
          deFactory.getPlace(searchQuery).then (resp)->
            scope.possiblePlaces = resp.data.places if resp.data
            scope.loading = false
      else
        scope.searchTableOpen = false

    scope.selectPlace = (place) ->
      scope.modifyTopic.fePlaceName = place.placeName
      scope.modifyTopic.placeId = place.id
      scope.searchTableOpen = false


    scope.switchEdit = ()->
      scope.topic.editTopic=!scope.topic.editTopic
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'deTopicsEdit' , isEdit: scope.topic.editTopic})
      scope.modifyTopic = angular.copy(scope.topic)

    scope.cancel = ()->
      scope.topic.editTopic=!scope.topic.editTopic
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'deTopicsEdit' , isEdit: scope.topic.editTopic})
      scope.$root.$broadcast('editing-topic-end')
      scope.onCancel({topic:scope.topic})

    scope.saveTopic = ()->
      params={}
      if $state.params.documentEntityId
        params.documentId = $state.params.documentEntityId
      else
        params.documentId = $state.params.docId
      params.newTopicPlace={}
      params.newTopicPlace.placeId=scope.modifyTopic.placeId
      params.newTopicPlace.topicListId=scope.modifyTopic.topicListId
      if oldTopic
        params.oldTopicPlace=oldTopic
        modDeFactory.editDocumentTopic(params).then (resp)->
          if resp.status is "ok"
            scope.switchEdit()
            scope.reloadTopic()
      else
        modDeFactory.addDocumentTopic(params).then (resp)->
          if resp.status is "ok"
            scope.switchEdit()
            scope.reloadTopic()

    scope.deleteTopic = ()->
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = $filter('translate')("modal.DELETINGTOPICDOC")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
        if resp
          params={}
          if $state.params.documentEntityId
            params.documentId = $state.params.documentEntityId
          else
            params.documentId = $state.params.docId
            params.topicPlaceToBeDeleted={}
            params.topicPlaceToBeDeleted.placeId=scope.topic.placeId
            params.topicPlaceToBeDeleted.topicListId=scope.topic.topicListId
            modDeFactory.deleteDocumentTopic(params).then (resp)->
              if resp.status is 'ok'
                scope.reloadTopic()
    

    scope.addANewPlace = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.isAnotherTopicEdit = () ->
      isAnoterTopicEdit = (scope.$parent.deTopics.some (topic) -> topic.editTopic == true)
      return isAnoterTopicEdit
