modParent = angular.module 'modParent', [ 'addPersonApp' ]
modParent.directive 'modParentDir', (deFactory, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/mod-parent/mod-parent.html'
  scope:
    id: '='
    person: '='
    role: '='
    onSelect: '&'
    onCancel:'&'
  link: (scope, elem, attr) ->
    scope.selected = false
    scope.gender = scope.person.gender
    scope.loading = false
    scope.searchTableOpen = false
    scope.$watch('person.gender', (newVal,oldVal)->
      if oldVal != newVal
        scope.searchTableOpen = false
        scope.loading=false
        scope.tempName=null
        scope.gender = newVal
      )
    if scope.person.id
      isNewPerson = false
    else
      isNewPerson = true
    scope.selectedPeople = {}

    if scope.person.feData
      scope.tempName = scope.person.feData.mapNameLf
    else
      scope.tempName = scope.person.name


    temp = {}
    temp.id = undefined
    temp.unsure = undefined

    scope.getPeople = (searchQuery) ->
      scope.selected = false
      if searchQuery
        if searchQuery.length == 3
          scope.loading=true
          scope.searchTableOpen=true
          deFactory.getPeople(searchQuery).then (resp)->
            if resp.data
              scope.possiblePeople = resp.data.people
            scope.loading=false

    scope.selectPerson = (person) ->
      scope.selected = true
      scope.tempName = person.mapNameLf
      temp.id = person.id
      temp.unsure = "s"
      scope.searchTableOpen = false
      scope.selectedPeople = person
      scope.possiblePeople = []

    scope.savePerson = ()->
      toPass={}
      temp.gender = scope.gender
      if scope.role
        toPass.temp = temp
        toPass.role = scope.role
      else
        toPass=temp
      scope.onSelect({params:toPass})

    scope.addANewPerson = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-person/add-person.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.setUnsure = (unsure) ->
      scope.person.unsure = "u" if not unsure
      scope.person.unsure = "s" if unsure

    scope.cancel = ()->
      if scope.role
        scope.person.editPerson = false
        if isNewPerson
          scope.onCancel({person:scope.person}, {role:scope.role})
      else
      scope.onCancel()

    scope.filterGender=(item)->
      return item.gender == scope.gender && item.id != scope.id
