modalInstance = angular.module 'modalInstance',[
  'ui.bootstrap'
  'templates'
]


modalInstance.controller 'modalIstanceController', ($scope, $modalInstance, options) ->
  $scope.options = options || {}
  $scope.modalSelected = []

  $scope.select = (item) ->
    $modalInstance.close item

  $scope.addSelection = (item) ->
    $scope.modalSelected.push(item)

  $scope.removeSelection = (item) ->
    $scope.modalSelected.pop(item)

  $scope.ok = ->
    $modalInstance.close $scope.modalSelected || true

  $scope.cancel=  ->
    $modalInstance.dismiss false

modalUtils = angular.module 'modalUtils',[]

modalUtils.factory 'modalFactory', ($modal, $rootScope) ->
  factory =
    defaultOptions:
      size: "lg"
      templateUrl: 'modules/directives/modal/messageModal.html'
      controller: "modalIstanceController"
      scope: $rootScope
      backdrop: true
      resolve:{
        options:
          -> {}
      }

    # returns data (this is a promise too, but it'll be used soon)
    openModal: (options) ->
      $modal.open(angular.extend({},
        @defaultOptions,
        options)).result.then( (selectedItem)->
          return selectedItem
        )

    openMessageModal: (title, message, questionPanel, whichSize) ->
      resolveOptions =
        size : whichSize
        resolve:{
          options:
            -> {"title":title, "message":message, "questionPanel":questionPanel}
        }

      @openModal(angular.extend({},@defaultOptions, resolveOptions)).then( (selectedItem)->
        return true
      , ->
        return false
      )

  factory
