docChineseSynopsis = angular.module 'docChineseSynopsis', []

docChineseSynopsis.filter 'replaceTagsSyn', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<syn>/g, '')
    text = text.replace(/<\/syn>/g, '')
    text = text.replace(/<wordCount>/g, '[Avviso word count: ')
    text = text.replace(/<\/wordCount>/g, ']')
    text = text.replace(/<writtenPagesNo>/g, '[Written pages number: ')
    text = text.replace(/<\/writtenPagesNo>/g, ']')
    text

docChineseSynopsis.directive 'docChineseSynopsisDir', ($state, modDeFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-synopsis/doc-chinese-synopsis.html'
  scope:
    synopsis: '='
  
  link: (scope, elem, attr) ->
    scope.editSyn = false

    scope.saveSynopsis = (syn)->
      synops = {}
      synops.documentId = $state.params.docId
      synops.chineseSynopsis = syn
      
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'synopsis', isEdit: false})
      
      modDeFactory.saveChineseSynopsis(synops).then (resp)->
        if resp.status is "ok"
          scope.editSyn = false
        if resp.status is "w"
          scope.editSyn = false

    scope.switchSynopsis = ()->
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'synopsis', isEdit: true})
      scope.editSyn = !scope.editSyn