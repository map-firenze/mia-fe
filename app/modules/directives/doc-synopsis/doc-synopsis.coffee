docSynopsis = angular.module 'docSynopsis', []

docSynopsis.filter 'replaceTagsSyn', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<syn>/g, '')
    text = text.replace(/<\/syn>/g, '')
    text = text.replace(/<wordCount>/g, '[Avviso word count: ')
    text = text.replace(/<\/wordCount>/g, ']')
    text = text.replace(/<writtenPagesNo>/g, '[Written pages number: ')
    text = text.replace(/<\/writtenPagesNo>/g, ']')
    text

docSynopsis.directive 'docSynopsisDir', ($state, modDeFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-synopsis/doc-synopsis.html'
  scope:
    synopsis: '='
  
  link: (scope, elem, attr) ->
    scope.editSyn = false

    scope.saveSynopsis = (syn)->
      synops={}
      synops.documentId = $state.params.docId
      synops.generalNotesSynopsis = syn
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'synopsis', isEdit: false})
      modDeFactory.saveSynopsis(synops).then (resp)->
        if resp.status is "ok"
          scope.editSyn = false
        if resp.status is "w"
          scope.editSyn = false #workaround for permision to save syn in documents owned by someone else (bug in back-end)

    scope.switchSynopsis = ()->
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'synopsis', isEdit: true})
      scope.editSyn = !scope.editSyn