deRelatedPeople = angular.module 'deRelatedPeople', []
deRelatedPeople.directive 'deRelatedPeopleDir', (modDeFactory, deFactory, modalFactory, $state, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/de-related-people/de-related-people.html'
  link: (scope, elem, attr) ->
    getRelatedPeople = () ->
      modDeFactory.getRelatedPeople($state.params.docId).then (resp)->
        if resp.status is 'ok'
          scope.relatedPeople = resp.data.people
#          console.log(resp.data)
          angular.forEach(scope.relatedPeople, (person)->
            deFactory.getPeopleById(person.id).then (resp)->
              person.feData = resp.data
            )
        else
          scope.relatedPeople = []


    scope.addNewRelPerson = () ->
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'dePersonEdit_Related', isEdit: true})
      scope.relatedPeople.push({"editPerson":true})

    scope.deleteRelPerson = (person) ->
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = $filter('translate')("modal.DELETINGPERSONDOC")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
        if resp
          params = {
            "documentId":$state.params.docId,
            "deletePersonCitedInDoc":person.id
          }
          modDeFactory.deleteRelatedPeople(params).then (resp)->
#            console.log(resp)
            if resp.status is 'ok'
              scope.relatedPeople.splice(scope.relatedPeople.indexOf(person),1)

    scope.saveRelPerson = (params, person) ->
      alreadyPresent = false
      if scope.$parent.$parent.$parent.people
        for role in scope.$parent.$parent.$parent.people
          if role.peoples
            for singlePerson in role.peoples
              if singlePerson.id and (singlePerson.id is params.temp.id)
                alreadyPresent = true
      if alreadyPresent
        pageTitle = $filter('translate')("general.ERROR")
        messagePage = $filter('translate')("people.ALREADYIN")
        modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->
      else
        if not person.feData #Is a new person
          newPerson = {
            "documentId":$state.params.docId,
            "newPersonCitedInDoc":{
              "id":params.temp.id,
              "unsure":params.temp.unsure
              }
            }
          modDeFactory.addRelatedPeople(newPerson).then (resp)->
            deFactory.getPeopleById(newPerson.newPersonCitedInDoc.id).then (resp)->
              person.feData = resp.data
              person.editPerson = false
              getRelatedPeople()
        else #Is an edited person
          paramsToPass = {
            "documentId":$state.params.docId,
            "oldPersonCitedInDoc": {
              "id":person.id
            },
            "newPersonCitedInDoc":{
              "id":params.temp.id,
              "unsure":params.temp.unsure
            }
          }
          modDeFactory.editRelatedPeople(paramsToPass).then (resp)->
            deFactory.getPeopleById(paramsToPass.newPersonCitedInDoc.id).then (resp)->
              person.feData = resp.data
              person.editPerson = false
              getRelatedPeople()

    scope.cancelNewRelPeople = (person, position) ->
      if person.id
        person.editPerson = false
      else
        scope.relatedPeople.splice(position, 1)

    getRelatedPeople()