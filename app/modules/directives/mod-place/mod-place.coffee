modPlace = angular.module 'modPlace', [ 'addPlaceApp' ]
modPlace.directive 'modPlaceDir', (deFactory, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/mod-place/mod-place.html'
  scope:
    place: '='
    role: '='
    onSelect: '&'
    onCancel: '&'
    onChange: '&'
  link: (scope, elem, attr) ->
    scope.searchTableOpen = false
    scope.selected = false

    if scope.place and scope.place.id
      isNewPlace = false
    else
      isNewPlace = true
      scope.place = {}
    scope.selectedPlaces = {}
    scope.loading = false

    if scope.place and scope.place.feData
      scope.tempName = scope.place.feData.placeName

    scope.unsure = scope.place.unsure is 'u'

    temp = {}
    temp.id = undefined
    temp.unsure = undefined

    scope.closeTable = ()->
      scope.searchTableOpen = false

    scope.getPlaces = (searchQuery) ->
      scope.onChange({placeName:searchQuery})
      if searchQuery
        if searchQuery.length == 3
          scope.loading=true
          scope.searchTableOpen=true
          deFactory.getPlace(searchQuery).then (resp)->
            if resp.data
              scope.possiblePlaces = resp.data.places
            scope.loading=false

    scope.selectPlace = (place) ->
      if not scope.loading
        scope.selected = true
        scope.tempName = place.placeName
        temp.id = place.id
        temp.unsure = place.unsure
        scope.searchTableOpen = false
        scope.selectedPlaces = place
        scope.possiblePlaces = []
        if not scope.role
          scope.savePlace()

    scope.savePlace = ()->
      toPass={}
      toPass.role = scope.role
      if scope.selected
        toPass.temp = temp
      else
        toPass.temp = {}
        toPass.temp.id = scope.place.id
        toPass.temp.unsure = scope.place.unsure
      scope.onSelect({params:toPass})

    scope.addANewPlace = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.setUnsure = (unsure) ->
      scope.place.unsure = "u" if not unsure
      scope.place.unsure = "s" if unsure

    scope.cancel = ()->
      scope.place.editPlace = false
      if isNewPlace
        scope.onCancel({place:scope.place}, {role:scope.role})
