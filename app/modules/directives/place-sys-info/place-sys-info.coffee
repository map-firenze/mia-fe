placeSysInfo = angular.module 'placeSysInfo', []
placeSysInfo.directive 'placeSysInfoDir', ($filter, modalFactory, deFactory, $state, $stateParams) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/place-sys-info/place-sys-info.html'
  scope:
    info: '='

  link: (scope, elem, attr) ->

    scope.goToPublicProfile = (user) ->
      $state.go('mia.publicProfile', { 'account': user  })
