newsApp = angular.module 'newsApp', [
  'newsFactoryApp',
  'ui.grid',
  'peopleNewsApp',
  'aeNewsApp',
  'docNewsApp',
  'placeNewsApp',
  'recentNewsApp',
  'lastRecordsNewsApp',
  'newsfeedApp',
  'basicControlsApp',
  'volumeInsertDescrApp',
  'ui.bootstrap'
  ]

docTranscription.filter 'replaceTagsTransMyDocs', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<newsHeader>/g, '[')
    text = text.replace(/<\/newsHeader>/g, '')
    text = text.replace(/<newsFrom>/g, '[')
    text = text.replace(/<\/newsFrom>/g, '')
    text = text.replace(/<from>/g, 'News From: ')
    text = text.replace(/<\/from>/g, '')
    if text.indexOf('fromUnsure') >= 0
      text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/fromUnsure>/g, '')
    text = text.replace(/<hub>/g, 'News Hub: ')
    text = text.replace(/<\/hub>/g, '')
    if text.indexOf('hubUnsure') >= 0
      text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
      text = text.replace(/<\/hubUnsure>/g, '')
    text = text.replace(/<date>/g, ' - Date: ')
    text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
    text = text.replace(/<\/date>/g, ']')
    text = text.replace(/<plTransit>/g, '[Place of Transit: ')
    text = text.replace(/<\/plTransit>/g, ']')
    text = text.replace(/<plTransitDate>/g, '- Date: ')
    text = text.replace(/<\/plTransitDate>/g, '')
    text = text.replace(/<transc>/g, '')
    text = text.replace(/<\/transc>/g, '')
    text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
    text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
    text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
    #line required for each occurence except the first one
    text = text.replace(/<position.*?<\/position>/g, '')
    # text = text.replace(/(\r\n|\n|\r)/, "")
    text

docTranscription.filter 'replaceTagsSynMyDocs', ->
  (text) ->
    if !text
      return text
    text = text.replace(/<syn>/g, '')
    text = text.replace(/<\/syn>/g, '')
    text = text.replace(/<wordCount>/g, '[Avviso word count: ')
    text = text.replace(/<\/wordCount>/g, ']')
    text = text.replace(/<writtenPagesNo>/g, '[Written pages number: ')
    text = text.replace(/<\/writtenPagesNo>/g, ']')
    #console.log(text)
    text

newsApp.directive 'newsDir', (newsFactory, $http, $q, volumeInsertDescrFactory, $rootScope, $timeout, $state, $location, $anchorScroll, userServiceFactory, newsFeedService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/news.html'
  link: (scope, elem, attr) ->
#    setTimeout ( ->
#      scope.tabs = [
#        { "name": "newsfeed",     "value": false },
#        { "name": "highlights",   "value": false },
#        { "name": "recent",       "value": true },
#        { "name": "discoveries",  "value": false },
#        { "name": "last_records", "value": false },
#        { "name": "help",         "value": false }
#      ]
#
#      scope.activeTab = (tabName) ->
#        angular.forEach(scope.tabs, (singleTab) ->
#          if singleTab.name is tabName
#            singleTab.value = true
#          else
#            singleTab.value = false
#        )
#    ), 1000
    scope.tabs = [
      { "name": "newsfeed",     "value": false },
      { "name": "highlights",   "value": false },
      { "name": "recent",       "value": true },
      { "name": "discoveries",  "value": false },
      { "name": "last_records", "value": false },
      { "name": "help",         "value": false }
    ]

    scope.activeTab = (tabName) ->
      angular.forEach(scope.tabs, (singleTab) ->
        if singleTab.name is tabName
          singleTab.value = true
        else
          singleTab.value = false
      )

    scope.goToNewsFeed = () ->
      scope.activeTab('newsfeed')
      $location.hash('newsfeed')
      $anchorScroll()

    scope.determineSectionFocus = () ->
      if newsFeedService.hasFocus()
        scope.goToNewsFeed()
        newsFeedService.clearFocusStatus()
      
      # If exist a filter put focus to newsfeed anyway
      else newsFeedService.getMyNewsfeed().then (response) ->
        if response.data.status == 'ok' and response.data.data.length
          scope.goToNewsFeed()

    scope.updatesNum = ["10", "20", "30", "40", "50"]
    scope.upd = scope.updatesNum[1]

    scope.lastRecords = {}
    scope.arrayLR     = []
    scope.loadingNW   = true
    scope.loadingLR   = false
    scope.previewObj  = null

    userServiceFactory.getMe().then (response) ->
      scope.determineSectionFocus()

    scope.$on 'preview-obj', (evt, arg) ->
      scope.previewObj = arg
      $rootScope.$broadcast('preview-obj-open') if arg

      scope.getUserLastRecords() if tabName == 'last_records' && _.isEmpty(scope.lastRecords)
      scope.createArrayLR()      if tabName == 'last_records' && !_.isEmpty(scope.lastRecords) && !scope.arrayLR.length
      scope.initDiscoveries() if tabName == 'discoveries'

    scope.closePreviewObj = () ->
      scope.previewObj = null
      $rootScope.$broadcast('preview-obj', scope.previewObj)

    scope.goToPreviewObj = () ->
      $state.go 'mia.singleupload', {'aeId':     scope.previewObj.data.uploadId}   if scope.previewObj.type == 'ae'       && !scope.previewObj.tab
      $state.go 'mia.singleupload', {'aeId':     scope.previewObj.id}              if scope.previewObj.type == 'ae'       && scope.previewObj.tab == 'recent'
      $state.go 'mia.modifyDocEnt', {'docId':    scope.previewObj.data.documentId} if scope.previewObj.type == 'document' && !scope.previewObj.tab
      $state.go 'mia.modifyDocEnt', {'docId':    scope.previewObj.id}              if scope.previewObj.type == 'document' && scope.previewObj.tab == 'recent'
      $state.go 'mia.bio-people',   {'id':       scope.previewObj.data.peopleId}   if scope.previewObj.type == 'people'   && !scope.previewObj.tab
      $state.go 'mia.bio-people',   {'id':       scope.previewObj.id}              if scope.previewObj.type == 'people'   && scope.previewObj.tab == 'recent'
      $state.go 'mia.bio-place',    {'id':       scope.previewObj.data.placeId}    if scope.previewObj.type == 'place'    && !scope.previewObj.tab
      $state.go 'mia.bio-place',    {'id':       scope.previewObj.id}              if scope.previewObj.type == 'place'    && scope.previewObj.tab == 'recent'
      $state.go 'mia.volumeDescr',  {'volumeId': scope.previewObj.data.volumeId}   if scope.previewObj.type == 'volume'
      $state.go 'mia.insertDescr',  {'insertId': scope.previewObj.data.insertId}   if scope.previewObj.type == 'insert'

    scope.changeUpdNum = (upd) ->
      newsFactory.updateNumberOfNews(upd)
      newsFactory.findRecentNews().then (resp) ->
        scope.recentNews = resp.data

    scope.getUserLastRecords = () ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        account = me.data[0].account
        newsFactory.getLastRecords(account).then (resp) ->
          scope.lastRecords = resp.data
          scope.createArrayLR()

    scope.createArrayLR = () ->
      scope.loadingLR = true
      $q.all([
        scope.getLastDocument(),
        scope.getLastPerson(),
        scope.getLastPlace(),
        scope.getLastUpload(),
        scope.getLastVolume(),
        scope.getLastInsert()
      ]).then (results) ->
        scope.loadingLR = false
        scope.arrayLR   = results

    scope.getLastDocument = () ->
      data = {type: 'document', data: null}
      return data unless scope.lastRecords.lastDocument
      $http.get('json/document/findDocument/' + scope.lastRecords.lastDocument).then (resp) ->
        if resp.data.status == 'ok'
          data.data = resp.data.data.documentEntity
        return data

    scope.getLastPerson = () ->
      data = {type: 'people', data: null}
      return data unless scope.lastRecords.lastPerson
      $http.get('json/biographical/findBiographicalPeople/' + scope.lastRecords.lastPerson).then (resp) ->
        if resp.data.status == 'ok'
          data.data = resp.data.data
        return data

    scope.getLastPlace = () ->
      data = {type: 'place', data: null}
      return data unless scope.lastRecords.lastPlace
      $http.get('json/geographical/findGeographicalPlace/' + scope.lastRecords.lastPlace).then (resp) ->
        if resp.data.status == 'ok'
          data.data = resp.data.data
        return data

    scope.getLastUpload = () ->
      data = {type: 'ae', data: null}
      return data unless scope.lastRecords.lastUpload
      $http.get('json/archive/getArchives/aentity/' + scope.lastRecords.lastUpload + '/7/1').then (resp) ->
        if resp.data.aentity
          data.data = resp.data.aentity
        return data

    scope.getLastVolume = () ->
      data = {type: 'volume', data: null}
      return data unless scope.lastRecords.lastVolumeOrInsert && scope.lastRecords.lastVolumeOrInsert.type == 'Volume'
      volumeInsertDescrFactory.findVolumeDescription(scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
        if resp.status == 'ok'
          data.data = resp.data.data
          volumeInsertDescrFactory.findVolumeSpine(scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
            data.data.spine = resp.data
            return data

    scope.getLastInsert = () ->
      data = {type: 'insert', data: null}
      return data unless scope.lastRecords.lastVolumeOrInsert && scope.lastRecords.lastVolumeOrInsert.type == 'Insert'
      volumeInsertDescrFactory.findInsertDescription(scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
        if resp.status == 'ok'
          data.data = resp.data.data
          volumeInsertDescrFactory.findInsertGuardia(scope.lastRecords.lastVolumeOrInsert.id).then (resp) ->
            data.data.spine = resp.data
            return data

#    newsFactory.findDailyNews().then (resp)->
#      scope.dailyStatus = resp.status
#      scope.dailyNews = resp.data
#    newsFactory.findWeeklyNews().then (resp)->
#      scope.weeklyStatus = resp.status
#      scope.weeklyNews = resp.data
#    newsFactory.findMonthlyNews().then (resp)->
#      scope.monthlyStatus = resp.status
#      scope.monthlyNews = resp.data


    newsFactory.findRecentNews().then (resp)->
      scope.recentStatus = resp.status
      scope.recentNews = resp.data
      scope.loadingNW = false

    scope.getDiscoveries = (offset) ->
      $http.get('json/discovery/findDiscoveriesByStatus?status=PUBLISHED&limit='+scope.limit+'&offset='+(offset-1)*scope.limit).then (resp) ->
        scope.loadingData = false
        if resp.data.status == 'ok'
          scope.errorMsg = undefined
          scope.discoveries = resp.data.data.discoveries
          scope.discoveriesCount = resp.data.data.count
          console.log(scope.discoveries)
          console.log(scope.discoveriesCount)
        else
          scope.errorMsg = resp.data.message

    scope.discoveriesPageChanged = (offset) ->
      scope.offset = offset
      scope.getDiscoveries(offset)

    scope.initDiscoveries = () ->
      scope.offset = 1
      scope.limit = 5
      scope.getDiscoveries(1)

    scope.showSpotlightReview = (discovery) ->
      if discovery.status == 'Published'
        window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/discovery/'+discovery.id

    $rootScope.$on('addNewsfeedItem', (evt, newsfeedItem) ->
      scope.goToNewsFeed()
    )