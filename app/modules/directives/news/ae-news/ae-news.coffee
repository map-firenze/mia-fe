aeNewsApp = angular.module 'aeNewsApp', []
aeNewsApp.directive 'aeNewsDir', ($rootScope) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/ae-news/ae-news.html'
  scope:
    aes: '='
    status: '='
  link: (scope, elem, attr) ->
    scope.isAdmin = $rootScope.isAdmin
    scope.authorize = (roles) ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        account = me.data[0].account
        scope.status = false
        angular.forEach(roles, (role) ->
          if me.data[0].userGroups.indexOf(role) > -1
            return scope.status = true
        )
        return scope.status