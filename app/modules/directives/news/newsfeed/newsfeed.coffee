newsfeed = angular.module 'newsfeedApp', ['newsfeedAppSerivce']

newsfeed.directive 'newsfeedDir', ($rootScope, newsFeedService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/newsfeed/newsfeed.html'

newsfeed.controller 'newsfeedController',($rootScope, $scope, $state, $location, $anchorScroll, userServiceFactory, newsFeedService, searchDeService, deFactory) ->
  $scope.placesNames = new Map()
  $scope.personsNames = new Map()

  $scope.init = () ->
    userServiceFactory.getMe().then (response) ->
      $scope.getMyNewsFeed()

  # Get Newsfeed
  $scope.getMyNewsFeed = () ->
    newsFeedService.getMyNewsfeed().then (response) ->
      if response.data.status == 'ok'
        $scope.newsFeed = response.data.data

        angular.forEach($scope.newsFeed, (newsfeedItem) ->
          getNames(newsfeedItem.searchFilter)
        )
      else
        console.error(response.message)

  # Delete NewsfeedItem
  $scope.deleteMyNewsfeedItem = (index) ->
    id = $scope.newsFeed[index].id
    newsFeedService.deleteMyNewsfeedItem(id).then (response) ->
      if response.data.status == 'ok'
        $scope.newsFeed.splice(index, 1)
      else
        console.error(response.message)
   
  # Add newsfeedItem
  $rootScope.$on('addNewsfeedItem', (evt, newsfeedItem) ->
    if newsfeedItem?
      $scope.newsFeed.push(newsfeedItem)
      getNames(newsfeedItem.searchFilter)

    newsFeedService.setFocus(true)
  )

  $scope.getResults = (index) ->
    newsfeedItem = $scope.newsFeed[index]
    searchDeService.getResults(newsfeedItem.searchFilter, 0, 20, 'docYear', 'asc', true).then (response) ->
      newResult = {
        'searchName' : newsfeedItem.title,
        'type' : 'newsfeed',
        'results' : response.data,
        'searchArray' : newsfeedItem.searchFilter,
        'content' : {
          'searchType' : newsfeedItem.title,
          'countResult' : newsfeedItem.matchingDocuments
          'where' : '',
          'title' : 'Newsfeed Search',
          'type' : 'newsfeed'
        },
        'searchForm' : {}
      }

      $rootScope.$broadcast('new-result', newResult)

  $scope.customizeFilters = () ->
    $state.go 'mia.newsfeedSearch'

  $scope.getPlaceName = (placeId) ->
    placeName = if $scope.placesNames.has(placeId) then \
    $scope.placesNames.get(placeId) else ''
    
    return placeName

  $scope.getPersonName = (personId) ->
    personName = if $scope.personsNames.has(personId) then \
    $scope.personsNames.get(personId) else ''
    
    return personName

  $scope.getTopicsNames = (topics) ->
    result = ""
    angular.forEach(topics, (topic) ->
      result += "\n" + topic.topicTitle
      if topic.placeAllId? and topic.placeAllId != ""
        result += " - " + $scope.getPlaceName(topic.placeAllId) + "\n"
    )

    return result

  getNames = (searchArray) ->
    if searchArray?.length > 5 && searchArray[5].people?.length > 0
      angular.forEach(searchArray[5].people, (people) ->
        if $scope.personsNames.has(people) == false
          return deFactory.getPeopleById(+people).then (resp) ->
            $scope.personsNames.set(people, resp.data.mapNameLf)
      )

    if searchArray?.length > 4 && searchArray[4].places?.length > 0 && searchArray[4].isActiveFilter
      angular.forEach(searchArray[4].places, (place) ->
        if $scope.placesNames.has(place) == false
          return deFactory.getPlaceById(+place).then (resp) ->
            $scope.placesNames.set(place, resp.data.placeName)
      )

    if searchArray?.length > 6 && searchArray[6].isActiveFilter
      angular.forEach(searchArray[6].topics, (topic) ->
        if $scope.placesNames.has(topic.placeAllId) == false
          return deFactory.getPlaceById(+topic.placeAllId).then (resp) ->
            $scope.placesNames.set(topic.placeAllId, resp.data.placeName)
      )

  $scope.init()