newsfeedAppService = angular.module 'newsfeedAppSerivce', []

newsfeedAppService.service 'newsFeedService', ($rootScope, $http, ENV) ->
  newsBasePath = ENV.newsBasePath
  sectionFocus = false

  getMyNewsfeed = () ->
    url = newsBasePath + 'getMyNewsfeed/' + $rootScope.me.account

    return $http.get(url).then( (response) ->
      return response
    , (error) ->
      return error
    )

  createMyNewsfeedItem = (searchArray) ->
    url = newsBasePath + 'createMyNewsfeedItem/' + $rootScope.me.account

    return $http.post(url, searchArray).then( (response) ->
      return response
    , (error) ->
      return error
    )

  deleteMyNewsfeedItem = (newsfeedItemId) ->
    url = newsBasePath + 'deleteMyNewsfeedItem/' + \
    $rootScope.me.account + '/' + newsfeedItemId

    return $http.delete(url).then( (response) ->
      return response
    , (error) ->
      return error
    )

  hasFocus = () ->
    return sectionFocus

  setFocus = (focus) ->
    sectionFocus =  focus

  clearFocusStatus = (focus) ->
    setFocus(false)

  service = {
    getMyNewsfeed: getMyNewsfeed,
    createMyNewsfeedItem: createMyNewsfeedItem
    deleteMyNewsfeedItem: deleteMyNewsfeedItem
    hasFocus: hasFocus
    setFocus: setFocus
    clearFocusStatus: clearFocusStatus
  }

  return service