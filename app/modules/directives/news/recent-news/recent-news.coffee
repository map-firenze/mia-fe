docNewsApp = angular.module 'recentNewsApp', []
docNewsApp.directive 'recNewsDir', ($rootScope, $timeout, bioPeopleFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/recent-news/recent-news.html'
  scope:
    rec: '='
    status: '='
  link: (scope, elem, attr) ->

    scope.user = {}

    scope.$on 'preview-obj', (evt, arg) ->
      scope.previewObj = arg

    scope.authorize = (roles) ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        scope.user = me.data[0]
        scope.status = false
        angular.forEach(roles, (role) ->
          if me.data[0].userGroups.indexOf(role) > -1
            return scope.status = true
        )
        return scope.status

    scope.showObjInPreview = (obj) ->
      modObj     = obj
      modObj.tab = 'recent'
      if modObj.type == 'people'
        #bioPeopleFactory.countDoc(modObj.id).then (resp)->
        #  modObj.peopleCountDoc = resp.data.countDocuments || 0
        bioPeopleFactory.getPersonTitles(modObj.id).then (resp)->
          modObj.peoplePreferredRole = resp.data.preferredRole || 'Not specified'
      if modObj.type == 'document' and typeof text != 'undefined' and text != null
        text = modObj.transcription[0].transcription
        text = text.replace(/<newsHeader>/g, '[')
        text = text.replace(/<\/newsHeader>/g, '')
        text = text.replace(/<newsFrom>/g, '[')
        text = text.replace(/<\/newsFrom>/g, '')
        text = text.replace(/<from>/g, 'News From: ')
        text = text.replace(/<\/from>/g, '')
        if text.indexOf('fromUnsure') >= 0
          text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
          text = text.replace(/<\/fromUnsure>/g, '')
        text = text.replace(/<hub>/g, 'News Hub: ')
        text = text.replace(/<\/hub>/g, '')
        if text.indexOf('hubUnsure') >= 0
          text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
          text = text.replace(/<\/hubUnsure>/g, '')
        text = text.replace(/<date>/g, ' - Date: ')
        text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
        text = text.replace(/<\/date>/g, ']')
        text = text.replace(/<transc>/g, '')
        text = text.replace(/<\/transc>/g, '')
        text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
        text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
        text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
        #line required for each occurence except the first one
        text = text.replace(/<position.*?<\/position>/g, '')
        modObj.transcription[0].transcription = text
      $rootScope.$broadcast('preview-obj', modObj)

    scope.bgColor = (recNew) ->
      return unless scope.previewObj
      return {'background-color':'#f2f6c3'} if recNew == scope.previewObj


    scope.recentNewsFilter = (object) ->
      if object.type == 'document'
        return scope.isObjectOwnerOrAdmin(object)
      return true


    scope.isObjectOwnerOrAdmin = (object) ->
      return scope.user.account == object.owner || scope.user.userGroups?.includes('ADMINISTRATORS') || object.privacy == 0





#    scope.canSeeDocument = (recNew) ->
#      console.log('recNew', recNew)
#      return true

#      if recNew.action == 'DELETED'
#        return 'background-color: #f7bfbf;'
#      else if recNew.type == 'ae'
#        return 'background-color: #96ead7bf;'
#      else if recNew.type == 'document'
#        return 'background-color: #f7e7b4;'
#      else if recNew.type == 'people'
#        return 'background-color: #f2f6c3;'
#      else if recNew.type == 'place'
#        return 'background-color: #e0ffff;'
