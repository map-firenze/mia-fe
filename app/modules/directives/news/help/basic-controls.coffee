docNewsApp = angular.module 'basicControlsApp', []
docNewsApp.directive 'basicControlsDir', ($rootScope, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/help/basic-controls.html'

