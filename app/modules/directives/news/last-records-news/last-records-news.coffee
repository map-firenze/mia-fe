lastRecordsApp = angular.module 'lastRecordsNewsApp', []
lastRecordsApp.directive 'lastRecordsNewsDir', ($rootScope, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/news/last-records-news/last-records-news.html'
  scope:
    lrs: '='
  link: (scope, elem, attr) ->

    scope.getAccount = () ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        scope.user = me.data[0]

    scope.getAccount()

    scope.authorize = (roles) ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        scope.user = me.data[0]
#        account = me.data[0].account
        scope.status = false
        angular.forEach(roles, (role) ->
          if me.data[0].userGroups.indexOf(role) > -1
            return scope.status = true
        )
        return scope.status

    scope.showObjInPreview = (obj) ->
      $rootScope.$broadcast('preview-obj', obj)
      scope.previewObj = obj

    scope.$on 'preview-obj', (evt, arg) ->
      scope.previewObj = arg

    scope.bgColor = (lastRecord) ->
      return unless scope.previewObj
      return {'background-color':'#f2f6c3'} if lastRecord == scope.previewObj.data

    scope.recentNewsFilter = (object) ->
      if object.type == 'document'
        return scope.isObjectOwnerOrAdmin(object)
      return true


    scope.isObjectOwnerOrAdmin = (object) ->
      if !object.data
        return false
      privacy = object.privacy || object.data.privacy
      owner = object.owner || object.data.owner
      result = (privacy == 0 || scope.user.account == owner || scope.user.userGroups?.includes('ADMINISTRATORS'))
      return result
