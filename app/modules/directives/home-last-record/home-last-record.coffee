homeLastRecordApp = angular.module 'homeLastRecordApp', []
homeLastRecordApp.directive 'homeLastRecordDir', ($rootScope, $timeout, newsFactory, bioPeopleFactory, $state) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/home-last-record/home-last-record.html'
  link: (scope, elem, attr) ->

    scope.goToRecord = () ->
      $state.go 'mia.singleupload', {'aeId':     scope.lastRecord.data.uploadId}   if scope.lastRecord.type == 'ae'
      $state.go 'mia.modifyDocEnt', {'docId':    scope.lastRecord.data.documentId} if scope.lastRecord.type == 'document'
      $state.go 'mia.bio-people',   {'id':       scope.lastRecord.data.peopleId}   if scope.lastRecord.type == 'people'
      $state.go 'mia.bio-place',    {'id':       scope.lastRecord.data.placeId}    if scope.lastRecord.type == 'place'
      $state.go 'mia.volumeDescr',  {'volumeId': scope.lastRecord.data.volumeId}   if scope.lastRecord.type == 'volume'
      $state.go 'mia.insertDescr',  {'insertId': scope.lastRecord.data.insertId}   if scope.lastRecord.type == 'insert'

    scope.prepareLastRecord = () ->
      if scope.lastRecord.type == 'people'
        #bioPeopleFactory.countDoc(scope.lastRecord.data.peopleId).then (resp)->
        #  scope.peopleCountDoc = resp.data.countDocuments || 0
        bioPeopleFactory.getPersonTitles(scope.lastRecord.data.peopleId).then (resp)->
          scope.peoplePreferredRole = resp.data.preferredRole || 'Not specified'

    scope.prepareLastRecord()