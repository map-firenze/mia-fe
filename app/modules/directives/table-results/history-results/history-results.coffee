historyResults = angular.module 'historyResults', []
historyResults.directive 'historyResultsDir', ($rootScope, $state, searchFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/table-results/history-results/history-results.html'
  link: (scope, elem, attr) ->
    scope.orderByColumn = 'activityDate'
    scope.orderType = 'desc'
    scope.startOrderType = 'asc'
    scope.pagination = {
      currentPage: 1,
      totalItems: 1,
      maxSize: 3,
      perPage: 20
    }

    scope.types = [
      { "name": "Any",        "value": undefined  },
      { "name": "Upload",     "value": "AE"       },
      { "name": "Document",   "value": "DE"       },
      { "name": "Person",     "value": "BIO"      },
      { "name": "Place",      "value": "GEO"      },
      { "name": "Volume",     "value": "VOL"      },
      { "name": "Insert",     "value": "INS"      }
    ]

    scope.searchHistoryLoaded = false
    scope.updateData = false
    scope.filterType = scope.types[0]

    scope.goToRecord = (row) ->
      $state.go 'mia.singleupload', {'aeId':     row.id} if row.type == 'Upload'
      $state.go 'mia.modifyDocEnt', {'docId':    row.id} if row.type == 'Document'
      $state.go 'mia.bio-people',   {'id':       row.id} if row.type == 'Person'
      $state.go 'mia.bio-place',    {'id':       row.id} if row.type == 'Place'
      $state.go 'mia.volumeDescr',  {'volumeId': row.id} if row.type == 'Volume'
      $state.go 'mia.insertDescr',  {'insertId': row.id} if row.type == 'Insert'

    scope.updateSearchData = (columnName = scope.orderByColumn, start = 0, maxResult = 20, orderType = scope.orderType, type = scope.filterType) ->
      me = JSON.parse(localStorage.getItem('miaMe'))
      if me and me.status is 'ok'
        account = me.data[0].account
        scope.updateData = true
        searchFactory.getMyResearchHistory(account, start, maxResult, columnName, orderType, type.value).then (resp) ->
          $rootScope.$broadcast('end-history-search')
          scope.searchHistoryLoaded = true
          scope.updateData = false
          if resp.data
            scope.results = resp.data.historyItems
            scope.pagination.totalItems = resp.data.count

    scope.pageChange = () ->
      start = scope.pagination.perPage * scope.pagination.currentPage - scope.pagination.perPage
      maxResult = scope.pagination.perPage
      scope.updateSearchData(scope.orderByColumn, start, maxResult)

    scope.$on("start-history-search", () ->
      scope.searchHistoryLoaded = false
      scope.results = []
      scope.pageChange()
    )

    # scope.$on("delete-history-search", () ->
    #   me = JSON.parse(localStorage.getItem('miaMe'))
    #   if me and me.status is 'ok'
    #     account = me.data[0].account
    #     scope.updateData = true
    #     searchFactory.deleteMyResearchHistory(account).then (resp) ->
    #       scope.searchHistoryLoaded = false
    #       scope.results = []
    #       scope.pagination.currentPage = 1
    #       scope.updateSearchData(scope.orderByColumn)
    # )
    
    scope.changeType = (type) ->
      scope.filterType = type
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)

    scope.requestOrderedData = (columnName) ->
      if scope.orderByColumn == columnName
        if scope.orderType == 'desc'
          scope.orderType = 'asc'
        else
          scope.orderType = 'desc'
      if scope.orderByColumn != columnName
        scope.orderType = scope.startOrderType
      scope.orderByColumn = columnName
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)

    scope.showChevronIsOrdered = (columnName)  ->
      if Array.from(arguments).includes(scope.orderByColumn) && !scope.updateData
        if scope.orderType == 'desc'
          return ['glyphicon-chevron-down', 'chevron-show']
        else
          return ['glyphicon-chevron-up', 'chevron-show']
      return 'glyphicon-chevron-down'
