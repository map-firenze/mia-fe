peopleResults = angular.module 'peopleResults', []
peopleResults.directive 'peopleResultsDir', ($state, searchFactory, searchPeService) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/table-results/people-results/people-results.html'
  scope:
    results: "="
  link: (scope, elem, attr) ->
    scope.orderByColumn = 'mapNameLf'
    scope.orderType = 'asc'
    scope.startOrderType = 'asc'
    scope.pagination = {
      currentPage: 1,
      totalItems: 1,
      maxSize: 3,
      perPage: 20
    }

    scope.updateData = false
    scope.pagination.totalItems = scope.results.content.countResult

    scope.personRowClick=(row)->
      params = {}
      # coming from basic search
      if row.id
        params.id = row.id
      # coming from advanced search
      if row.peopleId
        params.id = row.peopleId
      $state.go 'mia.bio-people', params

    scope.updateSearchData = (columnName = scope.orderByColumn, start = 0, maxResult = 20, orderType = scope.orderType) ->
      scope.updateData = true
      if scope.results.searchArray? # advanced search
        searchPeService.getResults(scope.results.searchArray, start, maxResult, columnName, orderType).then (resp) ->
          if resp.data
            scope.updateData = false
            scope.results.results = resp.data
          else
            scope.orderByColumn = 'PLACENAMEFULL'
            scope.updateData = false
            scope.results.results = []
      else # simple search
        searchFactory.searchPeopleWord(scope.results.content.params, start, maxResult, columnName, orderType).then (resp) ->
          if resp.data
            scope.updateData = false
            scope.results.results = resp.data.data
  #          scope.pagination.totalItems = resp.data.count # TODO: uncomment if the request contains the total count
          else
            scope.orderByColumn = 'PLACENAMEFULL'
            scope.updateData = false
            scope.results.results = []

    scope.pageChange = () ->
      start = scope.pagination.perPage * scope.pagination.currentPage - scope.pagination.perPage
      maxResult = scope.pagination.perPage
      scope.updateSearchData(scope.orderByColumn, start, maxResult)

    scope.requestOrderedData = (columnName) ->
      if scope.orderByColumn == columnName
        if scope.orderType == 'desc'
          scope.orderType = 'asc'
        else
          scope.orderType = 'desc'
      if scope.orderByColumn != columnName
        scope.orderType = scope.startOrderType
      scope.orderByColumn = columnName
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)

    scope.showChevronIsOrdered = (columnName)  ->
      if Array.from(arguments).includes(scope.orderByColumn) && !scope.updateData
        if scope.orderType == 'desc'
          return ['glyphicon-chevron-down', 'chevron-show']
        else
          return ['glyphicon-chevron-up', 'chevron-show']
      return 'glyphicon-chevron-down'
