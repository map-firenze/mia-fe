aeResults = angular.module 'aeResults', [
  'translatorApp'
]

aeResults.directive 'aeResultsDir', ($rootScope, $state, $location, $translate, messagingFactory, modalFactory, alertify, ENV, searchFactory, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/table-results/ae-results/ae-results.html'
  scope:
    results: "="
    content: "="
  link: (scope, elem, attr) ->
    scope.orderByColumn = 'aeName'
    scope.orderType = 'asc'
    scope.startOrderType = 'asc'
    scope.pagination = {
      currentPage: 1,
      totalItems: 1,
      maxSize: 3,
      perPage: 20
    }

    scope.updateData = false
    scope.pagination.totalItems = scope.content.countResult

    scope.selectAe = (row) ->
      params = {}
      params.aeId = row.uploadId
      params.fileId = row.thumbsFiles[0].uploadFileId if row.thumbsFiles
      params.page = 1
      $state.go 'mia.singleupload', params

#      if $state.includes('mia.singleupload')
#        $location.search('aeId', row.uploadId)
#        $location.search('fileId', row.thumbsFiles[0].uploadFileId) if row.thumbsFiles
#        $timeout ->
#          scope.$root.$broadcast('refresh-ae')
#      else
#        params = {}
#        params.aeId = row.uploadId
#        params.fileId = row.thumbsFiles[0].uploadFileId if row.thumbsFiles
#        params.page = 1
#        $state.go 'mia.singleupload', params

    scope.requestAccess = (row) ->
      alertify.confirm( 'The content you are trying to access is private. Would you like to request access to it?', (->
        # open modal for request
        modalFactory.openModal(
          templateUrl: 'modules/directives/modal-templates/modal-request-access/request-access.html'
          controller:'modalIstanceController'
          resolve:{
            options:
              -> {"uploadId": row.uploadId, "owner": row.owner}
            }
        ).then (data) ->
          aeUrl = "#{window.location.protocol}//#{window.location.host}/Mia/index.html#/mia/archival-entity/#{data.uploadId}?page=1"
          shareUrl = "#{ENV.archiveBasePath}shareWithUsersByArchivial/#{data.uploadId}"

          composeMessageData = {}
          composeMessageData.to = data.recipient
          composeMessageData.messageSubject = "ACCESS CONTENT REQUEST"
          composeMessageData.messageText = "<br/>#{scope.user.firstName} #{scope.user.lastName} asked to access your private content.<br/><br/>Request Message:<br/>-<br/>" + data.msg + "<br/>-<br/>The upload can be found here: <br/><a href=#{aeUrl}>#{aeUrl}</a><br/><br/><a onClick=\"sendApprovalRequest('#{shareUrl}', '#{scope.user.account}', '#{data.recipient}', '#{aeUrl}', 'archival entity', 'null', 'null')\">CLICK HERE TO APPROVE THE REQUEST</button></a> to give him/her access, <br/>otherwise delete this message"
          composeMessageData.account = scope.user.account
          messagingFactory.sendMessage(composeMessageData).then (resp) ->
            if resp.status == 'ok'
              alertify.success("Your message has been sent!")
        ), ->
      alertify.error 'Abort')

    scope.rowAction = (row) ->
      if scope.isRowPrivate(row)
        scope.requestAccess(row)
      else
        scope.selectAe(row)
    
    scope.user = JSON.parse(localStorage.getItem('miaMe')).data[0]
    
    scope.isRowPrivate = (row) ->
      if row.filePrivacy == 0 || row.filePrivacy == 2
        return false

      if $rootScope.secAuthorize(['ADMINISTRATORS']) || scope.user.account == row.owner || $rootScope.secAuthorize(['ONSITE_FELLOWS'])
        return false

      return true

    scope.isRowShared = (row) ->
      if row.filePrivacy == 2
        return true
      
      return false

    scope.updateSearchData = (columnName = scope.orderByColumn, start = 0, maxResult = 20, orderType = scope.orderType) ->
      scope.updateData = true
      searchFactory.searchAeWord(scope.content.params, start, maxResult, columnName, orderType).then (resp) ->
        if resp.data
          scope.updateData = false
          scope.results = resp.data.aentities
#          scope.pagination.totalItems = resp.data.count # TODO: uncomment
        else
          scope.orderByColumn = 'aeName'
          scope.updateData = false
          scope.results = []

    scope.pageChange = () ->
      start = scope.pagination.perPage * scope.pagination.currentPage - scope.pagination.perPage
      maxResult = scope.pagination.perPage
      scope.updateSearchData(scope.orderByColumn, start, maxResult)

    scope.requestOrderedData = (columnName) ->
      if scope.orderByColumn == columnName
        if scope.orderType == 'desc'
          scope.orderType = 'asc'
        else
          scope.orderType = 'desc'
      if scope.orderByColumn != columnName
        scope.orderType = scope.startOrderType
      scope.orderByColumn = columnName
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)

    scope.showChevronIsOrdered = (columnName)  ->
      if Array.from(arguments).includes(scope.orderByColumn) && !scope.updateData
        if scope.orderType == 'desc'
          return ['glyphicon-chevron-down', 'chevron-show']
        else
          return ['glyphicon-chevron-up', 'chevron-show']
      return 'glyphicon-chevron-down'

aeResults.controller 'requestAccessController',($scope, $translate) ->
  $scope.completeAndSendMessage = () ->
    $scope.select({msg: $scope.message, recipient: $scope.options.owner, uploadId: $scope.options.uploadId })