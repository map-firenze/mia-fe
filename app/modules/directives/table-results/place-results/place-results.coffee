placeResults = angular.module 'placeResults', []
placeResults.directive 'placeResultsDir', ($state, searchFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/table-results/place-results/place-results.html'
  scope:
    results: "="
    content: "="
  link: (scope, elem, attr) ->
    scope.orderByColumn = 'PLACENAMEFULL'
    scope.orderType = 'asc'
    scope.startOrderType = 'asc'
    scope.pagination = {
      currentPage: 1,
      totalItems: 1,
      maxSize: 3,
      perPage: 20
    }

    scope.updateData = false
    scope.pagination.totalItems = scope.content.countResult

    scope.selectPlace=(row)->
#      console.log ('pagina place')
      params = {}
      params.id = row.id
      $state.go 'mia.bio-place', params

    scope.updateSearchData = (columnName = scope.orderByColumn, start = 0, maxResult = 20, orderType = scope.orderType) ->
      scope.updateData = true
      searchFactory.searchPlaceWord2(scope.content.params, start, maxResult, columnName, orderType).then (resp) ->
        if resp.data
          scope.updateData = false
          scope.results = resp.data.data
#          scope.pagination.totalItems = resp.data.count # TODO: uncomment if the request contains the total count
        else
          scope.orderByColumn = 'PLACENAMEFULL'
          scope.updateData = false
          scope.results = []

    scope.pageChange = () ->
      start = scope.pagination.perPage * scope.pagination.currentPage - scope.pagination.perPage
      maxResult = scope.pagination.perPage
      scope.updateSearchData(scope.orderByColumn, start, maxResult)

    scope.requestOrderedData = (columnName) ->
      if scope.orderByColumn == columnName
        if scope.orderType == 'desc'
          scope.orderType = 'asc'
        else
          scope.orderType = 'desc'
      if scope.orderByColumn != columnName
        scope.orderType = scope.startOrderType
      scope.orderByColumn = columnName
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)

    scope.showChevronIsOrdered = (columnName)  ->
      if Array.from(arguments).includes(scope.orderByColumn) && !scope.updateData
        if scope.orderType == 'desc'
          return ['glyphicon-chevron-down', 'chevron-show']
        else
          return ['glyphicon-chevron-up', 'chevron-show']
      return 'glyphicon-chevron-down'
