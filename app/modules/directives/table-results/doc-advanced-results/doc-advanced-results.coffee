docAdvancedResults = angular.module 'docAdvancedResults', ['searchServiceApp']

docAdvancedResults.directive 'docAdvancedResultsDir', ($state, $rootScope, $translate, messagingFactory, modalFactory, alertify, ENV, $sce, searchDeService, toaster) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/table-results/doc-advanced-results/doc-advanced-results.html'
  scope:
    results: "="
    searchName: "="
    searchType: "="

  controller: ($scope) ->
    $scope.dynamicPopover = {}
    $scope.dynamicPopover.content = ''
    $scope.dynamicPopover.templateUrl = 'modules/directives/table-results/doc-results/popover-transcription-template.html'
    $scope.dynamicPopover.title = ''
    $scope.orderByColumn = 'docYear'
    $scope.orderType = 'asc'
    $scope.startOrderType = 'asc'
    $scope.pagination = {
      currentPage: 1,
      totalItems: 20
      maxSize: 3,
      perPage: 20
    }

  link: (scope, elem, attr) ->
    scope.rowAction = (row) ->
      if scope.isRowPrivate(row)
        scope.requestAccess(row)
      else
        scope.selectDoc(row)

    scope.pagination.totalItems = scope.results.content.countResult

    scope.selectDoc=(row)->
      params = {}
      params.docId  = row.documentId
      $state.go 'mia.modifyDocEnt', params

    scope.user = JSON.parse(localStorage.getItem('miaMe')).data[0]

    scope.isRowPrivate = (row) ->
      if row.privacy == 0 || row.privacy == 2
        return false

      if $rootScope.secAuthorize(['ADMINISTRATORS']) || scope.user.account == row.owner
        return false

      return true

    scope.requestAccess = (row) ->
      alertify.confirm( 'The content you are trying to access is private. Would you like to request access to it?', (->
        # open modal for request
        modalFactory.openModal(
          templateUrl: 'modules/directives/modal-templates/modal-request-access/request-access.html'
          controller:'modalIstanceController'
          resolve:{
            options:
              -> {"uploadId": row.documentId, "owner": row.owner}
            }
        ).then (data) ->
          deUrl = "#{window.location.protocol}//#{window.location.host}/Mia/index.html#/mia/document-entity/#{data.uploadId}"
          shareUrl = "#{ENV.archiveBasePath}shareWithUsersByDocument/#{data.uploadId}/false"

          composeMessageData = {}
          composeMessageData.to = data.recipient
          composeMessageData.messageSubject = "ACCESS CONTENT REQUEST<br/> #{scope.user.firstName} #{scope.user.lastName} (#{scope.user.account}) - asked to access your private content:<br/><a href=#{deUrl}>#{deUrl}</a>"
          composeMessageData.messageText = "<br/>#{scope.user.firstName} #{scope.user.lastName} asked to access your private content.<br/><br/>Request Message:<br/>-<br/>" + data.msg + "<br/>-<br/>The document can be found here: <br/><a href=#{deUrl}>#{deUrl}</a><br/><br/><a onClick=\"sendApprovalRequest('#{shareUrl}', '#{scope.user.account}', '#{data.recipient}', '#{deUrl}', 'document', '#{row.documentId}', '#{row.deTitle}')\">CLICK HERE TO APPROVE THE REQUEST</button></a><br/>Otherwise ignore or delete this message."
          composeMessageData.account = scope.user.account
          messagingFactory.sendMessage(composeMessageData).then (resp) ->
            if resp.status == 'ok'
              alertify.success("Your message has been sent!")
        ), ->
      alertify.error 'Abort')

    scope.setTranscription = (result) ->
      searchedTrans   = _.find(result.transcriptions, (t) -> findWordIndex(t.transcription) != -1)
      searchedTrans   = if searchedTrans
                          searchedTrans.transcription
                        else
                          if result.transcriptions then result.transcriptions[0].transcription else null
      # xml translator for transcription in result
      text = searchedTrans
      text = text.replace(/<newsHeader>/g, '[')
      text = text.replace(/<\/newsHeader>/g, '')
      text = text.replace(/<newsFrom>/g, '[')
      text = text.replace(/<\/newsFrom>/g, '')
      text = text.replace(/<from>/g, 'News From: ')
      text = text.replace(/<\/from>/g, '')
      if text.indexOf('fromUnsure') >= 0
        text = text.replace(/<fromUnsure>y/g, '-(Unsure)')
        text = text.replace(/<\/fromUnsure>/g, '')
      text = text.replace(/<hub>/g, 'News Hub: ')
      text = text.replace(/<\/hub>/g, '')
      if text.indexOf('hubUnsure') >= 0
        text = text.replace(/<hubUnsure>y/g, '-(Unsure)')
        text = text.replace(/<\/hubUnsure>/g, '')
      text = text.replace(/<date>/g, ' - Date: ')
      text = text.replace(/<\/date><dateUnsure>y<\/dateUnsure>/g, '-(Unsure)]')
      text = text.replace(/<\/date>/g, ']')
      text = text.replace(/<plTransit>/g, '[Place of Transit: ')
      text = text.replace(/<\/plTransit>/g, ']')
      text = text.replace(/<plTransitDate>/g, '- Date: ')
      text = text.replace(/<\/plTransitDate>/g, '')
      text = text.replace(/<transc>/g, '')
      text = text.replace(/<\/transc>/g, '')
      text = text.replace(/<newsTopic.*?<\/newsTopic>(\r\n|\n|\r)/g, '')
      text = text.replace(/<wordCount.*?<\/wordCount>(\r\n|\n|\r)/g, '')
      text = text.replace(/<position.*?<\/position>(\r\n|\n|\r)/g, '')
      #line required for each occurence except the first one
      text = text.replace(/<position.*?<\/position>/g, '')
      searchedTrans = text
      return searchedTrans

    # xml translator for synopsis in result
    scope.setSynopsis = (result) ->
      if result.generalNoteSynopsis
        searchedSyn = result.generalNoteSynopsis
        text = searchedSyn
        text = text.replace(/<syn>/g, '')
        text = text.replace(/<\/syn>/g, '')
        # text = text.replace(/<wordCount>/g, '[Avviso word count: ')
        # text = text.replace(/<\/wordCount>/g, ']')
        text = text.replace(/<wordCount.*?<\/wordCount>/g, '')
        # text = text.replace(/<writtenPagesNo>/g, '[Written pages number: ')
        # text = text.replace(/<\/writtenPagesNo>/g, ']')
        text = text.replace(/<writtenPagesNo.*?<\/writtenPagesNo>/g, '')
        searchedSyn = text
        return searchedSyn

    scope.truncateSearchedTrans = (result) ->
      searchedTrans   = scope.setTranscription(result)
      wordIndex       = findWordIndex(searchedTrans)
      basicTextTrans  = searchedTrans
      truncateRange   = 400
      wrapperRange    = scope.searchName.length + (truncateRange * 2)

      if wordIndex > truncateRange
        beforeNum     = wordIndex - truncateRange
        searchedTrans = '[more ...] ' + searchedTrans.substr(beforeNum, wrapperRange)
        searchedTrans = searchedTrans + ' [more ...]' if (wordIndex + wrapperRange) < basicTextTrans.length
      else if wordIndex <= truncateRange
        beforeNum     = 0
        searchedTrans = searchedTrans.substr(beforeNum, wrapperRange)
        searchedTrans = searchedTrans + ' [more ...]' if (wordIndex + wrapperRange) < basicTextTrans.length

      return searchedTrans

    scope.truncateSearchedSyn = (result) ->
      searchedSyn   = result
      wordIndex     = findWordIndex(searchedSyn)
      basicTextSyn  = searchedSyn
      truncateRange = 400
      wrapperRange  = scope.searchName.length + (truncateRange * 2)

      if wordIndex > truncateRange
        beforeNum   = wordIndex - truncateRange
        searchedSyn = '[more ...] ' + searchedSyn.substr(beforeNum, wrapperRange)
        searchedSyn = searchedSyn + ' [more ...]' if (wordIndex + wrapperRange) < basicTextSyn.length
      else if wordIndex <= truncateRange
        beforeNum   = 0
        searchedSyn = searchedSyn.substr(beforeNum, wrapperRange)
        searchedSyn = searchedSyn + ' [more ...]' if (wordIndex + wrapperRange) < basicTextSyn.length

      return searchedSyn

    findWordIndex = (text) ->
      searchName = scope.searchName
      if searchName.indexOf('"') == -1
        text.search(new RegExp(searchName, 'i'))
      else
        searchName = scope.searchName.replace(new RegExp('"', 'gi'), '')
        text.search(new RegExp('(^|\\W)' + searchName + '($|\\W)', 'i'))

    scope.highlightText = (text) ->
      searchName = scope.searchName
      if searchName.indexOf('"') == -1
        return $sce.trustAsHtml(text.replace(new RegExp(searchName, 'gi'), '<span class="highlightedText">$&</span>'))
      else
        searchName = scope.searchName.replace(new RegExp('"', 'gi'), '')
        return $sce.trustAsHtml(text.replace(new RegExp('(^|\\W)' + searchName + '($|\\W)', 'gi'), '<span class="highlightedText">$&</span>'))

    scope.requestOrderedData = (columnName) ->
      if columnName == 'archivalLocation' && scope.pagination.totalItems > 100
        toaster.pop('error', 'Too many search results, please refine your search criteria and try again')
        return
      if scope.orderByColumn == columnName
        if scope.orderType == 'desc'
          scope.orderType = 'asc'
        else
          scope.orderType = 'desc'
      if scope.orderByColumn != columnName
        scope.orderType = scope.startOrderType
      scope.orderByColumn = columnName
      scope.pagination.currentPage = 1
      scope.updateSearchData(scope.orderByColumn)


    scope.showChevronIsOrdered = (columnName)  ->
      if Array.from(arguments).includes(scope.orderByColumn) && !scope.updateData
        if scope.orderType == 'desc'
          return ['glyphicon-chevron-down', 'chevron-show']
        else
          return ['glyphicon-chevron-up', 'chevron-show']
      return 'glyphicon-chevron-down'

    scope.updateSearchData = (columnName = scope.orderByColumn, start = 0, maxResult = 20, orderType = scope.orderType) ->
      isNewsFeedSearch = if scope.searchType == 'newsfeed' then true else false
      scope.updateData = true
      searchDeService.getResults(scope.results.searchArray, start, maxResult, columnName, orderType, isNewsFeedSearch).then (resp) ->
        if resp
          scope.results.results = resp.data
          scope.updateData = false
        else
          scope.orderByColumn = 'docYear'
          scope.updateData = false

    scope.pageChange = () ->
      start = scope.pagination.perPage * scope.pagination.currentPage - scope.pagination.perPage
      maxResult = scope.pagination.perPage
      scope.updateSearchData(scope.orderByColumn, start, maxResult)

docAdvancedResults.controller 'requestAccessController',($scope, $translate) ->
  $scope.completeAndSendMessage = () ->
    $scope.select({msg: $scope.message, recipient: $scope.options.owner, uploadId: $scope.options.uploadId })