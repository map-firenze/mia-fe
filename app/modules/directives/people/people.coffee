people = angular.module 'people', [ 'addPersonApp' ]
people.directive 'peopleDir', (deFactory, modalFactory, $filter) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/people/people.html'
  scope:
    personfield: '='
  link: (scope, elem, attr) ->
    scope.loading = false
    scope.addAPerson = () ->
      firstPerson = { "id": "", "unsure": "s" }
      scope.selectedPeople.push(firstPerson)
      #scope.personfield.value = []
      scope.tempName.push({})
      scope.searchTableOpen.push(false)

    scope.searchTableOpen = []
    scope.tempName = []
    scope.selectedPeople = []
    scope.searchTableOpen.push(false)
    if scope.personfield.value is ""
      scope.personfield.value = []
    scope.addAPerson()

    scope.closeTable = (i) ->
      scope.searchTableOpen[i] = false

    scope.getPeople = (searchQuery, i) ->
      scope.loading = true
      scope.searchTableOpen[i] = true
      if searchQuery.length == 0
        scope.searchTableOpen[i] = false
        scope.loading = false
      if searchQuery.length >= 3
        deFactory.getPeople(searchQuery).then (resp) ->
          if resp.status is 'ok'
            scope.possiblePeople = resp.data.people
          if resp.status is 'ko'
            scope.possiblePeople = []
          scope.loading = false
      else
        scope.loading = false
          
    scope.selectPerson = (person, i) ->
      alreadyInserted = false
      for people in scope.selectedPeople
        if people.id is person.id
          alreadyInserted = true
      if not alreadyInserted
        scope.tempName[i].name = person.mapNameLf
        temp = {}
        temp.id = person.id
        temp.unsure = "s"
        scope.personfield.value[i] = temp
        scope.searchTableOpen[i] = false
        scope.selectedPeople[i] = person
        scope.possiblePeople = []
      else
        pageTitle = $filter('translate')("general.ERROR")
        messagePage = $filter('translate')("people.ALREADYIN")
        modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

    scope.setUnsure = (unsure, i) ->
      if scope.personfield && scope.personfield.value.length
        scope.personfield.value[i].unsure = "u" if not unsure
        scope.personfield.value[i].unsure = "s" if unsure

    scope.addANewPerson = (i) ->
      scope.tempName[i].name = ""
      scope.searchTableOpen[i] = false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-person/add-person.html'
        controller: 'modalIstanceController'
      ).then (resp) ->
