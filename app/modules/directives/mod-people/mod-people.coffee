modPeople = angular.module 'modPeople', [ 'addPersonApp' ]
modPeople.directive 'modPeopleDir', (deFactory, modalFactory, toaster) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/mod-people/mod-people.html'
  scope:
    id: '='
    person: '='
    role: '='
    onSelect: '&'
    onCancel:'&'
    sex: '='
  link: (scope, elem, attr) ->
    scope.lastSearchQuery = null

    scope.loading = false
    scope.searchTableOpen = false
    scope.selected = false
    scope.unsure = false
    
    if scope.person.id
      isNewPerson = false
      scope.unsure = (scope.person.unsure is 'u')
    else
      isNewPerson = true
    scope.selectedPeople = {}

    if scope.person.feData
      scope.tempName = scope.person.feData.mapNameLf
    else
      scope.tempName = scope.person.name

    scope.closeTable = ()->
      scope.searchTableOpen = false

    scope.getPeople = (searchQuery) ->
      scope.selected = false
      if searchQuery
        if searchQuery.length >= 3
          if scope.countWords(searchQuery) >= 3
            toaster.pop('error', 'Queries with 3 or more words are not permitted')
            return

          scope.lastSearchQuery = searchQuery

          scope.loading=true
          scope.searchTableOpen=true
          deFactory.getPeople(searchQuery).then (resp)->
            if resp.status is "ok"
              if searchQuery == scope.lastSearchQuery || scope.lastSearchQuery == null
                scope.possiblePeople = resp.data.people
            else
              scope.possiblePeople = null
            scope.loading=false

    scope.selectPerson = (person) ->
      scope.person = person
      if not scope.loading
        scope.selected = true
        scope.tempName = person.mapNameLf
        scope.person.unsure = if scope.unsure then 'u' else 's'
        scope.searchTableOpen = false
        scope.selectedPeople = person
        scope.possiblePeople = []
#        console.log scope.person

    scope.savePerson = ()->
      toPass = {}
      toPass.role = scope.role
      toPass.temp = {}
      toPass.temp.id = scope.person.id
      toPass.temp.unsure = scope.person.unsure
      scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'dePersonEdit_' + scope.role?.type, isEdit: false})
      scope.onSelect({params:toPass})

    scope.addANewPerson = () ->
      scope.tempName = ""
      scope.searchTableOpen=false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-person/add-person.html'
        controller:'modalIstanceController'
      ).then (resp) ->

    scope.setUnsure = () ->
#      console.log scope.person
      if scope.person.unsure == "u"
        scope.person.unsure = "s"
        scope.unsure = false
      else if scope.person.unsure == "s"
        scope.person.unsure = "u"
        scope.unsure = true

#      console.log(scope.unsure)
#      console.log(scope.person.unsure)

    scope.cancel = ()->
      #console.log('dePersonEdit_' + scope.role.type)
      if scope.role
        scope.person.editPerson = false
        scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'dePersonEdit_' + scope.role.type, isEdit: false})
        if isNewPerson
          scope.person = {editPerson: true}
          scope.onCancel({person:scope.person}, {role:scope.role})
      else
        scope.onCancel()

    scope.filterPerson=(item)->
      return item.id != scope.id

    scope.countWords = (searchQuery) ->
      return searchQuery.trim().split(' ').length