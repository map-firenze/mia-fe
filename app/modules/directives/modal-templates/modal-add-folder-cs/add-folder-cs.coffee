addFolderCsApp = angular.module 'addFolderCsApp', [
  'csServiceApp'
  'toaster'
]

addFolderCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addFolderCs',
        url: '/add-folder-cs'
        params: {
          caseStudy: ''
          folder: undefined
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-folder-cs/add-folder-cs.html'
            controller: 'addFolderCsController'
]

addFolderCsApp.controller 'addFolderCsController', ($scope, $http, csFactory, toaster) ->
  caseStudy     = $scope.options.caseStudy
  $scope.folder = $scope.options.folder

  $scope.newFolderCs = {name: ''}

  $scope.newFolderCs.name = $scope.folder.title if $scope.folder

  $scope.saveFolder = () ->
    folderData       = {}
    folderData.title = $scope.newFolderCs.name

    if !$scope.folder
      csFactory.addFolder(caseStudy.id, folderData).then (resp) ->
        if resp.status == 'success'
          $scope.select(resp.data)
        else
          toaster.pop('error', resp.message)
    else
      csFactory.editFolder(caseStudy.id, $scope.folder.id, folderData).then (resp) ->
        if resp.status == 'success'
          $scope.select(resp.data)
        else
          toaster.pop('error', resp.message)
