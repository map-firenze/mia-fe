addPersonApp = angular.module 'addPersonApp', ['modalInstance']

addPersonApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addperson',
        url: '/add-person'
        params: {
          uploadFileId: ""
        }
        views:
          'content':
            templateUrl: 'modules/doc-ent/modal-add-person/add-person.html'
            controller: 'addPersonController'
]

addPersonApp.controller 'addPersonController',($scope, deFactory, $window, $state) ->
  $scope.addingPerson = false
  $scope.id = null
  $scope.newPerson = {}
  $scope.newlink = ""
  $scope.personCreated = false

  $scope.confirm=()->
    $scope.addingPerson = true

  $scope.goToPerson=()->
    params={}
    params.id = $scope.id
    url = $state.href('mia.bio-people', params)
    window.open(url,'_blank')
    $scope.cancel()

  $scope.addNewPerson = () ->
    deFactory.createNewPeople($scope.newPerson).then (resp)->
      if resp.status is "ok"
        $scope.newlink = resp.path
        $scope.id = resp.peopleId
        $scope.personCreated = true
        params={}
        params.id = $scope.id
        url = $state.href('mia.bio-people', params)
        window.open(url)
