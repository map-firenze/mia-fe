addCollectionApp = angular.module 'addCollectionApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

addCollectionApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addCollection',
        url: '/add-collection'
        params: {
          parentId: ''
          fromSearch: ''
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-collection/add-collection.html'
            controller: 'addCollectionController'
]

addCollectionApp.controller 'addCollectionController',(uploadFactory, $scope, $translate, $filter, $log, searchDeService) ->
  $scope.addNew = false
  $scope.showTable = false
  $scope.wrongCollection = false
  $scope.repositoryId = $scope.options.parentId
  $scope.fromSearch = $scope.options.fromSearch

  $scope.switchNew = ()->
    $scope.addNew = !$scope.addNew

  $scope.saveCollection = (collection)->
    collection.collectionId = collection.collectionId or ''
    collection.collectionName = collection.collectionName or ''
    collection.collectionDescription = collection.collectionDescription or ''
    if $scope.addNew
      uploadFactory.checkCollection(collection.collectionName, $scope.repositoryId).then (resp) ->
        if resp.status is "ok"
          $scope.select(collection)
        else
          $scope.wrongCollection = true
    else
      $scope.select(collection)

  $scope.getCollection = (val) ->
    if val
      if val.length>=1
        $scope.showTable=true
        uploadFactory.getPossibleCollection(val, $scope.repositoryId).then (resp) ->
          $scope.possibleCollection = $filter('filter') resp.collection, val
  
  searchDeService.getCollection().then (resp)->
    $scope.showTable=true
    $scope.possibleCollection = resp
  