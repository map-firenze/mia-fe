addInsertSearchApp = angular.module 'addInsertSearchApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
    'ngAlertify'
    'insertDescrApp'
]

addInsertSearchApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addInsertSearch',
        url: '/add-insert-search'
        params: {
          parentId: ''
          fromSearch: ''
          collection: ''
          parentName: ''
          hasInserts: ''
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-insert-search/add-insert-search.html'
            controller: 'addInsertSearchController'
]

addInsertSearchApp.controller 'addInsertSearchController',(uploadFactory, $rootScope, searchFactory, $scope, $translate, $filter, $log, alertify, $state, $sce) ->
  $scope.addNew = !$scope.options.hasInserts
  $scope.showTable = false
  $scope.switched = false
  $scope.clicked = false
  $scope.collection = $scope.options.collection
  $scope.collectionId = $scope.options.collection.collectionId
  $scope.volumeId = $scope.options.parentId or JSON.parse(localStorage.getItem('volumeId'))
  $scope.volumeName = $scope.options.parentName
  $scope.fromSearch = $scope.options.fromSearch
  $scope.insParams = {}
  $scope.insCreated = false
  $scope.newInsert = {}
  $scope.insNotFound = false
  $scope.insLoading = false
  $scope.possibleInsert = []

  $scope.switchNew = () ->
    $scope.addNew = !$scope.addNew
    $scope.switched = true

  $scope.no = (insert) ->
    $scope.select(insert)

  $scope.saveInsert = (insert) ->
    insert.insertId = insert.insertId or ''
    insert.insertName = insert.insertName or ''
    localStorage.setItem("insertId", JSON.stringify(insert.insertId))
    if !$scope.insCreated
      $scope.select(insert)
  
  $scope.addNewInsert = (newInsert) ->
    $scope.clicked = true
    if $scope.insCreated
      localStorage.setItem("insertId", JSON.stringify($scope.createdInsert[0].insertId))
      $state.go("mia.insertDescr", {'insertId': $scope.createdInsert[0].insertId})
      $scope.select(newInsert)
    else
      if !$scope.switched && !$scope.addNew
        $scope.saveInsert(newInsert)
      else
        searchFactory.createAddInsert({'collectionId': $scope.collectionId, 'volumeNo': $scope.volumeName, 'insertNo': newInsert.insertName}).then (resp) ->
#          console.log(resp)
          if resp.status == "ok"
            $scope.getInsert(newInsert.insertName).then (createdInsert) ->
              $scope.createdInsert = createdInsert
              $scope.insCreated = true
              $scope.saveInsert($scope.createdInsert[0])
          else
            $scope.clicked = false
            errorMessage = resp.message || 'Cannot create insert.'
            alertify.alert errorMessage
            $scope.clicked = false
            $rootScope.$broadcast("insertNoSaved", $scope.insParams.insertNo)

  $scope.getInsert = (val) ->
    if val && val.length >= 1
      $scope.showTable = true
      $scope.insLoading = true
      uploadFactory.getPossibleInsert(val, $scope.volumeId).then (resp) ->
        $scope.insLoading = false
        $scope.possibleInsert = $filter('filter') resp.insert, val
        identical = _.find($scope.possibleInsert, { insertName: val })
        $scope.insNotFound = _.isUndefined(identical)
        $scope.possibleInsert
    else
      $scope.insNotFound = false
      $scope.showTable = false
      $scope.possibleInsert = []

  $scope.missingInsertText = (ins) ->
    text = 'Insert number # does not exist.'
    text += ' Click the Add insert button to add one' if $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS']) || $scope.collection.studyCollection
    highlightedText = '<span class="highlightedWord">' + ins + '</span>'
    text = text.replace('#', highlightedText)
    return $sce.trustAsHtml(text)