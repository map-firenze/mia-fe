addLinkCsApp = angular.module 'addLinkCsApp', [
  'toaster'
]

addLinkCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addLinkCs',
        url: '/add-link-cs'
        params: {
          caseStudy: ''
          folderId: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-link-cs/add-link-cs.html'
            controller: 'addLinkCsController'
]

addLinkCsApp.controller 'addLinkCsController', ($scope, $rootScope, csFactory, toaster) ->
  caseStudy = $scope.options.caseStudy
  folderId  = $scope.options.folderId

  $scope.newLinkCs = {title: '', link: ''}

  $scope.addNewLink = () ->
#    console.log('----', $scope.newLinkCs)
    data = {entityType: 'LINK', title: $scope.newLinkCs.title, link: $scope.newLinkCs.link}

    csFactory.addToCsFolder(caseStudy.id, folderId, data).then (resp) ->
#      console.log('resp addToCsFolder', resp)
      if resp.status == 'success'
        toaster.pop('success', 'Successfully added')
        newLink          = {}
        newLink.csId     = caseStudy.id
        newLink.folderId = folderId
        newLink.item     = resp.data
        $rootScope.$broadcast('addToCsFolder', newLink)
        $scope.select(newLink)
      else
        toaster.pop('error', resp.message)

