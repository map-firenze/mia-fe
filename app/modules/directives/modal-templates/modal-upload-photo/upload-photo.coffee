upPhotoApp = angular.module 'uploadPhotoApp', [
    'modalInstance'
    'personalProfileService'
    'ngFileUpload'
    'translatorAppEn'
    'translatorApp'
   ]

upPhotoApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.uploadPhoto',
        url: '/upload-photo'
        views:
          'content':
            controller: 'uploadPhotoController'
]
upPhotoApp.controller 'uploadPhotoController', (personalProfileFactory, Upload, $http, $scope, $rootScope, $log, $timeout, $state) ->

  $scope.picFile = null
  $scope.croppedData = null
  $scope.success = false
  
  $scope.removePic=() ->
    $scope.picFile = null
    $scope.croppedData = null
  
  $scope.uploadDone=()->
    $scope.success = true
    $scope.picFile = null
    $scope.croppedData = null
    $scope.progress = 0
  
  $scope.close=()->
    personalProfileFactory.getUser().then (resp)->
      if resp.data
        $scope.select(resp.data)

  $scope.checkImage = (file) ->
    $scope.fileError = null
    if file.type != "image/jpeg"
      $scope.picFile = null
      $scope.fileError = 'Invalid file type!'

  $scope.upload = (fileData) ->
    blob = personalProfileFactory.datatoBlob(fileData, "portrait", null, "image/jpeg")
    #blob = Upload.dataUrltoBlob(fileData, $scope.picFile.name)
    file = []
    file.push(blob)
    Upload.upload(
      url: 'json/userprofile/uploadPortraitUser'
      data:
        files: file).then ((response) ->
          $scope.result = response.data
          if $scope.result.status = "ok"
            $scope.uploadDone()
          ), ((response)->
            if response.status > 0
              $scope.errorMsg = response.status + ': ' + response.data
          ), (evt) ->
            $scope.progress = Math.min(100,parseInt(100.0 * evt.loaded / evt.total))
            $rootScope.$broadcast('progress', $scope.progress)
            if $scope.progress == 100
              $rootScope.$broadcast('upload-finished')
              $scope.close()

  handleFileSelect = (evt) ->
    file = evt.currentTarget.files[0]
    reader = new FileReader
    reader.onload = (evt) ->
      $scope.$apply ($scope) ->
        $scope.picFile = evt.target.result
    reader.readAsDataURL file

  # angular.element(document.querySelector('#fileInput')).on 'change', handleFileSelect
  return
