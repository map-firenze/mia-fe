addBioGeoCsApp = angular.module 'addBioGeoCsApp', [
  'toaster'
]

addBioGeoCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addBioGeoCs',
        url: '/add-bio-geo-cs'
        params: {
          caseStudy: ''
          folderId: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-bio-geo-cs/add-bio-geo-cs.html'
            controller: 'addBioGeoCsController'
]

addBioGeoCsApp.controller 'addBioGeoCsController', ($scope, deFactory, $rootScope, csFactory, toaster) ->
  caseStudy = $scope.options.caseStudy
  folderId  = $scope.options.folderId

  $scope.step = 'selectType' # selectType/selectBio/selectGeo

  $scope.selectionTypes = [
    {type: 'BIO', name: 'Add biographical record'},
    {type: 'GEO', name: 'Add geographical record'}
  ]

  $scope.search = {bio: '', geo: ''}

  $scope.possiblePeoples = []
  $scope.possiblePlaces  = []

  $scope.selectedRecType = undefined

  $scope.loading         = false
  $scope.showBioTable    = false
  $scope.showGeoTable    = false

  $scope.selectRecordType = (recType) ->
    $scope.selectedRecType = recType
    $scope.step = 'selectBio' if recType.type == 'BIO'
    $scope.step = 'selectGeo' if recType.type == 'GEO'

  $scope.getPeople = () ->
    searchQuery = $scope.search.bio
    if searchQuery && searchQuery.length >= 3
      $scope.loading = true
      $scope.showBioTable = true
      deFactory.getPeople(searchQuery).then (resp) ->
        $scope.loading = false
        if resp.status is "ok"
          $scope.possiblePeoples = resp.data.people
        else
          $scope.possiblePeoples = []
    else if searchQuery.length == 0
      $scope.showBioTable = false
      $scope.possiblePeoples = []

  $scope.selectBio = (bioRecord) ->
    data = {entityType: 'BIO', entityId: bioRecord.id}
    csFactory.addToCsFolder(caseStudy.id, folderId, data).then (resp) ->
      if resp.status == 'success'
        toaster.pop('success', 'Successfully added')
        data          = {}
        data.csId     = caseStudy.id
        data.folderId = folderId
        data.item     = resp.data
        $rootScope.$broadcast('addToCsFolder', data)
        $scope.select(data)
      else
        toaster.pop('error', resp.message)

  $scope.getPlaces = () ->
    searchQuery = $scope.search.geo
    if searchQuery && searchQuery.length >= 3
      $scope.loading = true
      $scope.showGeoTable = true
      deFactory.getPlace(searchQuery).then (resp) ->
        $scope.loading = false
        if resp.status is "ok"
          $scope.possiblePlaces = resp.data.places
        else
          $scope.possiblePlaces = []
    else if searchQuery.length == 0
      $scope.showGeoTable = false
      $scope.possiblePlaces = []

  $scope.selectGeo = (geoRecord) ->
    data = {entityType: 'GEO', entityId: geoRecord.id}
    csFactory.addToCsFolder(caseStudy.id, folderId, data).then (resp) ->
      if resp.status == 'success'
        toaster.pop('success', 'Successfully added')
        data          = {}
        data.csId     = caseStudy.id
        data.folderId = folderId
        data.item     = resp.data
        $rootScope.$broadcast('addToCsFolder', data)
        $scope.select(data)
      else
        toaster.pop('error', resp.message)

  $scope.backToPrevStep = () ->
    $scope.step = 'selectType'
    $scope.search = {bio: '', geo: ''}
    $scope.possiblePeoples = []
    $scope.possiblePlaces  = []
    $scope.selectedRecType = undefined
    $scope.showBioTable    = false
    $scope.showGeoTable    = false



