spotlightModifyRequest = angular.module 'spotlightModifyRequest', []

spotlightModifyRequest.controller 'spotlightModifyRequestController', ($scope, $http, $stateParams) ->

  $scope.requestModify = (motivationForm) ->
    $http.put("json/discovery/"+$stateParams.id+"/editRequest/", motivationForm).then (resp) ->
      if  resp.data.status == 'ok'
        $scope.errorMsg = undefined
        window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/welcome'
      else
        $scope.errorMsg = resp.data.message
