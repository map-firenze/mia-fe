spotlightPdfTemplate = angular.module 'spotlightPdfTemplate', []

spotlightPdfTemplate.controller 'spotlightPdfTemplateController',($scope, $stateParams, $http) ->

  init = () ->
    $http.get("json/discovery/find/"+$stateParams.id).then (resp) ->
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        $scope.discovery = resp.data.data.discovery
        initLinks($scope.discovery)
        window.print()
      else
        $scope.errorMsg = resp.data.message

  initLinks = (discovery) ->
    $scope.links = []
    for id in discovery.links
      currentLink = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+id
      $scope.links = $scope.links.concat([currentLink])

  $scope.getFileUrl = (file) ->
    return window.location.protocol + '//' + window.location.host + '/Mia/json/discovery/' + $scope.discovery.id + '/download/' + file.id

  init()