spotlightPendingReview = angular.module 'spotlightPendingReview', ['modalInstance']


spotlightPendingReview.controller 'spotlightPendingReviewController',($scope, $http) ->
  $scope.offset = 1
  $scope.limit = 5

  getDiscoveries = (searchText, offset) ->
    if searchText == undefined
      searchText = ""

    $scope.loading = true
    $http.get("json/discovery/findDiscoveries?status=&searchText="+searchText+"&limit="+$scope.limit+"&offset="+(offset-1)*+$scope.limit).then (resp) ->
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        $scope.discoveries = resp.data.data.discoveries
        $scope.discoveriesCount = resp.data.data.count
      else
        $scope.errorMsg = resp.data.message
        $scope.loading = false

  initDiscoveries = () ->
    getDiscoveries("", 1)

  $scope.review = (discovery) ->
    window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/spotlight-review-staff/' + discovery.id

  $scope.findSpotlightReview = (searchQuery) ->
    getDiscoveries(searchQuery, 1)

  $scope.discoveriesPageChanged = (searchQuery, offset) ->
    $scope.offset = offset
    getDiscoveries(searchQuery, offset)

  $scope.getStatus = (d) ->
    if d.status == 'Rejected' || d.status == 'Re-opened'
      return "user - " + d.status
    else
      return "spc/admin - " + d.status

  initDiscoveries()
