spotlightDownloadMaterial = angular.module 'spotlightDownloadMaterial', ['ui.router', 'spotlightPage', 'spotlightPdfTemplate']

spotlightDownloadMaterial.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.spotlightDownloadMaterial',
        url: '/spotlight/:id/download'
        views:
          'content':
            controller: 'spotlightPdfTemplateController'
            templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-pdf-template.html'
]

spotlightDownloadMaterial.controller 'spotlightDownloadMaterialController',($scope, $stateParams, $http) ->

  init = () ->
    $http.get("json/discovery/find/"+$stateParams.id).then (resp) ->
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        $scope.discovery = resp.data.data.discovery
        initLinks($scope.discovery)
        console.log(resp.data)
      else
        console.log(resp.data)
        $scope.errorMsg = resp.data.message

  $scope.print = () ->
    window.open(window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/spotlight/' + $scope.discovery.id + '/download')

  initLinks = (discovery) ->
    $scope.links = []
    for id in discovery.links
      currentLink = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+id
      $scope.links = $scope.links.concat([currentLink])

  $scope.getFileUrl = (file) ->
    return window.location.protocol + '//' + window.location.host + '/Mia/json/discovery/' + $scope.discovery.id + '/download/' + file.id

  init()