spotlight = angular.module 'spotlight', ['ui.router', 'spotlightPage']

spotlight.controller 'spotlightCheckModalController',($scope, $modalInstance, $stateParams) ->

  $scope.checks = [
    { label : "It contains a complete transcription (all abbreviations should be expanded)", value: false },
    { label : "All relevant people have been added to the document", value: false },
    { label : "All topics and places have been added to the document", value: false },
    { label : "It contains a complete synopsis", value: false },
    { label : "All images are legible", value: false }
  ]

  $scope.isOkEnabled = () ->
    for check in $scope.checks
      if check.value == false
        $scope.disableBtn = true
        return false
    $scope.disableBtn = false
    return true
  
  $scope.ok = () ->
    $modalInstance.dismiss true
    window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/document-entity/'+$stateParams.docId+'/spotlight'

  $scope.cancel = () ->
    $modalInstance.dismiss false

spotlight.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.spotlight',
        url: '/document-entity/:docId/spotlight'
        params: {
          docId: ''
        }
        views:
          'content':
            templateUrl: 'modules/views/spotlight/spotlight-page.html'
            controller: 'spotlightPageController'
          'sidebar':
            controller: 'sidebarController'
            templateUrl: 'modules/sidebar/sidebar.html'
]
