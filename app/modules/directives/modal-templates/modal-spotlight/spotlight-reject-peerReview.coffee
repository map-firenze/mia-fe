spotlightRejectPeerReview = angular.module 'spotlightRejectPeerReview', []

spotlightRejectPeerReview.controller 'spotlightRejectPeerReviewController', ($scope, $http, $stateParams) ->

  $scope.reject = (motivationForm) ->
    $http.put("json/discovery/"+$stateParams.id+"/reject/", motivationForm).then (resp) ->
      if  resp.data.status == 'ok'
        $scope.errorMsg = undefined
        window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/welcome'
      else
        $scope.errorMsg = resp.data.message
