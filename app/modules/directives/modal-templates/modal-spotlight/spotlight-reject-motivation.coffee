spotlightRejectMotivation = angular.module 'spotlightRejectMotivation', []

spotlightRejectMotivation.controller 'spotlightRejectMotivationController', ($scope, $http, $stateParams) ->

  $scope.reject = (motivationForm) ->
    $http.put("json/discovery/"+$stateParams.id+"/editRequest/", motivationForm).then (resp) ->
      if resp.data.status == 'ok'
        $scope.errorMsg = undefined
        window.location.href = window.location.protocol + '//' + window.location.host + '/Mia/index.html#/mia/welcome'
      else
        $scope.errorMsg = resp.data.message
