changePassAdmin = angular.module 'changePassAdminApp', [
    'adminServiceApp'
    'modalInstance'
   ]

changePassAdmin.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.changePassAdmin',
        url: '/change-pass-admin'
        views:
          'content':
            controller: 'changePassAdminController'
]
changePassAdmin.controller 'changePassAdminController', (adminFactory, $scope, $log) ->
  $scope.newPass = undefined
  $scope.confirmNewPass = undefined
  $scope.errorEqual = false
  $scope.success = false

  $scope.comparePass=()->
    if $scope.newPass == $scope.confirmNewPass
      $scope.errorEqual = false
    else
      $scope.errorEqual = true

  $scope.resetError=()->
    $scope.errorEqual = false
    $scope.errorOld = false

  $scope.save=()->
    params = {}
    params.account = modalInstance.account
    params.newPassword = $scope.newPass
    params.newPasswordConfirm = $scope.confirmNewPass
    $scope.comparePass()
    if !$scope.errorEqual
      adminFactory.changePass(params).then (resp)->
        if resp.status == "ok"
          $scope.resetError()
          $scope.success = true
