editCsApp = angular.module 'editCsApp', [
  'csServiceApp'
  'toaster'
]

editCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.editCs',
        url: '/edit-cs'
        params: {
          caseStudy: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-edit-cs/edit-cs.html'
            controller: 'editCsController'
]

editCsApp.controller 'editCsController', ($scope, csFactory, toaster) ->
  caseStudy    = $scope.options.caseStudy
  $scope.modCs = angular.copy(caseStudy)

  $scope.saveCs = () ->
    data             = {}
    data.title       = $scope.modCs.title
    data.description = $scope.modCs.description
    data.notes       = $scope.modCs.notes

    csFactory.updateCs($scope.modCs.id, data).then (resp) ->
      if resp.status == 'success'
        $scope.select(resp.data)
      else
        toaster.pop('error', resp.message)
