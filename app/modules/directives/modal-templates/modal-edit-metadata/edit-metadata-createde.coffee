editMetaDataApp = angular.module 'editMetaDataApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'ngDraggable'
    'deServiceApp'
    'volumeInsertDescrApp'
]

editMetaDataApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.editMetadata',
        url: '/edit-metadata'
        params: {
          uploadFileId: "",
          volumeId: "",
        }
        views:
          'content':
            controller: 'editMetaDataController'
            templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata-createde.html'
]

editMetaDataApp.controller 'editMetaDataController',(commonsFactory, volumeInsertDescrFactory, deFactory, $scope, $state, $sce, $translate, $filter, $log) ->

  uploadFileId = $scope.options.uploadFileId
  volumeId = $scope.options.volumeId
  insertId = $scope.options.insertId
  $scope.paginationNotes = ""
  $scope.imgName = ""
  $scope.folios=[]

  if insertId
    volumeInsertDescrFactory.findInsertDescription(insertId).then (resp) ->
      if resp.status == "ok"
        notes = resp.data.data.paginationNotes
      if notes && notes.length > 500
        $scope.paginationNotes = notes.substring(0, 500).concat('...')
      else
        $scope.paginationNotes = notes
  else
    volumeInsertDescrFactory.findVolumeDescription(volumeId).then (resp) ->
      if resp.status == "ok"
        notes = resp.data.data.paginationNotes
      if notes && notes.length > 500
        $scope.paginationNotes = notes.substring(0, 500).concat('...')
      else
        $scope.paginationNotes = notes

  deFactory.getFolioByUploadFileId(uploadFileId).then (resp)->
    if resp.status is "ok"
      $scope.folio1 = resp.data[0]
      $scope.imgName = $scope.folio1.imgName
      if resp.data[1]
        $scope.folio2 = resp.data[1]
        $scope.doubleFolio = true
      else
        $scope.doubleFolio = false
    else
      $scope.doubleFolio = false
      $scope.folio1 = {}
      $scope.folio1.folioId= ""
      $scope.folio1.folioNumber = ""
      $scope.folio1.uploadFileId = uploadFileId
      $scope.folio1.noNumb = false

      $scope.folio2 = {}
      $scope.folio2.folioId= ""
      $scope.folio2.folioNumber = ""
      $scope.folio2.uploadFileId = uploadFileId
      $scope.folio2.noNumb = false
    $scope.folios[0] = $scope.folio1
    if $scope.doubleFolio
      $scope.folios[1] = $scope.folio2
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+uploadFileId)

    $scope.saveFolio = () ->
      if $scope.folio1.noNumb
        $scope.folio1.folioNumber = ""
        $scope.folio1.rectoverso = ""
      if $scope.doubleFolio && $scope.folio2.noNumb
        $scope.folio2.folioNumber = ""
        $scope.folio2.rectoverso = ""
      if $scope.doubleFolio
        $scope.folio2.uploadFileId = $scope.folio1.uploadFileId
      $scope.folios = []
      $scope.folios[0] = $scope.folio1
      if $scope.doubleFolio
        $scope.folios[1] = $scope.folio2
      updateFolioInfo($scope.folios)

  updateFolioInfo =  (folios) ->
    deFactory.saveFolio(folios).then (resp)->
      $scope.select(folios)

  $scope.cancelAddToDe = ()->
    $scope.select(false)

  $scope.doubleChanged = (value) ->
    $scope.doubleFolio = value
    if value
      $scope.folio2 = {}
      $scope.folio2.folioNumber = ""
      $scope.folio2.rectoverso = ""
      $scope.folio2.noNumb = false
    else
      clearFolio2()

  clearFolio2 = () ->
    $scope.folio2 = {}

  $scope.hasSameValues = () ->
    if _.isEmpty($scope.folio2) || $scope.folio2.noNumb
      return false
    if $scope.folio1.folioNumber && !$scope.folio1.noNumb && !$scope.folio1.rectoverso && $scope.folio2.hasOwnProperty('folioNumber') && (!$scope.folio2.hasOwnProperty('rectoverso') || $scope.folio2.rectoverso == null)
      $scope.folio1.folioNumber.toUpperCase() == $scope.folio2.folioNumber.toUpperCase()
    else if $scope.folio1.folioNumber && !$scope.folio1.noNumb && $scope.folio1.rectoverso && $scope.folio2.hasOwnProperty('folioNumber') && $scope.folio2.hasOwnProperty('rectoverso')
      $scope.folio1.folioNumber.toUpperCase() == $scope.folio2.folioNumber.toUpperCase() && $scope.folio1.rectoverso == $scope.folio2.rectoverso
    else
      false

  $scope.determineFolio2 = () ->
    if $scope.folio2.noNumb
      $scope.folio2 = { noNumb: true }
    else
      clearFolio2()


  $scope.folioCheck = () ->
    if !$scope.doubleFolio
      return not ($scope.folio1?.folioNumber or $scope.folio1?.noNumb)
    else
      return not (($scope.folio1?.folioNumber or $scope.folio1?.noNumb) and ($scope.folio2?.folioNumber or $scope.folio2?.noNumb))