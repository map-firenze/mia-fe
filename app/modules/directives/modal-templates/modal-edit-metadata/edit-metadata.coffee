editMetaDataApp = angular.module 'editMetaDataApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'ngDraggable'
    'deServiceApp'
    'volumeInsertDescrApp'
]

editMetaDataApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.editMetadata',
        url: '/edit-metadata'
        params: {
          uploadFileId: "",
          volumeId: "",
        }
        views:
          'content':
            controller: 'editMetaDataController'
            templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata.html'
]

editMetaDataApp.controller 'editMetaDataController',(commonsFactory, $http, modalFactory, alertify, $rootScope, volumeInsertDescrFactory, deFactory, $scope, $state, $sce, $translate, $filter, $log) ->

  uploadFileId = $scope.options.uploadFileId
  volumeId = $scope.options.volumeId
  insertId = $scope.options.insertId
  $scope.paginationNotes = ""
  $scope.imgName = ""
  $scope.folios=[]

  $scope.pagination = {
    #currentPage: 1,
    maxSize: 15,
    perPage: 15
  }

  $scope.cleanFoliosRootscope = ()->
    delete $rootScope.folioNextVerso
    delete $rootScope.folioNextRecto
    delete $rootScope.doubleFolio

  $scope.cleanFolioRootscope

  if insertId
    volumeInsertDescrFactory.findInsertDescription(insertId).then (resp) ->
      if resp.status == "ok"
        notes = resp.data.data.paginationNotes
      if notes && notes.length > 500
        $scope.paginationNotes = notes.substring(0, 500).concat('...')
      else
        $scope.paginationNotes = notes
  else
    volumeInsertDescrFactory.findVolumeDescription(volumeId).then (resp) ->
      if resp.status == "ok"
        notes = resp.data.data.paginationNotes
      if notes && notes.length > 500
        $scope.paginationNotes = notes.substring(0, 500).concat('...')
      else
        $scope.paginationNotes = notes

  deFactory.getFolioByUploadFileId(uploadFileId).then (resp)->
    if resp.status is "ok"
      $scope.folio1 = resp.data[0]
      $scope.imgName = $scope.folio1.imgName
      #if resp.data[1] does not exist set $scope.doubleFolio = false !!!!

      if resp.data[1]
        $scope.folio2 = resp.data[1]
        $scope.doubleFolio = true
      else if resp.data[0]
        $scope.doubleFolio = false
      else
        $scope.doubleFolio = true
        #$scope.doubleFolio = false
    else
      if $rootScope.folioNextRecto? #if double folio
        $scope.doubleFolio = true
        $scope.folio1 = {}
        $scope.folio1.folioId= ""
        $scope.folio1.folioNumber = $rootScope.folioNextVerso
        $scope.folio1.uploadFileId = uploadFileId
        $scope.folio1.noNumb = false
        $scope.folio1.rectoverso = "verso"
        $scope.folio2 = {}
        $scope.folio2.folioId= ""
        $scope.folio2.folioNumber = $rootScope.folioNextRecto
        $scope.folio2.uploadFileId = uploadFileId
        $scope.folio2.noNumb = false
        $scope.folio2.rectoverso = "recto"
      else if $rootScope.doubleFolio? #if single folio
        $scope.doubleFolio = false
        $scope.folio1 = {}
        $scope.folio1.folioId= ""
        $scope.folio1.folioNumber = $rootScope.folioNextVerso
        $scope.folio1.uploadFileId = uploadFileId
        $scope.folio1.noNumb = false
        #se verso aumenta di 1
        #se recto non aumentare mi dai lo stesso
        if $rootScope.folioNextVersoRectorVerso == "recto" #if single folio, set rectoverso as it should be
          $scope.folio1.rectoverso = "verso"
        else if $rootScope.folioNextVersoRectorVerso == "verso"
          $scope.folio1.folioNumber = $rootScope.folioNextVerso + 1
          $scope.folio1.rectoverso = "recto"
        else
          $scope.folio1.rectoverso = ""
        $scope.folio2 = {}
        $scope.folio2.folioId= ""
        $scope.folio2.folioNumber = ""
        $scope.folio2.uploadFileId = uploadFileId
        $scope.folio2.noNumb = false
        $scope.folio2.rectoverso = ""
      else
        $scope.doubleFolio = true
        #$scope.doubleFolio = false
        $scope.folio1 = {}
        $scope.folio1.folioId= ""
        $scope.folio1.folioNumber = ""
        $scope.folio1.uploadFileId = uploadFileId
        $scope.folio1.noNumb = false
        $scope.folio1.rectoverso = "verso"

        $scope.folio2 = {}
        $scope.folio2.folioId= ""
        $scope.folio2.folioNumber = ""
        $scope.folio2.uploadFileId = uploadFileId
        $scope.folio2.noNumb = false
        $scope.folio2.rectoverso = "recto"
    $scope.folios[0] = $scope.folio1
    if $scope.doubleFolio
      $scope.folios[1] = $scope.folio2
    $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+uploadFileId)

    $scope.saveFolio = () ->
      if $scope.folio1.noNumb
        $scope.folio1.folioNumber = ""
        $scope.folio1.rectoverso = ""
      if $scope.doubleFolio && $scope.folio2.noNumb
        $scope.folio2.folioNumber = ""
        $scope.folio2.rectoverso = ""
      if $scope.doubleFolio
        $scope.folio2.uploadFileId = $scope.folio1.uploadFileId
      $scope.folios = []
      $scope.folios[0] = $scope.folio1
      if $scope.doubleFolio
        $scope.folios[1] = $scope.folio2
      updateFolioInfo($scope.folios)

  updateFolioInfo =  (folios) ->
    deFactory.saveFolio(folios).then (resp)->
      $scope.select(folios)

  $scope.cancelAddToDe = ()->
    $scope.select(false)

  $scope.closeAndRefresh = ()->
    $scope.select(false)
    $state.reload()

  $scope.doubleChanged = (value) ->
    $scope.doubleFolio = value
    if value = true
      $scope.folio1.rectoverso = "verso"
      $scope.folio2 = {}
      $scope.folio2.folioNumber = ""
      $scope.folio2.rectoverso = "recto"
      $scope.folio2.noNumb = false
    else
      $scope.folio1.rectoverso = 'recto'
      #clearFolio2()

  #clearFolio2 = () ->
    #$scope.folio2 = {}

  $scope.hasSameValues = () ->
    if _.isEmpty($scope.folio2) || $scope.folio2.noNumb
      return false
    if $scope.folio1.folioNumber && !$scope.folio1.noNumb && !$scope.folio1.rectoverso && $scope.folio2.hasOwnProperty('folioNumber') && (!$scope.folio2.hasOwnProperty('rectoverso') || $scope.folio2.rectoverso == null)
      $scope.folio1.folioNumber.toUpperCase() == $scope.folio2.folioNumber.toUpperCase()
    else if $scope.folio1.folioNumber && !$scope.folio1.noNumb && $scope.folio1.rectoverso && $scope.folio2.hasOwnProperty('folioNumber') && $scope.folio2.hasOwnProperty('rectoverso')
      $scope.folio1.folioNumber.toUpperCase() == $scope.folio2.folioNumber.toUpperCase() && $scope.folio1.rectoverso == $scope.folio2.rectoverso
      return true
    else
      false

  $scope.determineFolio2 = () ->
    if $scope.folio2.noNumb
      $scope.folio2 = { noNumb: true }
    else
      #clearFolio2()


  $scope.folioCheck = () ->
    if !$scope.doubleFolio
      return not ($scope.folio1?.folioNumber or $scope.folio1?.noNumb)
    else
      return not (($scope.folio1?.folioNumber or $scope.folio1?.noNumb) and ($scope.folio2?.folioNumber or $scope.folio2?.noNumb))

  #nes things
  
  $scope.saveFolioNext = () ->
    if $scope.folio1.noNumb
      $scope.folio1.folioNumber = ""
      $scope.folio1.rectoverso = ""
    if $scope.doubleFolio && $scope.folio2.noNumb
      $scope.folio2.folioNumber = ""
      $scope.folio2.rectoverso = ""
    if $scope.doubleFolio
      $scope.folio2.uploadFileId = $scope.folio1.uploadFileId
    $scope.folios = []
    $scope.folios[0] = $scope.folio1
    if $scope.doubleFolio
      $scope.folios[1] = $scope.folio2
    if $scope.folios[1]
      folioNumber = parseInt($scope.folios[1].folioNumber, 10)
      $rootScope.folioNextVerso = folioNumber
      $rootScope.folioNextRecto = folioNumber + 1
    else
      $rootScope.doubleFolio = $scope.doubleFolio
      folioNumber = parseInt($scope.folios[0].folioNumber, 10)
      $rootScope.folioNextVerso = folioNumber
      $rootScope.folioNextVersoRectorVerso = $scope.folios[0].rectoverso
    updateFolioInfoNext($scope.folios)

  updateFolioInfoNext =  (folios) ->
    deFactory.saveFolio(folios).then (resp)->
      
  $scope.numberNextFolio = (resp) ->
    url = window.location.href#gets the url
    #pageNumber = url.split("page=")[1][0]#parses the url to get the page number
    urlParams = new URLSearchParams(url)
    page = urlParams.get('page')
    #console.log page
    pageNumber = page

    commonsFactory.getMySingleuploadByFileId(uploadFileId, $scope.pagination.maxSize, pageNumber).then (resp)-> #uses the default pageNumber(maxSize)
      nextIndex = "" #nextIndex
      i = 0
      if nextIndex is ""
        while i < resp.aentity.thumbsFiles.length #number of images
          if(uploadFileId == resp.aentity.thumbsFiles[i].uploadFileId) #searches for the current image position
            if(i+1 == $scope.pagination.maxSize and (pageNumber < resp.aentity.pagination.totalPagesForSingleAe)) #next page over the max page size and need to turn page makes sure there is a next page
              # alertify.alert("Change page to continue numbering")
              alertify.alert 'Change page to continue numbering', ->
                $state.reload()
                return
            else if(i+1 >= resp.aentity.thumbsFiles.length) # no more images to number
              alertify.alert 'The are no more folios to number in this upload', ->
                $state.reload()
                return
            nextIndex = resp.aentity.thumbsFiles[i+1].uploadFileId #returns the next image uploadFileID
            break
          i++ #continue searching for the right image
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata.html'
        controller:'modalIstanceController'
        backdrop:'static'
        resolve:{
          options:
            -> {
              "uploadFileId": nextIndex,
              "volumeId": $rootScope.VolumeID,
              "insertId": $rootScope.InsertID || null
            }
        }
    ).then (editImageMetadataIsChange) ->
      if editImageMetadataIsChange
        $state.reload()