addDescApp = angular.module 'addDescApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

addDescApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addDesc',
        url: '/add-desc'
        params: {
          parentId: ""
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-desc/add-desc.html'
            controller: 'addDescController'
]

addDescApp.controller 'addDescController',(uploadFactory, $scope, $translate, $filter, $log) ->
