addVolumeSearchApp = angular.module 'addVolumeSearchApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
    'ngAlertify'
    'volumeDescrApp'
]

addVolumeSearchApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addVolumeSearch',
        url: '/add-volume-search'
        params: {
          parentId: ""
          fromSearch: ''
          
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-volume-search/add-volume-search.html'
            controller: 'addVolumeSearchController'
]

addVolumeSearchApp.controller 'addVolumeSearchController',(uploadFactory, $rootScope, searchFactory, $scope, $translate, $filter, $log, alertify, $state, $sce) ->
  $scope.addNew = false
  $scope.showTable = false
  $scope.switched = false
  $scope.clicked = false
  $scope.collectionId = $scope.options.parentId
  $scope.fromSearch = $scope.options.fromSearch
  $scope.volParams = {}
  $scope.volCreated = false
  $scope.volNotFound = false
  $scope.volLoading = false
  $scope.possibleVolume = []
  
  $scope.switchNew = () ->
    $scope.addNew = !$scope.addNew
    $scope.switched = true

  $scope.no = (volume) ->
    $scope.select(volume)

  $scope.saveVolume = (volume) ->
    volume.volumeId = volume.volumeId or ''
    volume.volumeName = volume.volumeName or ''
    localStorage.setItem("volumeId", JSON.stringify(volume.volumeId))
    if !$scope.volCreated
      $scope.select(volume)
  
  $scope.addNewVolume = (newVolume) ->
    $scope.clicked = true
    if $scope.volCreated
#      console.log($scope.createdVolume[0])
      localStorage.setItem("volumeId", JSON.stringify($scope.createdVolume[0].volumeId))
      $state.go("mia.volumeDescr", {'volumeId': $scope.createdVolume[0].volumeId, 'collectionId': $scope.collectionId})
      $scope.select(newVolume)
    else
      if !$scope.switched
        $scope.saveVolume(newVolume)
      else
        searchFactory.createAddVolume({'collectionId': $scope.collectionId, 'volumeNo': newVolume.volumeName}).then (resp) ->
#          console.log(resp)
          if resp.status == "ok"
            getVolumeQuery(newVolume.volumeName).then (createdVolume) ->
              $scope.createdVolume = createdVolume
              $scope.volCreated = true
              $scope.saveVolume($scope.createdVolume[0])
          else
            $scope.clicked = false
            alertify.alert 'Volume already exist!'
            $scope.clicked = false
            $rootScope.$broadcast("volumeNoSaved", $scope.volParams.volumeNo)

  timeout = null
  $scope.getVolume = (val) ->
    clearTimeout(timeout)
    if val && val.length >= 1
      timeout = setTimeout (->
        getVolumeQuery(val)
      ), 400
    else
      $scope.volNotFound = false
      $scope.showTable = false
      $scope.possibleVolume = []

  getVolumeQuery = (val) ->
    $scope.showTable = true
    $scope.volLoading = true
    uploadFactory.getPossibleVolume(val, $scope.collectionId, 50).then (resp) ->
      $scope.volLoading = false
      $scope.possibleVolume = $filter('filter') resp.volume, val
      possibleVolumes = angular.copy($scope.possibleVolume)
      _.forEach possibleVolumes, (volume) -> volume.volumeName = _.toLower(volume.volumeName)
      lowerVal = _.toLower(val)
      identical = _.find(possibleVolumes, { volumeName: lowerVal })
      $scope.volNotFound = _.isUndefined(identical)
      $scope.possibleVolume

  $scope.missingVolumeText = (vol) ->
    text = 'Volume number # does not exist.'
    text += ' Click the Add a New Volume button to add record.' if $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS'])
    highlightedText = '<span class="highlightedWord">' + vol + '</span>'
    text = text.replace('#', highlightedText)
    return $sce.trustAsHtml(text)