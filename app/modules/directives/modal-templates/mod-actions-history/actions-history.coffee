actionsHistoryApp = angular.module 'actionsHistoryApp', [
    'ui.router'
]

actionsHistoryApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.actionsHistory',
        url: '/actions-history'
        params: {
          recordId: ''
          recordType: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/mod-actions-history/actions-history.html'
            controller: 'actionsHistoryController'
]

actionsHistoryApp.controller 'actionsHistoryController', ($scope, $http, deFactory) ->

  recordId   = $scope.options.recordId
  recordType = $scope.options.recordType

  $scope.actionsData   = undefined
  $scope.currentAction = undefined

  $scope.pagination = {
    currentPage: 1,
    totalItems: 1,
    maxSize: 5,
    perPage: 10
  }

  initData = () ->
    $http.get('json/historyLog/getActionsHistory?recordId=' + recordId + '&recordType=' + recordType + '&page=' + $scope.pagination.currentPage + '&perPage=' + $scope.pagination.perPage).then (resp) ->
#      console.log('===getActionsHistory==>>>>', resp.data)
      $scope.actionsData = resp.data
      $scope.pagination.totalItems = $scope.actionsData.count

  initData()

  $scope.pageChange = () ->
    initData()

  $scope.actionDetails = (action) ->
#    console.log(action)
    return unless action.data
    $scope.currentAction = action

  $scope.fullActionName = (action) ->
    return action.actionType unless action.data
    if action.recordType == 'Place'
      return 'Edit details'           if action.data.hasOwnProperty('details')
      return 'Edit name and variants' if action.data.hasOwnProperty('placeNameVariant')

    if action.recordType == 'Person'
      return 'Edit details'            if action.data.hasOwnProperty('details')
      return 'Edit name variant'       if action.data.hasOwnProperty('altNames')
      return 'Edit titles/occupations' if action.data.hasOwnProperty('titleOccupation')
      return 'Edit parents'            if action.data.hasOwnProperty('parents')
      return 'Edit children'           if action.data.hasOwnProperty('children')
      return 'Edit spouse'             if action.data.hasOwnProperty('spouses')
      return 'Edit portrait'           if action.data.hasOwnProperty('portrait')

    if action.recordType == 'Volume'
      return 'Edit basic metadata'     if action.data.hasOwnProperty('basicMetadata')
      return 'Edit advanced metadata'  if action.data.hasOwnProperty('advancedMetadata')
      return 'Edit spine'              if action.data.hasOwnProperty('spine')

    if action.recordType == 'Insert'
      return 'Edit basic metadata'     if action.data.hasOwnProperty('basicMetadata')
      return 'Edit advanced metadata'  if action.data.hasOwnProperty('advancedMetadata')
      return 'Edit guardia'            if action.data.hasOwnProperty('guardia')

    if action.recordType == 'Upload'
      return 'Edit privacy'            if action.data.hasOwnProperty('upload')
      return 'Edit shared users'       if action.data.hasOwnProperty('shareAE')
      return 'Edit archive metadata'   if action.data.hasOwnProperty('metadata')
      return 'Edit files'              if action.data.hasOwnProperty('uploadFile')
      return 'Edit order file'         if action.data.hasOwnProperty('reorderFile')
      return 'Edit rotate files'       if action.data.hasOwnProperty('rotateFile')
      return 'Edit file metadata'      if action.data.hasOwnProperty('fileMetadata')
      return 'Edit file (replace)'     if action.data.hasOwnProperty('replaceFile')

    if action.recordType == 'Document'
      return 'Edit privacy'            if action.data.hasOwnProperty('documentPrivacy')
      return 'Edit shared users'       if action.data.hasOwnProperty('shareDocument')
      return 'Edit images'             if action.data.hasOwnProperty('documentImages')
      return 'Edit transcription'      if action.data.hasOwnProperty('documentTranscription')
      return 'Edit synopsis'           if action.data.hasOwnProperty('documentSynopsis')
      return 'Edit bibliographical'    if action.data.hasOwnProperty('documentBiblio')
      return 'Edit details'            if action.data.hasOwnProperty('basicDescription')
      return 'Edit people'             if action.data.hasOwnProperty('documentPeople')
      return 'Edit places'             if action.data.hasOwnProperty('documentPlaces')
      return 'Edit topics'             if action.data.hasOwnProperty('documentTopics')


  $scope.backToActions = () ->
    $scope.currentAction = undefined

  $scope.closeModal = () ->
    $scope.actionsData   = undefined
    $scope.currentAction = undefined
    $scope.cancel()

  $scope.months = [
    {'id': 1, 'name':'January'},
    {'id': 2, 'name':'February'},
    {'id': 3, 'name':'March'},
    {'id': 4, 'name':'April'},
    {'id': 5, 'name':'May'},
    {'id': 6, 'name':'June'},
    {'id': 7, 'name':'July'},
    {'id': 8, 'name':'August'},
    {'id': 9, 'name':'September'},
    {'id': 10, 'name':'October'},
    {'id': 11, 'name':'November'},
    {'id': 12, 'name':'December'}
  ]

  $scope.getMonthName = (monthId) ->
    month = _.find($scope.months, { 'id': _.toInteger(monthId) })
    if month then month.name else monthId

  ###############
  # Places
  ###############

  $scope.befBornPlaceName  = ''
  $scope.aftBornPlaceName  = ''
  $scope.befDeathPlaceName = ''
  $scope.aftDeathPlaceName = ''

  $scope.getPlaceName = (placeId, type, change) ->
    return if placeId == null
    deFactory.getPlaceById(placeId).then (resp)->
      if resp.status == 'ok'
        if type == 'birth'
          $scope.befBornPlaceName = resp.data.placeName if change == 'before'
          $scope.aftBornPlaceName = resp.data.placeName if change == 'after'
        else
          $scope.befDeathPlaceName = resp.data.placeName if change == 'before'
          $scope.aftDeathPlaceName = resp.data.placeName if change == 'before'

  ###############
  # DE
  ###############

  $scope.allLanguages = (languages) ->
    return unless languages
    allLanguages = []
    _.forEach languages, (obj) -> allLanguages.push(obj.language)
    allLanguages.join(', ')

