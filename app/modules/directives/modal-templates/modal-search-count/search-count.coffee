searchCountApp = angular.module 'searchCountApp', [

]

searchCountApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.searchCount',
        url: '/search-count'
        params: {
          queryString: ''
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-search-count/search-count.html'
            controller: 'searchCountController'
]

searchCountApp.controller 'searchCountController',(searchFactory, $scope, $rootScope) ->

  $scope.count =
    archivalEntities: 0
    people: 0
    places: 0
    synopses: 0
    transcriptions: 0

  $scope.loadCount = false

  getSearchCounts = () ->
    $scope.loadCount = true
    searchFactory.basicSearch($scope.options.queryString).then((resp) ->
#      console.log resp
      $scope.loadCount = false
      $scope.count = resp.data if resp.status == 'ok'
    )

  getSearchCounts()

  $scope.selectSearch = (searchType)->
    switch searchType
      when "ae"
        return if $scope.count.archivalEntities == 0
        wordSearchAe($scope.options.queryString)
        $scope.select('scelta')
      when "syn"
        return if $scope.count.synopses == 0
        wordSearchDe($scope.options.queryString, 'DE_SYNOPSIS')
        $scope.select('scelta')
      when "tra"
        return if $scope.count.transcriptions == 0
        wordSearchDe($scope.options.queryString, 'DE_TRANSCRIPTION')
        $scope.select('scelta')
      when "peo"
        return if $scope.count.people == 0
        searchPeople($scope.options.queryString)
        $scope.select('scelta')
      when "pla"
        return if $scope.count.places == 0
        searchPlace($scope.options.queryString)
        $scope.select('scelta')
#    $scope.select('scelta')

  wordSearchAe = (query)->
    params = {}
    params.owner = $rootScope.me.account
    params.partialSearchWords = query
    params.searchType = 'AE_NAME_AND_DESCRIPTION'
    params.onlyMyArchivalEnties = false
    params.everyoneElsesArchivalEntities = false
    params.allArchivalEntities = true
    params.partialSearchWords = ''
    params.matchSearchWords = ''

    searchWord                = parseSearchWords(query)
    params.matchSearchWords   = searchWord.matchSearchWords
    params.partialSearchWords = searchWord.partialSearchWords

    $rootScope.$broadcast('start-selected-search')
    searchFactory.searchAeWord(params).then (resp)->
      newResult = {}
#      newResult.searchName = angular.copy(params.partialSearchWords)
      newResult.searchName = query
      place = 'All Images'
      whereS = 'titles and descriptions'
      newResult.content = {
        searchType: newResult.searchName,
        where: place,
        where2: whereS,
        title: 'Images',
        matchSearchWords: params.matchSearchWords,
        partialSearchWords: params.partialSearchWords,
        searchIn: 'Name and Description',
#        countResult: $scope.count.archivalEntities || 0,
        countResult: resp.data?.count || 0,
        params: params
      }
      newResult.type='ae'
      newResult.subtype='simple_search'
      
      newResult.results=resp.data.aentities
      $rootScope.$broadcast('new-result', newResult)

  wordSearchDe = (query, where)->
    params = {}
    params.owner = $rootScope.me.account
#    params.partialSearchWords = query
    params.searchType = where
    params.onlyMyEntities = false
    params.everyoneElsesEntities = false
    params.allEntities = true
    params.partialSearchWords = ''
    params.matchSearchWords = ''

    searchWord                = parseSearchWords(query)
    params.matchSearchWords   = searchWord.matchSearchWords
    params.partialSearchWords = searchWord.partialSearchWords

    $rootScope.$broadcast('start-selected-search')
    searchFactory.searchDocWord(params).then (resp)->
      #console.log('resp', resp.data)
      newResult = {}
#      newResult.searchName = angular.copy(params.partialSearchWords)
      newResult.searchName = query
      place = 'in <b>all documents</b>'
      whereS = 'In <b>transcription</b>' if where == 'DE_TRANSCRIPTION'
      whereS = 'In <b>synopsis</b>' if where == 'DE_SYNOPSIS'
      newResult.searchTooltip = 'You have searched <b>' + newResult.searchName + '</b> for ' + whereS + ' ' + place
      newResult.type='doc'
      newResult.subtype='simple_search'
      newResult.content = {
        searchType: newResult.searchName,
        where: place,
        where2: whereS,
        title: 'Documents',
        type: where,
        matchSearchWords: params.matchSearchWords,
        partialSearchWords: params.partialSearchWords,
        searchIn: if where == 'DE_TRANSCRIPTION' then 'All Transcriptions' else 'All Synopses',
        countResult: resp.data?.count || 0,
        params: params
      }
      newResult.results = resp.data.docs
      $rootScope.$broadcast('new-result', newResult)

  searchPeople = (queryString) ->
    params =
      "searchType":[
        "ALL_NAME_TYPES"
      ]

    params.partialSearchWords = ''
    params.matchSearchWords = ''

    searchWord                = parseSearchWords(queryString)
    params.matchSearchWords   = searchWord.matchSearchWords
    params.partialSearchWords = searchWord.partialSearchWords

    $rootScope.$broadcast('start-selected-search')
    searchFactory.searchPeopleWord(params).then (resp)->
      newResult = {}
      newResult.searchName = queryString
      newResult.searchTooltip = 'You have searched <b>' + newResult.searchName + '</b> in people.'
      newResult.type='people'
      newResult.subtype='simple_search'
      newResult.content = {
        searchType: newResult.searchName,
        title: 'Biographical Records',
        matchSearchWords: params.matchSearchWords,
        partialSearchWords: params.partialSearchWords,
        searchIn: 'All Name Types',
        countResult: $scope.count.people || 0,
        params: params
      }
#      console.log resp.data
      newResult.results=resp.data.data
      $rootScope.$broadcast('new-result', newResult)

  searchPlace = (queryString) ->
    params =
      "placeType":null

    params.partialSearchWords = ''
    params.matchSearchWords = ''

    searchWord                = parseSearchWords(queryString)
    params.matchSearchWords   = searchWord.matchSearchWords
    params.partialSearchWords = searchWord.partialSearchWords

    $rootScope.$broadcast('start-selected-search')
    searchFactory.searchPlaceWord2(params).then (resp)->
      newResult = {}
      newResult.searchName = queryString
      newResult.searchTooltip = 'You have searched <b>' + newResult.searchName + '</b> in places.'
      newResult.type='place'
      newResult.subtype='simple_search'
      newResult.content = {
        searchType: newResult.searchName,
        title: 'Geographical Records',
        matchSearchWords: params.matchSearchWords,
        partialSearchWords: params.partialSearchWords,
        searchIn: 'All Place Names',
        countResult: $scope.count.places || 0,
        params: params
      }
#      console.log resp.data
      newResult.results=resp.data.data
      $rootScope.$broadcast('new-result', newResult)

  parseSearchWords = (query) ->
    matchSearchWords   = ''
    partialSearchWords = ''

    tokens = [].concat.apply [], query.split('"').map (v, i) ->
      return if i%2 then '"' + v + '"' else v.split(' ')
    .filter(Boolean)

    tokens.forEach (token) ->
      if token.charAt(0) == '"'
        matchSearchWords += token.substr(1, token.length - 2) + ' '
      else
        partialSearchWords += token + ' '

    if matchSearchWords != ''
      matchSearchWords = matchSearchWords.substr(0, matchSearchWords.length - 1)

    if partialSearchWords != ''
      partialSearchWords = partialSearchWords.substr(0, partialSearchWords.length - 1)

    return { matchSearchWords: matchSearchWords, partialSearchWords: partialSearchWords }