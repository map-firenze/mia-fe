rotateImgApp = angular.module 'rotateImgApp', ['modalInstance', 'ui.bootstrap']

rotateImgApp.controller 'rotateImgController', ($scope, commonsFactory, $state) ->
  $scope.images = $scope.options.uploadFiles
  $scope.degrees = $scope.options.degrees
  $scope.loading = false
  $scope.progress = 0
  $scope.image = {}
  $scope.timer = ''
  $scope.random = Math.random()
  if Object.keys($scope.images).length > 0
    $scope.image = $scope.images[Object.keys($scope.images)[0]]

  $scope.confirm = () ->
    $scope.loading = true
    $scope.start()
    rotationType = 'left' if $scope.degrees is 270
    rotationType = 'right' if $scope.degrees is 90
    rotationType = 'flip' if $scope.degrees is 180
    if Object.keys($scope.images).length > 0
      commonsFactory.rotate(Object.keys($scope.images), rotationType).then (resp) ->
#        console.log(resp)
        $scope.complete()
        $scope.loading = false
        if resp.status is 'ok'
          $state.reload()
          $scope.cancel()

  $scope.start = () ->
    $scope.timerId = setInterval ( ->
      remaining = 100 - $scope.progress
      $scope.progress = Math.trunc($scope.progress + 0.15 * (1 - Math.sqrt(remaining)) ** 2)
      $scope.$apply()
    ), 200

  $scope.complete = () ->
    clearInterval($scope.timerId)
    $scope.progress = 100

  