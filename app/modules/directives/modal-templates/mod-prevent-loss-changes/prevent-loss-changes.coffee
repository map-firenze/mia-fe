preventLossChangesApp = angular.module 'preventLossChangesApp', [
    'commonServiceApp'
    'translatorApp'
    'ui.router',
]

preventLossChangesApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.preventLossChanges',
        url: '/prevent-loss-changes'
        params: {
          isBeingEdit: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/mod-prevent-loss-changes/prevent-loss-changes.html'
            controller: 'preventLossChangesController'
]

preventLossChangesApp.controller 'preventLossChangesController', ($scope, $http, $translate, $filter) ->
  $scope.isBeingEdit = $scope.options.isBeingEdit

  $scope.ignoreChanges = () ->
    $scope.select(true)

  $scope.backToEdit = () ->
    $scope.select(false)
  



