addSeriesApp = angular.module 'addSeriesApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

addSeriesApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addSeries',
        url: '/add-series'
        params: {
          parentId: ""
          fromSearch: ''
          
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-series/add-series.html'
            controller: 'addSeriesController'
]

addSeriesApp.controller 'addSeriesController',(uploadFactory, $scope, $translate, $filter, $log) ->
  $scope.addNew = false
  $scope.showTable=false
  $scope.collectionId = $scope.options.parentId
  $scope.fromSearch = $scope.options.fromSearch
  

  $scope.switchNew = ()->
    $scope.addNew = !$scope.addNew

  $scope.saveSeries = (series)->
    series.seriesId = series.seriesId or ''
    series.seriesName = series.seriesName or ''
    series.seriesSubtitle = series.seriesSubtitle or ''
    $scope.select(series)

  $scope.getSeries = (val) ->
    if val
      if val.length>=1
        $scope.showTable=true
        uploadFactory.getPossibleSeries(val, $scope.collectionId).then (resp) ->
          $scope.possibleSeries = $filter('filter') resp.series, val
