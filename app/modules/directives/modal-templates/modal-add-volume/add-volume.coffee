addVolumeApp = angular.module 'addVolumeApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
    'ngAlertify'
]

addVolumeApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addVolume',
        url: '/add-volume'
        params: {
          parentId: ""
          fromSearch: ''
          
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-volume/add-volume.html'
            controller: 'addVolumeController'
]

addVolumeApp.controller 'addVolumeController',(uploadFactory, $rootScope, searchFactory, $scope, $translate, $filter, $log, alertify, $sce) ->
  $scope.addNew = false
  $scope.showTable = false
  $scope.switched = false
  $scope.collectionId = $scope.options.parentId
  $scope.fromSearch = $scope.options.fromSearch
  $scope.volParams = {}
  $scope.volNotFound = false
  $scope.volLoading = false
  $scope.possibleVolume = []

  $scope.switchNew = () ->
    $scope.addNew = !$scope.addNew
    $scope.switched = true

  $scope.saveVolume = (volume) ->
    volume.volumeId = volume.volumeId or ''
    volume.volumeName = volume.volumeName or ''
    $scope.select(volume)
  
  $scope.addNewVolume = (newVolume) ->
    if !$scope.switched
      $scope.saveVolume(newVolume)
    else
      $scope.volParams = {
        "collectionId": $scope.collectionId
        "volumeNo": newVolume.volumeName
        }
      searchFactory.createAddVolume($scope.volParams).then (resp) ->
        if resp.status == "ok" && resp.data
          newVolume.volumeId = resp.data.volumeId
          $scope.saveVolume(newVolume)
        else
          alertify.alert 'Volume already exist!'
          $rootScope.$broadcast("volumeNoSaved", $scope.volParams.volumeNo)

#  $scope.getVolume = (val) ->
#    if val
#      if val.length >= 1
#        $scope.showTable = true
#        uploadFactory.getPossibleVolume(val, $scope.collectionId).then (resp) ->
#          $scope.possibleVolume = $filter('filter') resp.volume, val

  timeout = null
  $scope.getVolume = (val) ->
    clearTimeout(timeout)
    if val && val.length >= 1
      timeout = setTimeout (->
        getVolumeQuery(val)
      ), 400
    else
      $scope.volNotFound = false
      $scope.showTable = false
      $scope.possibleVolume = []

  getVolumeQuery = (val) ->
    $scope.showTable = true
    $scope.volLoading = true
    uploadFactory.getPossibleVolume(val, $scope.collectionId, 50).then (resp) ->
      $scope.volLoading = false
      $scope.possibleVolume = $filter('filter') resp.volume, val
      possibleVolumes = angular.copy($scope.possibleVolume)
      _.forEach possibleVolumes, (volume) -> volume.volumeName = _.toLower(volume.volumeName)
      lowerVal = _.toLower(val)
      identical = _.find(possibleVolumes, { volumeName: lowerVal })
      $scope.volNotFound = _.isUndefined(identical)
      $scope.possibleVolume

  $scope.missingVolumeText = (vol) ->
    text = 'Volume number # does not exist.'
    text += ' Click the Add volume button to add one' if $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS'])
    highlightedText = '<span class="highlightedWord">' + vol + '</span>'
    text = text.replace('#', highlightedText)
    return $sce.trustAsHtml(text)