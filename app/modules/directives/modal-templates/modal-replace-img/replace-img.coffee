replaceImgApp = angular.module 'replaceImgApp', ['modalInstance']

replaceImgApp.controller 'replaceImgController', ($scope, $rootScope, $state, $sce, Upload) ->
  $scope.uploadFinished = true
  $scope.imageToReplace = $scope.options.uploadFileId

  $scope.viewerAddress = $sce.trustAsResourceUrl("/Mia/json/imagePreview/image/"+$scope.imageToReplace)

  # $scope.getFiles = () ->
  #   document.getElementById("file-replace").click()

  document.getElementById("file-replace").onchange = () ->
  # multiple file upload
  $scope.uploadFiles = (file) ->
#    console.log file
#    console.log $scope.imageToReplace
    $scope.uploading = true
    $scope.uploadFinished = false
    $scope.progress = 0
    fileToUpload=[file]
    if file
      $rootScope.isBusy = true
      Upload.upload(
        url: 'json/archive/replaceFileInAE/'+$state.params.aeId+'/'+$scope.imageToReplace
        data:
          files: file).then ((response) ->
            #file uploaded ok
            $scope.result = response.data
            $scope.uploading = false
            $rootScope.isBusy = false
            $scope.uploadFinished = true
            $state.reload()
            $scope.cancel()
          ), ((response) ->
            if response.status > 0
              $scope.errorMsg = response.status + ': ' + response.data
          ), (evt) ->
            #file uploaded progress
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
            $rootScope.$broadcast('progress', $scope.progress)

  $scope.uploadDone = () ->
    $scope.picFiles = []
    $scope.progress = 0
    $rootScope.$broadcast('upload-finished')