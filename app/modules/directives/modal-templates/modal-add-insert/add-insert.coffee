addInsertApp = angular.module 'addInsertApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

addInsertApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addInsert',
        url: '/add-insert'
        params: {
          parentId: ""
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-add-insert/add-insert.html'
            controller: 'addInsertController'
]

addInsertApp.controller 'addInsertController', (uploadFactory, searchFactory, alertify, $rootScope, $scope, $translate, $filter, $sce) ->
  $scope.addNew = false
  $scope.showTable = false
  $scope.fromSearch = $scope.options.fromSearch
  $scope.volumeId = $scope.options.parentId
  $scope.volumeName = $scope.options.parentName
  $scope.collection = $scope.options.collection
  $scope.collectionId = $scope.options.collection.collectionId
  $scope.switched = false
  $scope.insParams = {}
  $scope.insNotFound = false
  $scope.insLoading = false
  $scope.possibleInsert = []
  
  $scope.switchNew = () ->
    $scope.addNew = !$scope.addNew
    $scope.switched = true

  $scope.saveInsert = (insert) ->
    insert.insertId = insert.insertId or ''
    insert.insertName = insert.insertName or ''
    $scope.select(insert)

  $scope.addNewInsert = (newInsert) ->
    if !$scope.switched
      $scope.saveInsert(newInsert)
    else
      $scope.insParams = {
        "collectionId": $scope.collectionId
        "volumeNo": $scope.options.parentName
        "insertNo": $scope.newInsert.insertName
      }
      searchFactory.createAddInsert($scope.insParams).then (resp) ->
        if resp.status == "ok"
          $scope.saveInsert(newInsert)
        else
          errorMessage = resp.message || 'Cannot create insert.'
          alertify.alert errorMessage
          $rootScope.$broadcast("insertNoSaved", $scope.insParams.insertNo)


  $scope.getInsert = (val) ->
    if val && val.length >= 1
      $scope.showTable = true
      $scope.insLoading = true
      uploadFactory.getPossibleInsert(val, $scope.volumeId).then (resp) ->
        $scope.insLoading = false
        $scope.possibleInsert = $filter('filter') resp.insert, val
        identical = _.find($scope.possibleInsert, { insertName: val })
        $scope.insNotFound = _.isUndefined(identical)
        $scope.possibleInsert
    else
      $scope.insNotFound = false
      $scope.showTable = false
      $scope.possibleInsert = []

  $scope.missingInsertText = (ins) ->
    text = 'Insert number # does not exist.'
    text += ' Click the Add insert button to add one' if $rootScope.secAuthorize(['ADMINISTRATORS','ONSITE_FELLOWS']) || $scope.collection.studyCollection
    highlightedText = '<span class="highlightedWord">' + ins + '</span>'
    text = text.replace('#', highlightedText)
    return $sce.trustAsHtml(text)