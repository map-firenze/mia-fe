changePass = angular.module 'changePassApp', [
    'personalProfileService'
    'modalInstance'
    'toaster'
   ]

changePass.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.changePass',
        url: '/change-pass'
        views:
          'content':
            controller: 'changePassController'
]
changePass.controller 'changePassController', (personalProfileFactory, $scope, $log, toaster) ->
  $scope.oldPass = undefined
  $scope.newPass = undefined
  $scope.confirmNewPass = undefined
  $scope.errorEqual = false
  $scope.errorOld = false
  $scope.success = false

  $scope.comparePass=()->
    if $scope.newPass == $scope.confirmNewPass
      $scope.errorEqual = false
    else
      $scope.errorEqual = true

  $scope.resetError = () ->
    $scope.errorEqual = false
    $scope.errorOld = false

  $scope.save = () ->
    params = {}
    params.account = modalInstance.account
    params.oldPassword = $scope.oldPass
    params.newPassword = $scope.newPass
    params.newPasswordConfirm = $scope.confirmNewPass
    $scope.comparePass()
    if !$scope.errorEqual
      personalProfileFactory.changePass(params).then (resp)->
        if resp.status == "ok"
          $scope.resetError()
          $scope.success = true
          toaster.pop('success', 'Password', 'Your password has been successfully updated')
          $scope.cancel()
          
        if resp.status == "koPwd"
          $scope.errorOld = true
