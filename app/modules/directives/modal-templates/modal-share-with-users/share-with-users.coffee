shareWithUsersApp = angular.module 'shareWithUsersApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'volumeDescrApp'
    'searchServiceApp'
    'ui.bootstrap'
    'commonServiceApp'
    'toaster'
]

shareWithUsersApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.shareWithUsers',
        url: '/share-with-users'
        params: {
          parentId: ""
          fromSearch: ''
          
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-share-with-users/share-with-users.html'
            controller: 'shareWithUsersController'
]

shareWithUsersApp.controller 'shareWithUsersController', (commonsFactory, searchDeService, $scope, $filter, csFactory, toaster) ->
  $scope.imageId = $scope.options.imageId
  $scope.archiveId = $scope.options.archiveId
  $scope.docId = $scope.options.docId
  $scope.caseStudy = $scope.options.caseStudy
  $scope.showTable = false
  $scope.tempName = ""
  $scope.listSelectedUser = []
  $scope.listAccountUsers = []

  if $scope.imageId && $scope.archiveId
    commonsFactory.getSharedUsersByImageId($scope.archiveId, $scope.imageId).then (resp)->
      if resp.data
        $scope.docId = null
        $scope.listSelectedUser = resp.data
        for user in $scope.listSelectedUser
          $scope.listAccountUsers.push user.account
  else if $scope.archiveId && !$scope.docId
    commonsFactory.getSharedUsersByArchiveId($scope.archiveId).then (resp)->
      if resp.data
        $scope.listSelectedUser = resp.data
        $scope.docId = null
        $scope.imageId = null
        for user in $scope.listSelectedUser
          $scope.listAccountUsers.push user.account
  else if $scope.docId
    commonsFactory.getSharedUsersByDocumentId($scope.docId).then (resp)->
      if resp.data
        $scope.listSelectedUser = resp.data
        $scope.imageId = null
        $scope.archiveId = null
        for user in $scope.listSelectedUser
          $scope.listAccountUsers.push user.account
  else if $scope.caseStudy
    csFactory.getCsSharedUsers($scope.caseStudy.id).then (resp) ->
      if resp.status == 'success' && resp.data.length
        $scope.listSelectedUser = resp.data
        $scope.imageId = null
        $scope.archiveId = null
        $scope.docId = null
        for user in $scope.listSelectedUser
          $scope.listAccountUsers.push user.account
      else
        toaster.pop('error', resp.message)
        $scope.select()

  $scope.getUserByName = (searchQuery) ->
    if searchQuery
      if searchQuery.length < 1
        $scope.loading = false
      if searchQuery.length >= 1
        $scope.loading = true
        $scope.showTable = true
        searchDeService.getUserByName(searchQuery).then (resp)->
          if resp.length
            $scope.users = resp
          else
            $scope.users = null
          $scope.loading = false

  $scope.closeTable = () ->
    $scope.showTable = false
  
  $scope.addUser = (user) ->
    $scope.isPresent = _.find $scope.listSelectedUser, account: user.account
    if !$scope.isPresent
      $scope.listSelectedUser.push user
      $scope.listAccountUsers.push user.account
      $scope.tempOwnerName = ""
      $scope.showTable = false
    else
      toaster.pop('error', 'User already added')

  $scope.removeUser = (index) ->
    $scope.listSelectedUser.splice(index, 1)
    $scope.listAccountUsers.splice(index, 1)

  $scope.saveSharedUsers = () ->
    if $scope.imageId && $scope.archiveId
      commonsFactory.saveSharedUsers($scope.archiveId, $scope.imageId, $scope.listAccountUsers).then (resp) ->
      $scope.select()
    else if $scope.archiveId
      commonsFactory.shareWithUsersByArchivial($scope.archiveId, $scope.listAccountUsers).then (resp) ->
        $scope.select()
    else if $scope.docId
      commonsFactory.shareWithUsersByDocument($scope.docId, $scope.listAccountUsers, true).then (resp) ->
        $scope.select()
    else if $scope.caseStudy
      csFactory.addUsersToCs($scope.caseStudy.id, $scope.listAccountUsers).then (resp) ->
        toaster.pop('error', resp.message) if resp.status == 'error'
        $scope.select()

