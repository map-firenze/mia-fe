uploadCVApp = angular.module 'uploadCVApp', [
    'modalInstance'
    'personalProfileService'
    'ngFileUpload'
    'translatorAppEn'
    'translatorApp'
   ]

uploadCVApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.uploadCV',
        url: '/upload-cv'
        views:
          'content':
            controller: 'uploadCVController'
]
uploadCVApp.controller 'uploadCVController', (personalProfileFactory, Upload, $http, $scope, $rootScope, $log, $timeout) ->

  $scope.cvFile = undefined
  $scope.success = false

  $scope.remove=() ->
    $scope.cvFile = undefined

  $scope.uploadDone=()->
    $scope.success = true
    $scope.cvFile = undefined
    $scope.progress = 0

  $scope.closeCV=()->
    personalProfileFactory.getUser().then (resp)->
      if resp.data
        $scope.select(resp.data)

  $scope.upload = (fileData) ->
    blob = personalProfileFactory.datatoBlob(fileData, "CV", null, "application/pdf")
    file = []
    file.push(blob)
    Upload.upload(
      url: 'json/userprofile/uploadCV'
      data:
        files: file).then ((response) ->
          $scope.result = response.data
          if $scope.result.status = "ok"
            $scope.uploadDone()
          ), ((response)->
            if response.status > 0
              $scope.errorMsg = response.status + ': ' + response.data
          ), (evt) ->
            $scope.progress = Math.min(100,parseInt(100.0 * evt.loaded / evt.total))
            $rootScope.$broadcast('progress', $scope.progress)
            if $scope.progress == 100
              $rootScope.$broadcast('upload-finished')


  handleFileSelect = (evt) ->
    file = evt.currentTarget.files[0]
    reader = new FileReader
    reader.onload = (evt) ->
      $scope.$apply ($scope) ->
        $scope.cvFile = evt.target.result
    reader.readAsDataURL file

  angular.element(document.querySelector('#fileInput')).on 'change', handleFileSelect
  return
