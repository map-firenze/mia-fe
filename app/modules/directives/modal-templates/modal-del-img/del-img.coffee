delImgApp = angular.module 'delImgApp', ['modalInstance']

delImgApp.controller 'delImgController', ($scope, $state, deFactory) ->
  documents = $scope.options.documents.documentIds
  $scope.documentsToDel = []
  angular.forEach(documents, (doc)->
    deFactory.getDocumentEntityById(doc).then (resp)->
      $scope.documentsToDel.push(resp.data.documentEntity)
  )
  $scope.open = (docId) ->
    params = {'docId': docId}
    $state.go('mia.modifyDocEnt', params)
    $scope.cancel()