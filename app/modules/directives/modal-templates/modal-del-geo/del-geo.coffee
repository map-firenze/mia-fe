delGeoApp = angular.module 'delGeoApp', ['modalInstance']

delGeoApp.controller 'delGeoController', ($scope, $state, deFactory, bioPeopleFactory) ->
  $scope.documentsToDel = []
  $scope.peopleToDel = []
  documents = $scope.options.documents.documentEntities
  people = $scope.options.documents.peopleRecords
  if $scope.options.documents.documentEntities
    angular.forEach(documents, (doc)->
      deFactory.getDocumentEntityById(doc).then (resp)->
        $scope.documentsToDel.push(resp.data.documentEntity)
    )
  if $scope.options.documents.peopleRecords
    angular.forEach(people, (doc)->
      bioPeopleFactory.getPerson(doc).then (resp)->
        $scope.peopleToDel.push(resp.data)
    )
#  console.log($scope.peopleToDel)
#  console.log($scope.documentsToDel)

  $scope.openDoc = (docId) ->
    params = {'docId': docId}
    $state.go('mia.modifyDocEnt', params)
    $scope.cancel()

  $scope.openPeople = (peopleId) ->
    params = {'id': peopleId}
    $state.go('mia.bio-people', params)
    $scope.cancel()