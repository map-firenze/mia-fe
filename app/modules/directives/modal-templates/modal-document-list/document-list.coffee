selectDeApp = angular.module 'selectDeApp', [
    'translatorApp'
    'ngAutocomplete'
    'deServiceApp'
    'ui.router'
]

selectDeApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.selectde',
        url: '/select-de'
        params: {
          uploadFileId: ""
        }
        views:
          'content':
            templateUrl: 'modules/doc-ent/modal-select-de/select-de.html'
            controller: 'selectDeController'
]

selectDeApp.controller 'selectDeController',(deFactory, $scope, $translate, $filter, $log) ->
  $scope.docAePresent = false
  uploadFileId = $scope.options.uploadFileId
  deFactory.getDocumentListByFileId(uploadFileId).then (resp)->
    if resp.data
      $scope.docAePresent = true
      $scope.documents = resp.data.documentEntities

  $scope.chooseDe = (documentEntity)->
    $scope.select(documentEntity)
