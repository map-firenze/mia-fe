addToCsApp = angular.module 'addToCsApp', [
  'toaster'
]

addToCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addToCs',
        url: '/add-to-cs'
        params: {
          recordType: ''
          recordId: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-to-cs/add-to-cs.html'
            controller: 'addToCsController'
]

addToCsApp.controller 'addToCsController', ($scope, $rootScope, csFactory, toaster) ->

  recordType  = $scope.options.recordType
  recordId    = $scope.options.recordId

  $scope.csStep = 'selectCs' # selectCs/selectCsFolder

  $scope.myCaseStudies  = []

  $scope.selectedCs     = undefined
  $scope.selectedFolder = undefined
  $scope.errorMessage   = undefined

  $scope.csLoading      = false

  getCaseStudies = () ->
    csFactory.getCsList().then (resp) ->
      $scope.myCaseStudies = resp.data

  getCaseStudies()

  $scope.selectCs = (cs) ->
    $scope.selectedCs = cs
    $scope.csStep = 'selectCsFolder'
    $scope.csLoading = true
    csFactory.getCsFolders(cs.id).then (resp) ->
      $scope.csLoading = false
      if resp.status == 'success'
        $scope.selectedCs.folders = resp.data
      else
        $scope.selectedCs.folders = []

  $scope.selectCsFolder = (folder) ->
    $scope.selectedFolder = folder

  $scope.addToCsFolder = () ->
    data = {entityType: recordType, entityId: recordId}
    csFactory.addToCsFolder($scope.selectedCs.id, $scope.selectedFolder.id, data).then (resp) ->
      if resp.status == 'success'
        $scope.errorMessage = undefined
        $scope.cancel()
        toaster.pop('success', 'Successfully added')
        data          = {}
        data.csId     = $scope.selectedCs.id
        data.folderId = $scope.selectedFolder.id
        data.item     = resp.data
        $rootScope.$broadcast('addToCsFolder', data)
      else
        $scope.errorMessage = resp.message

  $scope.backToPrevStep = () ->
    $scope.csStep         = 'selectCs'
    $scope.selectedCs     = undefined
    $scope.selectedFolder = undefined
    $scope.errorMessage   = undefined

