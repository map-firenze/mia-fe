createAddVolumeApp = angular.module 'createAddVolumeApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

createAddVolumeApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.createAddVolume',
        url: '/create-add-volume'
        params: {
          parentId: ""
          fromSearch: ''
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-create-add-volume/modal-create-add-volume.html'
            controller: 'createAddVolumeController'
]

createAddVolumeApp.controller 'createAddVolumeController',(uploadFactory, searchFactory, $scope, $rootScope, $translate, $filter, $log) ->
  $scope.showTable=false

  $scope.saveVolume = ()->
    $scope.volParams = {
      "collectionId": $scope.options.parentId
      "volumeNo": $scope.newVolume.volumeName
    }
    searchFactory.createAddVolume($scope.volParams).then (resp) ->
#      console.log resp
      $rootScope.$broadcast("volumeNoSaved", $scope.volParams.volumeNo)
      # if resp and resp.status is 'ok'
      #   $scope.cancel()
      # else
      #   console.log 'error occured'

  $scope.getVolume = (val) ->
    if val
      if val.length>=1
        $scope.showTable=true
        searchFactory.findVolumeDescription(val).then (resp) ->
          $scope.possibleVolume = $filter('filter') resp.volume, val
