diffVersImageApp = angular.module 'diffVersImageApp', [
  'modalInstance'
]

diffVersImageApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.diffVersImage',
        url: '/different-versions-of-image'
        views:
          'content':
            controller: 'diffVersImageController'
]
diffVersImageApp.controller 'diffVersImageController', ($scope, $rootScope, $state) ->
  $scope.image = $scope.options.image
  $scope.selectedImageId = $scope.$stateParams.fileId

  $scope.goToSingle = (image) ->
    params = {}
    params.aeId = image.uploadId
    params.fileId = image.uploadFileId
#    params.page = 1
    $scope.cancel()
    $state.go 'mia.singleupload', params

  $scope.openInViewer = (image) ->
    $rootScope.$broadcast('openInViewer', image)
    $scope.cancel()

  $scope.editMetadata = (image) ->
    $rootScope.$broadcast('editMetaDaraDiffVers', image.uploadFileId)

