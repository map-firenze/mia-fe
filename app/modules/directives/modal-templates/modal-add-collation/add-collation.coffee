addCollationApp = angular.module 'addCollationApp', [
  'ui.router'
  'ui.sortable'
  'toaster'
]

addCollationApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addCollation',
        url: '/add-collation'
        params: {
          document: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-collation/add-collation.html'
            controller: 'addCollationController'
]

addCollationApp.controller 'addCollationController', ($scope, $rootScope, $state, $filter, searchDeService, uploadFactory, deFactory, volumeInsertDescrFactory, $http, $timeout, toaster, messagingFactory) ->

  $scope.sourceDoc         = $scope.options.document
  $scope.firstImageSD      = $scope.sourceDoc.uploadFiles[0]
  $scope.sourceList        = [$scope.firstImageSD]
  $scope.bacImages         = []
  $scope.searchBacImages   = []
  $scope.sourceCollations  = []
  $scope.selectedSearchImg = undefined
  $scope.firstBacImage     = undefined
  $scope.totalBacImages    = undefined


  $scope.step         = 'checkExistDoc' # checkExistDoc/selectCollType/selectCollection/selectVolume/selectInsert/selectDE/dragDE
  $scope.lacunaPhase  = 'dragAndDrop'   # dragAndDrop/clickFolio

  $http.get('json/collation/findInDocumentImages/' + $scope.sourceDoc.documentId).then (resp) ->
    if resp.status == 200
      if resp.data.data.length
        $scope.sourceCollations = resp.data.data
      else
        $scope.step = 'selectCollType'

  $scope.collationTypes = [
    {type: 'lacuna',      name: 'REINSERT: Place the document back into its original location'},
    {type: 'attachments', name: 'REATTACH: Reunite the document with its original attachment(s)'}
  ]

  $scope.selectedCollation  = undefined
  $scope.selectedCollection = undefined
  $scope.selectedVolume     = undefined
  $scope.selectedInsert     = undefined
  $scope.selectedDocument   = undefined
  $scope.errorMessage       = undefined
  $scope.prevImageData      = undefined

  $scope.collections        = []
  $scope.possibleVolumes    = []
  $scope.possibleInserts    = []
  $scope.attachDocuments    = []

  $scope.showVolTable       = false
  $scope.showInsTable       = false
  $scope.showDocTable       = false
  $scope.loading            = false
  $scope.sourceMoved        = false
  $scope.canSaveLacuna      = false
  $scope.searchedByFolio    = false
  $scope.searchedWithMatch  = true

  initPagination = () ->
    $scope.pagination = {
      currentPage: 1,
      totalItems: 1,
      maxSize: 5,
      perPage: 10
    }

  initPagination()

  initUnsureData = () ->
    $scope.unsureData =
      unsure: false
      description: null

  initUnsureData()

  initSearchData = () ->
    $scope.searchCollation =
      de: ''
      folio: ''
      match: false

  initSearchData()

  $scope.goToCollationOrSource = (collation) ->
    if collation.type == 'ATTACHMENT'
      $state.go 'mia.docAttColl', {collId: collation.id}
    else
      $state.go 'mia.modifyDocEnt', {docId: collation.childDocument.documentId}
    $scope.closeModal()

  $scope.selectCollation = (collation) ->
    $scope.selectedCollation = collation.type
    getCollections()

  getCollections = () ->
    $scope.step = 'selectCollection'
    unless $scope.collections.length
      $scope.loading = true
      searchDeService.getCollection(1).then (resp) ->
        $scope.collections = resp
        $scope.loading = false

  $scope.selectCollection = (collection) ->
    $scope.step = 'selectVolume'
    $scope.showVolTable = false
    $scope.selectedCollection = collection

  $scope.getVolume = (searchQuery) ->
    $scope.showVolTable = true
    $scope.loading = true
    if searchQuery
      if searchQuery.length >= 1
        if $scope.selectedCollation == 'attachments'
          uploadFactory.getVolumesWithDE(searchQuery, $scope.selectedCollection.collectionId).then (resp) ->
            $scope.possibleVolumes = $filter('filter') resp.volume, searchQuery
            $scope.loading = false
        else
          uploadFactory.getVolumesWithAE(searchQuery, $scope.selectedCollection.collectionId).then (resp) ->
            $scope.possibleVolumes = $filter('filter') resp.volume, searchQuery
            $scope.loading = false
    else
      $scope.possibleVolumes = []
      $scope.loading = false

  $scope.getInsert = (searchQuery) ->
    $scope.showInsTable = true
    $scope.loading = true
    if searchQuery
      if searchQuery.length >= 1
        if $scope.selectedCollation == 'attachments'
          uploadFactory.getInsertsWithDE(searchQuery, $scope.selectedVolume.volumeId).then (resp) ->
            $scope.possibleInserts = $filter('filter') resp.insert, searchQuery
            $scope.loading = false
        else
          uploadFactory.getInsertsWithAE(searchQuery, $scope.selectedVolume.volumeId).then (resp) ->
            $scope.possibleInserts = $filter('filter') resp.insert, searchQuery
            $scope.loading = false
    else
      $scope.possibleInserts = []
      $scope.loading = false

  $scope.selectVolume = (volume) ->
    $scope.selectedVolume = volume
    volumeInsertDescrFactory.findVolumeDescription($scope.selectedVolume.volumeId).then (response) ->
      if response.data.data.hasInserts
        $scope.step = 'selectInsert'
      else
        if $scope.selectedCollation == 'attachments'
          $scope.step = 'selectDE'
          getDocuments()
        else
          $scope.step = 'dragDE'
          getLacunaData()

  $scope.selectInsert = (insert) ->
    $scope.selectedInsert = insert
    if $scope.selectedCollation == 'attachments'
      $scope.step = 'selectDE'
      getDocuments()
    else
      $scope.step = 'dragDE'
      getLacunaData()

  getLacunaData = () ->
    $scope.searchBacImages = []
    $scope.searchedWithMatch = true
    if $scope.selectedInsert
      findInsertWithNumbImages($scope.selectedInsert.insertId)
    else
      findVolumeWithNumbImages($scope.selectedVolume.volumeId)

  findVolumeWithNumbImages = (volId) ->
    $scope.loading = true
    $http.get('json/browseAllCollections/findVolumeWithImages/' + volId + '?numbered=true&page=1&perPage=5').then (resp) ->
      $scope.loading = false
      if resp.data
        $scope.bacImages      = resp.data.thumbsFiles
        $scope.firstBacImage  = $scope.bacImages[0]
        $scope.totalBacImages = resp.data.count

  findInsertWithNumbImages = (insId) ->
    $scope.loading = true
    $http.get('json/browseAllCollections/findInsertWithImages/' + insId + '?numbered=true&page=1&perPage=5').then (resp) ->
      $scope.loading = false
      if resp.data
        $scope.bacImages      = resp.data.thumbsFiles
        $scope.firstBacImage  = $scope.bacImages[0]
        $scope.totalBacImages = resp.data.count

  getLacunaDataBySearch = (searchQuery = '') ->
    if searchQuery
      $scope.searchedByFolio = true
      $scope.searchedWithMatch = $scope.searchCollation.match
      $scope.searchBacImages = []
      if $scope.selectedInsert
        findFolioInInsert($scope.selectedInsert.insertId, searchQuery)
      else
        findFolioInVolume($scope.selectedVolume.volumeId, searchQuery)
    else
      getLacunaData()

  findFolioInVolume = (volId, searchQuery) ->
    q = encodeURIComponent(searchQuery)
    $scope.loading = true
    $http.get('json/browseAllCollections/findFoliosInVolume/' + volId + '?q=' + q + '&exact=' + $scope.searchCollation.match).then (resp) ->
      $scope.loading = false
#      $scope.bacImages = resp.data.thumbsFiles if resp.data
      if resp.data && $scope.searchCollation.match
        $scope.bacImages = resp.data.thumbsFiles
      else if resp.data && !$scope.searchCollation.match
        $scope.searchBacImages = resp.data.thumbsFiles
        $scope.bacImages = []

  findFolioInInsert = (insId, searchQuery) ->
    q = encodeURIComponent(searchQuery)
    $scope.loading = true
    $http.get('json/browseAllCollections/findFoliosInInsert/' + insId + '?q=' + q + '&exact=' + $scope.searchCollation.match).then (resp) ->
      $scope.loading = false
#      $scope.bacImages = resp.data.thumbsFiles if resp.data
      if resp.data && $scope.searchCollation.match
        $scope.bacImages = resp.data.thumbsFiles
      else if resp.data && !$scope.searchCollation.match
        $scope.searchBacImages = resp.data.thumbsFiles
        $scope.bacImages = []

  $scope.searchByFolio = () ->
    $scope.selectedSearchImg = undefined
    $scope.lacunaPhase = if $scope.searchCollation.match then 'dragAndDrop' else 'clickFolio'
    getLacunaDataBySearch($scope.searchCollation.folio)

  findFolioInVolumeWithNearest = (volId, fileId) ->
    $scope.loading = true
    $http.get('json/browseAllCollections/findFoliosInVolume/' + volId + '?id=' + fileId).then (resp) ->
      $scope.loading = false
#      console.log('---findFolioInVolumeWithNearest', resp)
      $scope.bacImages = resp.data.thumbsFiles if resp.data

  findFolioInInsertWithNearest = (insId, fileId) ->
    $scope.loading = true
    $http.get('json/browseAllCollections/findFoliosInInsert/' + insId + '?id=' + fileId).then (resp) ->
      $scope.loading = false
#      console.log('---findFolioInInsertWithNearest', resp)
      $scope.bacImages = resp.data.thumbsFiles if resp.data

  getLacunaDataByFolioId = (fileId = null) ->
    if fileId
      $scope.searchedByFolio = true
      if $scope.selectedInsert
        findFolioInInsertWithNearest($scope.selectedInsert.insertId, fileId)
      else
        findFolioInVolumeWithNearest($scope.selectedVolume.volumeId, fileId)
    else
      $scope.searchedByFolio = false
      $scope.searchedWithMatch = true
      getLacunaData()

  $scope.getNearestFolios = (folio) ->
    return if $scope.canSaveLacuna
    $scope.lacunaPhase = 'dragAndDrop'
    $scope.selectedSearchImg = folio
    getLacunaDataByFolioId(folio.uploadFileId)

  $scope.resetSearchByFolio = () ->
    $scope.searchedByFolio = false
    $scope.lacunaPhase = 'dragAndDrop'
    basicLacunaOptions()
    initSearchData()
    getLacunaData()

  $scope.searchDE = () ->
    $scope.selectedDocument = undefined
    getDocuments($scope.searchCollation.de)

  getDocuments = (searchQuery = '') ->
    $scope.showDocTable = true
    $scope.loading = true

    if !$scope.selectedInsert
      deFactory.getDocumentsInVolume($scope.selectedVolume.volumeId, $scope.pagination.currentPage, $scope.pagination.perPage, searchQuery).then (resp) ->
#        $scope.attachDocuments = _.filter(resp.data.documentEntities, { 'flgLogicalDelete': 0 }) if resp.data
        $scope.attachDocuments = resp.data if resp.data
        $scope.pagination.totalItems = resp.totalCount
        $scope.loading = false
    else if $scope.selectedInsert
      deFactory.getDocumentsInInsert($scope.selectedInsert.insertId, $scope.pagination.currentPage, $scope.pagination.perPage, searchQuery).then (resp) ->
#        $scope.attachDocuments = _.filter(resp.data.documentEntities, { 'flgLogicalDelete': 0 }) if resp.data
        $scope.attachDocuments = resp.data if resp.data
        $scope.pagination.totalItems = resp.totalCount
        $scope.loading = false

  $scope.selectDocument = (document) ->
    console.log(document)
    $scope.selectedDocument = document

  $scope.pageChange = () ->
    getDocuments()

  basicLacunaOptions = () ->
    $scope.bacImages = []
    $scope.searchBacImages = []
    $scope.selectedSearchImg = undefined
    $scope.firstBacImage = undefined
    $scope.totalBacImages = undefined
    $scope.sourceMoved = false
    $scope.canSaveLacuna = false
    $scope.prevImageData = undefined
    $scope.errorMessage = undefined
    $scope.searchedByFolio = false
    $scope.searchedWithMatch = true
    $scope.lacunaPhase = 'dragAndDrop'
    initUnsureData()
    unlockImages()

  $scope.backToPrevStep = () ->
    switch $scope.step
      when 'selectCollection'
        $scope.step = 'selectCollType'
      when 'selectVolume'
        $scope.step = 'selectCollection'
      when 'selectInsert'
        $scope.step = 'selectVolume'
        $scope.possibleVolumes = []
        $scope.possibleInserts = []
        $scope.showVolTable = false
      when 'selectDE'
        if $scope.selectedInsert
          $scope.step = 'selectInsert'
          $scope.selectedDocument = undefined
          initPagination()
          $scope.possibleInserts = []
          $scope.selectedInsert = undefined
          $scope.showInsTable = false
        else
          $scope.step = 'selectVolume'
          $scope.selectedDocument = undefined
          initPagination()
          $scope.possibleVolumes = []
          $scope.possibleInserts = []
          $scope.selectedVolume = undefined
          $scope.selectedInsert = undefined
          $scope.showVolTable = false
        $scope.errorMessage = undefined
        $scope.showDocTable = false
        initSearchData()
      when 'dragDE'
        if $scope.selectedInsert
          $scope.step = 'selectInsert'
          $scope.possibleInserts = []
          $scope.selectedInsert = undefined
          $scope.showInsTable = false
        else
          $scope.step = 'selectVolume'
          $scope.possibleVolumes = []
          $scope.possibleInserts = []
          $scope.selectedVolume = undefined
          $scope.selectedInsert = undefined
          $scope.showVolTable = false
        basicLacunaOptions()
        initSortOtpions()
        initSearchData()


  # Attachment type
  $scope.saveCollation = () ->
    $scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'addCollation', isEdit: false})
    console.log('save, type', $scope.selectedCollation)

    data =
      parentDocumentId: $scope.selectedDocument.documentId
      childDocumentId: $scope.sourceDoc.documentId

    $http.post('json/collation/attachment', data).then (resp) ->
#      console.log('========', resp)
      collationData = resp.data.data
      if resp.data.status == 'error' || (resp.data.status != 'error' && !collationData)
        $scope.errorMessage = resp.data.message || "Cannot create collation for document"
      else
        $scope.errorMessage = undefined

        currentUser = $rootScope.me.account
        message     = 'A collation (Attachment type) has been created by '
        message     += '<a href="#/mia/public-profile/' + currentUser + '">' + currentUser + '</a>. Please review it '
        message     += '<a href="#/mia/doc-att-coll/' + collationData.id + '">here</a>.'

        composeMessageData                = {}
        composeMessageData.to             = 'admin'
        composeMessageData.messageSubject = 'Create Collation (Attachment type)'
        composeMessageData.account        = currentUser
        composeMessageData.messageText    = message
        messagingFactory.sendMessage(composeMessageData)

        $state.go 'mia.docAttColl', {collId: collationData.id}
        $scope.closeModal()

  $scope.closeModal = () ->
    $scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'addCollation', isEdit: false})
#    $scope.actionsData   = undefined
#    $scope.currentAction = undefined
    $scope.cancel()


  # Lacuna type

  initSortOtpions = () ->
    $scope.draggableOptions =
      connectWith: '.connected-drop-target-sortable'
      items: ".app:not(.not-sortable)"
      stop: (e, ui) ->
        # if the element is removed from the first container
#        console.log('DRAG STOP')
        if ui.item.sortable.source.hasClass('draggable-element-container') and ui.item.sortable.droptarget and ui.item.sortable.droptarget != ui.item.sortable.source and ui.item.sortable.droptarget.hasClass('connected-drop-target-sortable')
          # restore the removed item
          ui.item.sortable.sourceModel.push ui.item.sortable.model
          $timeout ->

            if $scope.searchedByFolio

              currentIndex = _.findIndex $scope.bacImages, (obj) -> obj.uploadFileId == $scope.sourceList[0].uploadFileId

              if currentIndex > 0
                lockImages()
                $scope.canSaveLacuna = true
                prevIndex   = currentIndex - 1
                prevImage   = $scope.bacImages[prevIndex]
                folioDataAr = []

                _.forEach prevImage.folios, (folio) ->
                  folioData = { number: folio.folioNumber, rv: folio.rectoverso }
                  folioDataAr.push(folioData)

                $scope.prevImageData = JSON.stringify(folioDataAr)
              else if currentIndex == 0

                nextImage = $scope.bacImages[1]

                if nextImage.uploadFileId == $scope.firstBacImage.uploadFileId
                  lockImages()
                  $scope.canSaveLacuna = true
                  folioDataAr = [{ number: "fpcollation", rv: null }]
                  $scope.prevImageData = JSON.stringify(folioDataAr)
                else
                  $scope.bacImages.splice(currentIndex, 1)

            else
              lockImages()
              $scope.canSaveLacuna = true
  #            console.log('bacImages', $scope.bacImages)
              currentIndex = _.findIndex $scope.bacImages, (obj) -> obj.uploadFileId == $scope.sourceList[0].uploadFileId
  #            console.log('currentIndex', currentIndex)

              if currentIndex > 0
                prevIndex   = currentIndex - 1
                prevImage   = $scope.bacImages[prevIndex]
                folioDataAr = []

                _.forEach prevImage.folios, (folio) ->
                  folioData = { number: folio.folioNumber, rv: folio.rectoverso }
                  folioDataAr.push(folioData)

                $scope.prevImageData = JSON.stringify(folioDataAr)
              else if currentIndex == 0
                folioDataAr = [{ number: "fpcollation", rv: null }]
                $scope.prevImageData = JSON.stringify(folioDataAr)
        return
      update: (e, ui) ->
#        console.log('DRAG UPDATE')
        $scope.sourceMoved = true

    $scope.sortableOptions =
      items: ".app:not(.not-sortable)"
      stop: (e, ui) ->
#        console.log('SORT STOP')
      update: (e, ui) ->
#        console.log('SORT UPDATE')
        ui.item.sortable.cancel() unless $scope.canSaveLacuna

  initSortOtpions()

  lockImages = () ->
    $scope.draggableOptions.connectWith = ''
    angular.element(document.getElementsByClassName("app-source")).addClass('not-sortable')
    angular.element(document.getElementsByClassName("app-target")).addClass('not-sortable')

  unlockImages = () ->
    $scope.draggableOptions.connectWith = '.connected-drop-target-sortable'
    angular.element(document.getElementsByClassName("app-source")).removeClass('not-sortable')
    angular.element(document.getElementsByClassName("app-target")).removeClass('not-sortable')

  $scope.determineUnsure = () ->
    $timeout ->
      $scope.unsureData.description = null unless $scope.unsureData.unsure

  $scope.saveLacunaCollation = () ->
    $scope.$root.$broadcast('documentEntityIsBeingEdited', {field: 'addCollation', isEdit: false})

    data =
      childDocumentId: $scope.sourceDoc.documentId
      uploadFileId: $scope.firstImageSD.uploadFileId
      folioNumber: $scope.prevImageData
      volumeId: $scope.selectedVolume.volumeId
      insertId: if $scope.selectedInsert then $scope.selectedInsert.insertId else null

    data.unsure      = $scope.unsureData.unsure       if $scope.unsureData.unsure
    data.description = $scope.unsureData.description  if $scope.unsureData.unsure && $scope.unsureData.description

    $http.post('json/collation/lacuna', data).then (resp) ->
      collationData = resp.data.data
      if collationData
        $scope.errorMessage = undefined
        $scope.closeModal()
        toaster.pop('success', 'Collation successful')
        $rootScope.$broadcast('createLacuna', {documentId: $scope.sourceDoc.documentId, lacunaData: collationData})

        currentUser = $rootScope.me.account
        message     = 'A collation (Lacuna type) has been created by '
        message     += '<a href="#/mia/public-profile/' + currentUser + '">' + currentUser + '</a>. Please review it '
        message     += '<a href="#/mia/document-entity/' + collationData.childDocument.documentId + '">here</a>.'

        composeMessageData                = {}
        composeMessageData.to             = 'admin'
        composeMessageData.messageSubject = 'Create Collation (Lacuna type)'
        composeMessageData.account        = currentUser
        composeMessageData.messageText    = message
        messagingFactory.sendMessage(composeMessageData)
      else
        $scope.errorMessage = resp.data.message || "Cannot create collation document"

  # TODO: Need move it in directive later
  $scope.folioNumberName = (image) ->
    folio1    = image.folios[0]
    folio2    = image.folios[1]
    toBeNumb  = $filter('translate')('docEntity.TOBENUMBERED')
    notNumb   = $filter('translate')('docEntity.NOTNUMBFOLIO2')
    folioNumb = ''

#    need use noNumb

    if image.collationType == 'LACUNA' && image.collations[0]?.isParent
      folioNumb = 'COLLATED DOCUMENT'
    else
      if folio1.noNumb != undefined || folio2.noNumb != undefined
        folioNumb += notNumb                    if folio1?.noNumb
        folioNumb += folio1.folioNumber         if folio1?.folioNumber
        folioNumb += ' ' + folio1.rectoverso    if folio1?.folioNumber && folio1?.rectoverso  && folio1?.rectoverso!=''
        folioNumb += ' / ' + notNumb            if folio2?.noNumb
        folioNumb += ' / ' + folio2.folioNumber if folio2?.folioNumber
        folioNumb += ' ' + folio2.rectoverso    if folio2?.folioNumber && folio2?.rectoverso  && folio2?.rectoverso!=''
      else
        folioNumb = toBeNumb

    folioNumb = folioNumb.substr(0, 45) + ' ...' if folioNumb.length > 45
    return folioNumb
