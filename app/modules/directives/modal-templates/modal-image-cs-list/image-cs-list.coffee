imageCsListApp = angular.module 'imageCsListApp', []

imageCsListApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.imageCsList',
        url: '/image-cs-list'
        views:
          'content':
            controller: 'imageCsListController'
]
imageCsListApp.controller 'imageCsListController', ($scope, $rootScope) ->
  image              = $scope.options.image
  $scope.imageCsList = image.caseStudies

  $scope.openCs = (cs) ->
    $rootScope.$broadcast('openCs', cs)
    $scope.cancel()
