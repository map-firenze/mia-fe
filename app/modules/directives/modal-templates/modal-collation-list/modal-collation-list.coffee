modCollationListApp = angular.module 'modCollationListApp', [
  'modalInstance'
]

modCollationListApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.modCollationList',
        url: '/mod-collation-list'
        views:
          'content':
            controller: 'modCollationListController'
]
modCollationListApp.controller 'modCollationListController', ($scope, $state) ->
  $scope.image = $scope.options.image
#  console.log('-----', $scope.image)

  $scope.goCollation = (col) ->
    $scope.cancel()
    $state.go('mia.docAttColl', { 'collId': col.id })
