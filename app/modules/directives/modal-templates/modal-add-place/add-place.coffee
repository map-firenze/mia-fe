addPlaceApp = angular.module 'addPlaceApp', [
]

addPlaceApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addplace',
        url: '/add-place'
        params: {
          uploadFileId: ""
        }
        views:
          'content':
            templateUrl: 'modules/doc-ent/modal-add-place/add-place.html'
            controller: 'addPlaceController'
]

addPlaceApp.controller 'addPlaceController',($scope, $window, deFactory, searchDeService, $state) ->
  $scope.newPlace = {}
  $scope.newlink= ""
  $scope.searchTableOpen = false
  $scope.possiblePlaces = []
  $scope.placeType = null
  $scope.loading = false

  deFactory.getPlaceTypes().then (resp)->
    $scope.placeTypes = resp.data

  $scope.addNewPlace = (place) ->
    placeToSend =  angular.copy(place)
    placeToSend.plType = placeToSend.plType.description
    
    delete placeToSend.parentTemp
    deFactory.createNewPlace(placeToSend).then (resp)->
      if resp.status is "ok"
        $scope.newlink = resp.path
        $scope.id = resp.placeId

  $scope.goToPlace = () ->
    params={}
    params.id = $scope.id
    $state.go 'mia.bio-place',    {'id': $scope.id}

  $scope.getPlaces = (searchQuery)->
    $scope.searchTableOpen=true
    $scope.loading = true
    if searchQuery.length == 0
      $scope.searchTableOpen=false
    if searchQuery.length >= 3
      deFactory.getPlace(searchQuery).then (resp)->
        if resp.status is 'ok'
          $scope.possiblePlaces = resp.data.places
        else
          $scope.possiblePlaces = null
        $scope.loading = false

  $scope.selectLocation = (location)->
    if location.prefFlag is 'V'
      searchDeService.getPrincipalPlaces(location.id).then (response) ->
        $scope.newPlace.placeParentId = response.id
        $scope.newPlace.parentTemp = response.placeName
        $scope.searchTableOpen=false
        $scope.possiblePlaces = []
    else
      $scope.newPlace.placeParentId = location.id
      $scope.newPlace.parentTemp = location.placeName
      $scope.searchTableOpen=false
      $scope.possiblePlaces = []
  
  
