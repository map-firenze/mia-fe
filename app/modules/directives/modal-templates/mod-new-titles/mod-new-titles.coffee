addTitleApp = angular.module 'addTitleApp', [
  'modalInstance'
  'translatorAppEn'
]

addTitleApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addtitle',
        url: '/add-title'
        params: {
          parentId: ""
        }
        views:
          'content':
            templateUrl: 'modules/directives/mod-new-titles/mod-new-titles.html'
            controller: 'addTitleController'
]

addTitleApp.controller 'addTitleController',($scope, $rootScope, titlesFactory, $window, $state) ->
  $scope.modified = false
  $scope.saved = false
  $scope.sureDelete = false
  $scope.unauthor = false
  $scope.titleUsed = false
  $scope.title = {}

  if modalInstance.modify
    $scope.modify = true
    $scope.occId = modalInstance.occId
    $scope.title.occupationName = modalInstance.name
    $scope.title.roleCatId = modalInstance.categoryId
    $scope.oldId = modalInstance.occId
    $scope.oldName = modalInstance.name
    $scope.oldCatId = modalInstance.categoryId
  else
    $scope.modify = false
    $scope.title.occupationName = undefined
    $scope.title.roleCatId = undefined

  titlesFactory.getCategories().then (resp)->
    if resp
      $scope.categories = resp.data

  $scope.saveTitle = () ->
    params = {}
    params.newTitleOccRoleJson = {}
    params.newTitleOccRoleJson.occupationName = $scope.title.occupationName
    params.newTitleOccRoleJson.roleCatId = $scope.title.roleCatId
    titlesFactory.createTitle(params).then (resp)->
      if resp.status = "ok"
        $scope.saved = true
        # $rootScope.titleSaved = true
        # console.log $rootScope


  $scope.modifyTitle = ()->
    params = {}
    params.titleOccId = $scope.occId
    params.newTitleOccRoleJson = {}
    params.oldTitleOccRoleJson = {}
    params.newTitleOccRoleJson.occupationName = $scope.title.occupationName
    params.newTitleOccRoleJson.roleCatId = $scope.title.roleCatId
    params.oldTitleOccRoleJson.occupationName = $scope.oldName
    params.oldTitleOccRoleJson.roleCatId = $scope.oldCatId
    titlesFactory.modifyTitle(params).then (resp)->
      if resp.status = "ok"
        $scope.modify = false
        modalInstance.modify = false
        $scope.modified = true

  $scope.preDeleteTitle = () ->
    $scope.sureDelete = !$scope.sureDelete
  $scope.deleteTitle = () ->
    titlesFactory.deleteTitle($scope.occId).then (resp)->
      if resp.status is "ok"
        $scope.cancel()
      else
        if resp.data.length
          $scope.presentPeople = resp.data
          $scope.titleUsed = true
        if not resp.data.length
          $scope.unauthor = true
      $state.reload()

  $scope.editPerson = (personId) ->
    params = {}
    params.id = personId
    $state.go 'mia.bio-people', params
    $scope.cancel()

  $scope.cancelTask= () ->
    $scope.saved = false
    $scope.modified = false
    modalInstance.modify = false
    $scope.cancel()
