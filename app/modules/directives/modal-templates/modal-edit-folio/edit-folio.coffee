documentEntityApp = angular.module 'documentEntityApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'ngDraggable'
    'deServiceApp'
]

documentEntityApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.documentEntity',
        url: '/document-entity'
        params: {
          uploadId: "",
          uploadFileId: "",
          documentEntityId: "",
          aeFile: ""
        }
        views:
          'content':
            controller: 'documentEntityController'
            templateUrl: 'modules/doc-ent/modal-edit-folio/edit-folio.html'
]

documentEntityApp.controller 'documentEntityController',(commonsFactory, deFactory, $scope, $sce, modalFactory, $translate, $filter, $log) ->
  workingFile = {}
  $scope.toTheDrop = true
  $scope.cantDelete = false
  $scope.hideDeModal = false

  # $scope.doubleFolio = ($scope.folio2 != {})

  $scope.pagination = {
    currentPage: 1,
    totalItems: 1,
    maxSize: 5,
    perPage: 6
  }

  uploadFileId = $scope.options.uploadFileId
  uploadId = $scope.options.uploadId
  documentEntityId = $scope.options.documentEntityId
  aeFile = $scope.options.aeFile

  $scope.imgName = ""

  #get chosen archive
  $scope.getArchiveTracker = true
  $scope.init = () ->
    return new Promise (resolve, reject) ->
      commonsFactory.getPageByFileId(uploadId, uploadFileId, $scope.pagination.perPage).then (respPage) ->
        currentPage = if respPage.data then respPage.data.page else $scope.pagination.currentPage
        commonsFactory.getMySingleuploadByFileId(uploadFileId, $scope.pagination.perPage, currentPage).then (resp) ->
          #$scope.selectedAe = commonsFactory.findAeById($scope.myArchives, uploadId)
          $scope.selectedAe = resp.aentity
          console.log($scope.selectedAe)
          $scope.pagination.totalItems = resp.count
          $scope.pagination.currentPage = currentPage
          angular.forEach($scope.selectedAe.thumbsFiles, (uploadFile)->
            deFactory.getFolioByUploadFileId(uploadFile.uploadFileId).then (resp)->
              if angular.isArray(resp.data)
                uploadFile.folios=resp.data
            )
          #existing document entity
          if documentEntityId
            deFactory.getDocumentEntityById(documentEntityId).then (resp)->
              $scope.documentEntity = resp.data.documentEntity
              angular.forEach($scope.selectedAe.thumbsFiles, (thumbFile) ->
                angular.forEach($scope.documentEntity.uploadFiles, (uploadFile) ->
                  if thumbFile.uploadFileId == uploadFile.uploadFileId
                    # console.log thumbFile.uploadFileId
                    thumbFile.selected = true
                )
              )
          else
            $scope.documentEntity = {}
            $scope.documentEntity.uploadFiles=[]
            if $scope.selectedAe
              firstUploadFile = commonsFactory.findImageById($scope.selectedAe.thumbsFiles, uploadFileId)
            if $scope.options.uploadFileId
              $scope.prepareToAdd(firstUploadFile)
          $scope.deChosen = true
          $scope.getArchiveTracker = false
          resolve(true)

  $scope.init().then ->
    $scope.hideDeModal = false

  $scope.pageChange = () ->
    $scope.getArchiveTracker = true
    commonsFactory.getMySingleuploadByFileId(uploadFileId, $scope.pagination.perPage, $scope.pagination.currentPage).then (resp) ->
      $scope.getArchiveTracker = false
      $scope.selectedAe = resp.aentity
      $scope.pagination.totalItems = resp.count
      angular.forEach($scope.selectedAe.thumbsFiles, (uploadFile)->
        deFactory.getFolioByUploadFileId(uploadFile.uploadFileId).then (resp)->
          if angular.isArray(resp.data)
            uploadFile.folios=resp.data
      )

      angular.forEach($scope.selectedAe.thumbsFiles, (thumbFile) ->
        angular.forEach($scope.documentEntity.uploadFiles, (uploadFile) ->
          if thumbFile.uploadFileId == uploadFile.uploadFileId
            thumbFile.selected = true
        )
      )

  #prepare an image (folios) to be added to document record
  $scope.prepareToAdd = (uploadFile) ->
    #let the user insert an image only if is not already present in document entity
    aeUploadFile = uploadFile || aeFile
    aeUploadFileId = aeUploadFile?.uploadFileId
    if not commonsFactory.findImageById($scope.documentEntity.uploadFiles, aeUploadFileId)
      deFactory.getFolioByUploadFileId(aeUploadFileId).then (resp)->
        if angular.isArray(resp.data)
          $scope.folios = resp.data
          $scope.folio1=angular.copy(resp.data[0])
          $scope.folio2=angular.copy(resp.data[1])
          $scope.addImageToDe(aeUploadFileId)
        else
          $scope.editFolio(aeUploadFile)

  $scope.editFolio = (uploadFile) ->
    $scope.hideDeModal = true
    modalFactory.openModal(
      templateUrl: 'modules/directives/modal-templates/modal-edit-metadata/edit-metadata-createde.html'
      controller:'modalIstanceController'
      resolve:{
        options:
          -> {
            "uploadFileId": uploadFile.uploadFileId,
            "volumeId": $scope.selectedAe.volume.volumeId,
            "insertId": $scope.selectedAe.insert?.insertId || null
          }
      }
    ).then (foliosOrFalseIfNotChange) ->
      if foliosOrFalseIfNotChange
        uploadFile.folios = foliosOrFalseIfNotChange
        $scope.folio1 = angular.copy(foliosOrFalseIfNotChange[0]) if foliosOrFalseIfNotChange[0]
        $scope.folio2 = angular.copy(foliosOrFalseIfNotChange[1]) if foliosOrFalseIfNotChange[1]
        $scope.addImageToDe(foliosOrFalseIfNotChange[0].uploadFileId)
        imageIndexInSelectedAe = ($scope.selectedAe.thumbsFiles.findIndex (file) -> file.uploadFileId == uploadFile.uploadFileId)
        if imageIndexInSelectedAe > -1
          $scope.selectedAe.thumbsFiles[imageIndexInSelectedAe].folios = foliosOrFalseIfNotChange
      $scope.hideDeModal = false

    .catch () ->
      $scope.hideDeModal = false

  updateFolioInfo =  (folios) ->
    deFactory.saveFolio(folios).then (resp)->
      $scope.documentEntity.uploadFiles.forEach (value, i) ->
        deFactory.getFolioByUploadFileId(value.folios[0].uploadFileId).then (resp) ->
          if resp.status is 'ok'
            value.folios = resp.data


  #routine to add set folios in document record
  $scope.addImageToDe = (passedFile)->
    #find the image in the Upload
    if $scope.folio1.noNumb
      $scope.folio1.folioNumber = ''
      $scope.folio1.rectoverso = ''
    if $scope.doubleFolio && $scope.folio2.noNumb
      $scope.folio2.folioNumber = ''
      $scope.folio2.rectoverso = ''
    uploadFile = commonsFactory.findImageById($scope.selectedAe.thumbsFiles, passedFile) || aeFile
    #console.log uploadFile
    #check if the image is already present in document record
    isAlreadyPresent = commonsFactory.findImageById($scope.documentEntity.uploadFiles, passedFile)
    if isAlreadyPresent
      isAlreadyPresent.folios[0] = $scope.folio1
#      console.log $scope.doubleFolio
      if $scope.doubleFolio
        isAlreadyPresent.folios[1] = $scope.folio2
        
      $scope.folio1.folioId = $scope.folio1.folioId || ''
      $scope.folio1.uploadFileId = $scope.folio1.uploadFileId || uploadFile.uploadFileId
      if $scope.doubleFolio
        $scope.folio2.folioId = $scope.folio2.folioId || ''
        $scope.folio2.uploadFileId = $scope.folio2.uploadFileId || uploadFile.uploadFileId
      if !$scope.doubleFolio && $scope.folio2
        deFactory.deleteFolio($scope.folio1.uploadFileId, $scope.folio2.folioId).then (respDel) ->
          isAlreadyPresent.folios.length = 1
#          console.log respDel
      updateFolioInfo(isAlreadyPresent.folios)
    else
      if not angular.isArray(uploadFile.folios)
        uploadFile.folios=[]
        uploadFile.folios.push($scope.folio1)
        if $scope.doubleFolio
          uploadFile.folios.push($scope.folio2)
      fileToAdd = {}
      fileToAdd.uploadFileId = uploadFile.uploadFileId
      fileToAdd.filePath = uploadFile.filePath
      fileToAdd.folios = uploadFile.folios
      fileToAdd.canView = true
      uploadFile.selected = true
      $scope.documentEntity.uploadFiles.push(fileToAdd)
      updateFolioInfo(uploadFile.folios)

  $scope.deleteImageFromDe = (image)->
    if $scope.documentEntity.uploadFiles.length > 1
      removedImage = _.find($scope.selectedAe.thumbsFiles, { 'uploadFileId': image.uploadFileId })
      if removedImage
        removedImage.selected = false
      $scope.documentEntity.uploadFiles.splice($scope.documentEntity.uploadFiles.indexOf(image), 1)
    else
      $scope.cantDelete = true

  $scope.rehab = () ->
    $scope.cantDelete = false

  $scope.next = () ->
    if $scope.documentEntity.uploadFiles.length
#      deFactory.recordDocAction($scope.$stateParams.docId, 'EDIT')
      $scope.select($scope.documentEntity)
