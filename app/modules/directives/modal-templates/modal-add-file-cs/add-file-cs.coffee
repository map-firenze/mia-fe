addFileCsApp = angular.module 'addFileCsApp', [
  'toaster'
  'ngFileUpload'
]

addFileCsApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addFileCs',
        url: '/add-file-cs'
        params: {
          caseStudy: ''
          folderId: ''
        }
        views:
          'content':
            templateUrl: 'modules/directives/modal-templates/modal-add-file-cs/add-file-cs.html'
            controller: 'addFileCsController'
]

addFileCsApp.controller 'addFileCsController', ($scope, $rootScope, Upload, toaster) ->
  caseStudy = $scope.options.caseStudy
  folderId  = $scope.options.folderId

  $scope.newFileCs = {title: '', file: undefined }

  $scope.addNewFile = () ->
    $rootScope.isBusy = true
    url = 'json/case_study/' + caseStudy.id + '/folders/' + folderId + '/items/upload'
    Upload.upload(
      url: url
      data:
        title: $scope.newFileCs.title
        file:  $scope.newFileCs.file
    ).then ((response) ->
      $rootScope.isBusy = false
      if response.data.status == 'success'
        toaster.pop('success', 'Successfully added')
        newFile          = {}
        newFile.csId     = caseStudy.id
        newFile.folderId = folderId
        newFile.item     = response.data.data
        $rootScope.$broadcast('addToCsFolder', newFile)
        $scope.select(newFile)
      else
        toaster.pop('error', response.data.message)
    ), ((response) ->
      if response.status == 500
        $rootScope.$broadcast('progress', NaN)
      if response.status > 0
        toaster.pop('error', response.data.message)
    ), (evt) ->
      $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
      $rootScope.$broadcast('progress', $scope.progress)

  $scope.validateSelectedFile = (image) ->
    if image.name.length > 100
      return 'This file name is too long. Max. 100 characters.'
    else
      return null
