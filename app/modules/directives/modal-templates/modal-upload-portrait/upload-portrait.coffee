upPortraitApp = angular.module 'uploadPortraitApp', [
    'modalInstance'
    'bioPeopleServiceApp'
    'ngFileUpload'
    'translatorAppEn'
    'translatorApp'
   ]

upPortraitApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.uploadPortrait',
        url: '/upload-portrait'
        views:
          'content':
            controller: 'uploadPortraitController'
]
upPortraitApp.controller 'uploadPortraitController', (personalProfileFactory, Upload, $http, $scope, $rootScope, $log, $timeout, $state, alertify) ->

  $scope.picFile = undefined
  $scope.croppedData = null
  $scope.success = false
  $scope.removePic=() ->
    $scope.picFile = undefined
    $scope.croppedData = null
  $scope.uploadDone=()->
    $scope.success = true
    $scope.picFile = undefined
    $scope.croppedData = null
    $scope.progress = 0

  $scope.upload = (fileData) ->
    blob = personalProfileFactory.datatoBlob(fileData, "portrait", null, "image/jpeg")
    file = []
    file.push(blob)
    Upload.upload(
      url: 'json/biographical/uploadPortraitUser'
      data:
        files: file
        personId: $scope.personId
      ).then ((response) ->
        $scope.result = response.data
        if $scope.result.status = "ok"
          $scope.uploadDone()
        ), ((response)->
          if response.status > 0
            # $scope.errorMsg = response.status + ': ' + response.data
            alertify.error('An error occured during portrait uploading. Error code is ' + response.status)
        ), (evt) ->
          $scope.progress = Math.min(100,parseInt(100.0 * evt.loaded / evt.total))
          $rootScope.$broadcast('progress', $scope.progress)
          if $scope.progress == 100
            $rootScope.$broadcast('upload-finished')

  handleFileSelect = (evt) ->
    file = evt.currentTarget.files[0]
    reader = new FileReader
    reader.onload = (evt) ->
      $scope.$apply ($scope) ->
        $scope.picFile = evt.target.result
#        console.log(evt)
    reader.readAsDataURL file
    $scope.selectedFileType = file.type
#    console.log file

  $scope.closeModal = () ->
    $scope.cancel()
    $state.reload()

  angular.element(document.querySelector('#filePortraitInput')).on 'change', handleFileSelect
  return
