personDocApp = angular.module 'personDocApp', ['modalInstance']

personDocApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.persondoc',
        url: '/person-doc'
        params: {
          docIds: ''
        }
        views:
          'content':
            templateUrl: 'modules/doc-ent/modal-person-doc/person-doc.html'
            controller: 'personDocController'
]

personDocApp.controller 'personDocController', ($scope, $translate, $state) ->
  $scope.options.docIds
  $scope.openDoc = (docId) ->
    $state.go('mia.modifyDocEnt', { 'docId': docId })
    $scope.cancel()