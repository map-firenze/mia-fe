addRepoApp = angular.module 'addRepoApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
    'modalInstance'
    'ngAnimate'
   ]


addRepoApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.addRepo',
        url: '/add-repo'
        views:
          'content':
            # templateUrl: 'modules/upload/modal-add-repo/add-repo.html'
            controller: 'addRepoController'
]
addRepoApp.controller 'addRepoController', (uploadFactory, $scope, $translate, $filter, $log) ->
  $scope.details = {}
  $scope.addNew=false
  $scope.showTable=false
  $scope.wrongName = false
  $scope.wrongAbbreviation = false

  #for ng-autocomplete google location
  $scope.autoCompOptions={}
  $scope.autoCompOptions.types='(cities)'

  $scope.switchNew = ()->
    $scope.addNew = !$scope.addNew

  $scope.getRepository = (val) ->
    $scope.loadingLocations = true
    if val
      if val.length>=2
        $scope.showTable=true
        uploadFactory.getPossibleRepository(val).then (resp) ->
          $scope.loadingLocations = false
          $scope.possibleRepo = $filter('filter') resp.repository, val

  $scope.saveRepo = (repo)->
    $scope.chosenRepo = {}
    $scope.chosenRepo.repositoryId = repo.repositoryId or ''
    $scope.chosenRepo.repositoryName = repo.repositoryName or ''
    $scope.chosenRepo.repositoryAbbreviation = repo.repositoryAbbreviation or ''
    $scope.chosenRepo.repositoryDescription = repo.repositoryDescription or ''
    if $scope.addNew
      $scope.chosenRepo.repositoryLocation = $scope.details.place_id
      $scope.chosenRepo.repositoryCity = $scope.details.address_components[0].long_name
      for addressName in $scope.details.address_components
        if addressName.types[0] == "country"
          $scope.chosenRepo.repositoryCountry = addressName.long_name
    else
      $scope.chosenRepo.repositoryLocation = repo.repositoryLocation or ''
      $scope.chosenRepo.repositoryCity = repo.repositoryCity or ''
      $scope.chosenRepo.repositoryCountry = repo.repositoryCountry or ''
    if $scope.addNew
      uploadFactory.checkRepository($scope.chosenRepo.repositoryName, $scope.chosenRepo.repositoryLocation, $scope.chosenRepo.repositoryAbbreviation).then (resp) ->
        if (resp.status is 'ok') or not $scope.addNew
          $scope.select($scope.chosenRepo)
        else
          if resp.message is 'wrongName'
            $scope.wrongName = true
          if resp.message is 'wrongAbbreviation'
            $scope.wrongAbbreviation = true
    else
#      console.log $scope.chosenRepo
      $scope.select($scope.chosenRepo)
