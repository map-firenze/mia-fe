saveFilterApp = angular.module 'saveFilterApp', []

saveFilterApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.saveNewsfeedItem',
        url: '/save-filter'
        params: {
          searchArray: "",
        }
        views:
          'content':
            controller: 'saveFilterController'
]

saveFilterApp.controller 'saveFilterController', ($rootScope, $scope, $state, newsFeedService, toaster) ->
  $scope.filterNameMinLength = 5
  
  $scope.newsfeedItem = {
    'title': 'my-filter',
    'searchFilter': $scope.options.searchArray
  }
  
  $scope.isValidName = () ->
    if _.isNull($scope.newsfeedItem.title) \
    or _.isEmpty($scope.newsfeedItem.title) \
    or $scope.newsfeedItem.title.length < $scope.filterNameMinLength
      return false
    
    return true

  $scope.save = () ->
    newsFeedService.createMyNewsfeedItem($scope.newsfeedItem).then (response) ->
      if response.data.status == 'ok'
        if $state.current.name is 'mia.welcome'
          $rootScope.$emit('addNewsfeedItem', response.data.data)
        else
          $state.go('mia.welcome').then (response) ->
            $rootScope.$emit('addNewsfeedItem', null)

      if response.data.status == 'ko'
        toaster.pop('error', response.data.message)

      $scope.select(response.data.status)