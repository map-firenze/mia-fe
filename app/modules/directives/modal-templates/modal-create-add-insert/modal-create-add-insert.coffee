createAddInsertApp = angular.module 'createAddInsertApp', [
    'translatorApp'
    'ngAutocomplete'
    'commonServiceApp'
    'ui.router'
    'uploadServiceApp'
]

createAddInsertApp.config ['$stateProvider'
  ($stateProvider) ->
    $stateProvider
      .state 'mia.createAddInsert',
        url: '/create-add-insert'
        params: {
          parentId: ""
        }
        views:
          'content':
            templateUrl: 'modules/upload/modal-create-add-insert/modal-create-add-insert.html'
            controller: 'createAddInsertController'
]

createAddInsertApp.controller 'createAddInsertController',(uploadFactory, searchFactory, $scope, $translate, $filter, $log) ->
  $scope.showTable = false

  $scope.saveInsert = () ->
    $scope.insParams = {
      "collectionId": $scope.options.parentId
      "volumeNo": $scope.options.volumeNo
      "insertNo": $scope.newInsert.insertName
    }
    searchFactory.createAddInsert($scope.insParams).then (resp) ->
      # if resp and resp.status is 'ok'
      #   $scope.cancel()
      #   $scope.select(selectedInsert)
      # else
      #   console.log 'error occured'

  $scope.getInsert = (val) ->
    if val
      if val.length >= 1
        $scope.showTable = true
        searchFactory.findInsertDescription(val).then (resp) ->
          $scope.possibleInsert = $filter('filter') resp.insert, val
