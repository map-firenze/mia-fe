docSysInfo = angular.module 'docSysInfo', ['spotlight']
docSysInfo.directive 'docSysInfoDir', ($filter, modalFactory, deFactory, $state, $location, $rootScope, $stateParams, csFactory, toaster) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/doc-sys-info/doc-sys-info.html'
  scope:
    info: '='

  link: (scope, elem, attr) ->
    scope.deleteDe=()->
      csFactory.checkExistsInCs('DE', $stateParams.docId).then (resp) ->
        if resp.status == 'success' && resp.data.length
          messagePage = 'This document is used in a Project. If you delete it, it will be permanently removed from the Project. Are you sure you want to delete this document?'
          deleteDeWarning(messagePage)
        else
          messagePage = $filter('translate')("modal.DELETINGDE")
          deleteDeWarning(messagePage)
    
    scope.currentUser = $rootScope.me.account
    # console.log(scope.currentUser)

    deleteDeWarning = (messagePage) ->
      pageTitle = $filter('translate')("modal.WARNING")
      modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
        if resp
          scope.$root.$broadcast('documentEntity_dropChanges', true)
          deFactory.deleteDe($stateParams.docId).then (resp)->
            if resp.status is "ok"
              $state.go('mia.docentities')
            if resp.status is "ko"
              pageTitle = $filter('translate')("modal.ERROR")
              messagePage = $filter('translate')("modal.DELAEERR")
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
                return resp
            if resp.status is "w"
#              scope.errorDeleting=true
#              scope.errorMess=resp.message
              pageTitle = $filter('translate')("modal.ERROR")
              messagePage = resp.message
              modalFactory.openMessageModal(pageTitle, messagePage, false , "sm").then (resp) ->
                return resp

    scope.unDeleteDe=()->
      deFactory.unDeleteDe($stateParams.docId).then (resp) ->
        $state.reload()

    scope.setDocumentToPrivate = () ->
      if scope.info.privacy == 0
        scope.updateDocument()

    scope.setDocumentToPublic = () ->
      if scope.info.privacy == 1
        scope.updateDocument()

    scope.updateDocument = () ->
      deFactory.updateDocumentPrivacy($stateParams.docId).then (resp) ->
        if resp.status is "ok"
          deFactory.getDocumentEntityById($stateParams.docId).then (resp) ->
            deData = resp.data.documentEntity
            scope.info = deData
            console.log(deData)
            scope.$root.$broadcast('updateDePrivacy', deData)
        else
          toaster.pop('error', resp.message)

    scope.openModalShareWithUsers = () ->
      # open modal
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-share-with-users/share-with-users.html'
        controller:'modalIstanceController'
        resolve:{
          options:
            -> {
              "docId": $stateParams.docId
              }
        }
      )
    scope.goToPublicProfile = (user) ->
      $state.go('mia.publicProfile', { 'account': user  })

    # check if directives is used in discovery module
    if $location.path().indexOf('discovery') > -1
      scope.spotlightPub = 'true'
    else if $location.path().indexOf('spotlight-review-staff') > -1
      scope.spotlightPub = 'true'
    else
      scope.spotlightPub = 'false'

    scope.isSpotlightEnabled = () ->
      if (scope.info.privacy != 1 && $rootScope.me.account != scope.info.owner)
        # not owned by me
        scope.disabledMessage = "You cannot publish a document that belongs to someone else."
        return false
      if scope.info.privacy != 1
        # not private
        scope.disabledMessage = "You cannot submit for publication if the document is public."
        return false
      if $rootScope.me.account != scope.info.owner
        # not owned by me
        scope.disabledMessage = "You cannot publish a document that belongs to someone else."
        return false
      if !isEnabled(scope.info.uploadFiles,scope.info.owner)
        scope.disabledMessage = "You cannot publish a document if the image(s) belong to someone else."
        return false
      scope.disabledMessage = null
      return true

    scope.isPrivacyEnabled = () ->
      if (scope.info.owner != scope.info.uploadFiles[0].uploadInfoOwner)
        scope.disabledPrivacyMessage = "You cannot change privacy settings of a document if the image(s) belong to someone else."
        return false
        console.log(scope.disabledPrivacyMessage)
      scope.disabledPrivacyMessage = null
      return true

    scope.createSpotlight = () ->
      if scope.isSpotlightEnabled()
        pageTitle = "Submit for publication"
        messagePage = "This sends the document to a peer review committee for approval prior to publication. Are you sure you want to proceed?"
        modalFactory.openMessageModal(pageTitle, messagePage, true, "md").then (resp) ->
          if resp
            console.log(scope.info)
            modalFactory.openModal(
              templateUrl: 'modules/directives/modal-templates/modal-spotlight/spotlight-check-modal.html'
              controller:'spotlightCheckModalController'
            )

isEnabled = (files, account) ->
  for f in files
    if f.uploadInfoOwner != account
      return false

  return true