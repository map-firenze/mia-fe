places = angular.module 'places', ['addPlaceApp']
places.directive 'placesDir', (deFactory, $filter, modalFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/places/places.html'
  scope:
    placesfield: '='
  link: (scope, elem, attr) ->
    scope.loading = false
    scope.addALocation = () ->
      firstLocation = { "id": "", "unsure": "s" }
      scope.selectedPlaces.push(firstLocation)
      #scope.placesfield.value = []
      scope.tempName.push({})
      scope.searchTableOpen.push(false)

    #vettore che contiene i valori per decidere se la tabella risultati è aperta
    scope.searchTableOpen = []
    scope.searchTableOpen.push(false)
    #vettore che contiene i valori per nomi place
    scope.tempName = []

    scope.selectedPlaces = []

    if scope.placesfield.value is ""
      scope.placesfield.value = []

    scope.addALocation()

    scope.closeTable = (i) ->
      scope.searchTableOpen[i] = false

    scope.getPlaces = (searchQuery, i) ->
      scope.loading = true
      scope.searchTableOpen[i] = true
      if searchQuery.length == 0
        scope.searchTableOpen[i] = false
        scope.loading = false
      if searchQuery.length >= 3
        deFactory.getPlace(searchQuery).then (resp) ->
          if resp.status is 'ok'
            scope.possiblePlaces = resp.data.places
          if resp.status is 'ko'
            scope.possiblePlaces = []
          scope.loading = false
      else
        scope.loading = false

    scope.selectLocation = (location, i) ->
      for place in scope.selectedPlaces
        if place.id is location.id
          alreadyInserted = true
      if not alreadyInserted
        scope.tempName[i].name = location.placeName
        temp = {}
        temp.id = location.id
        temp.unsure = "s"
        scope.placesfield.value[i] = temp
        scope.searchTableOpen[i] = false
        scope.selectedPlaces[i] = location
        scope.possiblePlaces = []
      else
        pageTitle = $filter('translate')("general.ERROR")
        messagePage = $filter('translate')("places.ALREADYIN")
        modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

    scope.setUnsure = (unsure, i) ->
      if scope.placesfield && scope.placesfield.value.length
        scope.placesfield.value[i].unsure = "u" if not unsure
        scope.placesfield.value[i].unsure = "s" if unsure

    scope.addANewPlace = (i) ->
      scope.tempName[i].name = ""
      scope.searchTableOpen[i] = false
      modalFactory.openModal(
        templateUrl: 'modules/directives/modal-templates/modal-add-place/add-place.html'
        controller: 'modalIstanceController'
      ).then (resp) ->
