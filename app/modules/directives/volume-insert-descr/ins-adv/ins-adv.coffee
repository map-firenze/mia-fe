volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'insAdvDir', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ins-adv/ins-adv.html'
  scope:
    documents: "="
  link: (scope, elem, attr) ->
    scope.edit = false
    scope.insert = ""
    scope.meta = {
      "insertId": '',
      "organizationalCriteria": '',
      "paginationNotes": '',
      "pagination": '',
      "folioCount": '',
      "missingFolios": '',
      "otherNotes": '',
    }

    scope.switchEdit = () ->
      scope.edit = ! scope.edit

    scope.save = () ->
      scope.meta.insertId = scope.documents.insertId
      scope.meta.organizationalCriteria = scope.documents.organizationalCriteria
      scope.meta.paginationNotes = scope.documents.paginationNotes
      scope.meta.pagination = scope.documents.pagination
      scope.meta.folioCount = scope.documents.folioCount
      scope.meta.missingFolios = scope.documents.missingFolios
      scope.meta.otherNotes = scope.documents.otherNotes
      volumeInsertDescrFactory.modInsertAdvancedMeta(scope.meta).then (resp) ->
        if resp.status is "ok"
          scope.edit = false