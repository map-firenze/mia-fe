volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'volAdditionalFieldsDir', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/vol-additional-fields/vol-additional-fields.html'
  scope:
    documents: "="
  link: (scope, elem, attr) ->