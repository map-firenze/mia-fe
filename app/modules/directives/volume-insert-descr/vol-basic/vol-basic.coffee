volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'volBasicDir', ($state, $location, volumeInsertDescrFactory, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/vol-basic/vol-basic.html'
  scope: {
    documents: "="
  }
  link: (scope, elem, attr) ->

    scope.months=[
      {'id': 1, 'name':'January'},
      {'id': 2, 'name':'February'},
      {'id': 3, 'name':'March'},
      {'id': 4, 'name':'April'},
      {'id': 5, 'name':'May'},
      {'id': 6, 'name':'June'},
      {'id': 7, 'name':'July'},
      {'id': 8, 'name':'August'},
      {'id': 9, 'name':'September'},
      {'id': 10, 'name':'October'},
      {'id': 11, 'name':'November'},
      {'id': 12, 'name':'December'}
    ]

    scope.volume = ""
    scope.meta = {
      "volumeId": null,
      "sezione": '',
      "volumeNo": '',
      "volumeTitle": '',
      "startYear": null,
      "startMonth": null,
      "startDay": null,
      "endYear": null,
      "endMonth": null,
      "endDay": null,
      "dateNotes": '',
      "volumeContext": ''
    }

    $timeout ->
      scope.showTable = false
      scope.documents.startYear = parseInt(scope.documents.startYear) if scope.documents.startYear
      scope.documents.endYear   = parseInt(scope.documents.endYear)   if scope.documents.endYear
      scope.documents.startDay  = parseInt(scope.documents.startDay)  if scope.documents.startDay
      scope.documents.endDay    = parseInt(scope.documents.endDay)    if scope.documents.endDay

    scope.switchEdit = () ->
      scope.edit = ! scope.edit

    scope.dates = (numb) ->
      selectMonth = ""
      angular.forEach(scope.months, (month) ->
        if month.id == numb
          selectMonth = month.name
      )
      return selectMonth

    scope.getCarteggioByName = (searchQuery) ->
      if searchQuery
        if searchQuery.length < 1
          scope.loading = false
        if searchQuery.length >= 1
          scope.loading = true
          scope.showTable = true
          volumeInsertDescrFactory.findSeriesBasicMeta(searchQuery, scope.documents.collectionId).then (resp)->
            if resp.series.length
              scope.series = resp.series
            else
              scope.series = null
            scope.loading = false

    scope.closeTable = () ->
      scope.showTable = false
    
    scope.selectSerie = (serieName, serieSubName) ->
      serie = serieName
      if serieSubName != "null"
        serie = serieName + "/" + serieSubName
      scope.documents.sezione = serie
      scope.showTable = false

    scope.save = () ->
      scope.meta.volumeId = scope.documents.volumeId
      scope.meta.sezione = scope.documents.sezione
      scope.meta.volumeNo = scope.documents.volumeNo
      scope.meta.volumeTitle = scope.documents.volumeTitle
      scope.meta.startYear = scope.documents.startYear
      # note: /mia/volume/5 always returns 0
      scope.meta.startMonth = if scope.documents.startMonth == 0 then null else scope.documents.startMonth
      scope.meta.startDay = scope.documents.startDay
      scope.meta.endYear = scope.documents.endYear
      # note: /mia/volume/5 always returns 0
      scope.meta.endMonth = if scope.documents.endMonth == 0 then null else scope.documents.endMonth
      scope.meta.endDay = scope.documents.endDay
      scope.meta.dateNotes = scope.documents.dateNotes
      scope.meta.volumeContext = scope.documents.volumeContext
      volumeInsertDescrFactory.modVolumeBasicMeta(scope.meta).then (resp) ->
        if resp.status is "ok"
          scope.edit = false

#volInsDescr.directive 'stringToNumber', ->
#  require: 'ngModel'
#  link: (scope, element, attrs, ngModel) ->
#    ngModel.$parsers.push (value) ->
#      '' + value
#    ngModel.$formatters.push (value) ->
#      parseFloat value
#    return
