volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'insAdditionalFieldsDIr', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ins-additional-fields/ins-additional-fields.html'
  scope:
    documents: "="
  link: (scope, elem, attr) ->
