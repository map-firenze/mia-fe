volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'volSysInfoDir', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/vol-sys-info/vol-sys-info.html'
  scope:
    documents: "="
  link: (scope, elem, attr) ->
    scope.volume = ""
    scope.months=[{'id': 1; 'name':'January'},
    {'id': 2; 'name':'February'},
    {'id': 3; 'name':'March'},
    {'id': 4; 'name':'April'},
    {'id': 5; 'name':'May'},
    {'id': 6; 'name':'June'},
    {'id': 7; 'name':'July'},
    {'id': 8; 'name':'August'},
    {'id': 9; 'name':'September'},
    {'id': 10; 'name':'October'},
    {'id': 11; 'name':'November'},
    {'id': 12; 'name':'December'}]

    scope.goToPublicProfile = (user) ->
      $state.go('mia.publicProfile', { 'account': user  })
