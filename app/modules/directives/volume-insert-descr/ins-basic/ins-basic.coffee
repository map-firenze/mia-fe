volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'insBasicDir', ($state, $location, volumeInsertDescrFactory, $timeout) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ins-basic/ins-basic.html'
  scope: {
    documents: "="
  }
  link: (scope, elem, attr) ->
    scope.insert = ""
    scope.meta = {
      "insertTitle": "",
      "startYear": null,
      "startMonth": null,
      "startDay": null,
      "endYear": null,
      "endMonth": null,
      "endDay": null,
      "dateNotes": "",
      "insertDescription": ""
    }
    scope.months=[
      {'id': 1, 'name':'January'},
      {'id': 2, 'name':'February'},
      {'id': 3, 'name':'March'},
      {'id': 4, 'name':'April'},
      {'id': 5, 'name':'May'},
      {'id': 6, 'name':'June'},
      {'id': 7, 'name':'July'},
      {'id': 8, 'name':'August'},
      {'id': 9, 'name':'September'},
      {'id': 10, 'name':'October'},
      {'id': 11, 'name':'November'},
      {'id': 12, 'name':'December'}
    ]
    scope.insertDescription = {}

    $timeout ->
      scope.documents.startYear = parseInt(scope.documents.startYear) if scope.documents.startYear
      scope.documents.endYear   = parseInt(scope.documents.endYear)   if scope.documents.endYear
      scope.documents.startDay  = parseInt(scope.documents.startDay)  if scope.documents.startDay
      scope.documents.endDay    = parseInt(scope.documents.endDay)    if scope.documents.endDay

    scope.switchEdit = () ->
      scope.edit = ! scope.edit

    scope.dates = (numb) ->
      selectMonth = ""
      angular.forEach(scope.months, (month) ->
        if month.id == numb
          selectMonth = month.name
      )
      return selectMonth

    scope.save = () ->
      scope.meta.insertId = scope.documents.insertId
      scope.meta.insertNo = scope.documents.insertNo
      scope.meta.insertTitle = scope.documents.insertTitle
      scope.meta.startYear = scope.documents.startYear
      scope.meta.startMonth = scope.documents.startMonth
      scope.meta.startDay = scope.documents.startDay
      scope.meta.endYear = scope.documents.endYear
      scope.meta.endMonth = scope.documents.endMonth
      scope.meta.endDay = scope.documents.endDay
      scope.meta.dateNotes = scope.documents.dateNotes
      scope.meta.insertDescription = scope.documents.insertDescription
      volumeInsertDescrFactory.modInsertBasicMeta(scope.meta).then (resp) ->
        if resp.status is "ok"
          scope.edit = false

#volInsDescr.directive 'stringToNumber', ->
#  require: 'ngModel'
#  link: (scope, element, attrs, ngModel) ->
#    ngModel.$parsers.push (value) ->
#      '' + value
#    ngModel.$formatters.push (value) ->
#      parseFloat value
#    return
