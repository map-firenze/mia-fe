volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'insSummaryDir', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ins-summary/ins-summary.html'
  scope:
    archival: "="
    documents: "="
  link: (scope, elem, attr) ->
    scope.insert = ""
    scope.months=[{'id': 1; 'name':'January'},
    {'id': 2; 'name':'February'},
    {'id': 3; 'name':'March'},
    {'id': 4; 'name':'April'},
    {'id': 5; 'name':'May'},
    {'id': 6; 'name':'June'},
    {'id': 7; 'name':'July'},
    {'id': 8; 'name':'August'},
    {'id': 9; 'name':'September'},
    {'id': 10; 'name':'October'},
    {'id': 11; 'name':'November'},
    {'id': 12; 'name':'December'}]

    scope.dates = (numb) ->
      selectMonth = ""
      angular.forEach(scope.months, (month) ->
        if month.id == numb
          selectMonth = month.name
      )
      return selectMonth
    

    scope.goToVolume = () ->
      $state.go("mia.volumeDescr", {'volumeId': scope.documents.volumeId, 'collectionId': scope.documents.collectionId})

    
