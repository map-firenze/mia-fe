volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'volAdvDir', ($state, $location, volumeInsertDescrFactory) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/vol-adv/vol-adv.html'
  scope:
    documents: "="
  link: (scope, elem, attr) ->
    scope.edit = false
    scope.volume = ""
    scope.meta = {
      "volumeId": '',
      "organizationalCriteria": '',
      "paginationNotes": '',
      "pagination": '',
      "folioCount": '',
      "missingFolios": '',
      "otherNotes": '',
    }
    
    scope.switchEdit = () ->
      scope.edit = ! scope.edit

    scope.save = () ->
      scope.meta.volumeId = scope.documents.volumeId
      scope.meta.organizationalCriteria = scope.documents.organizationalCriteria
      scope.meta.paginationNotes = scope.documents.paginationNotes
      scope.meta.pagination = scope.documents.pagination
      scope.meta.folioCount = scope.documents.folioCount
      scope.meta.missingFolios = scope.documents.missingFolios
      scope.meta.otherNotes = scope.documents.otherNotes
      volumeInsertDescrFactory.modVolumeAdvancedMeta(scope.meta).then (resp) ->
        if resp.status is "ok"
          scope.edit = false