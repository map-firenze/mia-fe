volInsDescr = angular.module 'volInsDescrApp'
volInsDescr.directive 'insGuardiaUplDir', (volumeInsertDescrFactory, $state, $location, Upload, $q) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ins-guardia-upl/ins-guardia-upl.html'
  scope:
    insertId: "="
    spineImage: "="
  link: (scope, elem, attr) ->
    scope.imageFile = undefined
    scope.imageInvalidFile = undefined
    scope.errorMessage = null
    scope.uploadValidationParams =
      minHeight: 250
      minWidth: 250
      maxSize: 10

    scope.selectImage = (file, errorFiles) ->
      scope.imageInvalidFile = errorFiles && errorFiles[0]

      if scope.imageInvalidFile
        switch
          when scope.imageInvalidFile.$error is 'minWidth' then scope.errorMessage = 'Image width is too small. The width of uploaded image should be greater or equal to ' + scope.uploadValidationParams.minWidth + 'px'
          when scope.imageInvalidFile.$error is 'minHeight' then scope.errorMessage = 'Image height is too small. The height of uploaded image should be greater or equal to ' + scope.uploadValidationParams.minHeight + 'px'
          when scope.imageInvalidFile.$error is 'maxSize' then scope.errorMessage = 'Image size is too large. It must be smaller than ' + scope.uploadValidationParams.maxSize + 'MB'
        return false
      else
        scope.errorMessage = null

      form =
        filesForm: file
        insertId: scope.insertId
        repositoryId: 1 #TODO fix (Currently hardcoded repositoryId)

      scope.uploadModifyInsertGuardia(form)

    scope.validatePicFiles = (image) ->
      defer = $q.defer()
      return null

    scope.uploadModifyInsertGuardia = (form) ->
      Upload.upload(
        url: 'json/volumeInsertDescription/uploadInsertGuardia'
        data: form
      ).then ((response) ->
#        console.log response
        $state.reload()
      ), ( (response) ->
        console.log response
      )