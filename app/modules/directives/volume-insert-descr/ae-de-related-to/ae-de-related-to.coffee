volInsDescr = angular.module 'volInsDescrApp', []
volInsDescr.directive 'aeDeRelatedToDir', ($state, $location) ->
  restrict: 'E'
  replace: false
  templateUrl: 'modules/directives/volume-insert-descr/ae-de-related-to/ae-de-related-to.html'
  scope:
    archivals: "="
    documents: "="
    search: "&"
    recType: "@"
  link: (scope, elem, attr) ->
    
