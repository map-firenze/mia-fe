resultsApp = angular.module 'resultsApp', [
  'peopleResults'
  'placeResults'
  'aeResults'
  'docResults'
  'docAdvancedResults'
  'historyResults'
]

resultsApp.filter 'reduceJson', () ->
  return (inputArray, key) ->
    return inputArray.reduce (acc, obj) ->
      return if inputArray[inputArray.length - 1] != obj then acc + obj[key] + ', ' else acc + obj[key]
    , ''

resultsApp.controller 'resultsController', ($scope, $rootScope, $state, modalFactory, $filter, deFactory, $timeout, $sce, $window, $http) ->
  $scope.open = false
  $scope.notebookOpen = false
  $scope.searchHistoryOpen = false
  $scope.showRefine = false
  $scope.months = [
      {'name':'', 'id':0}
      {'name':'January', 'id':1},
      {'name':'February', 'id':2},
      {'name':'March', 'id':3},
      {'name':'April', 'id':4},
      {'name':'May', 'id':5},
      {'name':'June', 'id':6},
      {'name':'July', 'id':7},
      {'name':'August', 'id':8},
      {'name':'September', 'id':9},
      {'name':'October', 'id':10},
      {'name':'November', 'id':11},
      {'name':'December', 'id':12}]
  $scope.placesNames = new Map()
  $scope.personsNames = new Map()

  $scope.init = () ->
    angular.forEach($rootScope.searches, (search) ->
      getNames(search.searchArray)
    )

  $scope.$on('new-result', (evt, args) ->
    $scope.open = true
    found = false
    $scope.loading = true
    $scope.loadSelected = false
    $scope.dynamicPopover = {}
    $scope.dynamicPopover.content = args.content
    $scope.dynamicPopover.templateUrl = 'modules/right-sidebar/popover-template.html'
    $scope.dynamicPopover.title = 'Title'
    
    angular.forEach($rootScope.searches, (search) ->
      if angular.equals(search, args)
        found = true
    )
    
    unless _.isUndefined(args.searchArray)
      getNames(args.searchArray)
    
    if $rootScope.searches.length == 0
      $rootScope.searches.push(args)
    else
      isAlreadyPresent = false
      $rootScope.searches.map (search) ->
        if angular.equals(args, search)
          isAlreadyPresent = true
      if !isAlreadyPresent
        $rootScope.searches.push(args)
    
    # Update the searchs item if exists otherwise it creates the key
    $window.localStorage.setItem('searches', JSON.stringify($rootScope.searches))
    
    $timeout ->
      $scope.loading = false
  )

  $scope.$on("start-selected-search", () ->
    $scope.open = true
    $scope.loading = true
  )

  $scope.$on('shared-notebook', () ->
    $scope.open = true
    $scope.notebookOpen = true
  )

  $scope.setTemplateUrl = (searchObj) ->
    advancedSearchTemplate = 'modules/right-sidebar/popover-template.html'
    simpleSearchTemplate   = 'modules/right-sidebar/simple-search-popover-template.html'
    wordSearchTemplate     = 'modules/right-sidebar/word-search-popover-template.html'
    archivalSearchTemplate = 'modules/right-sidebar/archival-search-popover-template.html'
    if searchObj?.subtype == 'simple_search'
      simpleSearchTemplate
    else if searchObj?.subtype == 'word_search'
      wordSearchTemplate
    else if searchObj?.subtype == 'archival_search'
      archivalSearchTemplate
    else
      advancedSearchTemplate

  $scope.dates = (numb) ->
    selectMonth = ""
    angular.forEach($scope.months, (month) ->
      if month.id == +numb
        selectMonth = month.name
    )
    return selectMonth

  $scope.openSearchHistory = () ->
    $scope.resultsOpen = false
    $scope.notebookOpen = false
    $scope.loadSelected = true
    $scope.searchHistoryOpen = true
    $rootScope.$broadcast('start-history-search')

  $scope.$on("end-history-search", () ->
    $scope.loadSelected = false
  )

  $scope.closeSearchHistory = () ->
    $scope.searchHistoryOpen = false

  # $scope.deleteSearchHistory = () ->
  #   pageTitle = $filter('translate')("modal.WARNING")
  #   messagePage = 'Are you sure you want to delete your research history?'
  #   modalFactory.openMessageModal(pageTitle, messagePage, true, "sm").then (resp) ->
  #     if resp
  #       $rootScope.$broadcast('delete-history-search')

  $scope.switchNotebook = () ->
    $scope.resultsOpen = false
    $scope.searchHistoryOpen = false
    $scope.notebookOpen = !$scope.notebookOpen

  $scope.deleteResults = (search) ->
    $rootScope.searches.splice($rootScope.searches.indexOf(search), 1)
    if $rootScope.searches.length == 0
      $scope.open = false
      $scope.resultsOpen = false
    $window.localStorage.setItem('searches', JSON.stringify($rootScope.searches))

  $scope.openResults = (results) ->
    $scope.showRefine = false
    if results.searchArray?
      $scope.showRefine = true
    $scope.loadSelected = true
    $scope.notebookOpen = false
    $scope.searchHistoryOpen = false
    # This bad code is needed to track when the scope is fully updated, there was a problem when there were about 1000 results and it was necessary to show the spinner. And Firefox does not work well with $timeout
    if results.results && results.results.length
      $scope.resultsOpen = true
      $scope.selectedResult = []
      timeoutLength = results.results / 10
      setTimeout (->
        $scope.selectedResult = results
        setTimeout (->
          $scope.$apply () ->
            $scope.loadSelected = false
        ), timeoutLength
      ), timeoutLength
    else
      $scope.loadSelected = false
      pageTitle = $filter('translate')("modal.WARNING")
      messagePage = $filter('translate')("modal.NORESULTS")
      modalFactory.openMessageModal(pageTitle, messagePage, false, "sm").then (resp) ->

  $scope.closeResults = () ->
    $scope.resultsOpen = false
    $scope.selectedResult = []

  $scope.personRowClick = (row) ->
    params = {}
    params.id = row.entity.id
    $state.go 'mia.bio-people', params
    #$scope.closeResults()

  $scope.openAe = (row) ->
    params = {}
    params.aeId = row.entity.uploadId
    $state.go 'mia.singleupload', params
    #$scope.closeResults()

  $scope.openDoc = (row) ->
    params = {}
    params.aeId = row.entity.uploadId
    $state.go 'mia.singleupload'

  $scope.switchSearchPanel = () ->
    $scope.open = !$scope.open
    if !$scope.open
      $scope.closeResults()

  $scope.resize = (evt) ->
    evt.preventDefault()
    document.addEventListener 'mousemove', $scope.mousemove
    document.addEventListener 'mouseup', $scope.mouseup

  $scope.mousemove = (evt) ->
    rightSidebar = document.getElementsByClassName 'right-sidebar'
    rightSidebar = rightSidebar[0]
    showResultsResizer = document.getElementById 'show-results-resizer'
    showResultsWindow = document.getElementsByClassName 'show-results'
    for i in [1...showResultsWindow.length]
      size = document.body.clientWidth - rightSidebar.offsetWidth - evt.pageX

      if size >= 300 and size <= document.body.clientWidth - rightSidebar.offsetWidth - showResultsResizer.offsetWidth
        showResultsResizer.style.left = -size + 'px'
        showResultsWindow[i].style.left = -(size - showResultsResizer.offsetWidth) + 'px'
        showResultsWindow[i].style.width = size - showResultsResizer.offsetWidth + 'px'

  $scope.mouseup = (evt) ->
#    console.log 1
    document.removeEventListener 'mousemove', $scope.mousemove
    document.removeEventListener 'mouseup', $scope.mouseup

  $scope.notebookSrc = () ->
    return unless $rootScope.currentCs
    src = "http://medici.sg.umbrellait.com/panel?token=" + $rootScope.currentCs.token + "&caseStudyId=" + $rootScope.currentCs.id
    return $sce.trustAsResourceUrl(src)

  $scope.refineSearch = () ->
    $rootScope.selectedResult = $scope.selectedResult
    $state.go 'mia.search'

  $scope.getPeopleSearchedRoles = (roles) ->
    result = ''
    
    for index in [0... roles.length]
      if index == 0
        result += roles[index].roleCatMinor
      else
        result += ', ' + roles[index].roleCatMinor
    return result

  $scope.getPlaceName = (placeId) ->
    placeName = if $scope.placesNames.has(placeId) then \
    $scope.placesNames.get(placeId) else ''
    
    return placeName

  $scope.getPersonName = (personId) ->
    personName = if $scope.personsNames.has(personId) then \
    $scope.personsNames.get(personId) else ''
    
    return personName

  $scope.getTopicsNames = (topics) ->
    result = ""
    angular.forEach(topics, (topic) ->
      result += "\n" + topic.topicTitle
      if topic.placeAllId? and topic.placeAllId != ""
        result += " - " + $scope.getPlaceName(topic.placeAllId) + "\n"
    )

    return result
  
  getNames = (searchArray) ->
    if searchArray?.length > 5 && searchArray[5].people?.length > 0
      angular.forEach(searchArray[5].people, (people) ->
        if $scope.personsNames.has(people) == false
          return deFactory.getPeopleById(+people).then (resp) ->
            $scope.personsNames.set(people, resp.data.mapNameLf)
      )

    if searchArray?.length > 4 && searchArray[4].places?.length > 0 && searchArray[4].isActiveFilter
      angular.forEach(searchArray[4].places, (place) ->
        if $scope.placesNames.has(place) == false
          return deFactory.getPlaceById(+place).then (resp) ->
            $scope.placesNames.set(place, resp.data.placeName)
      )

    if searchArray?.length > 6 && searchArray[6].isActiveFilter
      angular.forEach(searchArray[6].topics, (topic) ->
        if $scope.placesNames.has(topic.placeAllId) == false
          return deFactory.getPlaceById(+topic.placeAllId).then (resp) ->
            $scope.placesNames.set(topic.placeAllId, resp.data.placeName)
      )

  $scope.init()
