'use strict'

miaApp = angular.module 'miaApp', [
  'ui.router'
  'templates'
  'translatorApp'
  'translatorAppEn'
  'ngAnimate'
  'uiCropper'
  'angular-click-outside'
  'ngSanitize'
  'angularUtils.directives.dirPagination'
  'uploadServiceApp'
  'deServiceApp'
  'modDeServiceApp'
  'volInsDescrApp'
  'converterServiceApp'
  'userServiceApp'
  'sidebarApp'
  'resultsApp'
  'search'
  'basicSearch'
  'welcomeApp'
  'archiveApp'
  'browseArchiveApp'
  'singleuploadApp'
  'browseArchLocationApp'
  'createDocEntApp'
  'modifyDocEntApp'
  'adminApp'
  'personalProfileApp'
  'publicProfileApp'
  'mailApp'
  'messagingApp'
  'bioPeopoleApp'
  'bioPlaceApp'
  'folioNumb'
  'editFolio'
  'searchDe'
  'searchPe'
  'newsfeedApp'
  'newsfeedAppSerivce'
  'angularTrix'
  'ngAlertify'
  'searchVol'
  'ngCookies'
  'docAttCollApp'
  'saveFilterApp'
  'ngCookies'
]

miaApp.run ['$rootScope', '$state', '$stateParams',
  ($rootScope, $state, $stateParams) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $rootScope.debug = false
]

miaApp.config ['$urlRouterProvider', '$locationProvider', '$logProvider', '$stateProvider'
  ($urlRouterProvider, $locationProvider, $logProvider, $stateProvider) ->
    $locationProvider
      .html5Mode off

    $urlRouterProvider
      .otherwise '/mia/welcome'

    $stateProvider
      .state 'mia',
        url: '/mia'
        abstract: true
        views:
          'main':
            controller: 'miaAppController'
            templateUrl: 'modules/home.html'

          'right-sidebar':
            controller: 'resultsController'
            templateUrl: 'modules/right-sidebar/right-sidebar.html'
      
      .state 'nopage',
        url: '/no-page'
        views:
          'main':
            templateUrl: 'modules/nopage.html'
      
      .state 'mia.search',
        views:
          'search-dir':
            templateUrl: 'modules/directives/search/search.html'
        onEnter: ($rootScope) ->
          $rootScope.$broadcast('show-advanced-search')
      
      .state 'mia.newsfeedSearch',
        views:
          'search-dir':
            templateUrl: 'modules/directives/search/search.html'
        onEnter: ($rootScope) ->
          $rootScope.$broadcast('show-newsfeed-search')

    $logProvider.debugEnabled(true)
]


miaApp.controller 'miaAppController', ($rootScope, $scope, $state, $timeout, $log, userServiceFactory, commonsFactory, $window) ->
  $scope.sidebarCollapsed = true
  $scope.mobileSidebarCollapsed = true
  $scope.uploadPanel = false
  $scope.fileUploadPanel = false
  $scope.searchPanel = false
  $rootScope.isBusy = false
  $scope.aborting = false
  $rootScope.pageTitle = ""
  $rootScope.canModifyTitle = true
  $scope.count = 0
  $rootScope.deTitle = ""
  $scope.editingDeTitle = false

  $rootScope.newUpload = {}
  $rootScope.newUpload.metadata = {}
  $rootScope.newUpload.metadata.repository = undefined
  $rootScope.newUpload.metadata.collection = undefined
  $rootScope.newUpload.metadata.series = undefined
  $rootScope.newUpload.metadata.volume = undefined
  $rootScope.newUpload.metadata.insert = undefined

  $rootScope.navigator =
    step: 'collections'
    selected:
      selectedRepositoryId: undefined
      selectedCollectionId: undefined
      selectedVolumeId:     undefined
      selectedInsertId:     undefined
    data:
      repositories: []
      collections:  []
      volumes:      []
      inserts:      []

  #rightSidebar
  $rootScope.resultsOpen = false
  # Check if there is a search item already stored in local storage
  if $window.localStorage.getItem('searches')
    # Storage interfaces saves items as String so the value was previously parsed as json
    $rootScope.searches = JSON.parse($window.localStorage.getItem('searches'))
  else
    $rootScope.searches = []

  $scope.checkIsDE = () ->
    $state.includes("mia.modifyDocEnt")

  userServiceFactory.getMe().then (resp)->
    if resp.status is 'ok'
      $rootScope.me=resp.data[0]
      $scope.$broadcast('get-me')
    else
      $window.location.href = '/Mia/LoginUser.do'

  $log.debug "[Mia main app] Mia App started"
  
  $scope.toggleSidebar = () ->
    $scope.sidebarCollapsed = !$scope.sidebarCollapsed

  $scope.toggleMobileSidebar = () ->
    $scope.mobileSidebarCollapsed = !$scope.mobileSidebarCollapsed

  $scope.closeMobileSidebar = () ->
    $scope.mobileSidebarCollapsed = true

  $scope.$on('progress', (evt, args)->
    if not $scope.aborting
      if isNaN(args)
        $scope.isInErrorUpload()
      if args<100
        $scope.uploading=true
      else
        $scope.uploading=false
      $scope.progress = args
    )

  $scope.isInErrorUpload = () ->
    $scope.isInError = true
    setTimeout ( ->
      $scope.uploading = false
      $rootScope.isBusy = false
      $scope.isInError = true
      $rootScope.$broadcast('upload-finished')
    ), 3000

  $scope.abortUpload = () ->
    $scope.aborting = true
    setTimeout ( ->
      $scope.uploading = false
      $rootScope.isBusy = false
      $scope.aborting = true
      $rootScope.$broadcast('upload-finished')
    ), 3000

  # $scope.uploading=true
  # $rootScope.isBusy = true

  $scope.editDeTitle = () ->
    if $rootScope.canModifyTitle
      $scope.editingDeTitle = !$scope.editingDeTitle
  $scope.saveDeTitle = (deTitle) ->
    $rootScope.$broadcast('deTitle-edited', deTitle)
    $scope.editingDeTitle = false

  $scope.saveDeTitle

  #------------------------------
  #
  #--------- Utilities ----------
  #
  #------------------------------

  ##
  # TO BE USED IN JADE LIKE THIS:
  # div(ng-if="secAuthorize(['ADMINISTRATORS'])")
  ##
  $rootScope.secAuthorize = (roles) ->
    keepGoing = true
    if $rootScope.me
      angular.forEach(roles, (role)->
        if keepGoing and userServiceFactory.isMyUserLevel($rootScope.me, role)
          keepGoing = false
      )
    return !keepGoing

  $rootScope.niceJson = (jsonToPretty) ->
    angular.toJson(jsonToPretty, true)

  # https://github.com/angular-ui/ui-router/issues/3129
  $rootScope.$on '$stateChangeSuccess', ->
    document.body.scrollTop = document.documentElement.scrollTop = 0
